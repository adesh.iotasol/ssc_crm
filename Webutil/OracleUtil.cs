﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;
using SSCMvc.Models;
using System.Data;
using System.Data.SqlClient;
using SSCMvc.Webutil;
using System.Web.Mvc;
using System.Net;
using System.IO;

namespace SSCMvc.Webutil
{
    public class Status
    {
        SSCWebDBEntities1 db = new SSCWebDBEntities1();

        public void Sendsms(string mobile, string msg)
        {
            try
            {

                string url = String.Format("http://103.27.87.89/send.php?usr={0}&pwd={1}&ph={2}&sndr=SSC&text={3}", "5101", "8284032300", mobile, msg.Replace("&", "%26"));
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream datastream = response.GetResponseStream();
                StreamReader sr = new StreamReader(datastream);
                string s = sr.ReadToEnd();
                sr.Close();
                datastream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
            }
        }

     

        public string SendsmsWithResponse(string mobile, string msg)
        {
            try
            {

                string url = String.Format("http://103.27.87.89/send.php?usr={0}&pwd={1}&ph={2}&sndr=SSC&text={3}", "5101", "8284032300", mobile, msg.Replace("&", "%26"));
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream datastream = response.GetResponseStream();
                StreamReader sr = new StreamReader(datastream);
                string s = sr.ReadToEnd();
                sr.Close();
                datastream.Close();
                response.Close();
                return s;
            }
            catch (Exception ex)
            {
            }
            return "";
        }

        public Status()
        {
            ByBranch = false;
        }
       
        public void SendError(Exception ex)
        {
            if (ex.InnerException != null)
            {
                if (ex.InnerException.InnerException is SqlException)
                {
                    SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                    if (sqlex.Number == 547)
                    {
                        HttpContext.Current.Session["error"] = "!Record Is Used Somewhere Else.";

                    }

                    else if (sqlex.Number == 2627)
                    {
                        HttpContext.Current.Session["error"] = "!Cannot Insert Duplicate Value";

                    }
                    else
                    {
                        HttpContext.Current.Session["error"] = ex.InnerException.InnerException.Message;
                    }
                }
            }
            else
            {
                HttpContext.Current.Session["error"] = ex.Message;
            }
        }

        public String SessionError
        {
            get
            {
                return HttpContext.Current.Session["error"].ToString();
            }
            set
            {
                HttpContext.Current.Session["error"] = value;
            }
        }

        public DataTable GetLedger(int admid, string dt1 = "", string dt2 = "")
        {
            int deptid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);

            DateTime? fdt = db.MaAdmissions.Where(z => z.Id == admid).Select(z => z.Cdate).First();
            DateTime tdt = DateTime.Now;

            int roll = (int)db.MaAdmissions.Where(z => z.Id == admid).Select(z => z.RollNo).First();
            if (dt1 == "" && dt2 == "")
            {
                dt1 = fdt.ToString();
                dt2 = tdt.ToShortDateString();
            }



            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;


            SqlDataAdapter adp = new SqlDataAdapter();

            adp = new SqlDataAdapter("SELECT * FROM Get_StudentLedger(@accoundcode,@date1,@date2)", con);

            adp.SelectCommand.Parameters.AddWithValue("@accoundcode", roll);
            adp.SelectCommand.Parameters.AddWithValue("@date1", dt1);
            adp.SelectCommand.Parameters.AddWithValue("@date2", dt2);
            //   adp.SelectCommand.Parameters.AddWithValue("@saletype", "Performa Invoice");

            DataTable ds = new DataTable();
            adp.Fill(ds);



            return ds;


        }



        public DataTable GetDayBook(string dt1 = "", string dt2 = "")
        {
            int deptid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);


            DateTime fdt = new DateTime();
            if (dt1 != "")
            {
                fdt = Convert.ToDateTime(dt1);
            }
            else
            {
                fdt = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            }


            DateTime tdt = new DateTime();
            if (dt2 != "")
            {
                tdt = Convert.ToDateTime(dt2);
                tdt = tdt.AddDays(1.0);
            }
            else
            {
                tdt = DateTime.Now;
            }


            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;


            SqlDataAdapter adp = new SqlDataAdapter();

            adp = new SqlDataAdapter("SELECT * FROM DayBookFunction(@date1,@date2)", con);


            adp.SelectCommand.Parameters.AddWithValue("@date1", fdt);
            adp.SelectCommand.Parameters.AddWithValue("@date2", tdt);
            //   adp.SelectCommand.Parameters.AddWithValue("@saletype", "Performa Invoice");

            DataTable ds = new DataTable();
            adp.Fill(ds);



            return ds;
        }

        public DataTable GetRemarks(string enqtype)
        {
            DataTable dt = new DataTable();
            string sql = "";
            if (enqtype == "Visa")
            {
                sql = @"select distinct remarks,followuptype, Count(1) As rcount from maenqfollow where ReferId in(select id from MaEnquiry where IsAdmission=0) and followuptype like 'Visa%' and remarks not like 'New Enquiry%' and  remarks not like 'New Entery%' and  remarks not like 'New Entry%' and  remarks not like 'Visa Prospective%'  group by Remarks,followuptype";
            }
            else
            {
                sql = @"select distinct remarks,followuptype, Count(1) As rcount from maenqfollow where ReferId in(select id from MaEnquiry where IsAdmission=0) and followuptype like '" + enqtype + "' and remarks not like 'New Enquiry%' and  remarks not like 'New Entery%' and  remarks not like 'New Entry%'  group by Remarks,followuptype";
            }
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlDataAdapter adp = new SqlDataAdapter(sql, con);
            adp.Fill(dt);

            return dt;

        }

        //public DataTable StudentList(int Roll,int idd)
        //{
        //    DataTable dt = new DataTable();
        //    string sql = "";
        //    if (Roll >0)
        //    {
        //        sql = @"select fullname from MockTestStudents where BranchId = " + idd + " and( EnqAbt = 'IELTS' or EnqAbt = 'SPOKEN ENGLISH' or  EnqAbt = 'VISA' or  EnqAbt = 'PTE' or EnqAbt = 'EXAM BOOKING') and RollNo like % '" + Roll + "' % ";

        //    }
        //    SqlConnection con = new SqlConnection();
        //    con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
        //    SqlDataAdapter adp = new SqlDataAdapter(sql, con);
        //    adp.Fill(dt);
        //    return dt;
        //}

        public List<MockTestStudent> StudentList(int Roll)
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            string qury = @"select fullname from MockTestStudents where BranchId = " + branchid + " and( EnqAbt = 'IELTS' or EnqAbt = 'SPOKEN ENGLISH' or  EnqAbt = 'VISA' or  EnqAbt = 'PTE' or EnqAbt = 'EXAM BOOKING') and RollNo like '%" + Roll + "%' ";

            List<MockTestStudent> _lst = db.Database.SqlQuery<MockTestStudent>(qury).ToList();
            return _lst;
        }

        public DataTable GetPendingFollowUp(string enqtype, int uid = 0)
        {
            // int userid=Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

            if (uid != 0)
            {
                cmd.CommandText = @"select top(3) Max(Convert(date,enqf.NextDate)) as nxdt,enq.FirstName,enq.LastName,enq.Mobile,enq.Id from MaEnqFollow enqf inner join MaEnquiry enq
on enqf.ReferId=enq.Id where enq.IsAdmission=@admcondition and enqf.FollowUpType=@enqtype and enqf.NextDate<=@date1 and enq.EnqHandledBy=@user and enqf.Enqstatus ='Open' group by enq.FirstName,enq.LastName,enq.Mobile,enq.Id order by nxdt,Id";
            }
            else
            {
                cmd.CommandText = @"select top(3) Max(Convert(date,enqf.NextDate)) as nxdt,enq.FirstName,enq.LastName,enq.Mobile,enq.Id from MaEnqFollow enqf inner join MaEnquiry enq
on enqf.ReferId=enq.Id where enq.IsAdmission=@admcondition and enqf.FollowUpType=@enqtype and enqf.NextDate<=@date1 and enqf.Enqstatus ='Open' group by enq.FirstName,enq.LastName,enq.Mobile,enq.Id order by nxdt,Id";
            }
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);
            adp.SelectCommand.Parameters.AddWithValue("@admcondition", false);
            adp.SelectCommand.Parameters.AddWithValue("@enqtype", enqtype);
            adp.SelectCommand.Parameters.AddWithValue("@date1", CommonUtil.IndianTime());
            adp.SelectCommand.Parameters.AddWithValue("@user", uid);
            adp.Fill(dt);
            return dt;
        }

        public DataTable GetPendingFollowComplete(string enqtype, int uid = 0)
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

            if (uid != 0)
            {
                cmd.CommandText = @"select nxdt,enqf.cdate,enqf.FirstName,enqf.LastName, enqf.EnqHandledBy, enqf.Mobile,enqf.AlternateNo,enqf.Id,enqf.BranchId,enqf.EnqAbt,enqstatus, Grouptime, GroupChoice,EnqType,VisaType from Vw_EnquiryFollow enqf where enqf.IsAdmission=@admcondition and enqf.nxdt<=@date1 and enqf.BranchId=@branchid and ((enqf.EnqHandledBy=@user and enqf.transferid is null) or enqf.transferid=@user) order by nxdt,Id";
            }
            else
            {
                cmd.CommandText = @"select nxdt,enqf.cdate,enqf.FirstName,enqf.LastName, enqf.EnqHandledBy,enqf.TransferId, enqf.Mobile,enqf.AlternateNo,enqf.Id,enqf.BranchId,enqf.EnqAbt,enqstatus,Grouptime, GroupChoice ,EnqType,VisaType
 from Vw_EnquiryFollow enqf where enqf.IsAdmission=@admcondition and enqf.nxdt<=@date1  and enqf.BranchId=@branchid  order by nxdt,Id";

            }
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);
            adp.SelectCommand.Parameters.AddWithValue("@admcondition", false);
            adp.SelectCommand.Parameters.AddWithValue("@enqtype", enqtype);
            adp.SelectCommand.Parameters.AddWithValue("@date1", CommonUtil.IndianTime());
            adp.SelectCommand.Parameters.AddWithValue("@user", uid);
            adp.SelectCommand.Parameters.AddWithValue("@branchid", branchid);
            adp.Fill(dt);
            return dt;
        }

        public DataTable GetTodayFollowup(string enqtype, int uid = 0, string Fdate="", string TDate="")
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

            if (uid != 0)
            {
                cmd.CommandText = @"select nxdt,enqf.cdate,enqf.FirstName,enqf.LastName, enqf.EnqHandledBy, enqf.Mobile,enqf.AlternateNo,enqf.Id,enqf.BranchId,enqf.EnqAbt,enqstatus, Grouptime, GroupChoice from Vw_EnquiryFollow enqf where enqf.IsAdmission=@admcondition and enqf.nxdt<=@date1 and enqf.BranchId=@branchid and (enqf.EnqHandledBy=@user or enqf.transferid=@user) order by nxdt,Id";
            }
            else
            {
                cmd.CommandText = @"select nxdt,enqf.cdate,enqf.FirstName,enqf.LastName, enqf.EnqHandledBy,enqf.TransferId, enqf.Mobile,enqf.AlternateNo,enqf.Id,enqf.BranchId,enqf.EnqAbt,enqstatus,Grouptime, GroupChoice 
 from Vw_EnquiryFollow enqf innerjoin maenqfollow fol on enqf.Id = fol.Referid  where enqf.IsAdmission=@admcondition and fol.cdate  between @date1 and @date2  and enqf.BranchId=@branchid  order by Id";

            }
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);
            adp.SelectCommand.Parameters.AddWithValue("@admcondition", false);
            adp.SelectCommand.Parameters.AddWithValue("@enqtype", enqtype);
            adp.SelectCommand.Parameters.AddWithValue("@date1",Fdate );
            adp.SelectCommand.Parameters.AddWithValue("@date2", TDate);
            adp.SelectCommand.Parameters.AddWithValue("@user", uid);
            adp.SelectCommand.Parameters.AddWithValue("@branchid", branchid);
            adp.Fill(dt);
            return dt;
        }


        public DataTable GetLeftCounselPendingFollow(string enqtype, int uid = 0)
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

                cmd.CommandText = @"select enqf.nxdt,enqf.cdate,enqf.FirstName,enqf.LastName, enqf.EnqHandledBy, enqf.Mobile,enqf.AlternateNo,enqf.Id,enqf.BranchId,enqf.EnqAbt,enqf.enqstatus,enqf.Grouptime, enqf.GroupChoice  
 from Vw_EnquiryFollow  enqf inner join [User] en on enqf.EnqHandledBy = en.Id where enqf.IsAdmission=@admcondition and enqf.nxdt<=@date1  and enqf.BranchId=@branchid and en.status = 0  order by nxdt,Id";
  
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);
            adp.SelectCommand.Parameters.AddWithValue("@admcondition", false);
            adp.SelectCommand.Parameters.AddWithValue("@enqtype", enqtype);
            adp.SelectCommand.Parameters.AddWithValue("@date1", CommonUtil.IndianTime());
            adp.SelectCommand.Parameters.AddWithValue("@user", uid);
            adp.SelectCommand.Parameters.AddWithValue("@branchid", branchid);
            adp.Fill(dt);
            return dt;
        }


        //        public DataTable GetPendingFollowList(string enqtype,int branchid=0, int uid = 0/*,string fdate="",string tdate=""*/)
        //        {
        //            //int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
        //            SqlConnection con = new SqlConnection();
        //            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
        //            SqlCommand cmd = new SqlCommand();

        //            if (uid != 0)
        //            {
        //                cmd.CommandText = @"select nxdt,enqf.cdate,enq.FirstName,enq.LastName, enq.EnqHandledBy, enq.Mobile,enq.AlternateNo,enq.Id,enq.BranchId,enq.EnqAbt,enqstatus from Vw_EnquiryFollow enqf inner join MaEnquiry enq
        //on enqf.ReferId=enq.Id where enq.IsAdmission=@admcondition and enqf.FollowUpType=@enqtype and enqf.nxdt<=@date1 and enqf.Enqstatus ='Open' and enq.BranchId=@branchid and enq.EnqHandledBy=@user order by nxdt,Id";
        //            }
        //            else
        //            {
        //                cmd.CommandText = @"select nxdt,enqf.cdate,enq.FirstName,enq.LastName, enq.EnqHandledBy, enq.Mobile,enq.AlternateNo,enq.Id,enq.BranchId,enq.EnqAbt,enqstatus
        // from Vw_EnquiryFollow enqf inner join MaEnquiry enq on enqf.Id=enq.Id where enq.IsAdmission=@admcondition and enqf.nxdt<=@date1 and enqf.enqstatus ='Open' and enq.BranchId=@branchid  order by nxdt,Id";
        //            }
        //            DataTable dt = new DataTable();
        //            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);
        //            adp.SelectCommand.Parameters.AddWithValue("@admcondition", false);
        //            adp.SelectCommand.Parameters.AddWithValue("@enqtype", enqtype);
        //            adp.SelectCommand.Parameters.AddWithValue("@date1", DateTime.Now);
        //            adp.SelectCommand.Parameters.AddWithValue("@user", uid);
        //            adp.SelectCommand.Parameters.AddWithValue("@branchid", branchid);
        //            //adp.SelectCommand.Parameters.AddWithValue("@date2",fdate);
        //            //adp.SelectCommand.Parameters.AddWithValue("@date3",tdate);

        //            adp.Fill(dt);
        //            return dt;
        //        }


        public DataTable GetPendingFollowCompleteByCreateDate(string enqtype, int uid = 0, string date1 = "", string date2 = "")
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            if (date1 != "" && date2 != "")
            {
                //cmd.CommandText = @"select nxdt, fol.cdate  ,enqf.FirstName,enqf.LastName, enqf.EnqHandledBy, enqf.Mobile,enqf.Id,enqf.BranchId,fol.enqstatus from Vw_EnquiryFollow enqf ,maenqfollow fol where enqf.id = fol.referid and enqf.IsAdmission=@admcondition and Convert(date,fol.cdate) between @datefrom and @dateto and enqf.BranchId=@branchid  order by cdate,Id";

                cmd.CommandText = $@"select nxdt, fol.cdate  ,enqf.FirstName,enqf.LastName, enqf.EnqHandledBy, enqf.Mobile,enqf.Id,enqf.BranchId,fol.enqstatus from Vw_EnquiryFollow enqf ,maenqfollow fol where enqf.id = fol.referid and enqf.IsAdmission=0 and Convert(date,fol.cdate) between '{date1}' and '{date2}' and enqf.BranchId={branchid}  order by cdate,Id";

                //                cmd.CommandText = @"select nxdt,enq.cdate,enq.FirstName,enq.LastName, enq.EnqHandledBy, enq.Mobile,enq.Id,enq.BranchId,enqstatus from Vw_EnquiryFollow enqf inner join MaEnquiry enq
                //on enqf.ReferId=enq.Id where enq.IsAdmission=@admcondition and enqf.FollowUpType=@enqtype and enqf.cdate between @datefrom and @dateto and enqf.Enqstatus ='Open' and enq.BranchId=@branchid  order by cdate,Id";
            }
            else if (uid != 0)
            {
                cmd.CommandText = @"select enqf.nxdt,fol.cdate,enqf.FirstName,enqf.LastName, enqf.EnqHandledBy, enqf.Mobile,enqf.Id,enqf.BranchId,fol.enqstatus from Vw_EnquiryFollow enqf ,maenqfollow fol where enqf.id = fol.referid and enqf.IsAdmission=@admcondition and Convert(date,fol.cdate)=Convert(date, @date1) and enqf.BranchId=@branchid and enqf.EnqHandledBy=@user order by cdate,Id";
            }
            else
            {
                cmd.CommandText = @"select nxdt,fol.cdate,enqf.FirstName,enqf.LastName, enqf.EnqHandledBy, enqf.Mobile,enqf.Id,enqf.BranchId,fol.enqstatus from Vw_EnquiryFollow enqf  ,maenqfollow fol where enqf.id = fol.referid and enqf.IsAdmission=@admcondition and Convert(date,fol.cdate)=Convert(date,@date1) and enqf.BranchId=@branchid  order by cdate,Id";
            }
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);
            adp.SelectCommand.Parameters.AddWithValue("@admcondition", false);
            adp.SelectCommand.Parameters.AddWithValue("@enqtype", enqtype);
            adp.SelectCommand.Parameters.AddWithValue("@date1", CommonUtil.IndianTime());
            adp.SelectCommand.Parameters.AddWithValue("@user", uid);
            adp.SelectCommand.Parameters.AddWithValue("@branchid", branchid);
            adp.SelectCommand.Parameters.AddWithValue("@datefrom", date1);
            adp.SelectCommand.Parameters.AddWithValue("@dateto", date2);

            adp.Fill(dt);
            return dt;
        }


        #region all count functions

        /// <summary>
        /// this is for to get the Count of pending mock test
        /// </summary>
        /// <returns></returns>
        /// 

        public bool ByBranch { set; get; }
        public int Bid { set; get; }

        public int GetMockTestCount()
        {
            int uid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            if (ByBranch) branchid = Bid;

            if (HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeAdmin && HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeSuperAdmin && ByBranch == false && HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeVisaCounselor)
            {
                cmd.CommandText = @"with myst(EnqFollowBy,id)
as
( 
select max(enqf.EnqFollowBy) EnqFollowBy,enq.id from Vw_MockTestFollow enqf inner join Tb_MockTest enq
on enqf.Id=enq.Id where enqf.FollowUpType='MockTest' and enqf.nxdt<=getdate() and enqf.Enqstatus ='Open' and enqf.Enqstatus <> 'Move To Visa
' and enq.BranchId=" + branchid + @" and enqf.enqfollowby=" + uid + @" and enq.isfollowbtn=1 group by enq.id)
select count(1) from myst";
            }
            else
            {
                cmd.CommandText = @"with myst(EnqFollowBy,id)
as
( 
select max(enqf.EnqFollowBy) EnqFollowBy,enq.id from Vw_MockTestFollow enqf inner join Tb_MockTest enq
on enqf.Id=enq.Id where enqf.FollowUpType='MockTest' and enqf.nxdt<=getdate() and enqf.Enqstatus ='Open' and enqf.Enqstatus <> 'Move To Visa
'and enq.BranchId=" + branchid + @"and enq.isfollowbtn=1 group by enq.id)
select count(1) from myst";

            }
            cmd.Connection = con;
            con.Open();
            int _mockcount = (int)cmd.ExecuteScalar();
            con.Close();
            return _mockcount;
        }

        /// <summary>
        /// this is for to get count of the pending IELTS 
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public int GetIELTSCount()
        {
            int uid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            if (ByBranch) branchid = Bid;

            if (HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeAdmin && HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeSuperAdmin && ByBranch == false && HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeVisaCounselor)
            {
                cmd.CommandText = @"select count(1) from Vw_IELTSTestFollow enqf where 
                enqf.nxdt<=Convert(date,dateadd(minute, 810, getdate())) and  enqf.StudentId>0 and enqf.userid=" + uid + "and enqf.BranchId=" + branchid;

            }
            else
            {
                cmd.CommandText = @"select count(1) from Vw_IELTSTestFollow enqf 
                where enqf.nxdt<=Convert(date,dateadd(minute, 810, getdate()))  and enqf.StudentId>0 and enqf.BranchId=" + branchid;
            }
            cmd.Connection = con;
            con.Open();
            int _count = Convert.ToInt32(cmd.ExecuteScalar());
            con.Close();
            return _count;

        }

        /// <summary>
        /// this is for to get count of the pending Enquiry
        /// </summary>
        /// <returns></returns>
        public int GetTotalEnquiryCount()
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            int uid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            
            cmd.CommandText = @"select count(1) from Vw_EnquiryFollow enqf  where enqf.IsAdmission=0 and enqabt not in('VISA','VISITOR VISA') and enqf.nxdt<=dateadd(minute, 810, getdate()) and enqf.enqstatus ='Open'and enqf.BranchId=" + branchid;

            cmd.Connection = con;
            con.Open();
            int _countenquiry = Convert.ToInt32(cmd.ExecuteScalar());
            con.Close();
            return _countenquiry;
        }

        public int GetTotalVisaCount()
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            int uid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = @"select count(1) from Vw_EnquiryFollow enqf  where enqf.IsAdmission=0 and enqabt in('VISA','VISITOR VISA') and enqf.nxdt<=dateadd(minute, 810, getdate()) and enqf.enqstatus ='Open'and enqf.BranchId=" + branchid;

            cmd.Connection = con;
            con.Open();
            int _countenquiry = Convert.ToInt32(cmd.ExecuteScalar());
            con.Close();
            return _countenquiry;
        }

        public int GetTotalLeftCounselEnquiryCount()
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            int uid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

         
            cmd.CommandText = @"select count(1) from Vw_EnquiryFollow enqf inner join [User] en on enqf.EnqHandledBy = en.Id where enqf.IsAdmission=0 and enqf.nxdt<=dateadd(minute, 810, getdate()) and enqf.enqstatus ='Open'and en.status = 0 and enqf.BranchId=" + branchid;

            cmd.Connection = con;
            con.Open();
            int _countenquiry = Convert.ToInt32(cmd.ExecuteScalar());
            con.Close();
            return _countenquiry;
        }

        public List<PendingEnqSummary> EnqTypeCount()
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            List<PendingEnqSummary> summary = new List<PendingEnqSummary>();
            int uid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            if (HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeAdmin ||HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeSuperAdmin)
            {
                summary = db.Database.SqlQuery<PendingEnqSummary>($"select count(1) as Cnt, EnqType from Vw_EnquiryFollow as enq where enq.IsAdmission = 0 and  enq.nxdt <= dateadd(minute, 810, getdate()) and enq.BranchId ={branchid} group by EnqType").ToList();
            }
            else
            {
              
                summary = db.Database.SqlQuery<PendingEnqSummary>($"select count(1) as Cnt, EnqType from Vw_EnquiryFollow as enq where enq.IsAdmission = 0 and  enq.nxdt <= dateadd(minute, 810, getdate()) and((enq.EnqHandledBy = {uid} and enq.transferid is null) or enq.transferid = {uid}) and enq.BranchId ={branchid} group by EnqType").ToList();
            }

           
          
            return summary;
        }




        public int GetEnquiryCount()
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            int uid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = @"select count(1) from Vw_EnquiryFollow as enq
 where enq.IsAdmission=0 and  enq.nxdt<=dateadd(minute, 810, getdate()) and ((enq.EnqHandledBy=" + uid +  " and enq.transferid is null) or enq.transferid="+ uid + ") and enq.BranchId=" + branchid;

            //           cmd.CommandText = @"select count(1) from Vw_EnquiryFollow as enq
            //where enq.IsAdmission=0 and  enq.nxdt<=getdate() and enq.EnqHandledBy=" + uid + "and enq.BranchId=" + branchid;

            //            if (HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeAdmin && HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeSuperAdmin && HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeVisaCounselor)
            //            {

            //            }
            //            else
            //            {
            //                cmd.CommandText = @"select count(1) from Vw_EnquiryFollow enqf inner join MaEnquiry enq
            //on enqf.Id=enq.Id where enq.IsAdmission=0 and enqf.nxdt<=getdate() and enqf.enqstatus ='Open'and enq.BranchId=" + branchid;
            //            }

            cmd.Connection = con;
            con.Open();
            int _countenquiry = Convert.ToInt32(cmd.ExecuteScalar());
            con.Close();
            return _countenquiry;
        }

        /// <summary>
        /// this is for to get count of the pending visa follow
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public int GetPendingVisaCount()
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);

            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            int uid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            SqlCommand cmd = new SqlCommand();
            if (ByBranch) branchid = Bid;

            if (HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeAdmin && HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeSuperAdmin && ByBranch == false && HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeVisaCounselor)
            {
                cmd.CommandText = @"select count(1) from tb_visafollowup where  NextFollowdate<=getdate() and FollowStatus = 'Open' and branchid=" + branchid + "and LastFollowBy=" + uid;
            }
            else
            {
                cmd.CommandText = @"select count(1) from tb_visafollowup where  NextFollowdate<=getdate() and FollowStatus = 'Open' and branchid=" + branchid;
            }

            cmd.Connection = con;
            con.Open();
            int _countvisa = Convert.ToInt32(cmd.ExecuteScalar());
            con.Close();
            return _countvisa;
        }



        #endregion


        public DataTable GetPendingMockComplete(int uid = 0)
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

            if (uid == 0)
            {
                cmd.CommandText = @"select  max(convert(date,enqf.nxdt)) nxdt,max(convert(date,enqf.cdate)) cdate,enqf.FirstName,enqf.LastName,max(enqf.EnqFollowBy) EnqFollowBy,enq.BranchId,enq.TimeId,enqf.Mobile,enq.studentid, enq.id,enqf.enquiryid from Vw_MockTestFollow enqf inner join Tb_MockTest enq
on enqf.Id=enq.Id where enqf.FollowUpType='MockTest' and enqf.nxdt<=dateadd(minute, 810, getdate()) and enqf.Enqstatus ='Open'and enq.BranchId=@branchid and enq.isfollowbtn=1 group by
 enqf.FirstName,enqf.LastName,enq.BranchId,enq.TimeId,enqf.Mobile,enq.studentid,enq.id,enqf.enquiryid order by nxdt";
            }
            else
            {
                cmd.CommandText = @"select max(convert(date,enqf.nxdt)) nxdt,max(convert(date,enqf.cdate)) cdate,enqf.FirstName,enqf.LastName,max(enqf.EnqFollowBy) EnqFollowBy,enq.BranchId,enqf.Mobile,enq.TimeId,enq.studentid,max(enq.Id) id,enqf.enquiryid from 
Vw_MockTestFollow enqf inner join Tb_MockTest enq on enqf.Id=enq.Id 
where enqf.FollowUpType='MockTest' and enqf.nxdt<= dateadd(minute, 810, getdate()) and enqf.Enqstatus='Open' and enq.BranchId=@branchid and enqf.enqfollowby=" + uid + @" and enq.isfollowbtn=1 group by
enqf.FirstName,enqf.LastName,enq.BranchId,enq.TimeId,enqf.Mobile,enq.studentid,enqf.enquiryid order by nxdt";
            }
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);

            adp.SelectCommand.Parameters.AddWithValue("@branchid", branchid);
            adp.Fill(dt);
            return dt;
        }


        public DataTable GetPendingMockCompletebycreatedate(int uid = 0, string date1 = "", string date2 = "")
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            if (date1 != "" && date2 != "")
            {
                cmd.CommandText = @"select max(convert(date,enqf.cdate)) cdate , max(convert(date,enqf.nxdt)) nxdt ,enqf.FirstName,enqf.LastName,max(enqf.EnqFollowBy) EnqFollowBy,enq.BranchId,enqf.Mobile,enq.studentid, enq.id from Vw_MockTestFollow enqf inner join Tb_MockTest enq
on enqf.Id=enq.Id where enqf.FollowUpType='MockTest' and enqf.cdate between @date1 and @date2 and enqf.Enqstatus ='Open'and enq.BranchId=@branchid and enq.isfollowbtn=1 group by
 enqf.FirstName,enqf.LastName,enq.BranchId,enqf.Mobile,enq.studentid,enq.id order by cdate";
            }
            else if (uid == 0)
            {

                cmd.CommandText = @"select max(convert(date,enqf.cdate)) cdate , max(convert(date,enqf.nxdt)) nxdt ,enqf.FirstName,enqf.LastName,max(enqf.EnqFollowBy) EnqFollowBy,enq.BranchId,enqf.Mobile,enq.studentid, enq.id from Vw_MockTestFollow enqf inner join Tb_MockTest enq
on enqf.Id=enq.Id where enqf.FollowUpType='MockTest' and enqf.cdate<=getdate() and enqf.Enqstatus ='Open'and enq.BranchId=@branchid and enq.isfollowbtn=1 group by
 enqf.FirstName,enqf.LastName,enq.BranchId,enqf.Mobile,enq.studentid,enq.id order by cdate";

            }
            else
            {
                cmd.CommandText = @"select max(convert(date,enqf.cdate)) cdate , max(convert(date,enqf.nxdt)) nxdt ,enqf.FirstName,enqf.LastName,max(enqf.EnqFollowBy) EnqFollowBy,enq.BranchId,enqf.Mobile,enq.studentid,max(enq.Id) id from 
Vw_MockTestFollow enqf inner join Tb_MockTest enq on enqf.Id=enq.Id 
where enqf.FollowUpType='MockTest' and enqf.cdate<=getdate() and enqf.Enqstatus='Open' and enq.BranchId=@branchid and enqf.enqfollowby=" + uid + @" and enq.isfollowbtn=1 group by
enqf.FirstName,enqf.LastName,enq.BranchId,enqf.Mobile,enq.studentid order by cdate";
            }
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);

            adp.SelectCommand.Parameters.AddWithValue("@branchid", branchid);
            adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
            adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
            adp.Fill(dt);
            return dt;
        }


        public DataTable GetEnqFollowSummary()
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);

            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            string sql = @"select username, branch.BramchName as branchname,count(1) as enqcount  from Vw_EnquiryFollow enqf  inner join [User] on [user].id=enqf.EnqHandledBy  inner join mabranch branch on branch.Id=enqf.BranchId and enqf.Enqstatus !='Close' and enqf.IsAdmission=0 and enqf.nxdt<=getdate() group by username ,branch.BramchName";

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(sql, con);
            adp.Fill(dt);
            return dt;

        }

        #region pending Follow Up List
        public DataTable GetEnqFollowList(string enqtype, int branchid, int uid, string date2 = "", string date3 = "")
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = @"select nxdt, enqf.cdate ,enqf.alternateno ,enqf.fremarks,enqf.FirstName,enqf.LastName, user1.fullname,enqf.EnqAbt, enqf.Mobile,enqf.Id,enqf.BranchId,enqstatus from Vw_EnquiryFollow enqf inner join [user] user1 on user1.Id=enqf.EnqHandledBy  where enqf.IsAdmission=@admcondition and enqf.cdate between @datefrom and @dateto and user1.Id=@user and enqf.Enqstatus ='Open' and enqf.BranchId=@branchid  order by cdate,Id";

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);
            adp.SelectCommand.Parameters.AddWithValue("@admcondition", false);
            //adp.SelectCommand.Parameters.AddWithValue("@enqtype", enqtype);

            adp.SelectCommand.Parameters.AddWithValue("@user", uid);
            adp.SelectCommand.Parameters.AddWithValue("@branchid", branchid);
            adp.SelectCommand.Parameters.AddWithValue("@datefrom", date2);
            adp.SelectCommand.Parameters.AddWithValue("@dateto", date3);

            adp.Fill(dt);
            return dt;

        }

        public DataTable GetPendingMockList(string enqtype, int branchid, int uid, string date2 = "", string date3 = "")
        {

            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = @"select max(convert(date,enqf.nxdt)) nxdt,max(convert(date,enqf.cdate)) cdate,enqf.FirstName,enqf.Enqstatus,enqf.LastName,enq.fremarks,dbo.GetUserName(enq.followupby) as EnqFollowBy,enq.BranchId,enqf.Mobile,enq.studentid, enq.id from Vw_MockTestFollow enqf inner join Tb_MockTest enq
on enqf.Id=enq.Id where enqf.FollowUpType='MockTest' and enqf.nxdt<=getdate() and enqf.Enqstatus ='Open'and enq.BranchId=@branchid  and enq.UserId=@user and enqf.enqfollowby=@user and enq.nextdate between @datefrom and @dateto and enq.isfollowbtn=1 group by
enq.fremarks, enqf.FirstName,enqf.LastName,enq.BranchId,enqf.EnqStatus,enqf.Mobile,enq.followupby,enq.studentid,enq.id order by nxdt";


            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);
            adp.SelectCommand.Parameters.AddWithValue("@user", uid);
            adp.SelectCommand.Parameters.AddWithValue("@branchid", branchid);
            adp.SelectCommand.Parameters.AddWithValue("@datefrom", date2);
            adp.SelectCommand.Parameters.AddWithValue("@dateto", date3);
            adp.Fill(dt);
            return dt;
        }

        public DataTable GetPendingIELTSList(int branchid, int uid, string date2 = "", string date3 = "")
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();


            cmd.CommandText = @"select enqf.nxdt,enqf.cdate,enqf.enqstatus, user1.fullname,enq.Id,enq.StudentType,enq.BranchId,dbo.GetStudentName(enq.StudentType,enq.StudentId) as studentname,dbo.GetMobileNo(enq.StudentType,enq.StudentId) as MobileNo,enq.remarks from Vw_IELTSTestFollow enqf inner join Tb_IELTSResult enq
inner join [user] user1 on user1.Id=enq.userid  on enqf.Id=enq.Id where enq.userid=@user and enqf.FollowUpType='IELTS' and enqf.nxdt between @datefrom and @dateto  and enqf.Enqstatus ='Open' and enqf.Enqstatus <> 'Move To Visa' and enq.BranchId=@branchid  order by  enqf.nxdt,Id";

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);

            adp.SelectCommand.Parameters.AddWithValue("@user", uid);
            adp.SelectCommand.Parameters.AddWithValue("@branchid", branchid);
            adp.SelectCommand.Parameters.AddWithValue("@datefrom", date2);
            adp.SelectCommand.Parameters.AddWithValue("@dateto", date3);



            adp.Fill(dt);
            return dt;
        }

        #endregion

        public DataTable GetPendingIELTSComplete(int uid = 0)
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

            if (uid == 0)
            {
                cmd.CommandText = @"select * from Vw_IELTSTestFollow enq,last_ieltsfollowup f where enq.fid=f.fid and  enq.nxdt<=Convert(date,dateadd(minute, 810, getdate())) and  enq.BranchId=@branchid  order by enq.nxdt,Id";
            }
            else
            {
                cmd.CommandText = @"select * from Vw_IELTSTestFollow enq,last_ieltsfollowup f where enq.fid=f.fid  and enq.nxdt<=Convert(date,dateadd(minute, 810, getdate())) and enq.BranchId=@branchid  order by  enq.nxdt,Id";
            }

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);

            adp.SelectCommand.Parameters.AddWithValue("@date1", CommonUtil.IndianTime());
            // adp.SelectCommand.Parameters.AddWithValue("@userid", uid);
            adp.SelectCommand.Parameters.AddWithValue("@branchid", branchid);
            adp.Fill(dt);
            return dt;
        }

        public DataTable GetPendingIELTSCompleteByCdate(int uid = 0, string date1 = "", string date2 = "")
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            if (date1 != "" && date2 != "")
            {
                //                cmd.CommandText = @"select enqf.nxdt, enqf.cdate, enq.UserId,enq.Id,enq.StudentType,enq.StudentId,enq.BranchId from Vw_IELTSTestFollow enqf inner join Tb_IELTSResult enq
                //on enqf.Id=enq.Id where enqf.FollowUpType='IELTS' and enqf.cdate between @datefrom and @dateto and enqf.Enqstatus ='Open' and enqf.Enqstatus <> 'Move To Visa' and enq.BranchId=@branchid and enqf.userid=" + uid + @" order by  enqf.cdate,Id";
                cmd.CommandText = @"select enqf.nxdt, enqf.cdate, enq.UserId,enq.Id,enq.StudentType,enq.StudentId,enq.BranchId from Vw_IELTSTestFollow enqf inner join Tb_IELTSResult enq
on enqf.Id=enq.Id where enqf.FollowUpType='IELTS' and enqf.cdate between @datefrom and @dateto and enqf.Enqstatus ='Open' and enqf.Enqstatus <> 'Move To Visa' and enq.BranchId=@branchid  order by  enqf.cdate,Id";
            }
            else if (uid == 0)
            {
                cmd.CommandText = @"select enqf.nxdt, enqf.cdate, enq.UserId,enq.Id,enq.StudentType,enq.StudentId,enq.BranchId from Vw_IELTSTestFollow enqf inner join Tb_IELTSResult enq
on enqf.Id=enq.Id where enqf.FollowUpType='IELTS' and enqf.cdate<=@date1  and enqf.Enqstatus ='Open' and enqf.Enqstatus <> 'Move To Visa' and enq.BranchId=@branchid  order by  enqf.cdate,Id";
            }
            else
            {
                cmd.CommandText = @"select enqf.nxdt, enqf.cdate, enq.UserId,enq.Id,enq.StudentType,enq.StudentId,enq.BranchId from Vw_IELTSTestFollow enqf inner join Tb_IELTSResult enq
on enqf.Id=enq.Id where enqf.FollowUpType='IELTS' and enqf.cdate<=@date1 and enqf.Enqstatus ='Open' and enqf.Enqstatus <> 'Move To Visa' and enq.BranchId=@branchid and enqf.userid=" + uid + @" order by  enqf.cdate,Id";
            }

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);

            adp.SelectCommand.Parameters.AddWithValue("@date1", CommonUtil.IndianTime());
            // adp.SelectCommand.Parameters.AddWithValue("@userid", uid);
            adp.SelectCommand.Parameters.AddWithValue("@branchid", branchid);
            adp.SelectCommand.Parameters.AddWithValue("@datefrom", date1);
            adp.SelectCommand.Parameters.AddWithValue("@dateto", date2);
            adp.Fill(dt);
            return dt;
        }



        public DataTable GetPendingVisaComplete(int uid = 0)
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

            if (uid != 0)
            {
                //cmd.CommandText = @"select * from Vw_VisaFollow  where  nxdt<=@date1 and EnqFollowBy=@userid";
                cmd.CommandText = @"select NextFollowdate, followstatus,remarks,lastfollowby,Referenceno,Followtype,name,Mobileno,referid,enquiryid,admissionid,entertype from tb_visafollowup where branchid=" + branchid + " and lastfollowBy=" + uid + @"and NextFollowdate<=getdate()  group by followstatus,remarks,Referenceno,Followtype,name,Mobileno,referid,enquiryid,admissionid,[type]";
            }
            else
            {
                cmd.CommandText = @"select NextFollowdate, followstatus,remarks,lastfollowby,Referenceno,Followtype,name,Mobileno,referid,enquiryid,admissionid,entertype from tb_visafollowup where  NextFollowdate<=getdate()  group by followstatus,remarks,Referenceno,Followtype,name,Mobileno,referid,enquiryid,admissionid,[type]";
            }

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd.CommandText, con);

            // adp.SelectCommand.Parameters.AddWithValue("@date1", DateTime.Now);
            adp.SelectCommand.Parameters.AddWithValue("@userid", uid);
            adp.Fill(dt);

            return dt;
        }


        #region Get the scores of Mock Test And IELTS Test

        public List<CompareScorereport> GetScores()
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            IEnumerable<CompareScorereport> _Scoreslist = db.Database.SqlQuery<CompareScorereport>("select admisn.FirstName,admisn.RollNo, * from tb_ieltsresult  inner join maadmission admisn on StudentId=admisn.Id where StudentType='Admission' and StudentId > 0 and tb_ieltsresult.branchid=" + branchid).Select(s =>
                          new CompareScorereport()
                          {
                              TestDate = s.TestDate,
                              firstname = s.firstname,
                              LBand = s.LBand,
                              Overall = s.Overall,
                              RBand = s.RBand,
                              RollNo = s.RollNo,
                              SBand = s.SBand,
                              WBand = s.WBand,
                              StudentId = s.StudentId,
                              testtype = "IELTS",

                          }
                      );

            return _Scoreslist.ToList();

        }


        public List<CompareScorereport> GetMockTestScores()
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            IEnumerable<CompareScorereport> _Scoreslist = db.Database.SqlQuery<CompareScorereport>("select * from tb_mocktest where  StudentId > 0 and branchid=" + branchid).Select(s =>
                          new CompareScorereport()
                          {
                              TestDate = s.TestDate,
                              firstname = s.firstname,
                              LBand = s.LBand,
                              Overall = s.Overall,
                              RBand = s.RBand,
                              SBand = s.SBand,
                              WBand = s.WBand,
                              StudentId = s.StudentId,
                              testtype = "MockTest",
                              CurrentCourseLevel = s.CurrentCourseLevel,
                          }
                      );

            return _Scoreslist.ToList();

        }



        #endregion


        public List<Tb_IELTSResult> GetIELTSLIst()
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            string qury = @"select * from tb_ieltsresult inner join maadmission as admisn on tb_ieltsresult.studentid=admisn.id
 where StudentId>0 and admisn.isleft=0 and tb_ieltsresult.BranchId=" + branchid;

            List<Tb_IELTSResult> _lst = db.Database.SqlQuery<Tb_IELTSResult>(qury).ToList();
            return _lst;
        }

        public List<Receiptsummary> GetSummaryDetail(string fromdate, string tdate)
        {
            IEnumerable<Receiptsummary> _lst = new List<Receiptsummary>();
            try
            {
                string FDate = Convert.ToDateTime(fromdate).ToString("yyyy-MM-dd");
                string TDate = Convert.ToDateTime(tdate).ToString("yyyy-MM-dd");

                //_lst = db.Database.SqlQuery<ReportsSummery>(@"select userid, sum(amount) as Amount from tb_prescription prescrption where amount>0 and CONVERT(date,prescrption.cdate ) >='" + FDate + "'and CONVERT(date,prescrption.cdate ) <='" + TDate + "' group by userid").Select(s =>

                //changed on Jan 18
                int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
                _lst = db.Database.SqlQuery<Receiptsummary>(@"select  sum(PayAmt) as PayAmnt,Paymode as paymode from Vw_ReceiptSumry RcSummary where PayAmt>0 and BranchId =" + branchid + " and CONVERT(date,RcSummary.cdate ) >='" + FDate + "'and CONVERT(date,RcSummary.cdate ) <='" + TDate + "' group by paymode").Select(s =>

                  new Receiptsummary()
                  {
                      PayAmnt = s.PayAmnt,
                      Paymode = s.Paymode,
                  }
                        );

                return _lst.ToList();
            }
            catch (Exception ex)
            {
            }
            return _lst.ToList();
        }

        public List<ReceiptsumaryCon> GetSummaryCounselorwise(string fromdate, string tdate)
        {
            IEnumerable<ReceiptsumaryCon> _lst = new List<ReceiptsumaryCon>();
            try
            {
                string FDate = Convert.ToDateTime(fromdate).ToString("yyyy-MM-dd");
                string TDate = Convert.ToDateTime(tdate).ToString("yyyy-MM-dd");

                //_lst = db.Database.SqlQuery<ReportsSummery>(@"select userid, sum(amount) as Amount from tb_prescription prescrption where amount>0 and CONVERT(date,prescrption.cdate ) >='" + FDate + "'and CONVERT(date,prescrption.cdate ) <='" + TDate + "' group by userid").Select(s =>

                //changed on Jan 18
                int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
                _lst = db.Database.SqlQuery<ReceiptsumaryCon>(@"select  sum(PayAmt) as PayAmnt,Paymode as paymode,CounselorId from Vw_ReceiptSumry RcSummary where PayAmt>0 and Paymode = 'Cash' and BranchId =" + branchid + " and CONVERT(date,RcSummary.cdate ) >='" + FDate + "'and CONVERT(date,RcSummary.cdate ) <='" + TDate + "' group by paymode, CounselorId").Select(s =>

                  new ReceiptsumaryCon()
                  {
                      PayAmnt = s.PayAmnt,
                      Paymode = s.Paymode,
                      CounselorId = s.CounselorId
                  }
                        );

                return _lst.ToList();
            }
            catch (Exception ex)
            {
            }
            return _lst.ToList();
        }


        public List<Receiptsummary> GetStudentSummary(int id)
        {
            IEnumerable<Receiptsummary> _lst = new List<Receiptsummary>();
            try
            {
                //changed on Jan 18
                int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
                _lst = db.Database.SqlQuery<Receiptsummary>(@"select  sum(PayAmt) as PayAmnt,Paymode as paymode, Remarks2 as Remarks from Vw_StudRecSummry RcSummary where PayAmt>0 and id =" + id + " group by paymode,remarks2").Select(s =>

                new Receiptsummary()
                {
                    PayAmnt = s.PayAmnt,
                    Paymode = s.Paymode,
                    Remarks =s.Remarks
                }
                      );

                return _lst.ToList();
            }
            catch (Exception ex)
            {
            }
            return _lst.ToList();
        }


    }



    public class StaticLogic
    {

        public static int TimeDifference
        {
            get
            {
                return 810;
            }
        }
        public static int RoleId()
        {
            int rol = 0;
            if (HttpContext.Current.Request.Cookies[CommonUtil.CookieRoleid] != null)
            {
                try
                {
                    rol = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieRoleid].Value);
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
            }
            return rol;
        }

        public static string SubRole()
        {
            SSCWebDBEntities1 db = new SSCWebDBEntities1();
            int userid = 0;
            string role = "";
            try
            {
                userid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
                role = db.Users.Where(a => a.Id == userid).Select(a => a.SubRole).FirstOrDefault();
            }
            catch (Exception)
            {
            }
            return role;

        }
    }

    public class UserAction
    {
        public static int Delete
        {
            get
            {
                return 1;
            }
        }
        public static int View
        {
            get
            {
                return 2;
            }
        }
        public static int Add
        {
            get
            {
                return 3;
            }
        }
        public static int Edit
        {
            get
            {
                return 4;
            }
        }
    }


    public class Permission
    {
        public static bool CheckPermission(int rollid, string Menu, int action)
        {
            SSCWebDBEntities1 db = new SSCWebDBEntities1();

            if (HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeSuperAdmin)
            {
                return true;
            }

            int secid = db.MaSecurities.Where(a => a.RightName == Menu).Select(a => a.Id).FirstOrDefault();

            Tb_Permission obj = new Tb_Permission();

            obj = db.Tb_Permission.Where(a => a.RollId == rollid && a.SecurityId == secid).FirstOrDefault();
            if (obj != null)
            {
                if (action == UserAction.Delete)
                {
                    return obj.IsDelete;
                }
                else if (action == UserAction.View)
                {
                    return obj.IsView;
                }
                if (action == UserAction.Add)
                {
                    return obj.IsAdd;
                }
                else if (action == UserAction.Edit)
                {
                    return obj.IsEdit;
                }
            }
            return false;
        }

    }



}