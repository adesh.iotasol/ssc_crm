﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SSCMvc.Models;

namespace SSCMvc.Webutil
{
    public class MasterListing
    {
        SSCWebDBEntities1 db = new SSCWebDBEntities1();

        public List<MaRole> GetRoleList
        {
            get
            {
                return db.MaRoles.ToList();
            }
        }
        public List<St_UserType> GetUserTypeList
        {
            get
            {
                return db.St_UserType.ToList();
            }
        }

        public List<St_PackageHours> GetPackageHours
        {
            get
            {
                return db.St_PackageHours.ToList();
            }
        }

        public List<Tb_SmsTmp> GetSmsTemp
        {
            get
            {
                return db.Tb_SmsTmp.ToList();
            }
        }

    }
}
