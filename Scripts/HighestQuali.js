﻿var mouseX;
var mouseY;
$(document).mousemove(function (e) {
    mouseX = e.pageX;
    mouseY = e.pageY;
});

function showdetail(a) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Maenquiry/_EnqEdudetail",
        data: { id: a },
        dataType: "html",
        success: function (res) {
            $("#edudiv").html(res);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError);
        },
        async: false
    });
    
    

    $("#edudiv").css({ 'top': (mouseY+20), 'left': mouseX+40 }).fadeIn('slow');


}

function hidedetail(a) {
   
    $('#edudiv').fadeOut('slow');
}