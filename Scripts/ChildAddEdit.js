﻿$(document).ready(function () {

    $('input[type="date"]').attr('placeholder', 'mm/dd/yyyy');
    $('input[type="date"]').prop('type', 'text');
    $(".editAcBtn").button();

    $(".myeditAcBtn").button();

    var linkObjAcDetail;

    var mylinkObjAcDetail;


    $('#editDialog').dialog({
        autoOpen: false,
        width: 980,
        resizable: false,
        modal: true,
        buttons: {
            "Submit": function () {
                var date1 = $("#NextDate").val();
                var rem = $("#Remarks").val();
                if (date1 != "" && rem != "") {
                    $("#update-messageStatus").html(''); //make sure there is nothing on the message before we continue  
                    $("#updateCustAcHistory").submit();
                }
                else
                {
                    Alert("Date and Remarks required");
                }
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });

    $(".editAcBtn").click(function () {
        //change the title of the dialgo

        linkObjAcDetail = $(this);


        var AcDetailDIV = $('#editDialog');
        var viewairUrl = linkObjAcDetail.attr('href');


        $.get(viewairUrl, function (data) {

            AcDetailDIV.html(data);

            var $form = $("#updateCustAcHistory");
            // Unbind existing validation
            $form.unbind();
            $form.data("validator", null);
            // Check document for changes
           // $.validator.unobtrusive.parse(document);

            AcDetailDIV.dialog('open');

        });
        return false;
    });


    $(".myeditAcBtn").click(function () {
        //change the title of the dialgo

        linkObjAcDetail = $(this);


        var AcDetailDIV = $('#editDialog');
        var viewairUrl = linkObjAcDetail.attr('href');


        $.get(viewairUrl, function (data) {

            AcDetailDIV.html(data);
            AcDetailDIV.dialog('open');

        });
        return false;
    });



});




function updateSuccessStatus() {


    if ($("#update-messageStatus").html() == "Success") {

        //window.location.reload();
        alert("Added Successfully");

        $('#editDialog').dialog('close');

        return;

    }
    else if ($("#update-messageStatus").html() == "Invalid") {

        alert("Please Check Valid Installment Plan");
        $('#editDialog').dialog('close');

        return;
    }





}

function updateCouponSuccessStatus() {

    if ($("#update-messageStatus").html() == "Success") {


        alert("Added Successfully");

        $('#editDialog').dialog('close');

        return;

    }

}

function updateAcSuccessStatus() {


    $('#editDialog').dialog('close');

    var empval = $("#update-messageStatus").html();



    window.location.href = '/MaAccount/Edit/' + empval;

    //    var url = '/MaAccount/_EditCustAcHistory';

    //    $.get(url, { id: empval }, function (data) {


    //        $("#editlistac").html(data);

    //        alert("Successfully Done");
    //       

    //    });
}




$(".editAcBtnEdit").click(function () {
    //change the title of the dialgo

    linkObjAcDetail = $(this);


    var AcDetailDIV = $('#editDialog');
    var viewairUrl = linkObjAcDetail.attr('href');


    $.get(viewairUrl, function (data) {

        AcDetailDIV.html(data);

        var $form = $("#updateCustAcHistory");
        // Unbind existing validation
        $form.unbind();
        $form.data("validator", null);
        // Check document for changes
        $.validator.unobtrusive.parse(document);

        AcDetailDIV.dialog('open');
        Showoption();
    });
    return false;
});




function Showoption() {
    var myoption = $("#newdrop").val();
    if (myoption == "Cash") {
        $("#paydetailedit").hide();
    }
    else {
        $("#paydetailedit").show();
    }
}