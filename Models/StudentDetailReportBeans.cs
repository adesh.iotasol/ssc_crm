﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class StudentDetailReportBeans
    {
        public Nullable<DateTime> JoinDate { get; set; }
        public Nullable<int> RollNo { get; set; }
        public string FullName { get; set; }
        public string FatherName { get; set; }
        public string Mobile { get; set; }
        public string EmailId { get; set; }
        public string GroupChoice { get; set; }
        public string TimeCode { get; set; }
        public string PackageName { get; set; }
        public Nullable<Double> Amount { get; set; }
    }
}