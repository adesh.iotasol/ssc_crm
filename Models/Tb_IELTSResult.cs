﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SSCMvc.Models
{
    public class Tb_IELTSResult
    {
        public int Id { get; set; }
        
        [Display(Name = "Student")]
        public Nullable<int> StudentId { get; set; }

        [Required(ErrorMessage = "*")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> TestDate { get; set; }

        private Nullable<bool> _IsIELTSAC { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Category")]
        public string Category { get; set; }


        [Required(ErrorMessage = "*")]
        [Display(Name = "Exam Body")]
        public string ExamBody { get; set; }


       // [Required(ErrorMessage = "*")]
        [Display(Name = "Course")]
        public string CourseId { get; set; }


        [Display(Name = "Timing")]
        public string TimeId { get; set; }

        //[Required(ErrorMessage = "*")]
        [Display(Name = "Listening")]
        public Nullable<Double> LBand { get; set; }

        //[Required(ErrorMessage = "*")]
        [Display(Name = "Writing")]
        public Nullable<Double> WBand { get; set; }

        //[Required(ErrorMessage = "*")]
        [Display(Name = "Reading")]
        public Nullable<Double> RBand { get; set; }

        //[Required(ErrorMessage = "*")]
        [Display(Name = "Speaking")]
        public Nullable<Double> SBand { get; set; }

        //[Required(ErrorMessage = "*")]
        [Display(Name = "Overall")]
        public Nullable<Double> Overall { get; set; }


        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> CDate { get; set; }

        [Display(Name = "Branch")]
        public Nullable<int> BranchId { get; set; }


        [Display(Name = "Current Course Level")]
        public string CurrentCourseLvl { get; set; }


        public Nullable<int> UserId { get; set; }

        private Nullable<bool> _Status;

        public bool Status
        {

            get
            {
                if (_Status == true)
                {
                    return true;
                }
                return false;

            }
            set
            {
                _Status = value;
            }

        }


        private Nullable<bool> _isfollowbutton;

        public bool isfollowbtn
        {

            get
            {
                if (_isfollowbutton == true)
                {
                    return true;
                }
                return false;

            }
            set
            {
                _isfollowbutton = value;
            }

        }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Booking Type")]
        public Nullable<int> BookingType { get; set; }
        public Nullable<System.DateTime> BookedTime { get; set; }
        public Nullable<System.DateTime> ResUpdTime { get; set; }
        public string Institute { get; set; }
        public string Detal1 { get; set; }
        public string Detail2 { get; set; }
        public string BookedDBO { get; set; }
        [Required(ErrorMessage = "*")]
        public string Passport { get; set; }
        public Nullable<int> BookedBranch { get; set; }
        [Display(Name = "Remarks")]
        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [Required(ErrorMessage = "*")]
        public string StudentType { get; set; }

        [Display(Name = "Candidate No.")]
        //[Required(ErrorMessage = "*")]
        public Nullable<int> CandiateNo { get; set; }

        public virtual MaBranch MaBranch { get; set; }
        public virtual User User { get; set; }

        public virtual St_BookType St_BookType { get; set; }

    }
}