﻿namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using SScMvc.Models;
    using System.Linq;

    public class Tb_StudentTransfer
    {
        SSCWebDBEntities1 db = new SSCWebDBEntities1();
        public int Id { get; set; }

        [Required(ErrorMessage="*")]
        [Display(Name="Student")]
        public Nullable<int> StudentId { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Branch To")]
        public Nullable<int> BranchTo { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Branch From")]
        public Nullable<int> BranchFrom { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "User To")]
        public Nullable<int> TransferTo { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "User By")]
        public Nullable<int> TransferBy { get; set; }

        [Required (ErrorMessage="*")]
        [Display(Name = "Transfer Status")]
        public string TransStatus { get; set; }

        
        [Display(Name = "Last Updated")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> TransferDate { get; set; }
        public Nullable<DateTime> CDate { get; set; }
        public Nullable<double> BalanceAmt { get; set; }
        public Nullable<bool> _Status { get; set; }
        public bool Status
        {
            get
            {
                if (_Status == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                _Status = value;
            }
        }

        [Display(Name="Remarks")]
        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        public string Course { get; set; }
        public string Package { get; set; }
        public Nullable<int> OldRollNo { get; set; }
        public Nullable<int> OldStudId { get; set; }
        public Nullable<System.DateTime> AcctDate { get; set; }


        public Nullable<int> OldLsNo { get; set; }
        public Nullable<int> NewLsNo { get; set; }

        public string GetStudentid()
        {
            return db.MaAdmissions.Where(a => a.Id == StudentId).Select(a => a.FirstName + "" + a.LastName).FirstOrDefault();
        }
        public string GetFatherName()
        {
            return db.MaAdmissions.Where(a => a.Id == StudentId).Select(a => a.FatherName).FirstOrDefault();
        }
        public string GetMobile()
        {
            return db.MaAdmissions.Where(a => a.Id == StudentId).Select(a => a.Mobile).FirstOrDefault();
        }
        public DateTime? GetDOB()
        {
            return db.MaAdmissions.Where(a => a.Id == StudentId).Select(a => a.DOB).FirstOrDefault();
        }

        public string GetBranchTo()
        {
            return db.MaBranches.Where(a => a.Id == BranchTo).Select(a => a.BramchName).FirstOrDefault();
        }
        public string GetBranchName()
        {
            return db.MaBranches.Where(a => a.Id == BranchFrom).Select(a => a.BramchName).FirstOrDefault();
        }

        public string GetTransferTo()
        {
            return db.Users.Where(a => a.Id == TransferTo).Select(a => a.Fullname).FirstOrDefault();
        }
        public string GetTransferFrom()
        {
            return db.Users.Where(a => a.Id == TransferBy).Select(a => a.Fullname).FirstOrDefault();
        }


        public virtual MaAdmission MaAdmission { get; set; }
        public virtual MaBranch MaBranch { get; set; }
        public virtual MaBranch MaBranch1 { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
    }
}