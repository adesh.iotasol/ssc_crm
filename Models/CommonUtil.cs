﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SSCMvc;
using System.Data.SqlClient;
using System.Data;
using SSCMvc.Models;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Configuration;

namespace SSCMvc.Models
{
    public class CommonUtil
    {
        SSCWebDBEntities1 db = new SSCWebDBEntities1();

        public static string CookieBranchid = "branchid";

        public static string CookieUsertype = "usertype";

        public static string CookieUserid = "Userid";

        public static string CookieRoleid = "Roleid";


        public static string FeeRefundType = "Fee Refund";

        public static string BranchName = "BranchName";


        public static string UserTypeAdmin = "Admin";
        public static string UserTypeCounselor = "Counselor";
        public static string UserTypeNewCounselor = "NewCounselor";
        public static string UserTypeSuperAdmin = "Super Admin";

        public static string UserTypeVisaCounselor = "Visa Counselor";
        public static string UserTypeManager = "Manager";
        public static string UserTypeReport = "Report";






        /*--------------Bug Report Status------------------*/
        public static string BugStatusOpen = "Open";
        public static string BugStatusClose = "Closed";
        public static string BugStatusResolved = "Resolved";
        public static string BugStatusImplemented = "Implemented";
        /*--------------------------------------------------*/


        /*--------------pending visa followup------------------*/
        public static string selected = "Registered";

        /*--------------------------------------------------*/

        /*------------------Voucher type-------------*/

        public static string FeeAdjustment = "FeeAdjustment";
        /*------------------------------------------*/


        /*---------------------------EnqFollowUp Status ---------------------*/
        public static string CloseEnqFollow = "Close";
        public static string OpenEnqFollow = "Open";
        /*---------------------------EnqFollowUp Status ---------------------*/


        /*---------------------------Legder Summary Option---------------------*/
        public static string LedgerCourseFees = "CourseFees";
        public static string LedgerFeeReceipt = "FeeReceipt";
        public static string LedgerFeeRefund = "Fee Refund";
        public static string LedgerFeeAdjustment = "FeeAdjustment";
        /*---------------------------Legder Summary Option---------------------*/

        public static string Admission = "Admission";
        public static string Enquiry = "Enquiry";
        public static string prospstudents = "Prosp. Students";

        public static string AdmissionMcktst = "Prosp. Students Mock Test";
        public static string EnquiryIELTS = "Enquiry IELTS";

        public static string ProspStudentsIELTS = "Prosp. Students IELTS";


        #region Static Right Name

        public static string Branches = "BRANCHES";
        public static string Designation = "DESIGNATION";
        public static string User = "USER";
        public static string SendSMS = "SENDSMS";
        public static string Reference = "REFERENCE";
        public static string AllStudents = "ALL STUDENTS";
        public static string Course = "COURSE";
        public static string Package = "PACKAGE";
        public static string Time = "TIME";
        public static string Discount = "DISCOUNT";
        public static string AssignTimeCode = "ASSIGN TIME CODE";
        public static string Reason = "REASON";
        public static string AddNewUser = "ADD NEW USER";
        public static string AddArea = "ADD AREA";
        public static string EnquiryString = "ENQUIRY";
        public static string ProspStudent = "PROSP STUDENT";
        public static string FeeReceipt = "FEE RECEIPT";
        public static string FeeAdjustmentString = "FEE ADJUSTMENT";
        public static string FeeRefund = "FEE REFUND";
        public static string Daybook = "DAY BOOK";
        public static string StudentLeave = "STUDENT LEAVE";
        public static string StudentTransfer = "STUDENT TRANSFER";
        public static string MockTest = "MOCK TEST";
        public static string IeltsPteTest = "IELTS PTE TEST";
        public static string BugReport = "BUG REPORT";
        public static string AddSecurity = "ADD SECURITY";
        public static string AddPackageHour = "ADD PACKAGE HOUR";
        public static string Paymode = "PAY MODE";
        public static string Homework = "HOMEWORK";
        public static string Preference = "PREFERENCE";
        public static string GSTslip = "GST SLIP";
        public static string SwitchBranch = "SWITCH BRANCH";
        public static string GSTReport = "GST REPORT";

        public static string VisaStudent = "VISA STUDENT";
        public static string DailyFeeReprot = "DAILY FEE REPORT";
        public static string StudentDetail = "STUDENT DETAIL";
        public static string PendingFeeList = "PENDING FEE LIST";
        public static string DefaultureList = "DEFAULTER LIST";
        public static string AllReceipt = "ALL RECEIPT";
        public static string EnquiryAnalusis = "ENQUIRY ANALYSIS";
        public static string TimeWiseList = "TIME WISE LIST";
        public static string TimeSlot = "TIME SLOT";
        public static string ReferenceReport = "REFERENCE REPORT";
        public static string DiscountReport = "DISCOUNT REPORT";
        public static string CoursePackageReport = "COURSE PACKAGE REPORT";
        public static string PhotUploadReport = "PHOTO UPLOAD REPORT";
        public static string Performance = "PERFORMANCE";
        public static string PendingFollowUp = "PENDING FOLLOWUP";
        public static string PendingFollowList = "PENDING FOLLOWUP LIST";
        public static string FeeReceiptString = "FEE RECEIPT";
        public static string ClosedReason = "CLOSED REASON";
        public static string DemoConversion = "DEMO CONVERSION";
        public static string ScoreReport = "SCORE REPORT";
        public static string IeltsPteReport = "IELTS PTE REPORT";
        public static string LeftStuentReport = "LEFT STUDENT RECORD";
        public static string RemoveLeftStudent = "REMOVE LEFT STUDENT";
        public static string AddRole = "ADD ROLE";
        public static string AreaReport = "AREA REPORT";
        public static string MobileEdit = "MOBILE EDIT";
        public static string BranhDiscount = "BRANCH DISCOUNT";
        public static string InactiveCourse = "INACTIVE COURSE REPORT";
        public static string VisaType = "Visa Type";

        #endregion


        public int? GetCopyId(int id)
        {
            int? cid = db.MaEnquiries.Where(w => w.CopyFrom == id).Select(s => s.CopyFrom).FirstOrDefault();
            return cid == null ? 0 : cid;
        }

       

        // m stands for method account
        /// <summary>
        /// Common Method For ledger entry 
        /// </summary>
        /// <param name="maccountid"></param>
        /// <param name="mdebitval"></param>
        /// <param name="mcreditval"></param>
        /// <param name="mnarration"></param>
        /// <param name="mrecid"></param>
        /// <param name="mvdate"></param>
        /// <param name="mvtype"></param>
        public static void InsertToLedger(int? vouchno, double? vdebitval, double? vcreditval, string vnarration, int? vouadmisid, DateTime voudate, string voutype)
        {

            SSCWebDBEntities1 db = new SSCWebDBEntities1();

            Tb_Ledger lg = new Tb_Ledger();
            lg.VoucherNo = vouchno;
            lg.DebitAmt = vdebitval;
            lg.CreditAmt = vcreditval;
            lg.Narration = vnarration;
            lg.AdmissionId = vouadmisid;
            lg.Vouchdate = voudate;
            lg.VoucherType = voutype;

            db.Tb_Ledger.Add(lg);
            db.SaveChanges();
        }
        /// <summary>
        /// Update ledger (Problem with Updating in case Of Performa Invoice if Recid of sale table or Recid of Reciept Table Matches then There will be Error);
        /// 
        /// in case where VTYPE is P.I
        /// </summary>
        /// <param name="maccountid"></param>
        /// <param name="mdebitval"></param>
        /// <param name="mcreditval"></param>
        /// <param name="mnarration"></param>
        /// <param name="mrecid"></param>
        /// <param name="mvdate"></param>
        /// <param name="mvtype"></param>
        public static void UpdateToLedger(int? vouchno, double? vdebitval, double? vcreditval, string vnarration, DateTime voudate, int? vouadmisid, string vtype)
        {
            SSCWebDBEntities1 db = new SSCWebDBEntities1();

            //int count = db.Tb_Ledger.Where(m => m.VoucherNo == vouchno).Where(m => m.VoucherType == vtype).Count();

            Tb_Ledger lg = db.Tb_Ledger.Where(m => m.VoucherNo == vouchno).Where(m => m.VoucherType == vtype).First();


            lg.VoucherNo = vouchno;
            lg.DebitAmt = vdebitval;
            lg.CreditAmt = vcreditval;
            lg.Narration = vnarration;

            lg.Vouchdate = voudate;
            lg.VoucherType = vtype;

            db.SaveChanges();

        }

        public string GetBranchName(int id)
        {
            try
            {
                string bname = db.MaBranches.Find(id).BramchName;
                return bname;
            }
            catch (Exception) { }
            return "";
        }

        public List<User> GetUserList(int? brachid = 0, int parameter = 0) // int paramater added on 24 june to implement coding of deactivation get the enquires handled by that person who have left the job
        {
            if (brachid > 0 && parameter > 0)
            {
                return db.Users.Where(z => z.BranchId == brachid && z.Status == true).OrderBy(x => x.Fullname).ToList();
            }
            if (brachid > 0)
            {
                return db.Users.Where(z => z.BranchId == brachid && z.Status == true).OrderBy(x => x.Fullname).ToList();
            }
            return db.Users.OrderBy(o=> o.username).ToList();
        }

        public List<User> GetUserListLeft(int? brachid = 0, int parameter = 0) // int paramater added on 24 june to implement coding of deactivation get the enquires handled by that person who have left the job
        {
            //if (brachid > 0 && parameter > 0)
            //{
            //    return db.Users.Where(z => z.BranchId == brachid && z.Status == true).ToList();
            //}
            //if (brachid > 0)
            //{
            //    return db.Users.Where(z => z.BranchId == brachid && z.Status == true).ToList();
            //}
            return db.Users.Where(z => z.BranchId == brachid).OrderBy(x => x.Fullname).ToList();
        }

        public List<AdmissionView> GetStudents(int branchid = 0)
        {
            if (branchid > 0)
            {
                return db.AdmissionViews.Where(z => z.BranchId == branchid).ToList();
            }
            return db.AdmissionViews.ToList();
        }

        public List<MaAdmission> GetAdmStudent()
        {

            return db.MaAdmissions.ToList();
        }

        public List<MockTestStudent> GetMockTestStudents(int branchid = 0)
        {

            if (branchid > 0)
            {

                //return db.MockTestStudents.Where(z => z.BranchId == branchid).Where(z => z.EnquiryIELAC == true || z.EnquiryIELGT == true || z.EnquirySpkEng == true || z.EnquiryPTE == true || z.EnquiryStyVisa == true || z.IsLeft == true).ToList();
                return db.MockTestStudents.Where(z => z.BranchId == branchid && (z.EnqAbt == "IELTS" || z.EnqAbt == "SPOKEN ENGLISH" || z.EnqAbt == "VISA" || z.EnqAbt == "PTE" || z.EnqAbt == "EXAM BOOKING")).ToList();


            }

            return db.MockTestStudents.ToList();

        }

        public string GetDateFormat(DateTime Dt)
        {
            string Fd1 = Dt.ToString("MM/dd/yyyy");
            return Fd1;
        }

        public List<St_BookType> GetBooking()
        {
            return db.St_BookType.ToList();
        }


        public void UpdateCourseHistoryAmout(int courseid)
        {
            List<Tb_Installment> lst = db.Tb_Installment.Where(z => z.CourseHistId == courseid).ToList();
            double? amount = 0;
            foreach (Tb_Installment objnew in lst)
            {
                amount += objnew.Amount;
            }

            Tb_CourseHist objCoursehist = db.Tb_CourseHist.Where(z => z.Id == courseid).First();
            objCoursehist.Amout = amount;
            db.SaveChanges();
        }

        public static string HomeAddress(string utype)
        {
            if (utype == CommonUtil.UserTypeAdmin)
            {
                return "/Home";
            }
            if (utype == CommonUtil.UserTypeSuperAdmin)
            {
                return "/Home";
            }
            if (utype == CommonUtil.UserTypeCounselor)
            {
                return "/Counselor";
            }
            if (utype == CommonUtil.UserTypeVisaCounselor)
            {
                return "/Home";
            }

            return " ";
        }





        //public static void UpdateToLedger(int maccountid, double mdebitval, double mcreditval, string mnarration, int mrecid, DateTime mvdate, string mvtype)
        //{
        //    LadharDbEntities db = new LadharDbEntities();
        //    Ledger lg = db.Ledgers.Where(m => m.recid == mrecid).Where(m => m.vtype == mvtype).First();


        //    lg.accode = maccountid;
        //    lg.debit = mdebitval;
        //    lg.credit = mcreditval;
        //    lg.descrip = mnarration;
        //    // lg.recid = mrecid;
        //    lg.vdate = mvdate;
        //    lg.vtype = mvtype;

        //    db.SaveChanges();




        //}

        //works on Enquiry creation

        public void SendEmailToUser(MaEnquiry maenquiry, bool ishtml = false)
        {
           
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            string brachname = db.MaBranches.Where(z => z.Id == branchid).Select(z => z.BramchName).First();
            string mymessage;
            if (ishtml == true)
            {
                StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/HTML/Emailer.htm"));
                string s = sr.ReadToEnd();

                mymessage = String.Format(s, maenquiry.FirstName.ToUpper() + " " + maenquiry.LastName.ToUpper(), maenquiry.Id);
            }
            else
            {
                mymessage = "Dear " + maenquiry.FirstName.ToUpper() + " " + maenquiry.LastName.ToUpper() + ",<br> Greetings from SSC.<br>Thanks for your visit at our campus " + brachname + " Your Enquiry No is <b>" + maenquiry.Id + "</b> <br> <b> SSC TEAM </b> <br> <img src='http://crm-ssceducation.org/ssclogo.jpg' />";
            }

            try
            {
                // string mail = "info@crm-ssceducation.org";
                string mail = ConfigurationManager.AppSettings["Mail"];
                MailMessage message = new MailMessage();

                if (maenquiry.EmailId != null && maenquiry.EmailId != "")
                {
                    message.To.Add(new MailAddress(maenquiry.EmailId));
                }
                message.Subject = "Enquiry Information By Sethi Study Circle: ";
                message.From = new MailAddress(mail);
                //if (maenquiry.ReferenceId != null)
                //{
                //    MaFrmReference objref = db.MaFrmReferences.Where(z => z.Id == maenquiry.ReferenceId).First();

                //    if (objref.Email != "" && objref.Email != null)
                //    {
                //        message.Bcc.Add(new MailAddress(objref.Email));
                //    }
                //}
                //message.Body = "Dear " + maenquiry.FirstName.ToUpper() + " " + maenquiry.LastName.ToUpper() + ",<br> Greetings from SSC.<br>Thanks for your visit at our campus " + brachname + " Your Enquiry No is <b>" + maenquiry.Id + "</b> <br> <b> SSC TEAM </b> <br> <img src='http://crm-ssceducation.org/ssclogo.jpg' />";


                message.Body = mymessage;
                message.IsBodyHtml = true;
                //string smtpserver = "mail.crm-ssceducation.org";
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                SmtpClient smtpClient = new SmtpClient(smtpserver);
                smtpClient.Port = Int32.Parse(ConfigurationManager.AppSettings["Port"]);
                smtpClient.Credentials = new NetworkCredential(mail, ConfigurationManager.AppSettings["credential"].ToString());
                smtpClient.EnableSsl = false;
                smtpClient.Send(message);

            }
            catch (Exception ex)
            {


            }
        }


        //works on Admission

        public void SendEmailToUser(MaAdmission maadmission, bool ishtml = false)
        {
            try
            {
                // string mail = "info@crm-ssceducation.org";
                string mail = ConfigurationManager.AppSettings["Mail"];

                MailMessage message = new MailMessage();

                string mymessage;
                if (ishtml)
                {
                    StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/HTML/AdmissionEmailer.htm"));
                    string s = sr.ReadToEnd();

                    mymessage = String.Format(s, maadmission.FirstName.ToUpper() + " " + maadmission.LastName.ToUpper(), maadmission.RollNo);
                }
                else
                {
                    mymessage = "Dear " + maadmission.FirstName.ToUpper() + " " + maadmission.LastName.ToUpper() + "Welcome to SSC. <br> Your Roll No is <b>" + maadmission.RollNo + "</b>.<br> We wish you happy learning. <br> <b>SSC TEAM</b> <br> <b> SSC TEAM </b> <br> <img src='http://crm-ssceducation.org/ssclogo.jpg' />";
                }


                if (maadmission.EmailId != null && maadmission.EmailId != "")
                {
                    message.To.Add(new MailAddress(maadmission.EmailId));
                }
                message.Subject = "Admission Information By Sethi Study Circle: ";
                message.From = new MailAddress(mail);
                if (maadmission.ReferenceId != null)
                {
                    MaFrmReference objref = db.MaFrmReferences.Where(z => z.Id == maadmission.ReferenceId).First();

                    if (objref.Email != "" && objref.Email != null)
                    {
                        message.Bcc.Add(new MailAddress(objref.Email));
                    }
                }
                //message.Body = "Dear " + maadmission.FirstName.ToUpper() + " " + maadmission.LastName.ToUpper() + "Welcome to SSC. <br> Your Roll No is <b>" + maadmission.RollNo + "</b>.<br> We wish you happy learning. <br> <b>SSC TEAM</b> <br> <b> SSC TEAM </b> <br> <img src='http://crm-ssceducation.org/ssclogo.jpg' />";

                message.Body = mymessage;
                message.IsBodyHtml = true;
                //string smtpserver = "mail.crm-ssceducation.org";
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
                SmtpClient smtpClient = new SmtpClient(smtpserver);
                smtpClient.Port = Int32.Parse(ConfigurationManager.AppSettings["Port"]);
                smtpClient.Credentials = new NetworkCredential(mail, ConfigurationManager.AppSettings["credential"].ToString());
                smtpClient.EnableSsl = false;
                smtpClient.Send(message);

            }
            catch (Exception ex)
            {

            }
        }



        //works on first fee payment

        public void SendEmailToReferene(Tb_FeeReceipt receipt)
        {
            try
            {
                //string mail = "info@crm-ssceducation.org";
                string mail = ConfigurationManager.AppSettings["Mail"];
                MailMessage message = new MailMessage();
                MaAdmission maadmission = db.MaAdmissions.Where(z => z.Id == receipt.StudentId).First();
                //if (maadmission.EmailId != null && maadmission.EmailId != "")
                //{
                //    message.To.Add(new MailAddress(maadmission.EmailId));
                //}
                message.Subject = "Fee Receipt, By Sethi Study Circle: ";
                message.From = new MailAddress(mail);

                if (maadmission.ReferenceId != null)
                {
                    MaFrmReference objref = db.MaFrmReferences.Where(z => z.Id == maadmission.ReferenceId).First();

                    if (objref.Email != "" && objref.Email != null)
                    {
                        message.To.Add(new MailAddress(objref.Email));
                    }
                }
                message.Body = "Dear " + maadmission.FirstName.ToUpper() + " " + maadmission.LastName.ToUpper() + "<br> Your Roll No is <b>" + maadmission.RollNo + "</b><br>Amount Paid = <b>" + receipt.PaidAmount + "</b><br> <b> SSC TEAM </b> <br> <img src='http://crm-ssceducation.org/ssclogo.jpg' />";
                message.IsBodyHtml = true;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
                SmtpClient smtpClient = new SmtpClient(smtpserver);
                smtpClient.Port = Int32.Parse(ConfigurationManager.AppSettings["Port"]);
                smtpClient.Credentials = new NetworkCredential(mail, ConfigurationManager.AppSettings["credential"].ToString());
                smtpClient.EnableSsl = false;
                smtpClient.Send(message);

            }
            catch (Exception ex)
            {

            }
        }


        public void SendMailtoUser(string Emailid, string passwrd, string username, string fullname)
        {
            try
            {
                string mail = ConfigurationManager.AppSettings["Mail"];
                MailMessage message = new MailMessage();
                message.Subject = "Reset Password";
                message.Body = " <p>Dear " + username + "</p><br/> <p>To access SSC <b>CRM</b> software, please click on the link below:</p> <br/><a href='http://crm-ssceducation.org/'>crm-ssceducation.org</a><br /> <p>Your access details are as follows:</p><br /> User Name : " + username + " <br />Password : " + passwrd + "<br /> In case of any issue please contact at techteam@ssceducation.org";
                message.From = new MailAddress(mail);
                message.CC.Add("team@dstindia.com");
                message.To.Add(new MailAddress(Emailid));
                message.IsBodyHtml = true;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
                SmtpClient smtpClient = new SmtpClient(smtpserver);
                smtpClient.Port = Int32.Parse(ConfigurationManager.AppSettings["Port"]);
                smtpClient.Credentials = new NetworkCredential(mail, ConfigurationManager.AppSettings["credential"].ToString());
                smtpClient.EnableSsl = false;
                smtpClient.Send(message);

            }
            catch (Exception ex)
            { }


        }




        /*These following functions are used when student transfer occurs*/

        public void UpdateMaAdmission(int stuid, int branchid, int userid)
        {
            MaAdmission objAdm = db.MaAdmissions.Where(z => z.Id == stuid).First();
            objAdm.BranchId = branchid;
            objAdm.CounselorName = userid;
            db.SaveChanges();
        }

        public void UpdateFeeReceipt(int stuid, int branchid, int userid)
        {
            List<Tb_FeeReceipt> lstfee = db.Tb_FeeReceipt.Where(z => z.StudentId == stuid).ToList();
            foreach (Tb_FeeReceipt obj in lstfee)
            {
                obj.BranchId = branchid;
                obj.CounselorId = userid;
                obj.IsTransfer = true;
                int? max = db.Tb_FeeReceipt.Where(z => z.BranchId == branchid).Select(z => z.LocSerialNo).Max();
                if (max == null || max == 0)
                {
                    max = 1;
                }
                else
                {
                    max = max + 1;
                }
                obj.LocSerialNo = max;
                db.SaveChanges();

            }
        }

        public void UpdateInstalment(int stuid, int branchid)
        {
            List<Tb_CourseHist> lstcoursehist = db.Tb_CourseHist.Where(z => z.AdmissionId == stuid).ToList();
            foreach (Tb_CourseHist obj in lstcoursehist)
            {
                List<Tb_Installment> lstinstal = db.Tb_Installment.Where(z => z.CourseHistId == obj.Id).ToList();
                foreach (Tb_Installment objin in lstinstal)
                {
                    objin.BranchId = branchid;
                    db.SaveChanges();
                }
            }

        }

        public void UpdateStudentLeave(int stuid, int branchid)
        {
            List<Tb_StudLeave> lstleave = db.Tb_StudLeave.Where(z => z.AdmissionId == stuid).ToList();
            foreach (Tb_StudLeave obj in lstleave)
            {
                obj.BranchId = branchid;
                db.SaveChanges();
            }
        }

        public void UpdateFeeRefund(int stuid, int branchid)
        {
            List<Tb_FeeRefund> lstrefund = db.Tb_FeeRefund.Where(z => z.StudentId == stuid).ToList();
            foreach (Tb_FeeRefund obj in lstrefund)
            {
                obj.BranchId = branchid;
                db.SaveChanges();
            }
        }

        public void UpdateFeeAdjustment(int stuid, int branchid)
        {
            List<Tb_FeeAdjustment> lstadjust = db.Tb_FeeAdjustment.Where(z => z.StudentId == stuid).ToList();
            foreach (Tb_FeeAdjustment obj in lstadjust)
            {
                obj.BranchId = branchid;
                db.SaveChanges();
            }
        }


        public void UpdateMaEnquiry(int stuid, int branchid)
        {
            int? EnquiryId = db.MaAdmissions.Where(a => a.Id == stuid).Select(a => a.EnquiryId).FirstOrDefault();
            MaEnquiry Obj = new MaEnquiry();
            Obj = db.MaEnquiries.Where(a => a.Id == EnquiryId).FirstOrDefault();
            Obj.BranchId = branchid;
            db.SaveChanges();
        }

        /*end of the student transfer functionality*/


        public int? GetEnqId(int? stuid)
        {
            int? id = 0;
            try
            {
                id = db.MaAdmissions.Find(stuid).EnquiryId;
            }catch(Exception)
            {
                id = stuid;
            }
            return id;
        }

        public int? GetEnqId(int? stuid,string reftype)
        {   
            int? id = 0;
            try
            {
                if (reftype.Equals("VisaMockTest"))
                    id = db.MaAdmissions.Find(stuid).EnquiryId;
                else
                    id = stuid;
                return id;
            }
            catch (Exception)
            {
                return stuid;
            }
        }
         

        public string ToTitleCase(string str)
        {
            string first = str.Substring(0, 1);
            str = first.ToUpper() + str.Substring(1);
            return str;

        }
        public DataTable GetSqlResult(string sql)
        {
            DataTable dt = new DataTable();
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
            var sscsb = new SqlConnectionStringBuilder(cs);
            sscsb.ConnectTimeout = 2000;
            using (SqlConnection con = new SqlConnection(sscsb.ConnectionString))
            {
                con.Open();
                SqlDataAdapter cmd = new SqlDataAdapter(sql, con);
                cmd.SelectCommand.CommandTimeout = 2000;
                cmd.Fill(dt);
                con.Close();
            }
            return dt;
        }

        public int GetPendingTransferCount(int id = 0)
        {
            int userid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            string usrtype = HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value;
            if (usrtype == CommonUtil.UserTypeSuperAdmin || usrtype == CommonUtil.UserTypeAdmin)
            {
                int count = db.Tb_StudentTransfer.Where(z => z.Status == false).Count();

                return count;
            }
            else
            {
                int count = db.Tb_StudentTransfer.Where(z => z.TransferTo == userid && z.Status == false).Count();
                return count;
            }
        }

        public int GetLeftCouselorsEnqcount(int id = 0)
        {
            int count = db.MaEnquiries.Where(x => x.User1.Status == false && x.FEnqstatus == "Open").Count();
            return count;
        }



        public void SendMessage(string mobileno, string messagetext)
        {
            //mobileno = studentRegisterDataGridView.Rows[i].Cells[10].Value.ToString();
            // Create a request using a URL that can receive a post. 

            WebRequest request = WebRequest.Create(String.Format("http://199.189.250.157/smsclient/api.php?username=sscedu&password={2}&source=dstindia&dmobile=91{0}&message={1}", mobileno, messagetext, "kanwarsethi"));

            WebResponse response = request.GetResponse();

            Stream dataStream = response.GetResponseStream();

            StreamReader reader = new StreamReader(dataStream);

            string responseFromServer = reader.ReadToEnd();
            //MessageBox.Show(responseFromServer);

            reader.Close();

            dataStream.Close();

            response.Close();
        }


        public void ResetPassword(string username)
        {

            int count = (from p in db.Users where p.username == username && p.Status == true select p).Count();

            if (count == 1)
            {
                User obj = db.Users.Where(z => z.username == username).First();
                string pass = Guid.NewGuid().ToString().Substring(0, 8);
                obj.Password = pass;
                obj.expirydate = DateTime.Now.AddDays(60);
                db.SaveChanges();

                SendMailtoUser(obj.Email, obj.Password, obj.username, obj.Fullname);
            }
        }

        public static DateTime IndianTime()
        {
            TimeZone time2 = TimeZone.CurrentTimeZone;
            DateTime test = time2.ToUniversalTime(DateTime.Now);
            var indiatime = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            var india = TimeZoneInfo.ConvertTimeFromUtc(test, indiatime);
            return india;
        }

        public static int? IndianTimeDiff()
        {
            int? timediff = 0;
            TimeZone time2 = TimeZone.CurrentTimeZone;
            string Timez = TimeZone.CurrentTimeZone.StandardName.ToString();
            DateTime test = time2.ToUniversalTime(DateTime.Now);
            var indiatime = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            var tter = TimeZoneInfo.FindSystemTimeZoneById(Timez);


            var india = TimeZoneInfo.ConvertTimeFromUtc(test, indiatime);
            DateTime ind = Convert.ToDateTime(india);

            timediff = test.Minute - ind.Minute;


            return timediff;
        }

        public void SendError(Exception ex)
        {
            if (ex.InnerException != null)
            {
                if (ex.InnerException.InnerException is SqlException)
                {
                    SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                    if (sqlex.Number == 547)
                    {
                        HttpContext.Current.Session["error"] = "!Record Is Used Somewhere Else.";

                    }

                    else if (sqlex.Number == 2627)
                    {
                        HttpContext.Current.Session["error"] = "!Cannot Insert Duplicate Value";

                    }
                    else
                    {
                        HttpContext.Current.Session["error"] = ex.InnerException.InnerException.Message;
                    }
                }
            }
            else
            {
                HttpContext.Current.Session["error"] = ex.Message;
            }
        }





        public String SessionError
        {
            get
            {
                return HttpContext.Current.Session["error"].ToString();
            }
            set
            {
                HttpContext.Current.Session["error"] = value;
            }
        }

        public string ValidateLogin()
        {
            Tb_Prefrence tb_pref = db.Tb_Prefrence.FirstOrDefault();

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            string s=context.Request.ServerVariables["REMOTE_ADDR"];
            string utype = HttpContext.Current.Request.Cookies[SSCMvc.Models.CommonUtil.CookieUsertype].Value;

            if (tb_pref.IPAddress.Contains(s) || utype.Equals(CommonUtil.UserTypeAdmin) || utype.Equals(CommonUtil.UserTypeSuperAdmin))
            {
                return "YES";
            }
            else
            {
                return "NO";
            }

        }

        //public List<UserApproval> UpdateUserStatusList(int gid, string st)
        //{
        //    List<int> array1 = new List<int>();
        //    try
        //    {
        //        List<UserApproval> lst = (List<UserApproval>)HttpContext.Current.Session["user"];
        //        UserApproval obj = new UserApproval();
        //        if (lst == null)
        //        {
        //            lst = new List<UserApproval>();
        //        }
        //        if (st == "true")
        //        {

        //            obj.id = gid;
        //            try
        //            {
        //                obj.status = Convert.ToBoolean(st);
        //            }
        //            catch (Exception ex)
        //            { }
        //            lst.Add(obj);
        //            HttpContext.Current.Session["user"] = lst;

        //        }
        //        else
        //        {

        //            obj = lst.Where(z => z.id == gid).FirstOrDefault();
        //            //here remove 
        //            lst.Remove(obj);

        //            //here update
        //            try
        //            {
        //                obj.status = Convert.ToBoolean(st);
        //            }
        //            catch (Exception ex)
        //            { }
        //            lst.Add(obj);
        //            HttpContext.Current.Session["user"] = lst;
        //        }
        //    }
        //    catch (Exception ex)
        //    { }

        //    return (List<UserApproval>)HttpContext.Current.Session["user"];
        //}



        public class SwitchPermsn
        {
            public static string switchOn = "On";
            public static string switchOff = "Off";
        }


    }

}