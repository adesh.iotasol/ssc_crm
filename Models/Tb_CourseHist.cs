//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SSCMvc.Models
{
    using SSCMvc.Webutil;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Tb_CourseHist
    {

        public Tb_CourseHist()
        {
            this.Tb_Installment = new HashSet<Tb_Installment>();
            FromDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
        }

        public int Id { get; set; }

        [Display(Name = "Course Name")]
        [Required(ErrorMessage = "Course Name is required")]
        public Nullable<int> CourseId { get; set; }

        [Display(Name = "Package ")]
        [Required(ErrorMessage = "Package is required")]
        public Nullable<int> PackageId { get; set; }

        [Display(Name = "Time")]
        [Required(ErrorMessage = "Time is required")]
        public Nullable<int> TimeId { get; set; }

        [Display(Name = "Admission")]
        
        public Nullable<int> AdmissionId { get; set; }

        [Display(Name = "From Date")]
        [Required(ErrorMessage = "From Date is required")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> FromDate { get; set; }

        [Display(Name = "To Date")]
        //[Required(ErrorMessage = "To Date is required")]
        [DataType(DataType.Date)]
        
        public Nullable<System.DateTime> Todate { get; set; }

        private Nullable<bool> _Status;
        public bool Status
        {
            get
            {
                if (_Status == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            set
            {
                _Status = value;
            }

        }


        private Nullable<bool> _IsActive;
        public bool IsActive
        {
            get
            {
                if (_IsActive == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            set
            {
                _IsActive = value;
            }

        }
        [DataType(DataType.Date)]
        public Nullable<DateTime> DueDate { get; set; }
        public Nullable<Double> Amout { get; set; }
        public string GroupName { get; set; }

        public string currentcourselevel { get; set; }
        public string CourseRemarks { get; set; }
        public string EnterBy { get; set; }
        public Nullable<bool> _IsReport { get; set; }
        public bool IsReport
        {
            get
            {
                if (_IsReport == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            set
            {
                _IsReport = value;
            }

        }

        public Nullable<bool> _IsSync { get; set; }
        public bool IsSync
        {
            get
            {
                if (_IsSync == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            set
            {
                _IsSync = value;
            }

        }

        public virtual MaAdmission MaAdmission { get; set; }
        public virtual MaCourse MaCourse { get; set; }
        public virtual MaPackage MaPackage { get; set; }
        public virtual MaTime MaTime { get; set; }
        public virtual ICollection<Tb_Installment> Tb_Installment { get; set; }
    }
}
