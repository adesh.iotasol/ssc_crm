﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{

    public class Bean
    {
        public string branch { get; set; }
        public string followtype { get; set; }
        public int fcount { get; set; }
    }
    public class FollowUpEnqList
    {
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public int? Id { get; set; }
        public string EnqHandledBy { get; set; }
        public string Enqstatus { get; set; }
        public string Remarks { get; set; }
        public string FatherNo { get; set; }
        public DateTime? nextdate { get; set; }
        public DateTime? CreateDate { get; set; }
        public string EnqAbt { get; set; }
      
    }
        public class Enquirycountbean
    {
        public string username { get; set; }

        public int usercount { get; set; }

        public string branchname { get; set; }
    }


    //demo fee conversion start here
    public class democonversionbean
    {
        public int EnquiryNo { get; set; }

        public string Name { get; set; }

        public string Mobile { get; set; }
        public DateTime? DemoDate { get; set; }
        public string GroupChoice { get; set; }

        public string GroupTime { get; set; }

        public int totaldemoconverted { get; set; }
        public int ConvertedNot { get; set; }

        public string conversiontype { get; set; }

        public string lastremarks { get; set; }

        public string lastFollowdate { get; set; }

        public string AdmisnDate { get; set; }

        public string Status { get; set; }

    }

    //closed reason report
    public class closedresonbean
    {
        public string Enquiryfollwcomment { get; set; }
        public int Enquiryfollwcount { get; set; }
        public string followuptype { get; set; }
    }


    public class HelperForAreaReport
    {
        public string area { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }

        public string SelectedArea { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }


    public class HelperForInactiveCourseReport
    {
        public string Name { get; set; }
        public int? RollNo { get; set; }
        public string Course { get; set; }
        public string GroupName
        {
            get;
            set;
        }
        public string Timing { get; set; }
        public string CurrentCourse { get; set; }
        public int? AdmissionId { get; set; }


        public bool IsLeft { get; set; }
        public DateTime? CourseFrom { get; set; }
        public DateTime? CouerseTo { get; set; }
        public bool IsActive { get; set; }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }


    //compare score  report
    public class CompareScorereport
    {
        public string firstname { get; set; }
        public int? RollNo { get; set; }
        public DateTime? TestDate { get; set; }
        public double LBand { get; set; }
        public double RBand { get; set; }
        public double WBand { get; set; }
        public double SBand { get; set; }
        public double Overall { get; set; }
        public string testtype { get; set; }
        public DateTime? CDate { get; set; }
        public int? StudentId { get; set; }

        public string CurrentCourseLevel { get; set; }
    }


    public class Receiptsummary
    {
        public string Paymode { get; set; }
        public double? PayAmnt { get; set; }
        public string Remarks { get; set; }
    }

    public class ReceiptsumaryCon
    {
        public int CounselorId { get; set; }
        public string Paymode { get; set; }
        public double? PayAmnt { get; set; }
    }
    public class ReceiptsumaryConReport
    {
        public string Counselor { get; set; }
        public string Paymode { get; set; }
        public double? PayAmnt { get; set; }
    }



    public class EnquiryAnalysisHelper
    {
        public int? Enqno { get; set; }
        public string Name { get; set; }
        public string Enqabout { get; set; }
        public string Mobile { get; set; }
        public DateTime? enqdate { get; set; }
        public string enqstatus { get; set; }
        public string enqhandleby { get; set; }
        public string remarks { get; set; }
        public string status { get; set; }
        public string Enqstatus { get; set; }
        public string area { get; set; }
        public string source { get; set; }
        public string othrsource { get; set; }
        public string altNo { get; set; }
        public string WalkinType { get; set; }
        public string PinCode { get; set; }
        public string Address { get; set; }
        public string EnqType { get; set; }
        public string VisaType { get; set; }
        public DateTime? Folldate { get; set; }
        public string CloseReason { get; set; }

    }

    public class MockTestHelper
    {
        public int? Roll { get; set; }
        public string Name { get; set; }
      
        public string Mobile { get; set; }
        public DateTime? MockDate { get; set; }
        public DateTime? NextFDate { get; set; }
        public string Cat { get; set; }
        public double? Overall { get; set; }
        public string Courselvl { get; set; }
        public string CourseTime { get; set; }
        public string FRemarks { get; set; }
        public string Enqstatus { get; set; }
        public DateTime? TestDate { get; set; }
        public string IeltsTestDate { get; set; }
        public string FirstFollowupby { get; set; }
        public string IsLeft { get; set; }



    }

    public class WithoutFeeHelper
    {
        public int? Roll { get; set; }
        public string Name { get; set; }

        public int? EnquiryNum { get; set; }
       
        public string Mobile { get; set; }
      
        public string AltNum { get; set; }
        public string Enqabout { get; set; }
        public string Remarks { get; set; }
        public DateTime? Enqdate { get; set; }
        public DateTime? Cdate { get; set; }
        public DateTime? LDate { get; set; }
        public string Counsellor { get; set; }


    }

    public class AdmWithoutFeeHelper
    {
        public int? Id { get; set; }
        public int? BId { get; set; }
        public int? Roll { get; set; }
        public string Name { get; set; }
        public int? EnquiryNum { get; set; }
        public string Mobile { get; set; }
        public int? EnqEnteredby { get; set; }
        public string AltNum { get; set; }
        public string Enqabout { get; set; }
        public string Remarks { get; set; }
        public DateTime? Enqdate { get; set; }
        public DateTime? Cdate { get; set; }
        public DateTime? LDate { get; set; }
        public string Counsellor { get; set; }
    }

    public class TransferStudentHelper
    {
        public int? OldRoll { get; set; }
        public int? NewRoll { get; set; }
        public string Name { get; set; }
        public string Package { get; set; }
        public string Course { get; set; }
        public string BranchTo { get; set; }
        public string BranchFrom { get; set; }
        public string TransferTo { get; set; }
        public string TransferBy { get; set; }
        public DateTime? TDate { get; set; }
        public string ApStatus { get; set; }
        public string KitProvided { get; set; }
        public DateTime? AcptDate { get; set; }

    }

    public class doubleEntryEnq
    {
        public int? Enqno { get; set; }
        public string BrName { get; set; }
        public string Name { get; set; }
        public string FName { get; set; }
        public string UserName { get; set; }
        public string Mobile { get; set; }
        public string AltMobile { get; set; }
        public string Enqabout { get; set; }
        public DateTime? enqdate { get; set; }
        public string enqstatus { get; set; }
        public string enqhandleby { get; set; }
    }

    public class PendingEnqSummary
    {
        public int? Cnt { get; set; }
        public string EnqType { get; set; }
    }


    public class IELTReportSHelper
    {
        public string CurntCoursTmng { get; set; }
        public int? ReferenceNo { get; set; }
        public string Name { get; set; }
        public string Passport { get; set; }
        public string ExamBdy { get; set; }
        public string Category { get; set; }
        public string BookingType { get; set; }
        public string MobileNo { get; set; }
        public string AlternteNo { get; set; }
        public double? Listening { get; set; }
        public double? Reading { get; set; }
        public double? Writing { get; set; }
        public double? Speaking { get; set; }
        public double? Overall { get; set; }
        public string Remarks { get; set; }
        public DateTime? testdate { get; set; }
        public DateTime? ExamBooked { get; set; }
        public DateTime? ResUpdt { get; set; }
        public string currenrcourselvl { get; set; }
        public string IsLeft { get; set; }
        public string Remarks2 { get; set; }
        public string Institute { get; set; }
        public string Branch { get; set; }
        public string BranchOf { get; set; }
        public string detail1 { get; set; }
        public string detail2 { get; set; }
        public string Booked { get; set; }
        public DateTime? MtDate { get; set; }
        public double? MTListening { get; set; }
        public double? MTReading { get; set; }
        public double? MTWriting { get; set; }
        public double? MTSpeaking { get; set; }
        public double? MTOverall { get; set; }
        public string MTCourseLevel { get; set; }
        public string Followupby { get; set; }
    }

}