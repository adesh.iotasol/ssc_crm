﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class TimeSlotSummaryBean
    {
        public int Slot8to10 { get; set; }
        public int Slot10to12 { get; set; }
        public int Slot12to3 { get; set; }
        public int Slot3to7 { get; set; }
        public DateTime? fdate { get; set; }
        public DateTime? tdate { get; set; }

    }


}