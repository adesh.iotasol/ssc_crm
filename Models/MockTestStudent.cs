﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class MockTestStudent
    {
        public string fullname { get; set; }
        public Nullable<int> RollNo { get; set; }
        public int Id { get; set; }
        public Nullable<int> BranchId { get; set; }
   
        public string EnqAbt { get; set; }
        public Nullable<System.DateTime> Cdate { get; set; }
        public Nullable<bool> EnquiryIELAC { get; set; }
        public Nullable<bool> EnquiryIELGT { get; set; }
        public Nullable<bool> EnquirySpkEng { get; set; }
        public Nullable<bool> EnquiryStyVisa { get; set; }
        public Nullable<bool> EnquiryPTE { get; set; }
    }

}