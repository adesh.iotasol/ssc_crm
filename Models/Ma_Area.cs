﻿namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Ma_Area
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Area is required")]
        public string Area { get; set; }
        public Nullable<System.DateTime> Cdate { get; set; }
        private Nullable<bool> _Status;
        public Nullable<int> CityId { get; set; }

        public bool Status
        {
            get
            {
                if (_Status == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            set
            {
                _Status = value;
            }

        }

        public virtual MaCity MaCity { get; set; }

    }
}
