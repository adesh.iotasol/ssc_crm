﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class FeeReportByDatesBean
    {
        public Nullable<DateTime> FeeDate { get; set; }
        public Nullable<int> ReceiptNo { get; set; }
        public Nullable< int> RollNo { get; set; }
        public string FullName { get; set; }
        public string GroupName { get; set; }
        public string GroupChoice { get; set; }
        public string PackageName { get; set; }
        public Nullable<DateTime> FromDate { get; set; }
        public Nullable<double> PaidAmount { get; set; }
        public Nullable<double> BalanceAmount { get; set; }
        public string Remarks { get; set; }
        public string Counselor { get; set; }
        public string DiscountPercent { get; set; }
        public Nullable<int> LocalSerialNo { get; set; }
        public string Pay1 { get; set; }
        public string Pay2 { get; set; }
        public double? CashAmt { get; set; }
        public double? PMAmt1 { get; set; }
        public double? PMAmt2 { get; set; }
        public string Remarks1 { get; set; }
        public string Remarks2 { get; set; }
        public string Remarks3 { get; set; }
        public string TransferFrom { get; set; }
        public string TransferTo { get; set; }
        public string KitProvided { get; set; }
        public string Address { get; set; }
        public string Enqhandleby { get; set; }
        public string BranchCode { get; set; }
        public int? Fsrno { get; set; }
    }
}