﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;

    public partial class Vw_EnquiryFollow
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public Nullable<System.DateTime> nxdt { get; set; }
        public Nullable<int> FEnqFollowBy { get; set; }
        public string enqstatus { get; set; }
        public Nullable<bool> IsAdmission { get; set; }
        public Nullable<int> EnqHandledBy { get; set; }
        public Nullable<int> BranchId { get; set; }
        public string AlternateNo { get; set; }
        public string FirstName { get; set; }
        public Nullable<System.DateTime> Expr1 { get; set; }
        public string LastName { get; set; }
        public string FRemarks { get; set; }
        public string EnqAbt { get; set; }
        public string Mobile { get; set; }
        public string Grouptime { get; set; }
        public string GroupChoice { get; set; }
        public Nullable<int> TransferId { get; set; }
        public string EnqType { get; set; }
        public string VisaType { get; set; }
    }
}

