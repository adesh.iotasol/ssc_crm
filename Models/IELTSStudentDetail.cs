﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class IELTSStudentDetail
    {
        public string studentname { get; set; }

        public string category { get; set; }
        public string course { get; set; }
        public double? listening { get; set; }
        public double? reading { get; set; }
        public double? overall { get; set; }
        public string enterby { get; set; }
        public DateTime? testdate { get; set; }
        public string timing { get; set; }
        public double? writing { get; set; }
        public double? speaking { get; set; }
        public DateTime? date { get; set; }
        public string remarks { get; set; }
    }
}