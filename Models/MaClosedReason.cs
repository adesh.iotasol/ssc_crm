

namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    public partial class MaClosedReason
    {
        public int Id { get; set; }

          [StringLength(250)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        
        private Nullable<bool> _status;
        [Display(Name = "Status")]
        public bool status
        {
            get
            {
                if (_status == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                _status = value;
            }
        }

        public Nullable<System.DateTime> cdate { get; set; }


    }
}

