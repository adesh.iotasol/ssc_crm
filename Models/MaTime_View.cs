﻿namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;

    public partial class MaTime_View
    {
        public int Id { get; set; }
        public string BatchTime { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<System.DateTime> Cdate { get; set; }
        public string tcode { get; set; }
    }
}
