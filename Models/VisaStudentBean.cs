﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class VisaStudentBean
    {
        public int StudentId { get; set; }
        public int Visaid { get; set; }
        public Nullable<int> MockIELTSid { get; set; }
        public string EntryType { get; set; }
        public string type { get; set; }
        public string TestType { get; set; }
        public string SName { get; set; }
        public string Mobile { get; set; }
        public string Counselor { get; set; }
        public Nullable<int> Cid { get; set; }
        public double? EnqryOvlscr { get; set; }
        public string followtype { get; set; }
        public int? rollno { get; set; }

        public int? admissionid { get; set; }
        public int? enquiryid { get; set; }

        public int? branchid { get; set; }

        public DateTime? followupdate { get; set; }
        public string remarks { get; set; }
        public string status { get; set; }
        public string lastupdated { get; set; }

    }


    public class PendingVisaStudentBean
    {
        public int? referid { get; set; }
        public int Visaid { get; set; }
        public Nullable<int> rollno { get; set; }
        public string EntryType { get; set; }
        public string type { get; set; }
        public string SName { get; set; }
        public string Mobile { get; set; }
        public string Counselor { get; set; }
        public string followtype { get; set; }




        public DateTime? followupdate { get; set; }
        public string remarks { get; set; }
        public string status { get; set; }
        public string lastupdated { get; set; }

    }


}