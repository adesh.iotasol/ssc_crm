﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SSCMvc.Models
{
    public partial class Tb_MockTest
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Student")]
        public Nullable<int> StudentId { get; set; }

        [Required(ErrorMessage = "*")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> TestDate { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Category")]
        public string Category { get; set; }



        [Required(ErrorMessage = "*")]
        [Display(Name = "Course")]
        public string CourseId { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Timing")]
        public string TimeId { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Listening")]
        public Nullable<Double> LBand { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Writing")]
        public Nullable<Double> WBand { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Reading")]
        public Nullable<Double> RBand { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Speaking")]
        public Nullable<Double> SBand { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Overall")]
        public Nullable<Double> Overall { get; set; }


        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> CDate { get; set; }

        [Display(Name = "Branch")]
        public Nullable<int> BranchId { get; set; }


        public Nullable<int> UserId { get; set; }

        private Nullable<bool> _Status;

        public bool Status
        {

            get
            {
                if (_Status == true)
                {
                    return true;
                }
                return false;

            }
            set
            {
                _Status = value;
            }

        }
        [Display(Name = "Remarks")]
        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [Display(Name = "FollowUp Assign To")]
        public Nullable<int> FollowAsgnTo { get; set; }

        private Nullable<bool> _isfollowbutton;

        public bool isfollowbtn
        {

            get
            {
                if (_isfollowbutton == true)
                {
                    return true;
                }
                return false;

            }
            set
            {
                _isfollowbutton = value;
            }

        }

        public Nullable<int> followupby { get; set; }
        public Nullable<System.DateTime> fdate { get; set; }
        public Nullable<System.DateTime> nextdate { get; set; }
        public string fremarks { get; set; }
        public string fstatus { get; set; }

        public string Courselevel { get; set; }


        public virtual MaBranch MaBranch { get; set; }
        public virtual User User { get; set; }

    }
}