﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class CoursHisBean
    {
        public string CurrentcrsLvl { get; set; }
        public string Course { get; set; }
        public string Group { get; set; }
        public string PackageName { get; set; }
        public string Rollno { get; set; }
        public string Name { get; set; }
        public string Time { get; set; }
        public DateTime? Fromdate { get; set; }
        public double? Amount { get; set; }
        public DateTime Duedate { get; set; }
        public string username { get; set; }
        public double? packageamount { get; set; }
    }
}