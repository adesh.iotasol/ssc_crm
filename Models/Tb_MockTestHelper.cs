﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SSCMvc.Models;
using SSCMvc.Webutil;
namespace SSCMvc.Models
{
    public class Tb_MockTestHelper
    {
        public int Id { get; set; }

        public int? StudentId { get; set; }
        public int Rollno { get; set; }
        public string Name { get; set; }
        public string Phoneno { get; set; }
        public string Categ { get; set; }

        public DateTime? Mocktestdate { get; set; }

        public double? Overallscore { get; set; }
        public DateTime NextfollwDate { get; set; }

        public string FollowRemarks { get; set; }

        public string Currentcrselvl { get; set; }
        public string EnqStatus { get; set; }
        public string IeltsTestDate { get; set; }

        public int? FollowAsgnTo { get; set; }

        public int? Branchid { get; set; }
        public DateTime TestDate { get; set; }

        public string category { get; set; }

        public string CourseId { get; set; }

      
        public DateTime Cdate { get; set; }

        public Nullable<int> UserId { get; set; }



        public string TimeId { get; set; }


        public Nullable<Double> LBand { get; set; }


        public Nullable<Double> WBand { get; set; }


        public Nullable<Double> RBand { get; set; }

        public Nullable<Double> SBand { get; set; }


        public Nullable<Double> Overall { get; set; }
        public int? Followupby { get; set; }
        public string IsLeft { get; set; }

        public Nullable<bool> isfollowbtn;

    }
}