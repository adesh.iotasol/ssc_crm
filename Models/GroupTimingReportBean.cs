﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class GroupTimingReportBean
    {
        public Nullable<int> RollNo { get; set; }
        public string FullName { get; set; }
        public string TimeCode { get; set; }
        public string Email { get; set; }
        public string GroupChoice { get; set; }
        public string PackageName { get; set; }
        public Nullable<DateTime> FromDate { get; set; } //Date Of Joining
        public Nullable<int> MonthInInstitute { get; set; }
        public string Mobile { get; set; }
        public string BatchTime { get; set; }
        public Nullable<DateTime> NextDueDate { get; set; }
        public Nullable<DateTime> LDate { get; set; }
    }
}