﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class StudLeaveBean
    {
        public string Rollno { get; set; }
        public string Name { get; set; }
        public string Time { get; set; }
        public DateTime? Fromdate { get; set; }
        public DateTime? Todate { get; set; }
        public DateTime? NextDuedate { get; set; }
        public string CourseName { get; set; }
        public string username { get; set; }
        public Nullable<DateTime> enterdate { get; set; }
        public string Remarks { get; set; }
    }
}