﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class ReferenceReportBean
    {
        public int Id { get; set; }
        public Nullable< int> RollNo { get; set; }
        public string SName { get; set; }
        public string FatherName { get; set; }
        public string Mobile { get; set; }
        public Nullable<DateTime> DOJ { get; set; }
        public string CourseName { get; set; }
        public string PackageName { get; set; }
        public string ReferenceName { get; set; }
        public Nullable<DateTime> fromdate { get; set; }
        public Nullable<DateTime> todate { get; set; }
    }
}