﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SSCMvc.Models;

namespace SSCMvc.Models
{


    public class PhotoUploadHelper
    {
        public string rollno { get; set; }
        public string Name { get; set; }
        public string GroupChoice { get; set; }
        public string GroupTime { get; set; }
    }

    public class LeftStudentHelper
    {
        public int? RollNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JoinDate { get; set; }
        public string DueDate { get; set; }
        public string LeftDate { get; set; }
        public string Gender { get; set; }
        public string CounselorName { get; set; }
        public string Edate { get; set; }
    }

    public class ProsStudentHelper 
    {
        SSCWebDBEntities1 db = new SSCWebDBEntities1();

        public int Id { get; set; }
        public int CourseId { get; set; }
        public int EnquiryId { get; set; }
        public int RollNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FatherName { get; set; }
        public string Mobile { get; set; }
        public string AlernateNo { get; set; }
        public DateTime Cdate { get; set; }
        public string StudentPhoto { get; set; }
        public string CourseName {
            get {
                return db.MaCourses.Where(a => a.Id == CourseId).Select(a => a.CourseName).FirstOrDefault();
            }
        }
    }


    public class CoursePackageHelper
    {
        SSCWebDBEntities1 db = new SSCWebDBEntities1();

        public int PackageID { get; set; }
        public int CourseId { get; set; }
        public int Total { get; set; }
        public string PackageName
        {
            get
            {
                return db.MaPackages.Where(a => a.Id == PackageID).Select(a => a.PackageName).FirstOrDefault();
            }
        }
        public string CourseName
        {
            get
            {
                return db.MaCourses.Where(a => a.Id == CourseId).Select(a => a.CourseName).FirstOrDefault();
            }
        }

    }
}