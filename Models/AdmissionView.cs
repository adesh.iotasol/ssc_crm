﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class AdmissionView
    {
        public string fullname { get; set; }
        public Nullable<int> RollNo { get; set; }
        public int Id { get; set; }
        public Nullable<int> BranchId { get; set; }
        public string EnqAbt { get; set; }
    }
}