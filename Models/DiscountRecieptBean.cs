﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class DiscountRecieptBean
    {
        public int RollNo { get; set; }
        public string PackageName { get; set; }
        public double PackageAmount { get; set; }
        public string DiscountAmount { get; set; }
        public string remarks { get; set; }
        public string studentname { get; set; }
    }
}