//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Tb_StudLeave
    {
        public int Id { get; set; }
        public Nullable<int> AdmissionId { get; set; }

        [Display(Name = "User")]
        public Nullable<int> UserId { get; set; }

        [Required(ErrorMessage = "*")]
        [DataType(DataType.Date)]
        [Display(Name = "From Date")]
        public Nullable<System.DateTime> FromDate { get; set; }

        [Required(ErrorMessage = "*")]
        [DataType(DataType.Date)]
        [Display(Name = "To Date")]
        public Nullable<System.DateTime> ToDate { get; set; }

        private Nullable<bool> _status;
        public bool Status
        {
            get
            {
                if (_status == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                _status = value;
            }
        }

        public Nullable<bool> _AffectDate { get; set; }
        public bool AffectDate
        {
            get
            {
                if (_AffectDate == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                _AffectDate = value;
            }
        }

        [DataType(DataType.Date)]
        [Display(Name = "Enter Date")]
        public Nullable<System.DateTime> Cdate { get; set; }
        public Nullable<int> BranchId { get; set; }

        public string Remarks { get; set; }
        public virtual MaAdmission MaAdmission { get; set; }
        public virtual MaBranch MaBranch { get; set; }
    }
}
