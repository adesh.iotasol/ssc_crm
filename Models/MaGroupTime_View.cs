﻿

namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class MaGroupTime_View
    {
        public int Id { get; set; }

        [StringLength(7)]
        [Display(Name = "Batch Time")]
        [Required]
        public string BatchTime { get; set; }

        public Nullable<int> CourseId { get; set; }
        public string GroupName { get; set; }
        public Nullable<int> Timeid { get; set; }
        
        

        private Nullable<bool> _status;
        public bool Status
        {
            get
            {
                if (_status == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                _status = value;
            }
        }


        [Display(Name = "Package Hours")]
        public string PackageHours { get; set; }

    }
}