﻿namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    public class MaBugReport
    {
        public int Id { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> UserId { get; set; }
        [Display(Name = "Title")]
        [StringLength(150)]
        [Required(ErrorMessage = "Required")]
        public string Title { get; set; }
        [Display(Name="Message")]
        [StringLength(500)]
        [Required(ErrorMessage="Required")]
        [DataType(DataType.MultilineText)]
        
        public string BugMessage { get; set; }
        [Display(Name="Date")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> Cdate { get; set; }
        private Nullable<bool> _status;
        public bool Status
        {
            get
            {
                if (_status == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                _status = value;
            }
        }
        public string ReplyStatus { get; set; }
        [DataType(DataType.Date)]
        public Nullable<DateTime> LastUpdated { get; set; }

        [Required(ErrorMessage="Required")]
        [Display(Name = "Priority Level")]
        public string PriorityType { get; set; }
        public Nullable<int> SrNo { get; set; }
        public string Screenshot { get; set; }



        public Nullable<int> CountBug { get; set; }

        public virtual MaBranch MaBranch { get; set; }
        public virtual User User { get; set; }
    }
}