﻿
namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    public class MaGroupAssign
    {
        public MaGroupAssign()
        {
           
        }
        public int Id { get; set; }
        public Nullable<int> CourseId { get; set; }
        public string GroupName { get; set; }
        
        
        [Required(ErrorMessage = "*")]
        public Nullable<int> Timeid { get; set; }

        //[Display(Name = "Time Out ")]
        //[Required(ErrorMessage = "*")]
        //[StringLength(7)]
        //public string TimeOut { get; set; }

        //[Display(Name = "In Description ")]
        //[StringLength(7)]
        //public string Indesc { get; set; }

        //[Display(Name = "Out Description ")]
        //[StringLength(7)]
        //public string OutDesc { get; set; }

        private Nullable<bool> _status;
        public bool Status
        {
            get
            {
                if (_status == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                _status = value;
            }
        }
        //public string TCode { get; set; }

        public virtual MaCourse MaCourse { get; set; }
       
    }
}