﻿namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public partial class Tb_DemoHistory
    {
        SSCWebDBEntities1 db = new SSCWebDBEntities1();

        public int Id { get; set; }
        public Nullable<double> DemoFeeAmount { get; set; }
        public Nullable<System.DateTime> FeeDate { get; set; }
        public Nullable<System.DateTime> ClassDate { get; set; }
        public string GroupChoice { get; set; }
        public string Grouptime { get; set; }
        public Nullable<int> Demobranchid { get; set; }
        
        public string branch
        {
            get
            {
                return db.MaBranches.Where(a => a.Id == Demobranchid).Select(a => a.BramchName).FirstOrDefault();
            }
        }

        public string DemoBy { get; set; }
        public Nullable<int> EnquiryId { get; set; }
    }
}
