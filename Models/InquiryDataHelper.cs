﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class InquiryDataHelper
    {
        public string CounselorName { get; set; }
        public int enquirieshandled { get; set; }
        public int enquiriesconverted { get; set; }
        public double percenconversion { get; set; }
    }
}