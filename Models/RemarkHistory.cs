﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class RemarkHistory
    {

        //this is used only for remark history 
        //public string remarks { get; set; }
        //public string date { get; set; }
        //public string remrakstype { get; set; }

        public string enqtype { get; set; }

        //public string stutype { get; set; }

        public int? id { get; set; }

        public int? mockid { get; set; }

        public int? visaid { get; set; }

        public string mocktype { get; set; }

        public int? ieltsid { get; set; }
        public string ielttype { get; set; }

        public string name { get; set; }

        public int? rollno { get; set; }
    }
}