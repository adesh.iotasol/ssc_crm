﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace SSCMvc.Models
{
    public class FollowUpEnqModel
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public int Id { get; set; }
        public int BookingType { get; set; }
        public Nullable<int> EnqHandledBy { get; set; }
        public Nullable<int> TransferId { get; set; }

        public string Lastupdatedby { get; set; }

        [DataType(DataType.Date)]
        public Nullable<DateTime> Cdate { get; set; }



        [DataType(DataType.Date)]
        public Nullable<DateTime> nxdt { get; set; }

        public string StudentType { get; set; }

        public string followuptype { get; set; }

        public int? Rollno { get; set; }

        public string Status { get; set; }

        public string Fullname { get; set; }
        public string Enqstatus { get; set; }
        public string Remarks { get; set; }
        public string GroupChoice { get; set; }
        public string GroupTime { get; set; }
        public string enqab { get; set; }
        public DateTime? nextdate { get; set; }
        public DateTime? CreateDate { get; set; }

        public DateTime? WalkInDate { get; set; }

        public int AdmissionId { get; set; }

        public string currentcourselevel { get; set; }

        public DateTime? ieltstestdate { get; set; }

        public DateTime? mocktestdate { get; set; }

        public string overallscore { get; set; }
        public double overall { get; set; }

        public string AlternateMobile { get; set; }
        public string Counsellor { get; set; }
        public string EnqType { get; set; }
        public string VisaType { get; set; }
        public string IsLeft { get; set; }
        public DateTime? RemDate { get; set; }
        public string TimeId { get; set; }
        public int EnqId { get; set; }
        public int StuId{get;set;}

    }
}