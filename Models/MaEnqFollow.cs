﻿namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    public class MaEnqFollow
    {
        public int Id { get; set; }
        public Nullable<int> ReferId { get; set; }

        [Required(ErrorMessage = "*")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Remarks *")]
        public string Remarks { get; set; }

        [Display(Name = "Next Date *")]
        [Required(ErrorMessage = "*")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> NextDate { get; set; }

        public string FollowUpType { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        public Nullable<DateTime> CDate { get; set; }

        public Nullable<int> EnqFollowBy { get; set; }


        [Required(ErrorMessage = "*")]
        public string Enqstatus { get; set; }
        public string Entertype { get; set; }
        public Nullable<int> AdmissionId { get; set; }
        public string CloseReason { get; set; }


        public virtual User User { get; set; }
    }
}