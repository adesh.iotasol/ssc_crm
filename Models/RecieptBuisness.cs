﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SScMvc.Models;
using SScMvc;
using SSCMvc.Webutil;
using System.Data;
using Microsoft.Reporting.WebForms;
using System.Data.Entity;
using System.Web.Mvc;


namespace SSCMvc.Models
{

    public class RecieptBuisness
    {
        Status st = new Status();
        SSCWebDBEntities1 db = new SSCWebDBEntities1();

        #region Fee Receipt
        public List<FeeRecieptBean> StubForFeeRecieptDataSet()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetFeeRecieptView(int id)
        {
            // I will not go through getting data from ef, i will assume some test data here

            Tb_FeeReceipt obj = db.Tb_FeeReceipt.Where(m => m.Id == id).First();

            var feereciptdatset = new List<object>()
           {
               new FeeRecieptBean(){studentname = obj.MaAdmission.FirstName +" "+ obj.MaAdmission.LastName,
                   RollNo= (int)obj.MaAdmission.RollNo,
                   CourseName=obj.MaCourse.CourseName,
                   Paidamount= (double)obj.PaidAmount,
                   GroupChoice= obj.Tb_Installment.Tb_CourseHist.MaTime.TCode + " " + obj.Tb_Installment.Tb_CourseHist.MaTime.TimeIn + " " + obj.Tb_Installment.Tb_CourseHist.MaTime.Indesc + " "+ obj.Tb_Installment.Tb_CourseHist.MaTime.TimeOut + " "+ obj.Tb_Installment.Tb_CourseHist.MaTime.OutDesc ,
                   PackageName = obj.MaPackage.PackageName,
                   remarks= obj.Remarks,
                   Discount= (double)obj.Discount,
                   ReceiptNo=(int)obj.LocSerialNo,
                   BalAmount= (double)obj.BalanceAmount,
                   Kitprovided=obj.KitProvided,
                   Feedate = Convert.ToDateTime(obj.FeeDate),
                   FeeTime = obj.RTime,
                   GroupName=obj.GroupName,
                   Pay1 = obj.PayMode,
                   Pay2 = obj.PayMode1,
                   CashAmt = obj.CashAmt,
                   PMAmt1 = obj.PayAmt,
                   PMAmt2 = obj.PayAmt1,
                   Remarks1 = obj.Remarks1,
                   Remarks2 = obj.Remarks2,
                   Remarks3 = obj.Remarks3,
                   InstallmentNo = obj.Tb_Installment.InstallmentNo
               }
           };

            int admid = (int)db.Tb_FeeReceipt.Where(z => z.Id == id).Select(z => z.StudentId).First();
            int courseid = (int)db.Tb_FeeReceipt.Where(z => z.Id == id).Select(z => z.CourseId).First();
            int pkgid = (int)db.Tb_FeeReceipt.Where(z => z.Id == id).Select(z => z.PackageId).First();

            DateTime? dt = db.Tb_FeeReceipt.Where(z => z.Id == id).Select(z => z.DueDate).First();
            // DateTime? dt = db.Tb_Installment.Where(z => z.Id == obj.InstallmentNoId).Select(z => z.DueDate).First();

            //DateTime? dtjoin = db.Tb_CourseHist.Where(z => z.AdmissionId == admid && z.CourseId == courseid && z.PackageId == pkgid && z.IsActive == true).Select(z => z.FromDate).First();

            int counselorid = (int)db.Tb_FeeReceipt.Where(z => z.Id == id).Select(z => z.CounselorId).First();
            string cname = db.Users.Where(z => z.Id == counselorid).Select(z => z.Fullname).First();



            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";
            DateTime dojpackage = (DateTime)obj.PackageDate;
            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/FRecieptReport.rdlc",
                LeftMainTitle = this.CampusName(obj.MaAdmission.BranchId),
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dd MMM,yyyy HH:mm tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference)),
                Name = "Statistical Report",
                ReportDate = (DateTime)dt,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Fee Reciept",
                ReportLanguage = "en-US",
                UserNamPrinting = cname,
                ReportDoj = dojpackage,
                Format = ReportViewModel.ReportFormat.PDF,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = feereciptdatset.ToList(), DatasetName = "DataSet1" });

            return reportViewModel;

        }




        public string CampusName(int? id)
        {
            return db.MaBranches.Where(a => a.Id == id).Select(a => a.BramchName).FirstOrDefault();
        }
        #endregion

        public List<Receiptsummary> StubForReceiptSummary()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }
        public List<ReceiptsumaryConReport> StubForSummaryCounselorwise()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        #region Report_For_Daily_Fee_Receipt
        public List<FeeReportByDatesBean> StubForDailyFeeReceiptDataSet()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }
        public ReportViewModel GetDailyFeeReceiptView(string dt1 = "", string dt2 = "", string format = "")

        {
            // I will not go through getting data from ef, i will assume some test data here

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            int usrid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);

            string usrname = db.Users.Where(z => z.Id == usrid).Select(z => z.username).First();
            DateTime fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            DateTime tdate = DateTime.Now;
            if (dt1 != "")
            {
                try
                {
                    fdate = Convert.ToDateTime(dt1);

                }
                catch (Exception ex)
                {
                    fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                }
            }

            if (dt2 != "")
            {
                try
                {
                    tdate = Convert.ToDateTime(dt2);
                }
                catch (Exception ex)
                {
                    tdate = DateTime.Now;
                }
            }
            List<MaEnquiry> _lstenqry = db.MaEnquiries.Where(z => (z.ClassDate >= fdate && z.ClassDate <= tdate) && z.DemoFeeAmt > 0 && z.Demobranchid == branchid).ToList();
            //List<MaEnquiry> _lstenqry = db.MaEnquiries.Where(z => z.DemoFeeDate >= fdate && z.Cdate <= tdate && z.DemoFeeAmt > 0 && z.Demobranchid == branchid).ToList();
            double? demofee = 0;
            int count = 0;
            foreach (MaEnquiry _objenq in _lstenqry)
            {
                demofee += _objenq.DemoFeeAmt;
                count = _lstenqry.Count();
            }

            List<Tb_FeeReceipt> lst = db.Tb_FeeReceipt.Where(m => (m.FeeDate >= fdate && m.FeeDate <= tdate) && m.BranchId == branchid && m.IsTransfer == null).ToList();
            //if (lst.Count == 0)
            //{
            //    demofee = 0;
            //}

            /*------------------------------------------*/
            /* Get data of those students which are transfered from one branch to another branch*/
            List<Tb_StudentTransfer> lststudent = db.Tb_StudentTransfer.Where(z => z.BranchFrom == branchid).ToList();

            List<Tb_FeeReceipt> lsttransfee = new List<Tb_FeeReceipt>();
            foreach (Tb_StudentTransfer objtrans in lststudent)
            {
                List<Tb_FeeReceipt> lstinner = db.Tb_FeeReceipt.Where(m => m.FeeDate >= fdate && m.FeeDate <= tdate && m.StudentId == objtrans.StudentId && m.IsTransfer == true).ToList();
                lsttransfee.AddRange(lstinner);
            }

            lst.AddRange(lsttransfee);
            /*   */
            var feereciptdatset = new List<object>();

            foreach (Tb_FeeReceipt obj in lst)
            {
                string percent = "No";
                string TransferTo = "";
                string TransferFrom = "";
                if (obj.Discount != null)
                {
                    percent = obj.Discount.ToString();
                }
                if (obj.DiscountId != null)
                {
                    percent = db.MaDiscounts.Where(z => z.Id == obj.DiscountId).Select(z => z.DiscountPercen).First().ToString();
                    percent = percent + "%";
                }
                Tb_StudentTransfer tb_st = db.Tb_StudentTransfer.Where(x => x.StudentId == obj.StudentId || x.OldStudId == obj.StudentId).FirstOrDefault();
                if (tb_st != null)
                {
                    TransferTo = tb_st.MaBranch.BramchName;
                    TransferFrom = tb_st.MaBranch1.BramchName;
                }
                int? enqhand = 0;
                try
                {
                    enqhand = obj.MaAdmission.MaEnquiry.EnqHandledBy;
                }
                catch (Exception)
                {

                    throw;
                }
                string EnqHandleName = db.Users.Where(x => x.Id == enqhand).Select(x => x.Fullname).FirstOrDefault();
                feereciptdatset.Add(new FeeReportByDatesBean()
                {

                    FullName = obj.MaAdmission.FirstName + " " + obj.MaAdmission.LastName,
                    RollNo = (int)obj.MaAdmission.RollNo,
                    GroupChoice = obj.GroupName,
                    PaidAmount = (double)obj.PaidAmount,
                    GroupName = obj.Tb_Installment == null ? "" : obj.Tb_Installment.Tb_CourseHist.MaTime.TCode + " " + obj.Tb_Installment.Tb_CourseHist.MaTime.TimeIn + " " + obj.Tb_Installment.Tb_CourseHist.MaTime.Indesc + " " + obj.Tb_Installment.Tb_CourseHist.MaTime.TimeOut + " " + obj.Tb_Installment.Tb_CourseHist.MaTime.OutDesc,
                    PackageName = obj.MaPackage.PackageName,
                    FromDate = obj.Tb_Installment == null ? null : obj.Tb_Installment.Tb_CourseHist.FromDate,
                    BalanceAmount = obj.BalanceAmount,
                    Remarks = obj.Remarks,
                    Address = obj.MaAdmission.Address,
                    Counselor = obj.User.Fullname,
                    ReceiptNo = (int)obj.ReceiptNo,
                    FeeDate = obj.FeeDate,
                    DiscountPercent = percent,
                    LocalSerialNo = obj.LocSerialNo,
                    KitProvided = obj.KitProvided,
                    Pay1 = obj.PayMode,
                    Pay2 = obj.PayMode1,
                    CashAmt = obj.CashAmt,
                    PMAmt1 = obj.PayAmt,
                    PMAmt2 = obj.PayAmt1,
                    Remarks1 = obj.Remarks1 == null ? obj.CashAmt + "" : obj.CashAmt + "-" + obj.Remarks1,
                    Remarks2 = obj.Remarks2 == null ? obj.PayAmt + "-" + obj.PayMode : obj.PayAmt + "-" + obj.PayMode + "-" + obj.Remarks2,
                    Remarks3 = obj.Remarks3 == null ? obj.PayAmt1 + "-" + obj.PayMode1 : obj.PayAmt1 + "-" + obj.PayMode1 + "-" + obj.Remarks3,
                    TransferFrom = TransferFrom,
                    TransferTo = TransferTo,
                    Enqhandleby = EnqHandleName,
                    BranchCode = obj.MaBranch.Code,
                    Fsrno = obj.Fsrno

                });
            }

            List<Receiptsummary> Rec = st.GetSummaryDetail(dt1, dt2);
            var ReceiptSumma = new List<object>();
            foreach (Receiptsummary item in Rec)
            {
                ReceiptSumma.Add(new Receiptsummary()
                {
                    PayAmnt = item.PayAmnt,
                    Paymode = item.Paymode

                });
            }
            List<ReceiptsumaryCon> Rec1 = st.GetSummaryCounselorwise(dt1, dt2);
            var ReceiptConwise = new List<object>();
            foreach (ReceiptsumaryCon item in Rec1)
            {
                string con = "";
                try
                {
                    con = db.Users.Where(x => x.Id == item.CounselorId).Select(x => x.Fullname).FirstOrDefault();
                }
                catch (Exception)
                {
                }
                ReceiptConwise.Add(new ReceiptsumaryConReport()
                {
                    Counselor = con,
                    PayAmnt = item.PayAmnt,
                    Paymode = item.Paymode
                });
            }


            //Assuming the person printing the report is me

            string FileN = "";
            if (format == "PDF")
            {
                FileN = "~/Reports/FeeReportsByDates.rdlc";
            }
            else
            {
                FileN = "~/Reports/FeeReportsByDatesExcel.rdlc";
            }
            var reportViewModel = new ReportViewModel()
            {

                //FileName = "~/Reports/FeeReportsByDates.rdlc",
                FileName = FileN,
                LeftMainTitle = demofee.ToString(),
                LeftSubTitle = count.ToString(),
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dddd dd MMM,yyyy hh:mm tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference)),
                Name = "Statistical Report",
                ReportDate = fdate,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Fee Reciept",
                ReportLanguage = "en-US",
                UserNamPrinting = usrname,
                ReportDoj = tdate,
                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = feereciptdatset.ToList(), DatasetName = "DataSet1" });
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = ReceiptSumma.ToList(), DatasetName = "DataSet2" });
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = ReceiptConwise.ToList(), DatasetName = "DataSet3" });



            return reportViewModel;

        }



        #endregion



        #region Defaulter_List
        public List<DefaulterBean> StubForDefaulterList()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetDefaulterList(DateTime cdate)
        {
            // I will not go through getting data from ef, i will assume some test data here

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            // case added balance due >=0 because if fee is paid too  and duration gets over then he is also defaulter
            // and due date also taken from tb_coursehist
            DateTime now = cdate;

            //List<Tb_Installment> lst = db.Tb_Installment.Where(z => (z.BalanceDueAmount >= 0 || z.BalanceDueAmount == null) && z.BranchId == branchid && (z.Tb_CourseHist.DueDate.Value.Day <= now.Day && z.Tb_CourseHist.DueDate.Value.Month<=now.Month && z.Tb_CourseHist.DueDate.Value.Year<=now.Year) && z.Tb_CourseHist.MaAdmission.IsLeft == false && (z.Tb_CourseHist.MaPackage.PackageName != "Demo" || z.Tb_CourseHist.MaPackage.PackageDays != 1)).ToList();
            List<Tb_Installment> lst = db.Tb_Installment.Where(z => (z.BalanceDueAmount == null ? 0 : (z.BalanceDueAmount)) >= 0 && z.BranchId == branchid && z.Tb_CourseHist.IsActive == true && (z.Tb_CourseHist.DueDate.Value.Day <= now.Day && z.Tb_CourseHist.DueDate.Value.Month <= now.Month && z.Tb_CourseHist.DueDate.Value.Year <= now.Year) && z.Tb_CourseHist.MaAdmission.IsLeft == false && (z.Tb_CourseHist.MaPackage.PackageName != "Demo" || z.Tb_CourseHist.MaPackage.PackageDays != 1)).ToList();
            //List<Tb_CourseHist> corse = db.Tb_CourseHist.Where(z => (z.Amout == null ? 0 : (z.Amout)) >= 0 && z.MaAdmission.BranchId == branchid && (z.DueDate.Value.Day <= now.Day && z.DueDate.Value.Month <= now.Month && z.DueDate.Value.Year <= now.Year) && (z.MaPackage.PackageName != "Demo" || z.MaPackage.PackageDays != 1)).Where(z => z.IsActive == true).Where(z => z.MaAdmission.IsLeft == false).ToList();
            List<Tb_CourseHist> corse = db.Tb_CourseHist.Where(z => z.MaAdmission.BranchId == branchid).Where(z => z.IsActive == true).Where(z => z.MaAdmission.IsLeft == false && (z.DueDate <= now)).OrderBy(z => z.MaAdmission.FirstName).ThenBy(z => z.currentcourselevel).ToList();
            //var lst2 = (from ta in lst
            //            select new
            //            {
            //                ta.Tb_CourseHist.MaAdmission.RollNo,
            //                ta.Tb_CourseHist.MaAdmission.FirstName,
            //                ta.Tb_CourseHist.FromDate,
            //                Groupchoice=ta.Tb_CourseHist.GroupName,
            //                GroupName=ta.Tb_CourseHist.MaTime.TCode+" a",
            //            }).Distinct();
            if (db.MaPackages.Where(z => z.PackageDays == 1 || z.PackageName == "Demo").Count() > 0)
            {
                // System.Threading.Thread.Sleep(100);
                List<int> lstpackid = db.MaPackages.Where(z => z.PackageDays == 1 || z.PackageName == "Demo").Select(z => z.Id).ToList();
                foreach (int i in lstpackid)
                {
                    // System.Threading.Thread.Sleep(100);
                    if (db.Tb_Installment.Where(z => z.Tb_CourseHist.PackageId == i).Count() > 0)
                    {
                        // added branchwise condition plus added is active condition because only those student will come in defaulter ehose course is active and is in any case he joins another course then his previous course would be manually  inactive
                        lst.AddRange(db.Tb_Installment.Where(z => z.Tb_CourseHist.PackageId == i).Where(m => m.Tb_CourseHist.IsActive == true).Where(m => m.Tb_CourseHist.MaAdmission.IsLeft != true).Where(m => m.BranchId == branchid).ToList());
                    }
                }
            }
            //lst = lst.OrderBy(m => m.Tb_CourseHist.MaTime.TCode).ThenBy(m => m.Tb_CourseHist.DueDate).Distinct().ToList();
            var defaulterlistdataset = new List<DefaulterBean>();
            var defaulterlistdataset2 = new List<object>();
            foreach (Tb_CourseHist obj in corse)
            {
                double? dueamt = 0;
                Tb_Installment tb_ins = new Tb_Installment();
                //DateTime? dtdue = obj.DueDate;
                DateTime? dtdue = obj.DueDate;
                // dtdue = IndianTime(dtdue);
                try
                {
                    tb_ins = db.Tb_Installment.Where(x => x.CourseHistId == obj.Id).OrderByDescending(x => x.Id).FirstOrDefault();
                    if (tb_ins.BalanceDueAmount == null)
                    {
                        dueamt = tb_ins.Amount;
                    }
                    else
                    {
                        dueamt = tb_ins.BalanceDueAmount;
                        //dtdue = db.Tb_FeeReceipt.Where(z => z.InstallmentNoId == obj.Id).Select(z => z.DueDate).First();
                        dtdue = obj.DueDate;
                    }
                }
                catch (Exception)
                {
                }
                // exam date
                DateTime? exdat = new DateTime();
                try
                {
                    //exdat = obj.Tb_CourseHist.MaAdmission.DateOfExam;
                    int? id = obj.MaAdmission.Id;
                    exdat = db.Tb_IELTSResult.Where(x => x.StudentId == id && x.TestDate != null).OrderByDescending(x => x.Id).Select(x => x.TestDate).FirstOrDefault();
                }
                catch (Exception ex)
                {
                }

                if (obj.DueDate <= now.Date) //this is done because if students pays partial payment today then he will go to pending list not defaulter list
                {
                    defaulterlistdataset.Add(new DefaulterBean()
                    {
                        FullName = obj.MaAdmission.FirstName + " " + obj.MaAdmission.LastName,
                        RollNo = (int)obj.MaAdmission.RollNo,
                        GroupChoice = obj.GroupName,
                        GroupName = obj.MaTime.TCode + " " + obj.MaTime.TimeIn + " " + obj.MaTime.Indesc + " " + obj.MaTime.TimeOut + " " + obj.MaTime.OutDesc,
                        PackageName = obj.MaPackage.PackageName,
                        FromDate = obj.FromDate,
                        Examdate = exdat,
                        DueAmount = dueamt,
                        NextDueOn = dtdue,
                        LocalDate = IndianTime(dtdue),
                        Mobile = obj.MaAdmission.Mobile,
                    });
                }
            }

            // this is for to distinct multiple columns and get in a list
            var lst2 = (from ta in defaulterlistdataset
                        select new
                        {
                            ta.RollNo,
                            ta.PackageName,
                            ta.NextDueOn,
                            ta.Mobile,
                            ta.GroupName,
                            ta.GroupChoice,
                            ta.FullName,
                            ta.FromDate,
                            ta.DueAmount,
                            ta.Examdate

                        }).Distinct();

            defaulterlistdataset2.AddRange(lst2);

            //Assuming the person printing the report is me
            //var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/DefaulterList.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "Dst",
                RightSubTitle = string.Format("{0:dd MMM,yyyy hh:mm tt}", CommonUtil.IndianTime()),
                Name = "Statistical Report",
                ReportDate = DateTime.Now,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Fee Reciept",
                ReportLanguage = "en-US",
                UserNamPrinting = "DST",
                ReportDoj = DateTime.Now,
                Format = ReportViewModel.ReportFormat.PDF,
                ViewAsAttachment = false,

            };


            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = defaulterlistdataset2.ToList(), DatasetName = "DataSet1" });



            return reportViewModel;

        }
        #endregion

        #region DOJ
        public List<DefaulterBean> StubForDOJList()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetDOJList(DateTime Fdate, DateTime Tdate, string format)
        {
            // I will not go through getting data from ef, i will assume some test data here

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            //List<MaAdmission> MaAdm = db.MaAdmissions.Where(x => (DbFunctions.TruncateTime(x.Cdate) >= Fdate && DbFunctions.TruncateTime(x.Cdate) <= Tdate) && x.BranchId==branchid).ToList();
            List<MaAdmission> MaAdm = new List<MaAdmission>();
            var q = (from pd in db.MaAdmissions
                     join od in db.Tb_CourseHist on pd.Id equals od.AdmissionId
                     where od.IsActive == true && (DbFunctions.TruncateTime(od.FromDate) >= Fdate && DbFunctions.TruncateTime(od.FromDate) <= Tdate) && pd.BranchId == branchid 
                     orderby od.FromDate select pd).ToList();
            MaAdm = q.ToList();

            var defaulterlistdataset = new List<DefaulterBean>();
            var defaulterlistdataset2 = new List<object>();

            foreach (var item in MaAdm)
            {
                Tb_CourseHist obj = db.Tb_CourseHist.Where(x => x.AdmissionId == item.Id && x.IsActive == true).OrderByDescending(x => x.Id).FirstOrDefault();
                string Time = "";
                string Package = "";
                DateTime? CCDate = new DateTime();
                try
                {
                    Time = obj.MaTime.TCode + " " + obj.MaTime.TimeIn + " " + obj.MaTime.Indesc + " " + obj.MaTime.TimeOut + " " + obj.MaTime.OutDesc;
                    Package = obj.MaPackage.PackageName;
                    CCDate = obj.DueDate;
                }
                catch (Exception)
                {
                }
                defaulterlistdataset2.Add(new DefaulterBean()
                {
                    FullName = item.FirstName + " " + item.LastName,
                    RollNo = (int)item.RollNo,
                    NextDueOn = CCDate,
                    PackageName = Package,
                    GroupChoice = Time,
                    FromDate = obj.FromDate
                });
            }

           
            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/DOJExcl.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "Dst",
                RightSubTitle = string.Format("{0:dd MMM,yyyy hh:mm tt}", CommonUtil.IndianTime()),
                Name = "Statistical Report",
                ReportDate = DateTime.Now,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Fee Reciept",
                ReportLanguage = "en-US",
                UserNamPrinting = "DST",
                ReportDoj = DateTime.Now,
                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };


            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = defaulterlistdataset2.ToList(), DatasetName = "DataSet1" });



            return reportViewModel;

        }
        #endregion
        //Timewise reports
        #region Group_timing_report
        public List<GroupTimingReportBean> StubForGroupTimingReport()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public DateTime IndianTime(DateTime? date)
        {
            TimeZone time2 = TimeZone.CurrentTimeZone;
            DateTime test = time2.ToUniversalTime((DateTime)date);
            var indiatime = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            var india = TimeZoneInfo.ConvertTimeFromUtc(test, indiatime);
            return india;
        }

        public ReportViewModel GetGroupTimingReport(string timeid = "All", string course = "", string format = "", string vw = "")
        {
            // I will not go through getting data from ef, i will assume some test data here

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            List<Tb_CourseHist> lst = new List<Tb_CourseHist>();
            //List<Tb_CourseHist> lst = db.Tb_CourseHist.Where(z => z.FromDate >= dt1 && z.FromDate <= dt2 && z.MaAdmission.BranchId == branchid).ToList();
            if (vw == "R")
            {
                lst = db.Tb_CourseHist.Where(z => z.MaAdmission.BranchId == branchid).Where(z => z.IsActive == true).Where(z => z.MaAdmission.IsLeft == false).OrderBy(z => z.MaAdmission.FirstName).ThenBy(z => z.currentcourselevel).ToList();
                try
                {
                    //int tid = Convert.ToInt32(timeid);
                    //lst = lst.Where(z => z.TimeId == tid).OrderBy(z => z.MaAdmission.FirstName).ToList();
                    lst = lst.Where(z => z.IsReport == true).OrderBy(z => z.MaAdmission.FirstName).ToList();
                    // lst = lst.Where(z => z.TimeId == tid).ToList();
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
                lst = db.Tb_CourseHist.Where(z => z.MaAdmission.BranchId == branchid).Where(z => z.IsActive == true).Where(z => z.MaAdmission.IsLeft == false).OrderBy(z => z.MaAdmission.FirstName).ThenBy(z => z.currentcourselevel).ToList();
                try
                {

                    int tid = Convert.ToInt32(timeid);
                    lst = lst.Where(z => z.TimeId == tid).OrderBy(z => z.MaAdmission.FirstName).ToList();
                    if (course != "")
                    {
                        int courseid = Convert.ToInt32(course);
                        lst = lst.Where(z => z.CourseId == courseid).OrderBy(z => z.MaAdmission.FirstName).ToList();
                    }
                    //lst = lst.Where(z => z.IsReport == true).OrderBy(z => z.MaAdmission.FirstName).ToList();
                    // lst = lst.Where(z => z.TimeId == tid).ToList();
                }
                catch (Exception ex)
                {
                    if (course != "")
                    {
                        int courseid = Convert.ToInt32(course);
                        lst = lst.Where(z => z.CourseId == courseid).OrderBy(z => z.MaAdmission.FirstName).ToList();
                    }
                }
            }


            var GroupTimingdataset = new List<object>();

            foreach (Tb_CourseHist obj in lst)
            {
                //DateTime fdate = Convert.ToDateTime(dt2);
                // TimeSpan ts = dt2.Subtract(fdate);
                GroupTimingdataset.Add(new GroupTimingReportBean()
                {
                    FullName = obj.MaAdmission.FirstName + " " + obj.MaAdmission.LastName,
                    RollNo = (int)obj.MaAdmission.RollNo,
                    GroupChoice = obj.currentcourselevel,//GroupChoice = obj.GroupName commented on because we always need to see the current course level
                    BatchTime = obj.MaTime.TCode + " " + obj.MaTime.TimeIn + " " + obj.MaTime.Indesc + "-" + obj.MaTime.TimeOut + " " + obj.MaTime.OutDesc,
                    TimeCode = obj.MaTime.TCode,
                    PackageName = obj.MaPackage.PackageName,
                    FromDate = obj.FromDate,
                    Mobile = obj.MaAdmission.Mobile,
                    Email = obj.MaAdmission.EmailId,
                    // MonthInInstitute = ts.Days / 30,
                    NextDueDate = obj.DueDate,
                    LDate = IndianTime(obj.DueDate)
                });
            }



            //Assuming the person printing the report is me

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/GroupTimingReport.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dd MMM,yyyy hh:mm:ss tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference)),
                Name = "Statistical Report",
                // ReportDate = dt1,
                //ReportDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference),
                ReportDate = DateTime.Now,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Fee Reciept",
                ReportLanguage = "en-US",
                UserNamPrinting = "DST",
                tdate = IndianTime(DateTime.Now.Date),
                // ReportDoj = dt2,
                ReportDoj = DateTime.Now,
                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = GroupTimingdataset.ToList(), DatasetName = "DataSet1" });



            return reportViewModel;

        }
        #endregion


        #region student_Detail Report
        public List<StudentDetailReportBeans> StubForStudentDetailReport()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }
        public ReportViewModel GetStudentDetailReport(DateTime dt1, DateTime dt2, string format = "")
        {
            // I will not go through getting data from ef, i will assume some test data here

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);


            List<Tb_CourseHist> lst = db.Tb_CourseHist.Where(z => z.FromDate >= dt1 && z.FromDate <= dt2 && z.MaAdmission.BranchId == branchid).ToList();

            var GroupTimingdataset = new List<object>();

            foreach (Tb_CourseHist obj in lst)
            {
                double? amount = db.Tb_Installment.Where(z => z.CourseHistId == obj.Id).Select(z => z.Amount).Sum();
                GroupTimingdataset.Add(new StudentDetailReportBeans()
                {
                    FullName = obj.MaAdmission.FirstName + " " + obj.MaAdmission.LastName,
                    RollNo = (int)obj.MaAdmission.RollNo,
                    GroupChoice = obj.GroupName,

                    TimeCode = obj.MaTime.TCode,
                    PackageName = obj.MaPackage.PackageName,
                    JoinDate = obj.FromDate,
                    Mobile = obj.MaAdmission.Mobile,
                    FatherName = obj.MaAdmission.FatherName,
                    Amount = amount,
                    EmailId = obj.MaAdmission.EmailId,
                });
            }

            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/StudentDetailReport.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = "اسم القسم",
                Name = "Statistical Report",
                ReportDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Fee Reciept",
                ReportLanguage = "en-US",
                UserNamPrinting = "DST",
                ReportDoj = dt2,
                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = GroupTimingdataset.ToList(), DatasetName = "DataSet1" });
            return reportViewModel;

        }
        #endregion


        #region Transfer Student Paticular Report
        public List<TransferStudentHelper> StubForTransferStudentPartReport()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }
        public ReportViewModel GetPartTransStuReport(int id)
        {
            // I will not go through getting data from ef, i will assume some test data here

            int userid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            string format = "PDF";
            DateTime dt2 = DateTime.Now;
            Tb_StudentTransfer tb_stu = db.Tb_StudentTransfer.Where(x => x.Id == id).FirstOrDefault();
            //int? packid = db.Tb_CourseHist.Where(z =>  z.AdmissionId == tb_stu.MaAdmission.Id &&  z.IsActive == true).Select(z => z.PackageId).FirstOrDefault();
            //string pckg = db.MaPackages.Where(x => x.Id == packid).Select(x => x.PackageName).FirstOrDefault();
            Tb_FeeReceipt fee = db.Tb_FeeReceipt.Where(x => x.StudentId == tb_stu.OldStudId).FirstOrDefault();

            var StTransferPart = new List<object>();
            StTransferPart.Add(new TransferStudentHelper()
            {
                OldRoll = tb_stu.OldRollNo,
                Name = tb_stu.MaAdmission.FirstName,
                BranchFrom = tb_stu.MaBranch1.BramchName,
                BranchTo = tb_stu.MaBranch.BramchName,
                TransferBy = tb_stu.User1.Fullname,
                Package = tb_stu.Package,
                Course = tb_stu.Course,
                KitProvided = fee.KitProvided

            });



            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/StudentTransferPart.rdlc",
                ReportDoj = DateTime.Now.AddMinutes(StaticLogic.TimeDifference),
                //Format = ReportViewModel.ReportFormat.PDF,
                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = StTransferPart.ToList(), DatasetName = "DataSet1" });
            return reportViewModel;

        }
        #endregion


        #region Pending_Fee_Report
        public List<DefaulterBean> StubForPendingFeeList()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetPendingFeeList()
        {
            // I will not go through getting data from ef, i will assume some test data here

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            DateTime dt = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            DateTime tdt = DateTime.Now.AddDays(3);

            List<Tb_Installment> lst = db.Tb_Installment.Where(z => z.BalanceDueAmount > 0 || z.BalanceDueAmount == null).Where(z => z.Tb_CourseHist.MaAdmission.IsLeft != true && z.BranchId == branchid).Where(z => z.DueDate > dt && z.DueDate <= tdt).ToList();

            /* if user paid partial payment then new duedate is updated in Tb_Feereceipt table on balance is also updated in installment table so here
             we are picking those students also who were defaulter today but they paid partial payment today.
             so they will be in pending list today and will
             be in defaulter list tommorrow*/
            lst.AddRange(db.Tb_Installment.Where(z => z.DueDate == dt && z.BalanceDueAmount > 0).ToList());
            var defaulterlistdataset = new List<object>();

            foreach (Tb_Installment obj in lst)
            {
                if (obj.Tb_CourseHist.IsActive == true)
                {
                    double? dueamt = 0;
                    DateTime? dtdue = obj.DueDate;
                    if (obj.BalanceDueAmount == null)
                    {
                        dueamt = obj.Amount;
                    }
                    else
                    {
                        dueamt = obj.BalanceDueAmount;
                        dtdue = db.Tb_FeeReceipt.Where(z => z.InstallmentNoId == obj.Id).Select(z => z.DueDate).First();
                    }

                    defaulterlistdataset.Add(new DefaulterBean()
                    {
                        FullName = obj.Tb_CourseHist.MaAdmission.FirstName + " " + obj.Tb_CourseHist.MaAdmission.LastName,
                        RollNo = (int)obj.Tb_CourseHist.MaAdmission.RollNo,
                        GroupChoice = obj.GroupName,

                        GroupName = obj.Tb_CourseHist.MaTime.TCode + " " + obj.Tb_CourseHist.MaTime.TimeIn + " " + obj.Tb_CourseHist.MaTime.Indesc + " " + obj.Tb_CourseHist.MaTime.TimeOut + " " + obj.Tb_CourseHist.MaTime.OutDesc,
                        PackageName = obj.Tb_CourseHist.MaPackage.PackageName,
                        FromDate = obj.Tb_CourseHist.FromDate,
                        DueAmount = dueamt,
                        NextDueOn = dtdue,
                    });
                }
            }

            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/PendingFeeListReport.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dd MMM,yyyy}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date),
                Name = "Statistical Report",
                ReportDate = DateTime.Now,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Fee Reciept",
                ReportLanguage = "en-US",
                UserNamPrinting = "DST",
                ReportDoj = DateTime.Now,
                Format = ReportViewModel.ReportFormat.PDF,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = defaulterlistdataset.ToList(), DatasetName = "DataSet1" });



            return reportViewModel;

        }


        public List<HelperForAreaReport> StubforAreaReport()
        {
            return null;
        }

        public ReportViewModel GetAreaReport(string dt1 = "", string dt2 = "", string format = "", int areaid = 0)
        {
            // I will not go through getting data from ef, i will assume some test data here

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);

            string areaslect = "";
            DateTime fdat = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            DateTime tdat = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;

            if (dt1 != "" && dt2 != "")
            {
                try
                {
                    fdat = Convert.ToDateTime(dt1);
                    tdat = Convert.ToDateTime(dt2);
                }
                catch (Exception)
                {
                }
            }
            var Arealistdataset = new List<object>();

            List<MaEnquiry> listenquiry = new List<MaEnquiry>();

            listenquiry = db.MaEnquiries.Where(a => a.BranchId == branchid && a.Cdate >= fdat && a.Cdate <= tdat).ToList();

            if (areaid > 0)
            {
                areaslect = db.Ma_Area.Where(a => a.Id == areaid).Select(a => a.Area).FirstOrDefault();
                listenquiry = listenquiry.Where(a => a.AreaId == areaid).ToList();
            }
            else
            {
                areaslect = "All";
            }


            foreach (MaEnquiry obj in listenquiry)
            {
                string area = db.Ma_Area.Where(a => a.Id == obj.AreaId).Select(a => a.Area).FirstOrDefault();

                Arealistdataset.Add(new HelperForAreaReport()
                {

                    Name = obj.FirstName + " " + obj.LastName,
                    FatherName = obj.FatherName,
                    Mobile = obj.Mobile,
                    Address = obj.Address,
                    Status = obj.FEnqstatus,
                    area = area,
                    FromDate = fdat,
                    ToDate = tdat,
                    SelectedArea = areaslect

                });
            }
            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/AreaReport.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dd MMM,yyyy}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date),
                Name = "Statistical Report",
                ReportDate = DateTime.Now,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Area Report",
                ReportLanguage = "en-US",
                UserNamPrinting = "DST",
                ReportDoj = DateTime.Now,
                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = Arealistdataset.ToList(), DatasetName = "DataSet1" });
            return reportViewModel;

        }


        public List<HelperForInactiveCourseReport> StubforInactivecourseReport()
        {
            return null;
        }

        public ReportViewModel GetInactivecourseReport(string dt1 = "", string dt2 = "", string format = "")
        {
            // I will not go through getting data from ef, i will assume some test data here

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);

            string areaslect = "";
            DateTime fdat = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            DateTime tdat = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;

            if (dt1 != "" && dt2 != "")
            {
                try
                {
                    fdat = Convert.ToDateTime(dt1);
                    tdat = Convert.ToDateTime(dt2);
                }
                catch (Exception)
                {
                }
            }
            var Arealistdataset = new List<object>();

            List<Tb_CourseHist> listenquiry = new List<Tb_CourseHist>();

            listenquiry = db.Tb_CourseHist.Where(a => a.FromDate >= fdat && a.FromDate <= tdat).OrderBy(a => a.IsActive).ToList();

            foreach (Tb_CourseHist obj in listenquiry)
            {

                Arealistdataset.Add(new HelperForInactiveCourseReport()
                {

                    Name = obj.MaAdmission.FirstName + " " + obj.MaAdmission.LastName,
                    RollNo = obj.MaAdmission.RollNo,
                    CouerseTo = obj.Todate,
                    CourseFrom = obj.FromDate,
                    CurrentCourse = obj.currentcourselevel,
                    Timing = obj.MaTime.TimeIn,
                    IsLeft = obj.MaAdmission.IsLeft,
                    GroupName = obj.GroupName,
                    Course = obj.MaCourse.CourseName,
                    AdmissionId = obj.AdmissionId,
                    FromDate = fdat,
                    ToDate = tdat,
                    IsActive = obj.IsActive,

                });
            }
            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/InactiveReport.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dd MMM,yyyy}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date),
                Name = "Statistical Report",
                ReportDate = DateTime.Now,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Area Report",
                ReportLanguage = "en-US",
                UserNamPrinting = "DST",
                ReportDoj = DateTime.Now,
                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = Arealistdataset.ToList(), DatasetName = "DataSet1" });
            return reportViewModel;

        }


        #endregion



        #region Enq_Search_Report

        public List<EnqSearchReportBeans> StubEnqSearchReportList()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public List<EnqSearchSummary1> StubEnqSearchSummary1()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public List<EnqSearchSummary2> StubEnqSearchSummary2()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetPendingFeeList(DateTime dt, DateTime dt1, string dayname, string timeslot, string format = "")
        {
            // I will not go through getting data from ef, i will assume some test data here

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);

            List<MaEnquiry> lst = db.MaEnquiries.Where(z => z.WalkInDate >= dt && z.WalkInDate <= dt1 && z.BranchId == branchid).ToList();
            if (dayname != "All")
            {
                lst = lst.Where(z => z.WalkInDate.Value.ToLongDateString().Contains(dayname)).ToList();
            }
            if (timeslot != "All")
            {
                lst = lst.Where(z => z.EnqTimeSlot == timeslot).ToList();
            }

            var enqlistdataset = new List<EnqSearchReportBeans>();
            var enqlistdataset2 = new List<object>();
            var enqlistdataset3 = new List<object>();



            foreach (MaEnquiry obj in lst)
            {
                DateTime? nxtdt = null;
                if (db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                {
                    nxtdt = db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).Select(z => z.NextDate).First();
                }
                enqlistdataset.Add(new EnqSearchReportBeans()
                {
                    SName = obj.FirstName + " " + obj.LastName,
                    Id = obj.Id,
                    TimeSlot = obj.EnqTimeSlot,
                    DayName = string.Format("{0:dddd}", obj.WalkInDate),
                    EnqDate = obj.WalkInDate,
                    WalkinType = obj.WalkInType,
                    EnqHandleBy = obj.User1.Fullname,
                    Mobile = obj.Mobile,
                    NxtFollowUp = nxtdt,
                });
            }



            //_lst = lst.ToList();
            var lst1 = enqlistdataset.ToList();
            enqlistdataset2.AddRange(enqlistdataset);

            //for timeslot
            var enq = (from c in enqlistdataset
                       group c by c.TimeSlot
                           into grp
                       select new
                       {
                           timeslot = grp.Key,
                           count = grp.Count()
                       });

            List<EnqSearchSummary1> sm1 = new List<EnqSearchSummary1>();
            List<object> smo1 = new List<object>();
            foreach (var d in enq)
            {
                sm1.Add(new EnqSearchSummary1()
                {
                    TimeSlot = d.timeslot,
                    Timesloatcount = d.count
                });

            }

            //call smo1 to report as dataset3
            smo1.AddRange(sm1.ToList());


            //summery of day wise start here
            var lst2 = enqlistdataset.ToList();
            enqlistdataset3.AddRange(enqlistdataset);

            var enq2 = (from c in enqlistdataset
                        group c by c.DayName
                            into grp
                        select new
                        {
                            dayname = grp.Key,
                            count = grp.Count()
                        });


            List<EnqSearchSummary2> sm2 = new List<EnqSearchSummary2>();
            List<object> smo2 = new List<object>();
            foreach (var d in enq2)
            {
                sm2.Add(new EnqSearchSummary2()
                {
                    Dayname = d.dayname,
                    Daycount = d.count
                });

            }

            //call smo2 to report as dataset2
            smo2.AddRange(sm2.ToList());




            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/EnquirySearchReport.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dd MMM,yyyy}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date),
                Name = "Statistical Report",
                ReportDate = dt,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Fee Reciept",
                ReportLanguage = "en-US",
                UserNamPrinting = "DST",
                ReportDoj = dt1,
                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = enqlistdataset2.ToList(), DatasetName = "DataSet1" });
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = smo1, DatasetName = "DataSet3" });

            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = smo2, DatasetName = "DataSet2" });

            return reportViewModel;

        }

        #endregion




        #region reference_report
        public List<ReferenceReportBean> StubReferenceReportList()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }
        public ReportViewModel GetReferenceReportList(string dt1 = "", string dt2 = "", string refid = "All", string format = "")
        {
            // I will not go through getting data from ef, i will assume some test data here

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            DateTime? fdate = null;
            DateTime? tdate = null;
            List<MaAdmission> lstadm = db.MaAdmissions.Where(z => z.ReferenceId != null && z.BranchId == branchid).OrderByDescending(z => z.Id).ToList();
            try
            {
                if (dt1 != "" && dt2 != "")
                {
                    fdate = Convert.ToDateTime(dt1);
                    tdate = Convert.ToDateTime(dt2);
                    lstadm = lstadm.Where(z => z.Cdate >= fdate && z.Cdate <= tdate).ToList();
                }
            }
            catch (Exception ex)
            { }
            try
            {
                if (refid != "All")
                {
                    int referid = Convert.ToInt32(refid);
                    lstadm = lstadm.Where(z => z.ReferenceId == referid).ToList();
                }
            }
            catch (Exception ex)
            { }



            var reflistdataset = new List<object>();



            foreach (MaAdmission obj in lstadm)
            {
                string cname = "";
                string packname = "";
                DateTime? dt = null;
                if (db.Tb_CourseHist.Where(z => z.AdmissionId == obj.Id).Count() > 0)
                {
                    Tb_CourseHist objhistory = db.Tb_CourseHist.Where(z => z.AdmissionId == obj.Id).First();

                    cname = objhistory.MaCourse.CourseName;
                    packname = objhistory.MaPackage.PackageName;
                    dt = objhistory.FromDate;
                }
                reflistdataset.Add(new ReferenceReportBean()
                {
                    Id = obj.Id,
                    RollNo = obj.RollNo,
                    SName = obj.FirstName + " " + obj.LastName,
                    FatherName = obj.FirstName,
                    ReferenceName = obj.MaFrmReference.CompName,
                    Mobile = obj.Mobile,
                    CourseName = cname,
                    DOJ = dt,
                    PackageName = packname,
                    fromdate = fdate,
                    todate = tdate,
                });
            }


            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/ReferenceReport.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dd MMM,yyyy}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date),
                Name = "Statistical Report",
                ReportDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Reference Report",
                ReportLanguage = "en-US",
                UserNamPrinting = "DST",
                ReportDoj = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date,
                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = reflistdataset.ToList(), DatasetName = "DataSet1" });



            return reportViewModel;

        }
        #endregion


        #region Report_For_Discount_receipt_Receipt
        public List<DiscountRecieptBean> StubForDiscountReportDataSet()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetDiscountReportList(string dt1 = "", string dt2 = "", string format = "")
        {
            // I will not go through getting data from ef, i will assume some test data here

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            int usrid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);

            string usrname = db.Users.Where(z => z.Id == usrid).Select(z => z.Fullname).First();
            DateTime fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            DateTime tdate = DateTime.Now;
            if (dt1 != "")
            {
                try
                {
                    fdate = Convert.ToDateTime(dt1);

                }
                catch (Exception ex)
                {
                    fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                }
            }

            if (dt2 != "")
            {
                try
                {
                    tdate = Convert.ToDateTime(dt2);
                }
                catch (Exception ex)
                {
                    tdate = DateTime.Now;
                }
            }

            List<Tb_FeeReceipt> lst = db.Tb_FeeReceipt.Where(m => m.FeeDate >= fdate && m.FeeDate <= tdate && m.BranchId == branchid && m.IsTransfer == null).Where(z => z.Discount > 0).ToList();

            /*------------------------------------------*/
            /* Get data of those students which are transfered from one branch to another branch*/
            List<Tb_StudentTransfer> lststudent = db.Tb_StudentTransfer.Where(z => z.BranchFrom == branchid).ToList();

            List<Tb_FeeReceipt> lsttransfee = new List<Tb_FeeReceipt>();
            foreach (Tb_StudentTransfer objtrans in lststudent)
            {
                List<Tb_FeeReceipt> lstinner = db.Tb_FeeReceipt.Where(m => m.FeeDate >= fdate && m.FeeDate <= tdate && m.StudentId == objtrans.StudentId && m.IsTransfer == true).ToList();
                lsttransfee.AddRange(lstinner);
            }

            lst.AddRange(lsttransfee);

            lst = lst.OrderBy(z => z.MaPackage.PackageName).ToList();
            /*   */
            var feereciptdatset = new List<object>();

            foreach (Tb_FeeReceipt obj in lst)
            {
                string percent = "No";
                if (obj.Discount != null)
                {
                    percent = obj.Discount.ToString();
                }
                if (obj.DiscountId != null)
                {
                    percent = db.MaDiscounts.Where(z => z.Id == obj.DiscountId).Select(z => z.DiscountPercen).First().ToString();
                    percent = percent + "%";
                }

                feereciptdatset.Add(new DiscountRecieptBean()
                {
                    studentname = obj.MaAdmission.FirstName + " " + obj.MaAdmission.LastName,
                    RollNo = (int)obj.MaAdmission.RollNo,
                    PackageName = obj.MaPackage.PackageName,
                    remarks = obj.Remarks,
                    PackageAmount = Convert.ToDouble(obj.MaPackage.Amount),
                    DiscountAmount = percent.ToString(),
                });
            }

            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";


            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/FeeReportsByDiscount.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dd MMM,yyyy hh:mm tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference)),
                Name = "Statistical Report",
                ReportDate = fdate,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Fee Reciept",
                ReportLanguage = "en-US",
                UserNamPrinting = usrname,
                ReportDoj = tdate,

                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = feereciptdatset.ToList(), DatasetName = "DataSet1" });



            return reportViewModel;

        }

        #endregion



        #region Left Student Report
        public List<LeftStudentHelper> StubForLeftStudentDataSet()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetLeftStudentReport(string dt1 = "", string dt2 = "", string format = "")
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            int usrid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);

            string usrname = db.Users.Where(z => z.Id == usrid).Select(z => z.Fullname).First();
            DateTime fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            DateTime tdate = DateTime.Now;
            if (dt1 != "")
            {
                try
                {
                    fdate = Convert.ToDateTime(dt1);

                }
                catch (Exception ex)
                {
                    fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                }
            }

            if (dt2 != "")
            {
                try
                {
                    tdate = Convert.ToDateTime(dt2);
                }
                catch (Exception ex)
                {
                    tdate = DateTime.Now;
                }
            }

            List<MaAdmission> lst = db.MaAdmissions.Where(z => z.BranchId == branchid && z.IsLeft == true && z.LeftDate > fdate && z.LeftDate < tdate).OrderByDescending(z => z.Id).ToList();

            var LeftStudent = new List<object>();

            foreach (MaAdmission obj in lst)
            {
                string Jdate = "";
                string Ddate = "";
                Tb_FeeReceipt feereceipt = new Tb_FeeReceipt();
                try
                {
                    feereceipt = db.Tb_FeeReceipt.Where(a => a.StudentId == obj.Id).OrderByDescending(a => a.Id).FirstOrDefault();
                }
                catch (Exception ex)
                { }
                try
                {
                    Jdate = feereceipt.PackageDate.Value.ToShortDateString();
                }
                catch (Exception ex)
                { }
                try
                {
                    Ddate = feereceipt.DueDate.Value.ToShortDateString();
                }
                catch (Exception ex)
                { }
                DateTime? exdat = new DateTime();
                string examdate = "";
                try
                {
                    exdat = db.Tb_IELTSResult.Where(x => x.StudentId == obj.Id && x.TestDate != null).OrderByDescending(x => x.Id).Select(x => x.TestDate).FirstOrDefault();
                    examdate = String.Format("{0:dd MMM,yyyy}", exdat);
                }
                catch (Exception ex)
                {
                }

                LeftStudent.Add(new LeftStudentHelper()
                {
                    RollNo = obj.RollNo,
                    FirstName = obj.FirstName,
                    LastName = obj.LastName,
                    //JoinDate = obj.Cdate.Value.ToShortDateString(),
                    JoinDate = Jdate,
                    DueDate = Ddate,
                    LeftDate = obj.LeftDate.Value.ToShortDateString(),
                    Gender = obj.Gender,
                    Edate = examdate,
                    CounselorName = db.Users.Where(a => a.Id == obj.CounselorName).Select(a => a.Fullname).FirstOrDefault()
                });
            }

            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";


            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/LeftStudentReport.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dd MMM,yyyy hh:mm tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference)),
                Name = "Statistical Report",
                ReportDate = fdate,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Fee Reciept",
                ReportLanguage = "en-US",
                UserNamPrinting = usrname,
                ReportDoj = tdate,

                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = LeftStudent.ToList(), DatasetName = "DataSet1" });
            return reportViewModel;

        }

        #endregion



        #region Course_History
        public List<CoursHisBean> StubForCrsHisReportDataSet()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }
        public ReportViewModel GetCrsHisReportList(int CrsHisid)
        {
            Tb_CourseHist newcoursehistobj = db.Tb_CourseHist.Find(CrsHisid);
            MaAdmission objadmission = db.MaAdmissions.Where(z => z.Id == newcoursehistobj.AdmissionId).FirstOrDefault();

            int? tmeid = newcoursehistobj.TimeId;
            string bathtme = db.MaGroupTime_View.Where(z => z.Timeid == tmeid).Select(z => z.BatchTime).First();

            int usrid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            string usrname = db.Users.Where(z => z.Id == usrid).Select(z => z.username).First();
            double paidamount = 0;

            var feereciptdatset = new List<object>();
            try
            {
                List<Tb_FeeReceipt> _lstfeereceipt = db.Tb_FeeReceipt.Where(z => z.CourseId == newcoursehistobj.MaCourse.Id && z.PackageId == newcoursehistobj.PackageId && z.StudentId == objadmission.Id).OrderByDescending(z => z.Id).ToList();
                foreach (Tb_FeeReceipt obj in _lstfeereceipt)
                {
                    paidamount += Convert.ToDouble(obj.PaidAmount);
                }
                //paidamount = Convert.ToDouble(db.Tb_FeeReceipt.Where(z => z.CourseId == newcoursehistobj.MaCourse.Id && z.PackageId == newcoursehistobj.PackageId && z.StudentId == objadmission.Id).OrderByDescending(z => z.Id).Select(z => z.PaidAmount).FirstOrDefault());
            }
            catch (Exception ex)
            { }
            feereciptdatset.Add(new CoursHisBean()
            {
                Group = newcoursehistobj.GroupName,
                CurrentcrsLvl = newcoursehistobj.currentcourselevel,
                Course = newcoursehistobj.MaCourse.CourseName,
                PackageName = newcoursehistobj.MaPackage.PackageName,
                Time = bathtme,
                Fromdate = newcoursehistobj.FromDate,

                Amount = paidamount,
                packageamount = newcoursehistobj.MaPackage.Amount,
                Rollno = newcoursehistobj.MaAdmission.RollNo.ToString(),
                Name = newcoursehistobj.MaAdmission.FirstName + " " + newcoursehistobj.MaAdmission.LastName,
                Duedate = Convert.ToDateTime(newcoursehistobj.DueDate),
                username = usrname,
            });


            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";


            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/CourseHisReport.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dd MMM,yyyy hh:mm tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference)),
                Name = "Statistical Report",
                ReportDate = DateTime.Now,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Course History",
                ReportLanguage = "en-US",
                UserNamPrinting = "aa",
                ReportDoj = DateTime.Now,
                Format = ReportViewModel.ReportFormat.PDF,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = feereciptdatset.ToList(), DatasetName = "DataSet1" });



            return reportViewModel;

        }



        #endregion


        #region Student_Leave
        public List<StudLeaveBean> StubForStuLveReportDataSet()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }
        public ReportViewModel GetStuLveReportList(int lveid)
        {
            int userid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieUserid].Value);
            Tb_StudLeave tb_studleave = db.Tb_StudLeave.Find(lveid);

            TimeSpan daydiff = tb_studleave.ToDate.Value - tb_studleave.FromDate.Value;
            int days = daydiff.Days;
            DateTime exactduedate = DateTime.Now;
            string coursenme = "";
            //DateTime duedate = DateTime.Now;

            //calculate due date
            int admid = Convert.ToInt32(tb_studleave.AdmissionId);
            List<Tb_CourseHist> lsthistory = db.Tb_CourseHist.Where(z => z.AdmissionId == admid).Where(z => z.IsActive == true).ToList();
            foreach (Tb_CourseHist obj in lsthistory)
            {
                exactduedate = Convert.ToDateTime(obj.DueDate);
                coursenme = obj.MaCourse.CourseName;
            }


            //  DateTime exactduedate = duedate.AddDays(days + 1);

            //
            string username = db.Users.Where(z => z.Id == userid).Select(z => z.username).First();

            var feestulvedatset = new List<object>();

            foreach (Tb_CourseHist obj in lsthistory)
            {
                feestulvedatset.Add(new StudLeaveBean()
                {
                    Rollno = tb_studleave.MaAdmission.RollNo.ToString(),
                    Name = tb_studleave.MaAdmission.FirstName + " " + tb_studleave.MaAdmission.LastName,
                    Fromdate = tb_studleave.FromDate,
                    Todate = tb_studleave.ToDate,
                    NextDuedate = Convert.ToDateTime(obj.DueDate),
                    CourseName = obj.MaCourse.CourseName,
                    username = username,
                    enterdate = tb_studleave.Cdate,
                    Remarks = tb_studleave.Remarks,
                });
            }

            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";


            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/StudentLveReport.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dd MMM,yyyy hh:mm tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference)),
                Name = "Statistical Report",
                ReportDate = DateTime.Now,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Student Leave",
                ReportLanguage = "en-US",
                UserNamPrinting = "aa",
                ReportDoj = DateTime.Now,
                Format = ReportViewModel.ReportFormat.PDF,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = feestulvedatset.ToList(), DatasetName = "DataSet1" });



            return reportViewModel;

        }



        #endregion


        #region print uploaded_photo
        public List<PhotoUploadHelper> StubForUploadPhotoReportDataSet()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetUploadphotoReportList(string prntoptn, List<PhotoUploadHelper> _data)
        {


            var feestulvedatset = new List<object>();

            _data = _data.OrderBy(z => z.GroupTime).ThenBy(z => z.rollno).ToList();

            foreach (PhotoUploadHelper obj in _data)
            {
                feestulvedatset.Add(new PhotoUploadHelper()
                {
                    rollno = obj.rollno,
                    Name = obj.Name,
                    GroupTime = obj.GroupTime,
                    GroupChoice = obj.GroupChoice
                });
            }

            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";


            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/UploadPhotoReport.rdlc",
                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = "DEF Department Name",
                RightMainTitle = "اسم الشركة",
                RightSubTitle = string.Format("{0:dd MMM,yyyy hh:mm tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference)),
                Name = "Statistical Report",
                ReportDate = DateTime.Now,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Student Leave",
                ReportLanguage = "en-US",
                UserNamPrinting = "aa",
                ReportDoj = DateTime.Now,
                Format = ReportViewModel.ReportFormat.PDF,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = feestulvedatset.ToList(), DatasetName = "DataSet1" });



            return reportViewModel;

        }

        #endregion


        #region print pending followup
        public List<Bean> StubForPendingFollowUpDataSet()
        {
            return null;
        }


        public List<Enquirycountbean> StubEnqfollowupbycount()
        {
            return null;
        }


        public ReportViewModel GetPendingFollowUpcountList()
        {

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            DataTable dt = new Status().GetEnqFollowSummary();

            // int pendcount = 0;

            string[] ftpe = { "MokTest", "IELTS", "VISA" };
            var branches = db.MaBranches.ToList();

            var pendingftypes = new List<object>();
            Bean b = new Bean();
            st.ByBranch = true;

            foreach (MaBranch br in branches)
            {
                for (int x = 0; x < ftpe.Count(); x++)
                {
                    b = new Bean();
                    b.branch = br.BramchName;
                    b.followtype = ftpe[x];
                    st.Bid = br.Id;

                    switch (ftpe[x])
                    {
                        case "MokTest":
                            b.fcount = st.GetMockTestCount();
                            break;
                        case "IELTS":
                            b.fcount = st.GetIELTSCount();
                            break;
                        case "VISA":
                            b.fcount = st.GetPendingVisaCount();
                            break;
                    }
                    pendingftypes.Add(b);
                }



            }

            st.ByBranch = false;

            var pendingfollowupBycountdatset = new List<object>();
            foreach (DataRow dr in dt.Rows)
            {
                pendingfollowupBycountdatset.Add(new Enquirycountbean()
                {
                    username = dr["username"].ToString(),
                    usercount = Convert.ToInt32(dr["enqcount"]),
                    branchname = dr["branchname"].ToString(),
                });
            }

            //Assuming the person printing the report is me
            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/PendingFollowUp.rdlc",
                ReportDoj = DateTime.Now,
                Format = ReportViewModel.ReportFormat.PDF,
                ViewAsAttachment = false,
            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = pendingftypes.ToList(), DatasetName = "DataSet1" });
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = pendingfollowupBycountdatset.ToList(), DatasetName = "DataSet2" });
            return reportViewModel;
        }


        public int Getmocktestpendingcount()
        {

            //mock test follow up count is start here
            Status st = new Status();
            DataTable dtpendmock = new DataTable();
            dtpendmock = st.GetPendingMockComplete(0);

            int pendcountmock = 0;
            foreach (DataRow dr in dtpendmock.Rows)
            {
                int id = Convert.ToInt32(dr["Id"].ToString());
                if (db.MaEnqFollows.Where(z => z.ReferId == id && z.FollowUpType == "MockTest" && (z.Enqstatus == "Close" || z.Enqstatus == "Move To Visa")).Where(z => z.Id > 0).Count() == 0)
                {
                    pendcountmock++;
                }
            }

            //mock test follow up count is end here

            return pendcountmock;
        }

        public int Getieltsexampendingcount()
        {

            //ielts exam follow up count is start here
            Status st = new Status();
            DataTable dtpendielts = new DataTable();
            dtpendielts = st.GetPendingIELTSComplete();

            int pendcountielts = 0;
            foreach (DataRow dr in dtpendielts.Rows)
            {


                int id = Convert.ToInt32(dr["Id"].ToString());



                if (db.MaEnqFollows.Where(z => z.ReferId == id && z.FollowUpType == "IELTS" && (z.Enqstatus == "Close" || z.Enqstatus == "Move To Visa")).Count() == 0)
                {
                    pendcountielts++;
                }
            }
            return pendcountielts;
            //ielts exam follow up count is end here

        }

        public int GetVisapendingfollowcount()
        {
            //visa follow up count is start here
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            Status st = new Status();
            System.Data.DataTable dtpendvisa = new System.Data.DataTable();
            dtpendvisa = st.GetPendingVisaComplete();

            int pendvisa = 0;

            foreach (System.Data.DataRow dr in dtpendvisa.Rows)
            {

                int referid = Convert.ToInt32(dr["ReferId"].ToString());
                if (db.MaEnqFollows.Where(z => z.ReferId == referid && (z.FollowUpType == "VisaIELTS" || z.FollowUpType == "VisaMockTest") && (z.Enqstatus == "Close" || z.Enqstatus == "Move To Visa")).Count() == 0)
                {
                    if (dr["FollowUpType"].ToString() == "VisaIELTS")
                    {
                        if (db.Tb_IELTSResult.Where(z => z.Id == referid && z.BranchId == branchid).Count() > 0)
                        {
                            pendvisa++;
                        }
                    }
                    if (dr["FollowUpType"].ToString() == "VisaMockTest")
                    {
                        if (db.Tb_MockTest.Where(z => z.Id == referid && z.BranchId == branchid).Count() > 0)
                        {
                            pendvisa++;
                        }
                    }
                    if (dr["FollowUpType"].ToString() == "VisaEnquiry")
                    //if (db.MaEnqFollow.Where(z => z.ReferId == referid && z.FollowUpType == "VisaEnquiry").Count() > 0)
                    {
                        if (db.MaEnquiries.Where(z => z.Id == referid && z.BranchId == branchid).Count() > 0)
                        {
                            pendvisa++;
                        }
                    }

                }

            }

            return pendvisa;
            //visa follow up count is end here

        }

        #endregion



        #region print pending followp list

        public List<Bean> StubForPendingFollowUpcount()
        {
            return null;
        }


        public List<FollowUpEnqList> StubEnqfollowuplist()
        {
            return null;
        }


        //public ReportViewModel GetPendingFollowUpList(string enqtype, int branch, int usr, string date2, string date3)
        //{
        //    DataTable dt = new Status().GetEnqFollowList(enqtype, branch, usr, date2, date3);


        //    //string[] ftpe = { "MokTest", "IELTS", "VISA" };
        //    //var branches = db.MaBranches.Where(w => w.Id == branch).ToList();

        //    //var pendingftypes = new List<object>();
        //    //Bean b = new Bean();
        //    //st.ByBranch = true;

        //    //st.ByBranch = false;

        //    var pendingfollowupByListdatset = new List<object>();


        //    // This loop is adding pending enquiry into the helper FollowupEnqList
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        pendingfollowupByListdatset.Add(new FollowUpEnqList()
        //        {

        //            Id = Convert.ToInt32(dr["Id"]),
        //            FirstName = dr["FirstName"].ToString(),
        //            LastName = dr["LastName"].ToString(),
        //            EnqHandledBy = Convert.ToString(dr["fullname"]),
        //            nextdate = Convert.ToDateTime(dr["nxdt"]),
        //            CreateDate = Convert.ToDateTime(dr["cdate"]),
        //            EnqAbt = dr["EnqAbt"].ToString(),
        //            Mobile = dr["Mobile"] == null ? "" : dr["Mobile"].ToString(),
        //            Enqstatus = dr["Enqstatus"].ToString(),
        //            FatherNo = dr["alternateno"].ToString(),
        //            Remarks = dr["fremarks"].ToString()
        //        });
        //    }

        //    // This loop is adding pending mock list into the helper FollowupEnqList

        //    DataTable dt1 = new Status().GetPendingMockList(enqtype, branch, usr, date2, date3);
        //    foreach (DataRow dr in dt1.Rows)
        //    {
        //        pendingfollowupByListdatset.Add(new FollowUpEnqList()
        //        {
        //            Id = Convert.ToInt32(dr["Id"]),
        //            FirstName = dr["FirstName"].ToString(),
        //            LastName = dr["LastName"].ToString(),
        //            EnqHandledBy = Convert.ToString(dr["EnqFollowBy"]),
        //            nextdate = Convert.ToDateTime(dr["nxdt"]),
        //            CreateDate = Convert.ToDateTime(dr["cdate"]),
        //            EnqAbt = "Mock Test",
        //            Mobile = dr["Mobile"].ToString(),
        //            Enqstatus = dr["Enqstatus"].ToString(),
        //            // FatherNo = dr["alternateno"].ToString(),
        //            Remarks = dr["fremarks"].ToString()
        //        });
        //    }

        //    // This loop is adding pending IeltsList into the helper FollowupEnqList

        //    DataTable dt2 = new Status().GetPendingIELTSList(branch, usr, date2, date3);
        //    foreach (DataRow dr in dt2.Rows)
        //    {
        //        pendingfollowupByListdatset.Add(new FollowUpEnqList()
        //        {
        //            Id = Convert.ToInt32(dr["Id"]),
        //            FirstName = dr["studentname"].ToString(),
        //            //  LastName = dr["LastName"].ToString(),
        //            EnqHandledBy = Convert.ToString(dr["fullname"]),
        //            nextdate = Convert.ToDateTime(dr["nxdt"]),
        //            CreateDate = Convert.ToDateTime(dr["cdate"]),
        //            EnqAbt = "IELTS Test",
        //            Mobile = dr["MobileNo"].ToString(),
        //            Enqstatus = dr["Enqstatus"].ToString(),
        //            // FatherNo = dr["alternateno"].ToString(),
        //            Remarks = dr["remarks"].ToString()
        //        });
        //    }

        //    // This loop is adding pending Visa list into the helper FollowupEnqList

        //    List<PendingVisaStudentBean> lstenqfollow = new List<PendingVisaStudentBean>();

        //    List<Tb_VisaFollowUp> _lstfollow = new List<Tb_VisaFollowUp>();

        //    DateTime ffdate = Convert.ToDateTime(date2);
        //    DateTime ttdate = Convert.ToDateTime(date3);
        //    //string userr = db.Users.Where(a => a.Id == usr).Select(a => a.Fullname).FirstOrDefault();
        //    string userr = Convert.ToString(usr);
        //    _lstfollow = db.Tb_VisaFollowUp.Where(z => z.NextFollowdate > ffdate && z.NextFollowdate < ttdate && z.FollowStatus == "Open" && z.Branchid == branch && z.LastFollowBy == userr).ToList();

        //    foreach (Tb_VisaFollowUp obj in _lstfollow)
        //    {
        //        FollowUpEnqList fuel = new FollowUpEnqList();

        //        fuel.Id = obj.ReferId;
        //        fuel.FirstName = obj.Name;
        //        fuel.Mobile = obj.Mobileno;
        //        fuel.Remarks = obj.Remarks;
        //        fuel.Enqstatus = obj.FollowStatus;
        //        if (obj.Followtype == "MockTest" || obj.Followtype == "VisaMockTest")
        //        {
        //            fuel.EnqAbt = CommonUtil.AdmissionMcktst;
        //        }

        //        else if (obj.Followtype == "VisaEnquiry")
        //        {
        //            fuel.EnqAbt = CommonUtil.Enquiry;

        //        }
        //        else if (obj.Followtype == "IELTS" || obj.Followtype == "VisaIELTS")
        //        {
        //            fuel.EnqAbt = CommonUtil.EnquiryIELTS;
        //        }
        //        else if (obj.Followtype == "VisaIELTSAdmission")
        //        {
        //            fuel.EnqAbt = CommonUtil.ProspStudentsIELTS;
        //        }

        //        int lastfollowid = Convert.ToInt32(obj.LastFollowBy);
        //        string enqhandle = db.Users.Where(a => a.Id == lastfollowid).Select(a => a.Fullname).FirstOrDefault();
        //        fuel.EnqHandledBy = enqhandle;
        //        fuel.nextdate = obj.NextFollowdate;
        //        pendingfollowupByListdatset.Add(fuel);
        //    }




        //    //  Assuming the person printing the report is me
        //    var reportViewModel = new ReportViewModel()
        //    {
        //        FileName = "~/Reports/PendingFollowUpList.rdlc",
        //        ReportDoj = DateTime.Now,
        //        Format = ReportViewModel.ReportFormat.PDF,
        //        ViewAsAttachment = false,
        //    };
        //    //  adding the dataset information to the report view model object
        //    //reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = pendingftypes.ToList(), DatasetName = "DataSet1" });
        //    reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = pendingfollowupByListdatset.ToList(), DatasetName = "DataSet2" });
        //    return reportViewModel;
        //}

        public ReportViewModel GetPendingFollowUpList(string enqtype, int branch, int usr, string date2, string date3)
        {
            DataTable dt = new Status().GetEnqFollowList(enqtype, branch, usr, date2, date3);


            //string[] ftpe = { "MokTest", "IELTS", "VISA" };
            //var branches = db.MaBranches.Where(w => w.Id == branch).ToList();

            //var pendingftypes = new List<object>();
            //Bean b = new Bean();
            //st.ByBranch = true;

            //st.ByBranch = false;

            var pendingfollowupByListdatset = new List<object>();


            // This loop is adding pending enquiry into the helper FollowupEnqList
            foreach (DataRow dr in dt.Rows)
            {
                pendingfollowupByListdatset.Add(new FollowUpEnqList()
                {

                    Id = Convert.ToInt32(dr["Id"]),
                    FirstName = dr["FirstName"].ToString(),
                    LastName = dr["LastName"].ToString(),
                    EnqHandledBy = Convert.ToString(dr["fullname"]),
                    nextdate = Convert.ToDateTime(dr["nxdt"]),
                    CreateDate = Convert.ToDateTime(dr["cdate"]),
                    EnqAbt = dr["EnqAbt"].ToString(),
                    Mobile = dr["Mobile"] == null ? "" : dr["Mobile"].ToString(),
                    Enqstatus = dr["Enqstatus"].ToString(),
                    FatherNo = dr["alternateno"].ToString(),
                    Remarks = dr["fremarks"].ToString()
                });
            }

            // This loop is adding pending mock list into the helper FollowupEnqList

            DataTable dt1 = new Status().GetPendingMockList(enqtype, branch, usr, date2, date3);
            foreach (DataRow dr in dt1.Rows)
            {
                pendingfollowupByListdatset.Add(new FollowUpEnqList()
                {
                    Id = Convert.ToInt32(dr["Id"]),
                    FirstName = dr["FirstName"].ToString(),
                    LastName = dr["LastName"].ToString(),
                    EnqHandledBy = Convert.ToString(dr["EnqFollowBy"]),
                    nextdate = Convert.ToDateTime(dr["nxdt"]),
                    CreateDate = Convert.ToDateTime(dr["cdate"]),
                    EnqAbt = "Mock Test",
                    Mobile = dr["Mobile"].ToString(),
                    Enqstatus = dr["Enqstatus"].ToString(),
                    // FatherNo = dr["alternateno"].ToString(),
                    Remarks = dr["fremarks"].ToString()
                });
            }

            // This loop is adding pending IeltsList into the helper FollowupEnqList

            DataTable dt2 = new Status().GetPendingIELTSList(branch, usr, date2, date3);
            foreach (DataRow dr in dt2.Rows)
            {
                pendingfollowupByListdatset.Add(new FollowUpEnqList()
                {
                    Id = Convert.ToInt32(dr["Id"]),
                    FirstName = dr["studentname"].ToString(),
                    //  LastName = dr["LastName"].ToString(),
                    EnqHandledBy = Convert.ToString(dr["fullname"]),
                    nextdate = Convert.ToDateTime(dr["nxdt"]),
                    CreateDate = Convert.ToDateTime(dr["cdate"]),
                    EnqAbt = "IELTS Test",
                    Mobile = dr["MobileNo"].ToString(),
                    Enqstatus = dr["Enqstatus"].ToString(),
                    // FatherNo = dr["alternateno"].ToString(),
                    Remarks = dr["remarks"].ToString()
                });
            }

            // This loop is adding pending Visa list into the helper FollowupEnqList

            List<PendingVisaStudentBean> lstenqfollow = new List<PendingVisaStudentBean>();

            List<Tb_VisaFollowUp> _lstfollow = new List<Tb_VisaFollowUp>();

            DateTime ffdate = Convert.ToDateTime(date2);
            DateTime ttdate = Convert.ToDateTime(date3);
            //string userr = db.Users.Where(a => a.Id == usr).Select(a => a.Fullname).FirstOrDefault();
            string userr = Convert.ToString(usr);
            _lstfollow = db.Tb_VisaFollowUp.Where(z => z.NextFollowdate > ffdate && z.NextFollowdate < ttdate && z.FollowStatus == "Open" && z.Branchid == branch && z.LastFollowBy == userr).ToList();

            foreach (Tb_VisaFollowUp obj in _lstfollow)
            {
                FollowUpEnqList fuel = new FollowUpEnqList();

                fuel.Id = obj.ReferId;
                fuel.FirstName = obj.Name;
                fuel.Mobile = obj.Mobileno;
                fuel.Remarks = obj.Remarks;
                fuel.Enqstatus = obj.FollowStatus;
                if (obj.Followtype == "MockTest" || obj.Followtype == "VisaMockTest")
                {
                    fuel.EnqAbt = CommonUtil.AdmissionMcktst;
                }

                else if (obj.Followtype == "VisaEnquiry")
                {
                    fuel.EnqAbt = CommonUtil.Enquiry;

                }
                else if (obj.Followtype == "IELTS" || obj.Followtype == "VisaIELTS")
                {
                    fuel.EnqAbt = CommonUtil.EnquiryIELTS;
                }
                else if (obj.Followtype == "VisaIELTSAdmission")
                {
                    fuel.EnqAbt = CommonUtil.ProspStudentsIELTS;
                }

                int lastfollowid = Convert.ToInt32(obj.LastFollowBy);
                string enqhandle = db.Users.Where(a => a.Id == lastfollowid).Select(a => a.Fullname).FirstOrDefault();
                fuel.EnqHandledBy = enqhandle;
                fuel.nextdate = obj.NextFollowdate;
                pendingfollowupByListdatset.Add(fuel);
            }




            //  Assuming the person printing the report is me
            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/PendingFollowUpList.rdlc",
                ReportDoj = DateTime.Now,
                Format = ReportViewModel.ReportFormat.PDF,
                ViewAsAttachment = false,
            };
            //  adding the dataset information to the report view model object
            //reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = pendingftypes.ToList(), DatasetName = "DataSet1" });
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = pendingfollowupByListdatset.ToList(), DatasetName = "DataSet2" });
            return reportViewModel;
        }


        #endregion




        #region demo conversion start here
        public List<democonversionbean> Stubdemoconversionreport()
        {
            return null;
        }



        public ReportViewModel Getdemoconversion(string dt1 = "", string dt2 = "", string type = "", string groupchoice = "", string Printtype = "", string Status = "")
        {

            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            DateTime? fdate = null;
            DateTime? tdate = null;
            string conversiontype1 = "";
            if (dt1 != "")
            {
                try
                {
                    fdate = Convert.ToDateTime(dt1);

                }
                catch (Exception ex)
                {
                    fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                }
            }

            if (dt2 != "")
            {
                try
                {
                    tdate = Convert.ToDateTime(dt2);
                }
                catch (Exception ex)
                {
                    tdate = DateTime.Now;
                }
            }
            //List<MaEnquiry> _lst = db.MaEnquiries.Where(z => z.DemoFeeDate >= fdate && z.Cdate <= tdate && z.DemoFeeAmt > 0 && z.Demobranchid == branchid).ToList();
            List<MaEnquiry> _lst = db.MaEnquiries.Where(z => (z.ClassDate >= fdate && z.ClassDate <= tdate) && z.DemoFeeAmt > 0 && z.Demobranchid == branchid).ToList();

            if (type != "")
            {
                if (type == "Converted")
                {
                    _lst = _lst.Where(z => z.IsAdmission == true).ToList();

                }
                if (type == "UnConverted")
                {
                    _lst = _lst.Where(z => z.IsAdmission == false).ToList();

                }
                if (type == "All")
                {
                    _lst = _lst.ToList();

                }

            }

            if (groupchoice != "All")
            {
                //_lst = _lst.Where(z => z.GroupChoice.Contains(groupchoice)).ToList();
                _lst = _lst.Where(z => z.GroupChoice == groupchoice).ToList();

            }

            var democonversiondatset = new List<democonversionbean>();

            foreach (var obj in _lst)
            {

                int grouptme = Convert.ToInt32(obj.Grouptime);
                string exactguptme = "";
                int totaldemoconverted = 0;
                int totalConvertedNot = 0;
                if (db.MaGroupTime_View.Where(z => z.GroupName == obj.GroupChoice && z.Timeid == grouptme).Select(z => z.BatchTime).Count() > 0)
                {
                    exactguptme = db.MaGroupTime_View.Where(z => z.GroupName == obj.GroupChoice && z.Timeid == grouptme).Select(z => z.BatchTime).First();
                }

                totaldemoconverted = _lst.Where(z => z.IsAdmission == true).Count();
                totalConvertedNot = _lst.Where(z => z.IsAdmission == false).Count();

                //int _convertcount = _lst.Where(z => z.Id == obj.Id && z.IsAdmission == true).Count();
                //if (_convertcount > 0)
                //{
                //    conversiontype1 = "Converted";
                //}
                //int _nonconvertcount = _lst.Where(z => z.Id == obj.Id && z.IsAdmission == false).Count();
                //if (_nonconvertcount > 0)
                //{
                //    conversiontype1 = "Non-Converted";
                //}

                string admisssiondate = string.Format("{0:dd MMM,yyyy}", db.MaAdmissions.Where(z => z.EnquiryId == obj.Id).Select(z => z.Cdate).FirstOrDefault());
                if (admisssiondate == "")
                {
                    conversiontype1 = "Non-Converted";
                }
                else
                {
                    conversiontype1 = "Converted";
                }

                democonversiondatset.Add(new democonversionbean()
                {
                    DemoDate = obj.ClassDate,
                    EnquiryNo = obj.Id,
                    GroupChoice = obj.GroupChoice,
                    GroupTime = exactguptme,
                    Mobile = obj.Mobile,
                    Name = obj.FirstName + " " + obj.LastName,
                    totaldemoconverted = totaldemoconverted,
                    ConvertedNot = totalConvertedNot,
                    lastremarks = db.MaEnqFollows.Where(z => z.ReferId == obj.Id).Where(z => z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).Select(z => z.Remarks).FirstOrDefault(),
                    lastFollowdate = string.Format("{0:dd MMM,yyyy}", db.MaEnqFollows.Where(z => z.ReferId == obj.Id).Where(z => z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).Select(z => z.CDate).FirstOrDefault()),
                    AdmisnDate = string.Format("{0:dd MMM,yyyy}", db.MaAdmissions.Where(z => z.EnquiryId == obj.Id).Select(z => z.Cdate).FirstOrDefault()),

                    Status = db.MaEnqFollows.Where(z => z.ReferId == obj.Id).Where(z => z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).Select(z => z.Enqstatus).FirstOrDefault(),
                    conversiontype = conversiontype1,
                });
            }

            //var democonversionList = democonversiondatset.ToList();


            if (Status != "All")
            {
                democonversiondatset = democonversiondatset.Where(z => z.Status == Status).OrderBy(x => x.DemoDate).ToList();
            }


            var DemoFinalList = new List<object>();

            var Dmo = (from obj in democonversiondatset select obj);





            DemoFinalList.AddRange(Dmo.OrderBy(x => x.DemoDate));



            //Assuming the person printing the report is me


            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/DemoConversionReport.rdlc",

                LeftMainTitle = "ABC Company Name",
                LeftSubTitle = Convert.ToDateTime(fdate).ToShortDateString(),
                RightMainTitle = Convert.ToDateTime(tdate).ToShortDateString(),
                RightSubTitle = string.Format("{0:dd MMM,yyyy hh:mm tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference)),
                Name = "Statistical Report",
                ReportDate = DateTime.Now,

                ConvertedCount = Dmo.Where(a => a.conversiontype == "Converted").Count().ToString(),
                UnConvertedCount = Dmo.Where(a => a.conversiontype == "Non-Converted").Count().ToString(),

                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Student Leave",
                ReportLanguage = "en-US",
                UserNamPrinting = "aa",
                ReportDoj = DateTime.Now,
                //  Format = ReportViewModel.ReportFormat.PDF,
                Format = Printtype == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = DemoFinalList.ToList(), DatasetName = "DataSet1" });

            return reportViewModel;

        }


        #endregion


        #region closed reson start here
        public List<closedresonbean> Stubclosereasonreport()
        {
            return null;
        }

        public ReportViewModel GetClosedreasoncountList(string type = "%")
        {
            var closedreasondatset = new List<object>();

            DataTable dt = new Status().GetRemarks(type);

            foreach (DataRow dr in dt.Rows)
            {
                string name = "";
                closedreasondatset.Add(new closedresonbean()
                {
                    Enquiryfollwcomment = dr["remarks"].ToString(),
                    Enquiryfollwcount = Convert.ToInt32(dr["rcount"]),
                    followuptype = dr["followuptype"].ToString(),
                });

            }
            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/ClosedReasonReport.rdlc",
                ReportDoj = DateTime.Now,
                Format = ReportViewModel.ReportFormat.PDF,
                ViewAsAttachment = false,

            };
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = closedreasondatset.ToList(), DatasetName = "DataSet1" });

            return reportViewModel;

        }


        #endregion




        #region compare score report start here
        public List<CompareScorereport> StubCompareScorereport()
        {
            return null;
        }

        public List<CompareScorereport> StubMockTestScorereport()
        {
            return null;
        }

        public ReportViewModel2 GetCompareScoreList(string dt1 = "", string dt2 = "", string stuid = "")
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            List<CompareScorereport> _lstcomparescore = st.GetScores().OrderByDescending(z => z.TestDate).ToList();
            if (stuid != "")
            {
                int admissionid = Convert.ToInt32(stuid);
                _lstcomparescore = _lstcomparescore.Where(z => z.StudentId == admissionid).ToList();
            }
            DateTime? fdate = null;
            DateTime? tdate = null;
            try
            {
                if (dt1 != "" && dt2 != "")
                {
                    fdate = Convert.ToDateTime(dt1);
                    tdate = Convert.ToDateTime(dt2);
                    _lstcomparescore = _lstcomparescore.Where(z => z.TestDate >= fdate && z.TestDate <= tdate).OrderBy(z => z.firstname).ThenBy(z => z.TestDate).ToList();
                }
            }
            catch (Exception ex)
            { }
            var comparescore = new List<object>();
            foreach (CompareScorereport obj in _lstcomparescore)
            {
                comparescore.Add(new CompareScorereport()
                {
                    CDate = obj.CDate,
                    TestDate = obj.TestDate,
                    LBand = obj.LBand,
                    firstname = obj.firstname,
                    Overall = obj.Overall,
                    RBand = obj.RBand,
                    RollNo = obj.RollNo,
                    SBand = obj.SBand,
                    testtype = obj.testtype,
                    WBand = obj.WBand,
                    StudentId = obj.StudentId
                });
            }

            //this is for mocktest

            var reportViewModel = new ReportViewModel2()
            {
                FileName = "~/Reports/CompareScoreReport.rdlc",
                Format = ReportViewModel2.ReportFormat.PDF,
                ViewAsAttachment = false,
            };


            List<CompareScorereport> _lstmocktstscore = new List<CompareScorereport>();
            _lstmocktstscore = st.GetMockTestScores().OrderByDescending(z => z.TestDate).ToList();

            var mocktstscore = new List<object>();
            foreach (CompareScorereport obj in _lstmocktstscore)
            {
                mocktstscore.Add(new CompareScorereport()
                {
                    CDate = obj.CDate,
                    TestDate = obj.TestDate,
                    LBand = obj.LBand,
                    Overall = obj.Overall,
                    RBand = obj.RBand,
                    SBand = obj.SBand,
                    WBand = obj.WBand,
                    StudentId = obj.StudentId,
                    testtype = obj.testtype
                });
            }

            reportViewModel.ReportDataSets.Add(new ReportViewModel2.ReportDataSet() { DataSetData = comparescore.ToList(), DatasetName = "DataSet1" });
            reportViewModel.ReportDataSets.Add(new ReportViewModel2.ReportDataSet() { DataSetData = mocktstscore.ToList(), DatasetName = "DataSet1" });


            return reportViewModel;

        }

        #region Left Student
        public ReportViewModel2 GetLeftStudentList(string dt1 = "", string dt2 = "", string stuid = "")
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            List<CompareScorereport> _lstcomparescore = st.GetScores().OrderByDescending(z => z.TestDate).ToList();


            if (stuid != "")
            {
                int admissionid = Convert.ToInt32(stuid);
                _lstcomparescore = _lstcomparescore.Where(z => z.StudentId == admissionid).ToList();
            }


            DateTime? fdate = null;
            DateTime? tdate = null;
            try
            {
                if (dt1 != "" && dt2 != "")
                {
                    fdate = Convert.ToDateTime(dt1);
                    tdate = Convert.ToDateTime(dt2);
                    _lstcomparescore = _lstcomparescore.Where(z => z.TestDate >= fdate && z.TestDate <= tdate).OrderBy(z => z.firstname).ThenBy(z => z.TestDate).ToList();
                }
            }
            catch (Exception ex)
            { }


            var comparescore = new List<object>();
            foreach (CompareScorereport obj in _lstcomparescore)
            {
                comparescore.Add(new CompareScorereport()
                {
                    CDate = obj.CDate,
                    TestDate = obj.TestDate,
                    LBand = obj.LBand,
                    firstname = obj.firstname,
                    Overall = obj.Overall,
                    RBand = obj.RBand,
                    RollNo = obj.RollNo,
                    SBand = obj.SBand,
                    testtype = obj.testtype,
                    WBand = obj.WBand,
                    StudentId = obj.StudentId
                });
            }

            //this is for mocktest

            var reportViewModel = new ReportViewModel2()
            {
                FileName = "~/Reports/CompareScoreReport.rdlc",
                Format = ReportViewModel2.ReportFormat.PDF,
                ViewAsAttachment = false,
            };


            List<CompareScorereport> _lstmocktstscore = new List<CompareScorereport>();
            _lstmocktstscore = st.GetMockTestScores().OrderByDescending(z => z.TestDate).ToList();

            var mocktstscore = new List<object>();
            foreach (CompareScorereport obj in _lstmocktstscore)
            {
                mocktstscore.Add(new CompareScorereport()
                {
                    CDate = obj.CDate,
                    TestDate = obj.TestDate,
                    LBand = obj.LBand,
                    Overall = obj.Overall,
                    RBand = obj.RBand,
                    SBand = obj.SBand,
                    WBand = obj.WBand,
                    StudentId = obj.StudentId,
                    testtype = obj.testtype
                });
            }

            reportViewModel.ReportDataSets.Add(new ReportViewModel2.ReportDataSet() { DataSetData = comparescore.ToList(), DatasetName = "DataSet1" });
            reportViewModel.ReportDataSets.Add(new ReportViewModel2.ReportDataSet() { DataSetData = mocktstscore.ToList(), DatasetName = "DataSet1" });


            return reportViewModel;

        }
        #endregion


        #endregion combine score report end here

        #region Enquiry analysis report
        public List<EnquiryAnalysisHelper> StubForEnquiryAnanlysisDataSet()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetEnquiryAnanlysis(List<MaEnquiry> lstenquiry, string fdate, string tdate, string format)
        {

            var enquiryanalysisdatset = new List<object>();

            foreach (var obj in lstenquiry)
            {
                string area = db.Ma_Area.Where(a => a.Id == obj.AreaId).Select(a => a.Area).FirstOrDefault();

            

                string status = "";

                if (db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                {
                    status = db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).Select(z => z.Enqstatus).First();
                }

                int _countremark = db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).Select(z => z.Remarks).Count();

                string remarks = "";

                if (_countremark > 0)
                {
                    remarks = db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).Select(z => z.Remarks).First();
                }

                string analysis = "UnConverted";
                if (db.MaAdmissions.Where(z => z.EnquiryId == obj.Id).Count() > 0)
                {
                    analysis = "Converted";
                }
                else
                {
                    analysis = "UnConverted";
                }

                enquiryanalysisdatset.Add(new EnquiryAnalysisHelper()
                {
                    Enqno = obj.Id,
                    Name = obj.FirstName + obj.LastName,
                    // Enqabout = enqab,
                    area = area,
                    Enqabout = obj.EnqAbt,
                    Mobile = obj.Mobile,
                    enqdate = obj.WalkInDate,
                    enqstatus = status,
                    enqhandleby = obj.User1.Fullname,
                    remarks = remarks,
                    status = analysis,
                    Enqstatus = obj.FEnqstatus,
                    source = obj.Source,
                    WalkinType = obj.WalkInType,
                    altNo = obj.AlternateNo,
                    othrsource = obj.Other,
                    PinCode = obj.PinCode,
                    Address = obj.Address,
                    EnqType = obj.EnqType,
                    Folldate = obj.FNextDate,
                    VisaType = obj.VisaType == null ? "-" : obj.VisaType,
                    CloseReason =  db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "Enquiry" && z.CloseReason != null).OrderByDescending(z => z.Id).Select(z => z.CloseReason).FirstOrDefault(),
                    
            });
            }

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/EnquiryEnalysisReport.rdlc",
                RightSubTitle = string.Format("{0:dd MMM,yyyy hh:mm tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference)),
                Name = "Statistical Report",
                ReportDoj = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date,
                ReportLogo = "~/Content/logo.jpg",
                ReportTitle = "Fee Reciept",
                ReportLanguage = "en-US",
                //UserNamPrinting = usrname,
                // ReportDoj = tdate,

                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };

            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = enquiryanalysisdatset.ToList(), DatasetName = "DataSet1" });

            return reportViewModel;

        }
        #endregion



        #region IELTS Report
        public List<IELTReportSHelper> StubIELTSReportList()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetIELTSReportList(string dt1 = "", string dt2 = "", string format = "", string Ielts = "", string Branch = "", string Score = "", string filteron = "")
        {
            int branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[CommonUtil.CookieBranchid].Value);
            string UserType = HttpContext.Current.Request.Cookies[CommonUtil.CookieUsertype].Value;
            DateTime? fdate = null;
            DateTime? tdate = null;
            List<Tb_IELTSResult> _lstielts = new List<Tb_IELTSResult>();
            try
            {
                if (dt1 != "" && dt2 != "")
                {
                    fdate = Convert.ToDateTime(dt1);
                    tdate = Convert.ToDateTime(dt2).AddHours(23);

                    if (filteron.ToLower().Equals("teston"))
                        _lstielts = db.Tb_IELTSResult.Where(z => z.TestDate >= fdate && z.TestDate <= tdate).ToList();
                    else
                        _lstielts = db.Tb_IELTSResult.Where(z => z.BookedTime >= fdate && z.BookedTime <= tdate).ToList();
                }
            }
            catch (Exception ex)
            { }


            if (Branch == "")
            {

                _lstielts = _lstielts.Where(z => z.StudentId > 0).OrderByDescending(z => z.Id).ToList();
            }
            else
            {
                int BId = Convert.ToInt32(Branch);
                _lstielts = _lstielts.Where(z => z.BranchId == BId).Where(z => z.StudentId > 0).OrderByDescending(z => z.Id).ToList();
            }


            if (Ielts == "PTE")
            {
                _lstielts = _lstielts.Where(z => z.StudentId > 0).Where(a => (a.ExamBody == null ? "PTE" : a.ExamBody) == "PTE").OrderByDescending(z => z.Id).ToList();
            }
            else if (Ielts == "IELTS")
            {
                _lstielts = _lstielts.Where(z => z.StudentId > 0).Where(a => (a.ExamBody == null ? "IELTS" : a.ExamBody) != "PTE").OrderByDescending(z => z.Id).ToList();
            }
            else
            {
                _lstielts = _lstielts.Where(z => z.StudentId > 0).OrderByDescending(z => z.Id).ToList();

            }
            if (Score != "")
            {
                if (Score == "All")
                {
                    //lst = lst.ToList();
                    _lstielts = _lstielts.ToList();
                }
                else
                {
                    if (Score == "A5")
                    {
                        _lstielts = _lstielts.Where(x => x.Overall >= 6).ToList();
                    }
                    if (Score == "B5")
                    {
                        _lstielts = _lstielts.Where(x => x.Overall <= 5.5).ToList();
                    }
                }
            }


            var IElTSdataset = new List<object>();

            List<CompareScorereport> _lstmocktstscore = new List<CompareScorereport>();
            _lstmocktstscore = st.GetMockTestScores().OrderByDescending(z => z.TestDate).ToList();

            CompareScorereport objcompare = new CompareScorereport();

            foreach (var obj in _lstielts)
            {

                objcompare = _lstmocktstscore.Where(a => a.StudentId == obj.StudentId).FirstOrDefault();
                if (objcompare == null)
                {
                    objcompare = new CompareScorereport();
                }

                DateTime? dt = null;
                if (obj.StudentType == "Admission")
                {
                    MaAdmission objadmission = db.MaAdmissions.Where(z => z.Id == obj.StudentId).FirstOrDefault();
                    if (objadmission != null)
                    {
                        string usr = db.Users.Where(x => x.Id == objadmission.Afollowupby).Select(x => x.Fullname).FirstOrDefault();
                        MaEnqFollow objenqfollow = db.MaEnqFollows.Where(z => z.ReferId == obj.Id).OrderByDescending(z => z.Id).FirstOrDefault();
                        string booktype = "";
                        try
                        {
                            booktype = obj.St_BookType.BookingType;
                        }
                        catch (Exception)
                        {
                        }
                        Tb_CourseHist objhist = new Tb_CourseHist();
                        try
                        {
                            objhist = db.Tb_CourseHist.Where(z => z.AdmissionId == objadmission.Id && (z.MaCourse.CourseName == "IELTS" || z.MaCourse.CourseName == "ENDEAVOUR" || z.MaCourse.CourseName == "PTE")).OrderByDescending(z => z.Id).FirstOrDefault();
                        }
                        catch (Exception ex)
                        { }

                        try
                        {
                            string lft = "";
                            string Brnch = "";
                            if (objadmission.IsLeft == true)
                            {
                                lft = "Left";
                            }
                            try
                            {
                                Brnch = db.MaBranches.Find(obj.BranchId).Code;
                            }
                            catch (Exception)
                            {

                            }

                            IElTSdataset.Add(new IELTReportSHelper()
                            {
                                Name = objadmission.FirstName + " " + objadmission.LastName,
                                Passport = obj.Passport,
                                ExamBdy = obj.ExamBody,
                                Listening = obj.LBand,
                                Reading = obj.RBand,
                                Overall = obj.Overall,
                                Speaking = obj.SBand,
                                Writing = obj.WBand,
                                ReferenceNo = objadmission.RollNo,
                                MobileNo = objadmission.Mobile,
                                currenrcourselvl = objhist == null ? "" : objhist.currentcourselevel,
                                CurntCoursTmng = objhist == null ? "" : objhist.MaTime.TCode + "-" + objhist.MaTime.TimeIn + " " + objhist.MaTime.Indesc + "-" + objhist.MaTime.TimeOut + " " + objhist.MaTime.OutDesc,
                                Category = obj.Category,
                                AlternteNo = objadmission.AlternateNo,
                                Remarks = objadmission.Afremarks,
                                Remarks2 = obj.Remarks,
                                testdate = obj.TestDate,
                                IsLeft = lft,
                                BookingType = booktype,
                                Institute = obj.Institute,
                                Branch = Brnch,
                                Booked = obj.BookedDBO,
                                detail1 = obj.Detal1,
                                detail2 = obj.Detail2,
                                ExamBooked = obj.BookedTime,
                                ResUpdt = obj.ResUpdTime,
                                MtDate = objcompare.TestDate == null ? null : objcompare.TestDate,
                                MTListening = objcompare.LBand,
                                MTReading = objcompare.RBand,
                                MTSpeaking = objcompare.SBand,
                                MTOverall = objcompare.Overall,
                                MTWriting = objcompare.WBand,
                                MTCourseLevel = objcompare.CurrentCourseLevel == null ? "" : objcompare.CurrentCourseLevel,
                                Followupby = usr

                            });

                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                else
                {
                    try
                    {
                        MaEnquiry objenquiry = db.MaEnquiries.Where(z => z.Id == obj.StudentId).FirstOrDefault();

                        MaEnqFollow objenqfollow = db.MaEnqFollows.Where(z => z.ReferId == obj.Id).OrderByDescending(z => z.Id).FirstOrDefault();
                        string usr = db.Users.Where(x => x.Id == objenqfollow.EnqFollowBy).Select(x => x.Fullname).FirstOrDefault();
                        string Brnch = "";
                        if (obj.BranchId == 19)
                        {
                            Brnch = "MTE";
                        }
                        if (obj.BranchId == 20)
                        {
                            Brnch = "HO";
                        }
                        if (obj.BranchId == 21)
                        {
                            Brnch = "MUL";
                        }
                        IElTSdataset.Add(new IELTReportSHelper()
                        {

                            Name = objenquiry.FirstName + " " + objenquiry.LastName,
                            ExamBdy = obj.ExamBody,
                            Passport = obj.Passport,
                            Listening = obj.LBand,
                            Reading = obj.RBand,
                            Overall = obj.Overall,
                            Speaking = obj.SBand,
                            Institute = obj.Institute,
                            Branch = Brnch,
                            //BranchOf = objenquiry.MaBranch.BramchName,
                            Booked = obj.BookedDBO,
                            detail1 = obj.Detal1,
                            detail2 = obj.Detail2,
                            ExamBooked = obj.BookedTime,
                            ResUpdt = obj.ResUpdTime,
                            BookingType = obj.St_BookType == null ? "" : obj.St_BookType.BookingType,
                            Writing = obj.WBand,
                            ReferenceNo = objenquiry.Id,
                            MobileNo = objenquiry.Mobile,
                            CurntCoursTmng = obj.TimeId,
                            Category = obj.Category,
                            AlternteNo = objenquiry.AlternateNo,
                            Remarks = objenqfollow.Remarks,
                            testdate = obj.TestDate,
                            currenrcourselvl = obj.CourseId == null ? "" : obj.CourseId,
                            Followupby = usr
                        });
                    }
                    catch (Exception ex)
                    { }

                }

            }

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/IELTSReport.rdlc",
                //Name = "IELTS Report",
                ReportDoj = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date,
                Format = format == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };

            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = IElTSdataset.ToList(), DatasetName = "DataSet1" });

            return reportViewModel;

        }

        #endregion

        #region MockTest_Report
        public List<MockTestHelper> StubForMockTestList()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetMockTestList(List<Tb_MockTestHelper> Tb_mock, string Form)
        {
            var Mocktestlist = new List<object>();
            string cousrsetime = "";
            foreach (Tb_MockTestHelper obj in Tb_mock)
            {
                string Counsel = db.Users.Where(x => x.Id == obj.Followupby).Select(x => x.Fullname).FirstOrDefault();
                Mocktestlist.Add(new MockTestHelper()
                {
                    Roll = obj.Rollno,
                    Name = obj.Name,
                    Mobile = obj.Phoneno,
                    Cat = obj.Categ,
                    MockDate = obj.Mocktestdate,
                    Overall = obj.Overallscore,
                    NextFDate = obj.NextfollwDate,
                    Courselvl = obj.Currentcrselvl,
                    FRemarks = obj.FollowRemarks,
                    Enqstatus = obj.EnqStatus,
                    TestDate = obj.TestDate,
                    CourseTime = obj.TimeId,
                    IeltsTestDate = obj.IeltsTestDate,
                    FirstFollowupby = Counsel,
                    IsLeft = obj.IsLeft


                });

            }

            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/MockTestReport.rdlc",
                ReportDoj = DateTime.Now.AddMinutes(StaticLogic.TimeDifference),
                //Format = ReportViewModel.ReportFormat.PDF,
                Format = Form == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = Mocktestlist.ToList(), DatasetName = "DataSet1" });



            return reportViewModel;

        }
        #endregion


        #region Without Fee Receipt Report
        public List<WithoutFeeHelper> StubForWithoutFeeList()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetWithoutFeeList(List<AdmWithoutFeeHelper> maadd, string Form)
        {
            var WithoutFeelist = new List<object>();
            foreach (AdmWithoutFeeHelper obj in maadd)
            {
                WithoutFeelist.Add(new WithoutFeeHelper()
                {
                    Roll = obj.Roll,
                    Name = obj.Name,
                    Mobile = obj.Mobile,
                    AltNum = obj.AltNum,
                    EnquiryNum = obj.EnquiryNum,
                    Enqabout = obj.Enqabout,
                    Enqdate = obj.Enqdate,
                    Remarks = obj.Remarks,
                    Cdate = obj.Cdate,
                    LDate = obj.LDate,
                    Counsellor = obj.Counsellor
                });
            }

            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/WithoutFeeReceipt.rdlc",
                ReportDoj = DateTime.Now.AddMinutes(StaticLogic.TimeDifference),
                //Format = ReportViewModel.ReportFormat.PDF,
                Format = Form == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = WithoutFeelist.ToList(), DatasetName = "DataSet1" });



            return reportViewModel;

        }
        #endregion

        #region Transfer Student Report
        public List<TransferStudentHelper> StubTransferStudent()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetTransferStudent(List<Tb_StudentTransfer> maadd, string Form)
        {
            var StudentTranfer = new List<object>();
            foreach (Tb_StudentTransfer obj in maadd)
            {
                StudentTranfer.Add(new TransferStudentHelper()
                {
                    OldRoll = obj.OldRollNo,
                    NewRoll = obj.MaAdmission.RollNo,
                    Name = obj.MaAdmission.FirstName + " " + obj.MaAdmission.LastName,
                    TransferTo = obj.User.Fullname,
                    TransferBy = obj.User1.Fullname,
                    BranchFrom = obj.MaBranch1.BramchName,
                    BranchTo = obj.MaBranch.BramchName,
                    TDate = obj.TransferDate,
                    ApStatus = obj.Status == true ? "Y" : "N",
                    AcptDate = obj.AcctDate

                });
            }

            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/TransferStudent.rdlc",
                ReportDoj = DateTime.Now.AddMinutes(StaticLogic.TimeDifference),
                //Format = ReportViewModel.ReportFormat.PDF,
                Format = Form == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = StudentTranfer.ToList(), DatasetName = "DataSet1" });



            return reportViewModel;

        }
        #endregion

        #region DoubleEntry
        public List<doubleEntryEnq> StubForDoubleEntryenq()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetDoubleEntryEnq(string dt1 = "", string dt2 = "", string branch = "", string user = "")
        {
            DateTime fdt = Convert.ToDateTime(dt1);
            DateTime tdt = Convert.ToDateTime(dt2);
            var Enq = db.Database.SqlQuery<MaEnquiry>($"select * from maenquiry A where id not in (select min(id) from maenquiry b where a.FirstName = b.FirstName and a.mobile = b.mobile and a.fathername = b.fathername  group by FirstName, Mobile, fathername)").ToList();
            List<MaEnquiry> list = Enq.ToList();
            if (dt1 != "" && dt2 != "")
            {
                //list = list.Where(x => x.Cdate >= fdt && x.Cdate <= tdt).ToList();
                list = list.Where(x => (x.Cdate.Value.Day >= fdt.Day && x.Cdate.Value.Month >= fdt.Month && x.Cdate.Value.Year >= fdt.Year) && (x.Cdate.Value.Day <= tdt.Day && x.Cdate.Value.Month <= tdt.Month && x.Cdate.Value.Year <= tdt.Year)).ToList();

            }
            if (branch != "")
            {
                int bid = Convert.ToInt32(branch);
                list = list.Where(x => x.BranchId == bid).ToList();
            }
            if (user != "")
            {
                int uid = Convert.ToInt32(user);
                list = list.Where(x => x.EnqHandledBy == uid).ToList();
            }
            var DoubleEntry = new List<object>();
            foreach (MaEnquiry obj in list)
            {
                DoubleEntry.Add(new doubleEntryEnq()
                {
                    Name = obj.FirstName + " " + obj.LastName,
                    BrName = db.MaBranches.Where(x => x.Id == obj.BranchId).Select(x => x.BramchName).FirstOrDefault(),
                    AltMobile = obj.AlternateNo,
                    Mobile = obj.Mobile,
                    FName = obj.FatherName,
                    UserName = db.Users.Where(x => x.Id == obj.CounselorName).Select(x => x.Fullname).FirstOrDefault(),
                    Enqabout = obj.EnqAbt,
                    enqdate = obj.WalkInDate,
                    enqhandleby = db.Users.Where(x => x.Id == obj.EnqHandledBy).Select(x => x.Fullname).FirstOrDefault(),
                    Enqno = obj.Id,
                    enqstatus = obj.FEnqstatus

                });
            }

            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/DoubleEntry.rdlc",
                ReportDoj = DateTime.Now.AddMinutes(StaticLogic.TimeDifference),
                //Format = ReportViewModel.ReportFormat.PDF,
                Format = ReportViewModel.ReportFormat.PDF,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = DoubleEntry.ToList(), DatasetName = "DataSet1" });

            return reportViewModel;

        }
        #endregion

        #region DoubleEntry
        public List<FollowUpEnqModel> StubForFollowupCreate()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }

        public ReportViewModel GetFollowupCreate(List<FollowUpEnqModel> Follow)
        {
            var EnFollow = new List<object>();
            foreach (FollowUpEnqModel obj in Follow)
            {
                MaEnquiry enq = db.MaEnquiries.Where(z => z.Id == obj.Id).First();
                int timeid = 0;
                try
                {
                    timeid = Convert.ToInt32(obj.GroupTime);
                }
                catch (Exception)
                {
                }
                string grouptime = "";
                if (timeid > 0)
                {
                    grouptime = db.MaGroupTime_View.Where(z => z.Timeid == timeid).Select(z => z.BatchTime).First();
                }
                string rem = db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).Select(z => z.Remarks).First();
                EnFollow.Add(new FollowUpEnqModel()
                {
                    Id = obj.Id,
                    FirstName = obj.FirstName,
                    LastName = obj.LastName,
                    enqab = obj.enqab,
                    Mobile = obj.Mobile,
                    AlternateMobile = obj.AlternateMobile,
                    WalkInDate = enq.WalkInDate,
                    GroupChoice = obj.GroupChoice,
                    GroupTime = grouptime,
                    Remarks = rem,
                    Enqstatus = obj.Enqstatus,
                    Fullname = enq.User1.Fullname,
                    nxdt = obj.nxdt,
                    EnqType = obj.EnqType,
                    VisaType = obj.VisaType
                });
            }
            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";
            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/Followupbycreate.rdlc",
                ReportDoj = DateTime.Now.AddMinutes(StaticLogic.TimeDifference),
                //Format = ReportViewModel.ReportFormat.PDF,
                Format = ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = EnFollow.ToList(), DatasetName = "DataSet1" });

            return reportViewModel;

        }
        #endregion
        public List<FollowUpEnqModel> StubForPendingIeltsReport()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }
        public ReportViewModel GetPendingIeltsReport(List<FollowUpEnqModel> follow, string Print)
        {
            var pendinglist = new List<object>();
            string cousrsetime = "";
            foreach (FollowUpEnqModel obj in follow)
            {
                pendinglist.Add(new FollowUpEnqModel()
                {
                    StudentType = obj.StudentType,
                    Rollno = obj.Rollno,
                    FirstName = obj.FirstName,
                    LastName = obj.LastName,
                    enqab = obj.enqab,
                    ieltstestdate = obj.ieltstestdate,
                    Mobile = obj.Mobile,
                    overallscore = obj.overallscore,
                    nxdt = obj.nxdt,
                    Remarks = obj.StudentType == "Admission" ? db.MaAdmissions.Where(x => x.Id == obj.AdmissionId).Select(x => x.Afremarks).FirstOrDefault() : "",
                    Status = obj.Status
                });

            }

            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/PendingIeltsFollow.rdlc",
                ReportDoj = DateTime.Now.AddMinutes(StaticLogic.TimeDifference),
                //Format = ReportViewModel.ReportFormat.PDF,
                Format = Print == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = pendinglist.ToList(), DatasetName = "DataSet1" });
            return reportViewModel;

        }

        public List<FollowUpEnqModel> StubForPendingMocktestReport()
        {
            //this is used only to help in adding the dataset of type employee to the report definition
            return null;
        }
        public ReportViewModel GetPendingMockTestReport(List<Mockfollowup_view> follow, string Print)
        {
            var pendinglist = new List<object>();

            foreach (Mockfollowup_view obj in follow)
            {
                pendinglist.Add(new FollowUpEnqModel()
                {
                    StudentType = obj.Category,
                    Rollno = obj.RollNo,
                    FirstName = obj.FirstName,
                    LastName = obj.LastName,
                    enqab = obj.Fullname,
                    ieltstestdate = obj.IeltsDate,
                    mocktestdate = obj.TestDate,
                    Mobile = obj.Mobile,
                    nextdate = obj.nxdt,
                    overallscore = obj.Overall.ToString(),
                    nxdt = obj.nxdt,
                    Remarks = obj.Afremarks,
                    Enqstatus = obj.EnqStatus,
                    Status = obj.EnqStatus,
                    IsLeft = (obj.IsLeft == true) ? "Left" : "",
                    Counsellor = obj.Fullname,
                    RemDate = obj.Afdate,
                    TimeId = obj.TimeId


                });

            }

            //Assuming the person printing the report is me
            var UserPrinting = "Ali Taki";

            var reportViewModel = new ReportViewModel()
            {
                FileName = "~/Reports/PendingMockFollow.rdlc",
                ReportDoj = DateTime.Now.AddMinutes(StaticLogic.TimeDifference),
                //Format = ReportViewModel.ReportFormat.PDF,
                Format = Print == "PDF" ? ReportViewModel.ReportFormat.PDF : ReportViewModel.ReportFormat.Excel,
                ViewAsAttachment = false,

            };
            //adding the dataset information to the report view model object
            reportViewModel.ReportDataSets.Add(new ReportViewModel.ReportDataSet() { DataSetData = pendinglist.ToList(), DatasetName = "DataSet1" });
            return reportViewModel;

        }
    }
}