﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class FeeRecieptBean
    {

        public string BranchName { get; set; }

        public string CourseName { get; set; }

        public int RollNo { get; set; }

        public string GroupName { get; set; }

        public string PackageName { get; set; }

        public int? InstallmentNo { get; set; }

        public double FeeAmount { get; set; }

        public double Paidamount { get; set; }

        public double Discount { get; set; }

        public double BalAmount { get; set; }

        public int ReceiptNo { get; set; }

        public DateTime Feedate { get; set; }

        public string FeeTime { get; set; }

        public string remarks { get; set; }

        public DateTime duedate { get; set; }

        public double balancedueamt { get; set; }

        public string studentname { get; set; }

        public string GroupChoice { get; set; }

        public string Kitprovided { get; set; }
        public string Pay1 { get; set; }
        public string Pay2 { get; set; }
        public double? CashAmt { get; set; }
        public double? PMAmt1{ get; set; }
        public double? PMAmt2 { get; set; }
        public string Remarks1 { get; set; }
        public string Remarks2 { get; set; }
        public string Remarks3 { get; set; }
        public DateTime Doj { get; set; }


    }
}