﻿namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    public class Tb_VisaPayment
    {
        public int Id { get; set; }
        
        public Nullable<int> StudentId { get; set; }

        [Display(Name = "Student Type")]
        public string StudentType { get; set; }

        [Display(Name = "Amount")]
        [Required(ErrorMessage="*")]
        public Nullable<double> AmountPaid { get; set; }

        [Display(Name = "Payment Mode")]
        public string PaymentMode { get; set; }

     
        [Display(Name = "Cheque/DD No")]
        public string ChequeNo { get; set; }

 
        [Display(Name = "Cheque/DD Date")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> ChequeDate { get; set; }


        [Display(Name = "Bank Name")]
        public string BankName { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> BranchId { get; set; }




        [Required(ErrorMessage = "*")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> CDate { get; set; }
        public Nullable<bool> Status { get; set; }

         [StringLength(500,ErrorMessage="Only 500 Chars")]
        public string Remarks { get; set; }

        public string EnqType { get; set; }  //extra property to check for Enquiry or admission




        public virtual User User { get; set; }
        public virtual MaBranch MaBranch { get; set; }

    }
}