﻿
namespace SSCMvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    public class Tb_FeeRefund
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Branch")]
        public Nullable<int> BranchId { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Student")]
        public Nullable<int> StudentId { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Course")]
        public Nullable<int> CourseId { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Package")]
        public Nullable<int> PackageId { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        public Nullable<DateTime> Edate { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Amount")]
        public Nullable<double> RefundAmount { get; set; }
        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }


        [Display(Name = "Sr. No.")]
        public Nullable<int> RefundNo { get; set; }

        public virtual MaAdmission MaAdmission { get; set; }
        public virtual MaBranch MaBranch { get; set; }
        public virtual MaCourse MaCourse { get; set; }
        public virtual MaPackage MaPackage { get; set; }
    }
}