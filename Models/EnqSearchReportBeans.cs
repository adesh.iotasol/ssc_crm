﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class EnqSearchReportBeans
    {
        public int Id { get; set; }
        public string SName { get; set; }
        public string Mobile { get; set; }
        public string DayName { get; set; }
        public Nullable<DateTime> EnqDate { get; set; }
        public Nullable<DateTime> NxtFollowUp { get; set; }
        public string TimeSlot { get; set; }
        public string EnqHandleBy { get; set; }
        public string WalkinType { get; set; }
        public int? Timesloatcount { get; set; }

        public string countbyday { get; set; }

    }

    public class EnqSearchSummary1
    {
        public string TimeSlot { get; set; }
        public int? Timesloatcount { get; set; }


    }

    public class EnqSearchSummary2
    {
        public string Dayname { get; set; }
        public int? Daycount { get; set; }


    }
}