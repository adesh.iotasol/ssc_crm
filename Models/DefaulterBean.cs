﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class DefaulterBean
    {
       
        public Nullable<int> RollNo { get; set; }
        public string FullName { get; set; }
        public string GroupName { get; set; }
        public string GroupChoice { get; set; }
        public string PackageName { get; set; }
        public Nullable<DateTime> FromDate { get; set; }
        public Nullable<DateTime> Examdate { get; set; }
        //Date Of Joining
        public Nullable<Double> DueAmount { get; set; }
        public Nullable<DateTime> NextDueOn { get; set; }
        public string Mobile { get; set; }
        public DateTime LocalDate { get; set; }
    }
}