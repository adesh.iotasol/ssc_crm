﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSCMvc.Models
{
    public class IELTSHelper
    {

        public string studenttype { get; set; }
        public string studenttypeDisply { get; set; }
        public string exambody { get; set; }
        public int? ReferNo { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }

        public DateTime? IeltsTesDate { get; set; }

        public string timing { get; set; }
        public double? Listening { get; set; }
        public double? Reading { get; set; }
        public double? Writing { get; set; }
        public double? Speaking { get; set; }
        public double? OverallScore { get; set; }

        public DateTime? NextFollowUpdate { get; set; }
        public string Remarks { get; set; }

        public string EnquiryStatus
        {
            get;
            set;

        }

        public int? branchid { get; set; }

        public int? StudentId { get; set; }

        public int? Id { get; set; }

        public DateTime? TestDate { get; set; }
        public DateTime? Exambooked { get; set; }
        public DateTime? ResUpdt { get; set; }
        public double? Overall { get; set; }
        public bool isfollowbtn { get; set; }

        public int? rollnoid { get; set; }

        public string mobile { get; set; }
        public string course { get; set; }
    }
   
}