﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebForms/ReportMaster.Master" AutoEventWireup="true"
    CodeBehind="ReceiptByDatesPage.aspx.cs" Inherits="SSCMvc.WebForms.ReceiptByDatesPage" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            $("#txtFromDate").datepicker();
            $("#txtToDate").datepicker();
        });
        function validateForm() {
            var x = $("#txtFromDate").val();
            if (x == null || x == "") {
                alert("From Date must be filled out");
                return false;
            }

            var t = $("#txtToDate").val();
            if (t == null || t == "") {
                alert("To Date must be filled out");
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="header">
        <div class="stats">
        </div>
        <h1 class="page-title">
            Daily Fee Report</h1>
    </div>
    <ul class="breadcrumb">
        <li><a href="/MaAdmission/Index">Home</a> <span class="divider">/</span></li>
        <li class="active">Dashboard</li>
    </ul>
    <table width="100%">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            From Date
                        </td>
                        <td>
                            To Date
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtFromDate" runat="server" ClientIDMode="Static"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtToDate" runat="server" ClientIDMode="Static"></asp:TextBox>
                        </td>
                        <td valign="top">
                            <asp:Button ID="txtShowReport" runat="server" Text="Show Report" ClientIDMode="Static"
                                OnClick="txtShowReport_Click" CssClass="btn btn-primary" OnClientClick="validateForm();" />
                        </td>
                    </tr>
                </table>
                
            </td>
        </tr>
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
                    WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" Height="874px"
                    Visible="false">
                    <LocalReport ReportPath="Reports\FeeReportsByDates.rdlc">
                       
                    </LocalReport>
                </rsweb:ReportViewer>
                
            </td>
        </tr>
    </table>
</asp:Content>
