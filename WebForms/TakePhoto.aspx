﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Configuration" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sethi Study Circle</title>
    <script runat="server">
        public void btnSave_Click(object sender, EventArgs e)
        {
            DateTime cdate=new DateTime();
            string imgpath = "";

            if (FL_Img.HasFile)
            {
                int id = Convert.ToInt32(Request.QueryString["filenumber"]);
                SqlConnection con= new SqlConnection();
                con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;

                con.Open();
                string sql = "Select cdate from MaAdmission where Id=@id";

                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader sqlReader = cmd.ExecuteReader();

                while (sqlReader.Read())

                {
                    cdate = Convert.ToDateTime(sqlReader["Cdate"]);
                }
                sqlReader.Close();
                cmd.Dispose();
                con.Close();


                DateTime ttdate = DateTime.Parse(ConfigurationManager.AppSettings["Datee"]);
                //DateTime ccdate = cdate;
                if (cdate <= ttdate)
                {

                    imgpath = "~/Content/themes/base/images/Documents/";
                }
                else
                {
                    var year = DateTime.Now.Year;
                    imgpath = "~/Content/themes/base/images/Documents/";
                }

                if (cdate==null)
                {
                    cdate = DateTime.Now;
                }

                try
                {
                    //int id = Convert.ToInt32(Request.QueryString["filenumber"]);
                    //var year = DateTime.Now.Year;
                    //FL_Img.SaveAs(Server.MapPath("~/Content/themes/base/images/Documents/" + year + "/") + id + FL_Img.FileName);
                    FL_Img.SaveAs(Server.MapPath(imgpath)+cdate.Year+"/" + id + FL_Img.FileName);
                    SqlConnection con1 = new SqlConnection();
                    //  con = new SqlConnection();
                    con1.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
                    con1.Open();

                    cmd.CommandText = "Update MaAdmission set StudPhoto=@st where Id=@idd";
                    cmd.Parameters.AddWithValue("@st",cdate.Year+"/"+ id + FL_Img.FileName);
                    cmd.Parameters.AddWithValue("@idd", id);
                    cmd.Connection = con1;
                    cmd.ExecuteNonQuery();
                    con1.Close();
                    lblmsg.Text = "Photo Uploaded Successfully";
                }
                catch (Exception ex)
                {
                    lblmsg.Text = ex.Message;
                }
            }
        }
    </script>
    <style type="text/css">
        html, body { height: 100%; overflow: auto; }

        body { font-family: Calibri; padding: 0; margin: 0; }

        #silverlightControlHost { height: 100%; text-align: center; }

        .roundcornertable { border: 2px solid #000000; border-radius: 10px; -moz-border-radius: 10px; -ie-border-radius: 10px; -webkit-border-radius: 10px; -khtml-border-radius: 10px; -o-border-radius: 10px; padding: 10px; }

        .headingbg { background-color: #81F7F3; border-radius: 10px; -moz-border-radius: 10px; -ie-border-radius: 10px; -webkit-border-radius: 10px; -khtml-border-radius: 10px; -o-border-radius: 10px; }

        .shadowclass { border: 1px solid #cccccc; border-radius: 8px; box-shadow: 2px 2px 15px #888888; -moz-box-shadow: 2px 2px 15px #888888; -webkit-box-shadow: 2px 2px 15px #888888; -khtml-box-shadow: 2px 2px 15px #888888; -ie-box-shadow: 2px 2px 15px #888888; -o-box-shadow: 2px 2px 15px #888888; }
    </style>
    <script type="text/javascript" src="../Scripts/Silverlight.js"></script>
    <script type="text/javascript">
        function onSilverlightError(sender, args) {
            var appSource = "";
            if (sender != null && sender != 0) {
                appSource = sender.getHost().Source;
            }

            var errorType = args.ErrorType;
            var iErrorCode = args.ErrorCode;

            if (errorType == "ImageError" || errorType == "MediaError") {
                return;
            }

            var errMsg = "Unhandled Error in Silverlight Application " + appSource + "\n";

            errMsg += "Code: " + iErrorCode + "    \n";
            errMsg += "Category: " + errorType + "       \n";
            errMsg += "Message: " + args.ErrorMessage + "     \n";

            if (errorType == "ParserError") {
                errMsg += "File: " + args.xamlFile + "     \n";
                errMsg += "Line: " + args.lineNumber + "     \n";
                errMsg += "Position: " + args.charPosition + "     \n";
            }
            else if (errorType == "RuntimeError") {
                if (args.lineNumber != 0) {
                    errMsg += "Line: " + args.lineNumber + "     \n";
                    errMsg += "Position: " + args.charPosition + "     \n";
                }
                errMsg += "MethodName: " + args.methodName + "     \n";
            }

            throw new Error(errMsg);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" style="height: 100%">
        <div id="silverlightControlHost">
            <%--<object data="data:application/x-silverlight-2," type="application/x-silverlight-2"
                width="900" height="450">
                <param name="source" value="../ClientBin/BvpCaptureSnap.xap" />
                <param name="onError" value="onSilverlightError" />
                <param name="background" value="white" />
                <param name="minRuntimeVersion" value="5.0.61118.0" />
                <param name="autoUpgrade" value="true" />
                <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=5.0.61118.0" style="text-decoration: none">
                    <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight"
                        style="border-style: none" />
                </a>
            </object>--%>
            <iframe id="_sl_historyFrame" style="visibility: hidden; height: 0px; width: 0px; border: 0px"></iframe>
            <div style="padding-top:300px"></div>
            <br />
            <table align="center" class="roundcornertable">
                <tr>
                    <td>Select File:
                    </td>
                    <td>
                        <asp:FileUpload ID="FL_Img" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Upload" OnClick="btnSave_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <asp:Label ID="lblmsg" runat="server" ForeColor="Green" Font-Size="15px" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
           <%-- <table align="center" class="shadowclass">
                <tr>
                    <td align="center" class="headingbg">
                        <h2>Point To Follow</h2>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <ol>
                            <li>Take a Picture</li>
                            <li>Save Image With a Unique Name</li>
                            <li>Click On Choose File In Black Box</li>
                            <li>Select The Photo</li>
                            <li>Click On Upload Button</li>
                            <li>A Message Will Appear On Successful Upload</li>
                        </ol>
                    </td>
                </tr>
            </table>--%>
        </div>
    </form>
</body>
</html>
