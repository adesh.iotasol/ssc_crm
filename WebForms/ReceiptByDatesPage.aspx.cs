﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Configuration;
namespace SSCMvc.WebForms
{
    public partial class ReceiptByDatesPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        DataTable GetData()
        {
            SqlDataAdapter sc = new SqlDataAdapter(@"SELECT        Tb_FeeReceipt.FeeAmount, Tb_FeeReceipt.PaidAmount, Tb_FeeReceipt.ReceiptNo, Tb_FeeReceipt.FeeDate, Tb_FeeReceipt.BalanceAmount, 
                         MaAdmission.FirstName + ' ' + MaAdmission.LastName AS FullName, MaAdmission.RollNo, Tb_FeeReceipt.Remarks, MaPackage.PackageName, 
                         Tb_Installment.CourseHistId, Tb_CourseHist.TimeId, MaGroupAssign.GroupName, 
                         MaGroupAssign.TimeIn + ':' + MaGroupAssign.Indesc + ' ' + MaGroupAssign.TimeOut + ':' + MaGroupAssign.OutDesc + '-' + MaGroupAssign.TCode AS GroupChoice,
                          Tb_CourseHist.FromDate
FROM            Tb_FeeReceipt INNER JOIN
                         MaAdmission ON Tb_FeeReceipt.StudentId = MaAdmission.Id INNER JOIN
                         MaPackage ON Tb_FeeReceipt.PackageId = MaPackage.Id INNER JOIN
                         Tb_Installment ON Tb_FeeReceipt.InstallmentNoId = Tb_Installment.Id INNER JOIN
                         Tb_CourseHist ON Tb_Installment.CourseHistId = Tb_CourseHist.Id INNER JOIN
                         MaGroupAssign ON Tb_CourseHist.TimeId = MaGroupAssign.Id
WHERE        (Tb_FeeReceipt.FeeDate >= @dt) AND (Tb_FeeReceipt.FeeDate <= @dt1)", new SqlConnection(ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString));
            sc.SelectCommand.Parameters.AddWithValue("@dt", txtFromDate.Text);
            sc.SelectCommand.Parameters.AddWithValue("@dt1", txtToDate.Text);
            DataTable dt = new DataTable();
            sc.Fill(dt);
            return dt;


        }
        protected void txtShowReport_Click(object sender, EventArgs e)
        {
            ReportViewer1.Visible = true;
            //ObjectDataSource1.SelectParameters["dt"].DefaultValue = txtFromDate.Text;
            //ObjectDataSource1.SelectParameters["dt1"].DefaultValue = txtToDate.Text;
            //ObjectDataSource1.Select();
            ReportParameter[] rp = new ReportParameter[2];
            rp[0] = new ReportParameter("Fdate", txtFromDate.Text, true);
            rp[1] = new ReportParameter("Tdate", txtToDate.Text, true);

            DataTable dt = GetData();
            if (dt == null)
            {
                dt = new DataTable();
            }
            ReportDataSource rdc = new ReportDataSource("DataSet1", dt);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(rdc);
           


            //ReportViewer1.LocalReport.ReportEmbeddedResource = "SSCMvc.Reports.FeeReportsByDates.rdlc";
            ReportViewer1.LocalReport.SetParameters(rp);
            ReportViewer1.LocalReport.Refresh();
        }
    }
}