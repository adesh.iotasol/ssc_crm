﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SSCMvc.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace SSCMvc.WebForms
{
    public partial class FeeReportPage : System.Web.UI.Page
    {
        SSCWebDBEntities1 db = new SSCWebDBEntities1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    int id = Convert.ToInt32(Request.QueryString["id"]);
                    FillParameters(id);
                }
                catch (Exception ex)
                {
                }
            }
        }

        DataTable GetData()
        {
            SqlDataAdapter sc = new SqlDataAdapter(@"SELECT Tb_FeeReceipt.Id, Tb_FeeReceipt.BranchId, Tb_FeeReceipt.StudentId, Tb_FeeReceipt.CourseId, Tb_FeeReceipt.PackageId, Tb_FeeReceipt.FeeAmount, 
                         Tb_FeeReceipt.PaidAmount, Tb_FeeReceipt.Discount, Tb_FeeReceipt.BalanceAmount, Tb_FeeReceipt.ReceiptNo, Tb_FeeReceipt.FeeDate, 
                         Tb_FeeReceipt.InstallmentNoId, Tb_FeeReceipt.Remarks, Tb_FeeReceipt.DueDate, Tb_FeeReceipt.BalanceDueAmt, Tb_FeeReceipt.DiscountId, 
                         Tb_FeeReceipt.CounselorId, MaBranch.BramchName, MaAdmission.FirstName + ' ' + MaAdmission.LastName AS SName, MaCourse.CourseName, 
                         MaPackage.PackageName, Tb_Installment.InstallmentNo, MaAdmission.RollNo, Tb_CourseHist.TimeId, Tb_CourseHist.AdmissionId, 
                         Tb_CourseHist.FromDate, Tb_CourseHist.Todate, Tb_CourseHist.PackageId AS PackName, MaGroupAssign.GroupName, 
                         MaGroupAssign.TCode + ' ' + MaGroupAssign.TimeIn + ' ' + MaGroupAssign.Indesc + ' - ' + MaGroupAssign.TimeOut + ' ' + MaGroupAssign.OutDesc AS GroupChoice
FROM            Tb_FeeReceipt INNER JOIN
                         MaBranch ON Tb_FeeReceipt.BranchId = MaBranch.Id INNER JOIN
                         MaAdmission ON Tb_FeeReceipt.StudentId = MaAdmission.Id INNER JOIN
                         MaCourse ON Tb_FeeReceipt.CourseId = MaCourse.Id INNER JOIN
                         MaPackage ON Tb_FeeReceipt.PackageId = MaPackage.Id INNER JOIN
                         Tb_Installment ON Tb_FeeReceipt.InstallmentNoId = Tb_Installment.Id AND 
                         MaCourse.Id = Tb_Installment.CourseId INNER JOIN
                         Tb_CourseHist ON MaAdmission.Id = Tb_CourseHist.AdmissionId AND MaCourse.Id = Tb_CourseHist.CourseId AND 
                         MaPackage.Id = Tb_CourseHist.PackageId AND Tb_Installment.CourseHistId = Tb_CourseHist.Id INNER JOIN
                         MaGroupAssign ON MaCourse.Id = MaGroupAssign.CourseId AND Tb_CourseHist.TimeId = MaGroupAssign.Id
WHERE        (Tb_FeeReceipt.Id = @id)", new SqlConnection(ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString));
            sc.SelectCommand.Parameters.AddWithValue("@id", Request.QueryString["id"]);
            DataTable dt = new DataTable();
            sc.Fill(dt);
            return dt;


        }

        public void FillParameters(int id)
        {
            ReportParameter[] rp = new ReportParameter[3];
            int admid = (int)db.Tb_FeeReceipt.Where(z => z.Id == id).Select(z => z.StudentId).First();
            int courseid = (int)db.Tb_FeeReceipt.Where(z => z.Id == id).Select(z => z.CourseId).First();
            DateTime? dt = db.Tb_CourseHist.Where(z => z.AdmissionId == admid && z.CourseId == courseid).Select(z => z.DueDate).First();
            DateTime? dtjoin = db.Tb_CourseHist.Where(z => z.AdmissionId == admid && z.CourseId == courseid).Select(z => z.FromDate).First();
            int counselorid = (int)db.MaAdmissions.Where(z => z.Id == admid).Select(z => z.CounselorName).First();
            string cname = db.Users.Where(z => z.Id == counselorid).Select(z => z.Fullname).First();
            rp[0] = new ReportParameter("DueDate", string.Format("{0:dd MMM,yyyy}", dt), true);
            rp[1] = new ReportParameter("DOJ", dtjoin.ToString(), true);
            rp[2] = new ReportParameter("User", cname, true);
            ReportDataSource rdc = new ReportDataSource("DataSet1", GetData());
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(rdc);
            ReportViewer1.LocalReport.SetParameters(rp);
            ReportViewer1.LocalReport.Refresh();
        }

    }
}