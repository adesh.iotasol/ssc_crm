﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebForms/ReportMaster.Master" AutoEventWireup="true" CodeBehind="FeeReportPage.aspx.cs" Inherits="SSCMvc.WebForms.FeeReportPage" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div class="header">
        <div class="stats">
        </div>

        <h1 class="page-title">Fee Receipt</h1>
    </div>
    <ul class="breadcrumb">
        <li><a href="/MaAdmission/Index" >Home</a>
             <span class="divider">/</span></li>
        <li class="active">Dashboard</li>
    </ul>
    <table width="100%">
        <tr>
            <td>
            
             
                

                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" Height="874px">
                    <LocalReport  ReportPath="Reports\FeeReceiptReport.rdlc">
                       
                    </LocalReport>

                </rsweb:ReportViewer>

             
            </td>

        </tr>

    </table>
</asp:Content>
