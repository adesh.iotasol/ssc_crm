﻿
function deleterecord(pname) {
    var deleteLinkObj;
    var pagename = pname;


    // delete Link
    $('.delete-link').click(function () {

        deleteLinkObj = $(this);  //for future use

        $('#delete-dialog').dialog('open');

        return false; // prevents the default behaviour

    });


    $('#delete-dialog').dialog({

        init: function (element, options) {
            this.options = options;

            this.$element = $(element)
				.delegate('[data-dismiss="modal"]', 'click.dismiss.modal', $.proxy(this.hide, this));

            this.options.remote && this.$element.find('.modal-body').load(this.options.remote);

            var manager = typeof this.options.manager === 'function' ?
				this.options.manager.call(this) : this.options.manager;

            manager = manager.appendModal ?
                manager : $(manager).modalmanager().data('modalmanager');

            manager.appendModal(this);
        },


        toggle: function () {
            return this[!this.isShown ? 'show' : 'hide']();
        },
        show: function () {
            var e = $.Event('show');

            if (this.isShown) return;

            this.$element.trigger(e);

            if (e.isDefaultPrevented()) return;

            this.escape();

            this.tab();

            this.options.loading && this.loading();
        },

        hide: function (e) {
            e && e.preventDefault();

            e = $.Event('hide');

            this.$element.trigger(e);

            if (!this.isShown || e.isDefaultPrevented()) return (this.isShown = false);

            this.isShown = false;

            this.escape();

            this.tab();

            this.isLoading && this.loading();

            $(document).off('focusin.modal');

            this.$element
				.removeClass('in')
				.removeClass('animated')
				.removeClass(this.options.attentionAnimation)
				.removeClass('modal-overflow')
				.attr('aria-hidden', true);

            $.support.transition && this.$element.hasClass('fade') ?
				this.hideWithTransition() :
				this.hideModal();
        },



        tab: function () {
            var that = this;

            if (this.isShown && this.options.consumeTab) {
                this.$element.on('keydown.tabindex.modal', '[data-tabindex]', function (e) {
                    if (e.keyCode && e.keyCode == 9) {
                        var $next = $(this),
							$rollover = $(this);

                        that.$element.find('[data-tabindex]:enabled:not([readonly])').each(function (e) {
                            if (!e.shiftKey) {
                                $next = $next.data('tabindex') < $(this).data('tabindex') ?
									$next = $(this) :
									$rollover = $(this);
                            } else {
                                $next = $next.data('tabindex') > $(this).data('tabindex') ?
									$next = $(this) :
									$rollover = $(this);
                            }
                        });

                        $next[0] !== $(this)[0] ?
							$next.focus() : $rollover.focus();

                        e.preventDefault();
                    }
                });
            } else if (!this.isShown) {
                this.$element.off('keydown.tabindex.modal');
            }
        },

        escape: function () {
            var that = this;
            if (this.isShown && this.options.keyboard) {
                if (!this.$element.attr('tabindex')) this.$element.attr('tabindex', -1);

                this.$element.on('keyup.dismiss.modal', function (e) {
                    e.which == 27 && that.hide();
                });
            } else if (!this.isShown) {
                this.$element.off('keyup.dismiss.modal')
            }
        },

        dialogClass: 'ui-dialog-green', autoOpen: false, width: 400, resizable: false, modal: true,  //Dialog options

        buttons: {

            "Continue": function () {

                $.post(deleteLinkObj[0].href, function (data) {  //Post to action

                    if (data == "True") {

                        // $(deleteLinkObj).closest('table').parents('tr').first().hide();

                        $(deleteLinkObj).closest('td').parents('tr').first().remove();


                        if (pname != "MaAccount") {

                            window.location.href = "/" + pname;
                        }
                        alert("Deleted Successfully");



                    }
                    else {

                    }
                });
                $(this).dialog("close");
            },

            "Cancel": function () {
                $(this).dialog("close");
            }
        }


    });
}



function errormessage() {

    alert("\t" + "!!! Error !!!" + "\n" + "You cannot Delete this Record." + "\n" + " Data is reffered somewhere else");
}



function getquerystring() {

    var qsarr = new Array();

    var qs = location.search.substring(1);
    var singleqs = new Array();
    var str = "";
    qsarr = qs.split('&&');


    for (i = 0; i < qsarr.length; i++) {
        singleqs = qsarr[i].split('=');

        str += singleqs[1];
        if (i < qsarr.length - 1) {
            str += ",";
        }
    }
    str = str.replace("%20", " ");
    return str;
}

function currentdate(txtbox) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = mm + '/' + dd + '/' + yyyy;

    $("#" + txtbox).val(today);
}

function overallband(total) {
    var str = total / 4;

    str = str.toString();
    var n = str.split(".");

    var pointval = parseFloat(str) - parseFloat("0." + n[1]);
    var pot = parseFloat("0." + n[1]);


    if (pot < 0.25) {
        n[1] = 0;
    }
    if (pot >= 0.25 && pot < 0.75) {

        n[1] = 0.5;
    }
    if (pot >= 0.75) {

        n[0] = parseInt(n[0]) + 1;
        n[1] = 0;
    }
    var x = parseFloat(n[0]) + parseFloat(n[1]);

    return x;
}

