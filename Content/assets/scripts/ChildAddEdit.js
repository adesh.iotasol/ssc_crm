﻿

$(document).ready(function () {

    $('input[type="date"]').attr('placeholder', 'mm/dd/yyyy');
    $('input[type="date"]').prop('type', 'text');
    $(".editAcBtn").button();

    $(".myeditAcBtn").button();

    var linkObjAcDetail;

    var mylinkObjAcDetail;


    $('#editDialog').dialog({


        
        toggle: function () {
            return this[!this.isShown ? 'show' : 'hide']();
        },
        show: function () {
            var e = $.Event('show');

            if (this.isShown) return;

            this.$element.trigger(e);

            if (e.isDefaultPrevented()) return;

            this.escape();

            this.tab();

            this.options.loading && this.loading();
        },

        hide: function (e) {
            e && e.preventDefault();

            e = $.Event('hide');

            this.$element.trigger(e);

            if (!this.isShown || e.isDefaultPrevented()) return (this.isShown = false);

            this.isShown = false;

            this.escape();

            this.tab();

            this.isLoading && this.loading();

            $(document).off('focusin.modal');

            this.$element
				.removeClass('in')
				.removeClass('animated')
				.removeClass(this.options.attentionAnimation)
				.removeClass('modal-overflow')
				.attr('aria-hidden', true);

            $.support.transition && this.$element.hasClass('fade') ?
				this.hideWithTransition() :
				this.hideModal();
        },



        tab: function () {
            var that = this;

            if (this.isShown && this.options.consumeTab) {
                this.$element.on('keydown.tabindex.modal', '[data-tabindex]', function (e) {
                    if (e.keyCode && e.keyCode == 9) {
                        var $next = $(this),
							$rollover = $(this);

                        that.$element.find('[data-tabindex]:enabled:not([readonly])').each(function (e) {
                            if (!e.shiftKey) {
                                $next = $next.data('tabindex') < $(this).data('tabindex') ?
									$next = $(this) :
									$rollover = $(this);
                            } else {
                                $next = $next.data('tabindex') > $(this).data('tabindex') ?
									$next = $(this) :
									$rollover = $(this);
                            }
                        });

                        $next[0] !== $(this)[0] ?
							$next.focus() : $rollover.focus();

                        e.preventDefault();
                    }
                });
            } else if (!this.isShown) {
                this.$element.off('keydown.tabindex.modal');
            }
        },

        escape: function () {
            var that = this;
            if (this.isShown && this.options.keyboard) {
                if (!this.$element.attr('tabindex')) this.$element.attr('tabindex', -1);

                this.$element.on('keyup.dismiss.modal', function (e) {
                    e.which == 27 && that.hide();
                });
            } else if (!this.isShown) {
                this.$element.off('keyup.dismiss.modal')
            }
        },

        dialogClass: 'ui-dialog-green',
        autoOpen: false,
        width: 350,
        resizable: false,
        modal: true,
        buttons: {
            "Submit": {
                text: "Submit",
                id: "buttonid",
                click: function () {

                    $("#update-messageStatus").html(''); //make sure there is nothing on the message before we continue  

                }
            },

            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });

    $(".editAcBtn").click(function () {
        //change the title of the dialgo

        linkObjAcDetail = $(this);


        var AcDetailDIV = $('#editDialog');
        var viewairUrl = linkObjAcDetail.attr('href');


        $.get(viewairUrl, function (data) {

            AcDetailDIV.html(data);

            var $form = $("#updateCustAcHistory");
            // Unbind existing validation

            // $form.unbind();
            $form.data("validator", null);
            // Check document for changes
            //$.validator.unobtrusive.parse(document);

            AcDetailDIV.dialog('open');

        });
        return false;
    });


    $(".myeditAcBtn").click(function () {
        //change the title of the dialgo

        linkObjAcDetail = $(this);


        var AcDetailDIV = $('#editDialog');
        var viewairUrl = linkObjAcDetail.attr('href');

        alert(viewairUrl);

        $.get(viewairUrl, function (data) {

            AcDetailDIV.html(data);



            AcDetailDIV.dialog('open');

        });
        return false;
    });



});




function updateSuccessStatus() {


    if ($("#update-messageStatus").html() == "Success") {


        window.location.reload();
    
        alert("Added Successfully");

        $('#editDialog').dialog('close');

        return;

    }
    else if ($("#update-messageStatus").html() == "Invalid") {

        alert("Please Check Valid Installment Plan");
        $('#editDialog').dialog('close');

        return;
    }

}

function updateCouponSuccessStatus() {

    if ($("#update-messageStatus").html() == "Success") {


        alert("Added Successfully");

        $('#editDialog').dialog('close');

        return;

    }

}

function updateAcSuccessStatus() {


    $('#editDialog').dialog('close');

    var empval = $("#update-messageStatus").html();



    window.location.href = '/MaAccount/Edit/' + empval;

    //    var url = '/MaAccount/_EditCustAcHistory';

    //    $.get(url, { id: empval }, function (data) {


    //        $("#editlistac").html(data);

    //        alert("Successfully Done");
    //       

    //    });
}




$(".editAcBtnEdit").click(function () {
    //change the title of the dialgo

    linkObjAcDetail = $(this);


    var AcDetailDIV = $('#editDialog');
    var viewairUrl = linkObjAcDetail.attr('href');


    $.get(viewairUrl, function (data) {

        AcDetailDIV.html(data);

        var $form = $("#updateCustAcHistory");
        // Unbind existing validation
        $form.unbind();
        $form.data("validator", null);
        // Check document for changes
        $.validator.unobtrusive.parse(document);

        AcDetailDIV.dialog('open');
        Showoption();
    });
    return false;
});




function Showoption() {
    var myoption = $("#newdrop").val();
    if (myoption == "Cash") {
        $("#paydetailedit").hide();
    }
    else {
        $("#paydetailedit").show();
    }
}