﻿var FormValidation = function () {

    return {
        //main function to initiate the module
        init: function () {

            // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#formMine');
            var error1 = $('.alert-error', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                   
                    //MaBrach start here//
                    BramchName: {
                        minlength: 2,
                        required: true
                    },
                   
                    MobileNo1: {
                        required: true,
                        digits: true,
                        minlength: 10,
                        maxlength: 10
                    },

                    MobileNo2: {
                        digits: true,
                        minlength: 10,
                        maxlength: 10
                    },
                    Regno: {
                        required: true,
                        maxlength: 30,
                    },
                    Phone: {

                        digits: true,
                        minlength: 10,
                        maxlength: 10
                    },
                    faxno: {
                        digits: true,
                    },

                    //MaBrach end here//
                    digits: {
                        required: true,
                        digits: true
                    },
                    creditcard: {
                        required: true,
                        creditcard: true
                    },

                    regdate: {
                        required: true,
                        date: true
                    },
                    Btype: {
                        required: true
                    },
                    //MaDesignation start here//
                    Designation: {
                        required: true
                    },
                    //MaDesignation end here//

                    //MaPlan start here//
                    Planname: {
                        required: true
                    },
                    Plancode: {
                        required: true
                    },

                    Unit: {
                        required: true
                    },

                    UnitValue: {
                        required: true,
                        digits: true
                    },

                    Planyear: {
                        required: true,

                    },

                    YearAmount: {
                        required: true,
                        digits: true
                    },
                    //MaPlan end here//


                    //Mauser start here//
                    BranchId: {
                        required: true
                    },

                    FullName: {
                        required: true
                    },

                    UserName:
                        {
                            required: true
                        },

                    UserPassword: {
                        maxlength: 10,
                        required: true
                    },

                    ConfirmPassword: {
                        maxlength: 10,
                        required: true,
                        equalTo: "#UserPassword"
                    },
                    UserType: {
                        required: true
                    },

                    email: {
                        email: true
                    },

                    DesignationId: {
                        required: true
                    },
                    //Mauser end here


                    //MaAgent start here
                  
                    appno: {
                        required: true
                    },

                    refcodeno: {
                        required: true

                    },

                    refrank: {
                        required: true

                    },


                    appname: {
                        maxlength: 49,
                        required: true
                    },



                    dob: {

                        required: true,
                        date: true

                    },

                    panno: {

                        maxlength: 12,
                        required: true
                    },

                    qualification: {
                        maxlength: 49,
                        required: true
                    },

                    occupation: {
                        maxlength: 49,
                        required: true
                    },

                    paddress: {
                        maxlength: 149,
                        required: true
                    },

                    state: {

                        maxlength: 30,
                        required: true
                    },



                    mobileno: {
                        digits: true,
                        minlength: 10,
                        maxlength: 10
                    },




                    branchid: {
                        required: true
                    },

                  

                    nominame: {
                        maxlength: 50,
                        required: true
                    },

                    age: {
                        maxlength: 10,
                        required: true
                    },

                    relation: {
                        maxlength: 50,
                        required: true
                    },

                    resiproof: {
                        maxlength: 250,
                        required: true
                    },

                    idproof: {
                        maxlength: 50,
                        required: true
                    },

                    //maagent end here

                    ApplicatioNo: {
                        required: true
                    },


                   
                    Amount: {
                        required: true
                    },

                    Bankname: {
                        required: true
                    },

                    PanNo: {
                        required: true
                    },
                    ApplicantNme: {
                        required: true
                    },

                    FathrHusbndNme: {
                        required: true
                    },
                   
                    MaritalStus: {
                        required: true
                    },


                    PermnAddress: {
                        required: true
                    },
                    Nominame: {
                        required: true
                    },
                    Age: {
                        required: true,
                        digits: true,
                    },


                    MobileNo: {
                        required: true,
                        digits: true,
                        minlength: 10,
                        maxlength: 11
                    },

                    Nationality: {
                        required: true

                    },





                    Nominame: {
                        required: true
                    },

                    Age: {
                        required: true,
                        digits: true,

                    },


                    NominAddress: {
                        required: true

                    },

                    ResidenceProof: {
                        required: true
                    },

                    Idproof: {
                        required: true
                    },

                    Branchid: {
                        required: true

                    },

                  
                    Registrationno: {
                        required: true

                    },


                  
                    ObjName: {
                        required: true
                    },
                   
                   //receipt--start--here

                   
                   
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {

                    success1.show();
                    error1.hide();

                    form.submit();
                }
            });





        }

    };

}();