﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Configuration;
using System.Data.Entity.Validation;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class Tb_IELTSResultController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        CommonUtil cu = new CommonUtil();

        Status st = new Status();
        //
        // GET: /Tb_IELTSResult/

        public ActionResult Index(string sttype = "", string fdt = "", string tdt = "", string mobile = "", int studentid = 0, string ReferNo = "", string sname = "", string stucat = "", string enqtype = "", string examtype = "", string Score = "", string branchid = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.IeltsPteTest, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            //Start
            int brid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            //if (Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeSuperAdmin)
            //{
            //    ViewBag.Branches = new SelectList(db.MaBranches, "Id", "BramchName");
            //}
            //else
            //{
            //    var blist = db.MaBranches.Where(w => w.Id == brid);
            //    ViewBag.Branches = new SelectList(blist, "Id", "BramchName");
            //}

            ViewBag.Branches = new SelectList(db.MaBranches, "Id", "BramchName");
            DateTime from = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["FromDate"]);
            from = from.AddMonths(6);
            string frodt = CommonUtil.IndianTime().ToShortDateString();
            string todt = CommonUtil.IndianTime().ToShortDateString();
            ViewBag.BookingType = new SelectList(cu.GetBooking(), "Id", "BookingType");
            try
            {
                DateTime Fd1 = Convert.ToDateTime(frodt);
                DateTime Fd2 = Convert.ToDateTime(todt);
                ViewBag.fdt = "01/01/2016";
                ViewBag.tdt = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1.0).Date.ToShortDateString();
            }
            catch (Exception)
            {
            }

            List<IELTSHelper> lst = new List<IELTSHelper>();
            List<IeltsResult_View> list2 = new List<IeltsResult_View>();
            if (ReferNo != "" || sname != "" || studentid > 0 || sttype != "" || (fdt != "" && tdt != "") || mobile != "" || stucat != "" || enqtype != "")
            {
                if (Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeSuperAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeCounselor && Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeVisaCounselor)
                {
                    if (branchid != "")
                    {
                        brid = Convert.ToInt32(branchid);
                        list2 = db.IeltsResult_View.Where(w => w.BranchId == brid).OrderByDescending(x => x.Id).ToList();
                    }
                    list2 = db.IeltsResult_View.OrderByDescending(x => x.Id).ToList();
                }
                else
                {
                    int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                    //lst = lst.Where(z => z.branchid == branchid).OrderByDescending(z => z.Id).ToList();
                    list2 = db.IeltsResult_View.Where(x => x.BranchId == brid).OrderByDescending(x => x.Id).ToList();
                }

                ViewBag.sttype = sttype;
                ViewBag.mobile = mobile;
                ViewBag.fdt = fdt;
                ViewBag.tdt = tdt;
                ViewBag.sname = sname;
                ViewBag.stucat = stucat;
                ViewBag.enqtype = enqtype;
                ViewBag.ReferNo = ReferNo;
                //}
                try
                {
                    int refno = 0;
                    if (ReferNo != "")
                    {
                        refno = Convert.ToInt32(ReferNo);
                        list2 = list2.Where(x => x.RollNo == refno).ToList();

                    }

                    if (sname != "")
                    {
                        //lst = lst.Where(m => m.Name.ToUpper().Contains(sname.ToUpper())).ToList();
                        list2 = list2.Where(x => (x.FirstName.ToUpper().Contains(sname.ToUpper())) || x.LastName.ToUpper().Contains(sname.ToUpper())).ToList();
                        ViewBag.TotalCount = list2.Count;
                    }

                    if (studentid > 0)
                    {
                        //lst = lst.Where(m => m.StudentId == studentid).ToList();
                        list2 = list2.Where(x => x.Studentid == studentid).ToList();
                        ViewBag.TotalCount = list2.Count;
                        //return View(lst);
                    }

                    if (sttype != "")
                    {
                        if (sttype == "All")
                        {
                            //lst = lst.ToList();
                            list2 = list2.ToList();
                        }
                        else
                        {

                            sttype = sttype.ToUpper();
                            //list2 = list2.Where(x => x.studenttypeDisply.ToUpper() == sttype.ToUpper()).ToList();
                            int stp = Convert.ToInt32(sttype);
                            list2 = list2.Where(x => x.BookingType == stp).ToList();
                            //lst = lst.Where(z => z.studenttypeDisply.ToUpper() == sttype.ToUpper()).ToList();
                        }
                        ViewBag.TotalCount = list2.Count;
                    }
                    if (Score != "")
                    {
                        if (Score == "All")
                        {
                            //lst = lst.ToList();
                            list2 = list2.ToList();
                        }
                        else
                        {
                            if (Score == "5.5")
                            {
                                list2 = list2.Where(x => x.Overall == 5.5).ToList();
                            }
                            if (Score == "A5")
                            {
                                list2 = list2.Where(x => x.Overall >= 6).ToList();
                            }
                            if (Score == "B5")
                            {
                                list2 = list2.Where(x => x.Overall <= 5.5).ToList();
                            }
                        }
                        ViewBag.TotalCount = list2.Count;
                    }
                    if (stucat != "")
                    {
                        if (stucat == "All")
                        {
                            //lst = lst.ToList();
                            list2 = list2.ToList();
                        }
                        else
                        {
                            stucat = stucat.ToUpper();
                            list2 = list2.Where(x => x.Category.ToUpper() == stucat.ToUpper()).ToList();
                            //lst = lst.Where(z => z.Category.ToUpper() == stucat.ToUpper()).ToList();
                        }
                        ViewBag.TotalCount = list2.Count;
                    }

                    if (enqtype != "")
                    {
                        if (enqtype == "All")
                        {
                            //lst = lst.ToList();
                            list2 = list2.ToList();
                        }
                        else
                        {

                            list2 = list2.Where(x => x.Enqstatus == enqtype).ToList();
                            //lst = lst.Where(z => z.EnquiryStatus.ToUpper() == enqtype.ToUpper()).ToList();
                        }
                    }
                    if (examtype != "")
                    {
                        if (examtype == "All")
                        {
                            //lst = lst.ToList();
                            list2 = list2.ToList();
                        }
                        else
                        {
                            list2 = list2.Where(x => x.ExamBody == examtype).ToList();
                            //lst = lst.Where(z => z.exambody.ToUpper() == examtype.ToUpper()).ToList();
                        }
                    }


                    if (fdt != "" && tdt != "")
                    {
                        try
                        {
                            ViewBag.fdt = fdt;
                            ViewBag.tdt = tdt;
                            DateTime dt1 = Convert.ToDateTime(fdt);
                            DateTime dt2 = Convert.ToDateTime(tdt);

                            list2 = list2.Where(z => z.Testdate >= dt1 && z.Testdate <= dt2).ToList();
                            //lst = lst.Where(z => z.TestDate >= dt1 && z.TestDate <= dt2).ToList();
                        }
                        catch (Exception ex)
                        {
                        }
                    }

                    if (mobile != "")
                    {
                        // lst = lst.Where(m => m.Overall >= 5.5).Where(m => m.isfollowbtn == true).ToList();

                        //lst = lst.Where(m => m.mobile == mobile).ToList();
                        list2 = list2.Where(m => m.Mobile == mobile).ToList();
                    }
                }
                catch (Exception ex) { }
            }
            ViewBag.TotalCount = list2.Count;
            return View(list2);
        }


        public List<IELTSHelper> _ielthelper(string fdt = "", string tdt = "", int bid = 0, string refer = "")
        {
            DateTime dt1 = new DateTime();
            DateTime dt2 = new DateTime();

            List<Tb_IELTSResult> Model = new List<Tb_IELTSResult>();
            if (refer != "")
            {
                Model = db.Tb_IELTSResult.Where(a => a.BranchId == bid).ToList();
            }
            else
            {
                try
                {
                    dt1 = Convert.ToDateTime(fdt);
                    dt2 = Convert.ToDateTime(tdt);
                }
                catch (Exception)
                {
                }

                Model = db.Tb_IELTSResult.Where(a => a.TestDate >= dt1 && a.TestDate <= dt2 && a.BranchId == bid).ToList();
            }


            List<IELTSHelper> _lsthelper = new List<IELTSHelper>();

            foreach (var item in Model)
            {
                IELTSHelper _objielts = new IELTSHelper();

                if (item != null)
                {

                    bool ismovetovisa = false;
                    string enquirstatus = "";
                    _objielts.Exambooked = item.BookedTime;
                    _objielts.ResUpdt = item.ResUpdTime;
                    _objielts.exambody = item.ExamBody;
                    _objielts.Category = item.Category;
                    _objielts.IeltsTesDate = item.TestDate;
                    _objielts.timing = item.TimeId;
                    _objielts.OverallScore = item.Overall;
                    _objielts.studenttype = item.StudentType;
                    _objielts.branchid = item.BranchId;
                    _objielts.StudentId = item.StudentId;
                    _objielts.TestDate = item.TestDate;
                    _objielts.Overall = item.Overall;
                    _objielts.isfollowbtn = item.isfollowbtn;
                    _objielts.Id = item.Id;
                    _objielts.Listening = item.LBand;
                    _objielts.Reading = item.RBand;
                    _objielts.Writing = item.WBand;
                    _objielts.Speaking = item.SBand;
                    _objielts.course = item.CourseId;
                    if (item.StudentType == "Admission")
                    {
                        try
                        {
                            MaAdmission obj = db.MaAdmissions.Where(z => z.Id == item.StudentId).FirstOrDefault();
                            _objielts.ReferNo = (int)obj.RollNo;
                            _objielts.Name = obj.FirstName + " " + obj.LastName;
                            _objielts.mobile = obj.Mobile;
                            _objielts.rollnoid = Convert.ToInt32(obj.RollNo);

                            if (item.StudentType == CommonUtil.Admission)
                            {
                                _objielts.studenttypeDisply = CommonUtil.prospstudents;
                            }

                            if (obj.IsMoveToVisa != null)
                            {
                                // changed by arjun(29-08-2013)
                                ismovetovisa = (bool)obj.IsMoveToVisa;
                                enquirstatus = ismovetovisa.ToString().Replace("True", "Move to Visa");
                            }
                            else
                            {
                                int _count = db.MaEnqFollows.Where(z => z.ReferId == item.StudentId).OrderByDescending(z => z.Id).Count();
                                if (_count > 0)
                                {
                                    try
                                    {
                                        MaEnqFollow _enqfoll = db.MaEnqFollows.Where(z => z.ReferId == item.StudentId).OrderByDescending(z => z.Id).First();
                                        enquirstatus = _enqfoll.Enqstatus;
                                    }
                                    catch (Exception ex)
                                    { }
                                }
                            }
                        }
                        catch (Exception ex)
                        { }
                    }

                    MaEnqFollow maenq = new MaEnqFollow();
                    maenq = db.MaEnqFollows.Where(z => z.ReferId == item.Id && z.FollowUpType == "IELTS").OrderByDescending(z => z.Id).FirstOrDefault();

                    if (item.StudentType == "Enquiry")
                    {
                        try
                        {
                            MaEnquiry obj = db.MaEnquiries.Where(z => z.Id == item.StudentId).FirstOrDefault();
                            _objielts.ReferNo = obj.Id;
                            _objielts.Name = obj.FirstName + " " + obj.LastName;
                            _objielts.mobile = obj.Mobile;
                            _objielts.rollnoid = obj.Id;
                            _objielts.studenttypeDisply = CommonUtil.Enquiry;

                            if (obj.IsMoveToVisa != null)
                            {
                                // changed by arjun(29-08-2013)
                                ismovetovisa = (bool)obj.IsMoveToVisa;
                                enquirstatus = ismovetovisa.ToString().Replace("True", "Move to Visa");
                            }

                            else
                            {
                                enquirstatus = maenq.Enqstatus;
                            }

                        }
                        catch (Exception ex)
                        { }
                    }

                    //_objielts.EnquiryStatus = "No";

                    // if (db.MaEnqFollows.Where(z => z.ReferId == item.Id && z.FollowUpType == "IELTS").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                    {
                        if (item.Overall >= 0)
                        {
                            try
                            {

                                _objielts.NextFollowUpdate = maenq.NextDate;
                                _objielts.Remarks = maenq.Remarks;
                                _objielts.EnquiryStatus = enquirstatus;
                            }
                            catch (Exception ex)
                            { }

                        }
                    }

                    /*
                    if (db.MaEnqFollows.Where(z => z.ReferId == item.Id && z.FollowUpType == "IELTS").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                    {
                        if (item.Overall >= 0)
                        {
                            try
                            {
                                //this is to get the enquiry status
                                enquirstatus = db.MaEnqFollows.Where(z => z.ReferId == item.Id && z.FollowUpType == "IELTS").OrderByDescending(z => z.Id).Select(z => z.Enqstatus).First();

                                string rem = db.MaEnqFollows.Where(z => z.ReferId == item.Id && z.FollowUpType == "IELTS").OrderByDescending(z => z.Id).Select(z => z.Remarks).First();
                                _objielts.Remarks = rem;
                            }
                            catch (Exception ex)
                            { }
                        }
                    }
                     

                                        
                    if (ismovetovisa)
                    {
                        if (ismovetovisa == false)
                        {
                            _objielts.EnquiryStatus = enquirstatus;
                        }
                        else
                        {

                            _objielts.EnquiryStatus = enquirstatus;
                        }
                    }
                    else
                    {
                        _objielts.EnquiryStatus = enquirstatus;
                    }
                      */
                }

                _lsthelper.Add(_objielts);
            }
            //return _lsthelper.Where(z => z.EnquiryStatus == "Open").ToList();
            return _lsthelper.ToList();

        }

        public ActionResult IeltsMarks(int stuid = 0)
        {


            List<Tb_IELTSResult> _lstielts = db.Tb_IELTSResult.Where(z => z.StudentId == stuid).ToList();
            List<IELTSStudentDetail> _ieltsstdent = new List<IELTSStudentDetail>();

            foreach (Tb_IELTSResult obj in _lstielts)
            {
                IELTSStudentDetail objstdnt = new IELTSStudentDetail();
                objstdnt.course = obj.CourseId;
                objstdnt.category = obj.Category;
                objstdnt.date = obj.CDate;
                objstdnt.listening = obj.LBand;
                objstdnt.reading = obj.RBand;
                objstdnt.speaking = obj.SBand;
                if (obj.StudentType == "Enquiry")
                {
                    objstdnt.studentname = db.MaEnquiries.Where(z => z.Id == obj.StudentId).Select(z => z.FirstName + " " + z.LastName).FirstOrDefault();
                }
                else
                {
                    objstdnt.studentname = db.MaAdmissions.Where(z => z.Id == obj.StudentId).Select(z => z.FirstName + " " + z.LastName).FirstOrDefault();

                }
                objstdnt.testdate = obj.TestDate;
                objstdnt.timing = obj.TimeId;
                objstdnt.writing = obj.WBand;
                objstdnt.overall = obj.Overall;
                objstdnt.remarks = obj.Remarks;
                objstdnt.enterby = db.Users.Where(z => z.Id == obj.UserId).Select(z => z.Fullname).FirstOrDefault();
                _ieltsstdent.Add(objstdnt);
            }
            ViewBag.TotalCount = _ieltsstdent.Count();

            return PartialView("_AllDetailOfStudnt", _ieltsstdent);
        }

        public ActionResult _AllDetailOfStudnt(int id = 0)
        {
            int? studentid = 0;
            int _count = db.Tb_IELTSResult.Where(z => z.Id == id).Select(z => z.StudentId).Count();
            if (_count > 0)
            {
                studentid = db.Tb_IELTSResult.Where(z => z.Id == id).Select(z => z.StudentId).First();
            }
            List<Tb_IELTSResult> _lstielts = db.Tb_IELTSResult.Where(z => z.StudentId == studentid).ToList();
            List<IELTSStudentDetail> _ieltsstdent = new List<IELTSStudentDetail>();

            foreach (Tb_IELTSResult obj in _lstielts)
            {
                IELTSStudentDetail objstdnt = new IELTSStudentDetail();
                objstdnt.course = obj.CourseId;
                objstdnt.category = obj.Category;
                objstdnt.date = obj.CDate;
                objstdnt.listening = obj.LBand;
                objstdnt.reading = obj.RBand;
                objstdnt.speaking = obj.SBand;
                if (obj.StudentType == "Enquiry")
                {


                    objstdnt.studentname = db.MaEnquiries.Where(z => z.Id == obj.StudentId).Select(z => z.FirstName + " " + z.LastName).FirstOrDefault();


                }
                else
                {
                    objstdnt.studentname = db.MaAdmissions.Where(z => z.Id == obj.StudentId).Select(z => z.FirstName + " " + z.LastName).FirstOrDefault();

                }
                objstdnt.testdate = obj.TestDate;
                objstdnt.timing = obj.TimeId;
                objstdnt.writing = obj.WBand;
                objstdnt.overall = obj.Overall;
                objstdnt.remarks = obj.Remarks;

                //int _usercount = db.Users.Where(z => z.Id == obj.UserId).Select(z => z.Fullname).Count();
                //if (_usercount > 0)
                //{
                objstdnt.enterby = db.Users.Where(z => z.Id == obj.UserId).Select(z => z.Fullname).FirstOrDefault();
                // }
                _ieltsstdent.Add(objstdnt);
            }
            ViewBag.TotalCount = _ieltsstdent.Count();

            return PartialView(_ieltsstdent);
        }


        //
        // GET: /Tb_IELTSResult/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.IeltsPteTest, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            Tb_IELTSResult tb_ieltsresult = db.Tb_IELTSResult.Find(id);

            if (tb_ieltsresult == null)
            {
                return HttpNotFound();
            }
            return View(tb_ieltsresult);
        }

        //
        // GET: /Tb_IELTSResult/Create

        public ActionResult Create(int? id, string stype)

        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.IeltsPteTest, UserAction.Add);
                string chbranch = Request.Cookies["chbranch"].Value;


                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            //DateTime datee = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["Datee"]);
            //DateTime cdatee = DateTime.Now;
            Tb_IELTSResult tbresult = new Tb_IELTSResult();
            if (id != 0)
            {
                ViewBag.stype = "Enquiry";
                ViewBag.Idd = id;

                //tbresult = db.Tb_IELTSResult.Where(x => x.Id == id).FirstOrDefault();

            }
            else
            {

                ViewBag.stype = stype;

            }

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "Id", "BatchTime");
            ViewBag.UserId = new SelectList(cu.GetUserList(branchid), "Id", "Fullname");
            //ViewBag.StudentId = new SelectList(cu.GetMockTestStudents(branchid), "Id", "fullname");
            ViewBag.StudentId = new SelectList(cu.GetMockTestStudents(branchid), "Id", "fullname");
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName");
            ViewBag.BookingType = new SelectList(cu.GetBooking(), "Id", "BookingType");
            // this has been valued 0 because of client requirement because want to update score


            tbresult.RBand = 0;
            tbresult.WBand = 0;
            tbresult.SBand = 0;
            tbresult.LBand = 0;
            tbresult.Overall = 0;
            tbresult.Remarks = "-";

            return View(tbresult);
        }

        //
        // POST: /Tb_IELTSResult/Create

        [HttpPost]
        public ActionResult Create(Tb_IELTSResult tb_ieltsresult, FormCollection frm)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.IeltsPteTest, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.ErrorMsg = "";
            int usrid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            ViewBag.BookingType = new SelectList(cu.GetBooking(), "Id", "BookingType");
            if (ModelState.IsValid)
            {
                try
                {

                    tb_ieltsresult.CDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
                    tb_ieltsresult.UserId = usrid;
                    tb_ieltsresult.BranchId = branchid;
                    tb_ieltsresult.isfollowbtn = false;
                    tb_ieltsresult.StudentType = frm["StudentType"].ToString();
                    tb_ieltsresult.StudentId = Convert.ToInt32(frm["SIdd"]);
                    tb_ieltsresult.BookingType = Convert.ToInt32(frm["BookingType"]);
                    tb_ieltsresult.BookedDBO = frm["BookedDBO"];
                    tb_ieltsresult.BookedTime = CommonUtil.IndianTime();
                    //if (tb_ieltsresult.Overall > 0)
                    {
                        tb_ieltsresult.ResUpdTime = CommonUtil.IndianTime();
                    }

                    db.Tb_IELTSResult.Add(tb_ieltsresult);
                    db.SaveChanges();

                    if (tb_ieltsresult.TestDate != null && tb_ieltsresult.StudentType == "Admission")
                    {
                        MaEnqFollow menq = new MaEnqFollow();
                        menq.AdmissionId = tb_ieltsresult.StudentId;
                        menq.CDate = DateTime.Now;
                        menq.EnqFollowBy = usrid;
                        menq.Enqstatus = "Close";
                        menq.Remarks = "Followup close after ielts date";
                        menq.ReferId = tb_ieltsresult.Id;
                        menq.NextDate = DateTime.Now;
                        menq.FollowUpType= "MockTest";
                        menq.Entertype = "Admission";
                        db.MaEnqFollows.Add(menq);
                        db.SaveChanges();

                    }
                }
                catch (Exception ex)
                {
                    Status myobj = new Status();
                    myobj.SendError(ex);
                    ViewBag.ErrorMsg = myobj.SessionError;
                    return View(tb_ieltsresult);

                }

                MaEnqFollow enq = new MaEnqFollow();
                //if (tb_ieltsresult.Overall >= 5.5)
                //{
                /*--------if overall bands are greater than 5.5 then send an enquiry follow up by default so that it should come in enquiry follow up list------*/
                Tb_Prefrence tb_pref = db.Tb_Prefrence.FirstOrDefault();
                double days = Convert.ToDouble(tb_pref.FollDays);
                try
                {
                    enq.ReferId = tb_ieltsresult.Id;
                    enq.FollowUpType = "IELTS";
                    enq.Enqstatus = "Open";
                    enq.EnqFollowBy = usrid;
                    //enq.NextDate = tb_ieltsresult.CDate.Value.AddDays(1).Date;
                    if (tb_ieltsresult.ExamBody == "PTE")
                    {
                        enq.NextDate = tb_ieltsresult.TestDate.Value.Date;
                    }
                    else
                    {
                        enq.NextDate = tb_ieltsresult.CDate.Value.AddDays(days).Date;
                    }
                    enq.Remarks = "New Entry on " + tb_ieltsresult.CDate;
                    enq.CDate = tb_ieltsresult.CDate;
                    db.MaEnqFollows.Add(enq);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Status myobj = new Status();
                    myobj.SendError(ex);
                    ViewBag.ErrorMsg = myobj.SessionError;
                    return View(tb_ieltsresult);
                }



                //update numbers 
                UpdateMblnumbers(frm, Convert.ToInt32(tb_ieltsresult.StudentId));
                string value = ConfigurationManager.AppSettings["Switch"];
                if (value == CommonUtil.SwitchPermsn.switchOn)
                {
                    string num = "9876533888";
                    string coursname = "";
                    if (tb_ieltsresult.ExamBody == "PTE")
                    {
                        coursname = "PTE";
                    }
                    else
                    {
                        coursname = "IELTS";
                    }
                    if (tb_ieltsresult.StudentType == "Enquiry" && tb_ieltsresult.Overall > 0)
                    {
                        MaEnquiry _objenqry = db.MaEnquiries.Where(z => z.Id == tb_ieltsresult.StudentId).First();
                        //  SendIELTSTestMail(_objenqry.FirstName + " " + _objenqry.LastName, _objenqry.Id.ToString(), tb_ieltsresult.CourseId, tb_ieltsresult.StudentType, tb_ieltsresult.Category, Convert.ToDateTime(tb_ieltsresult.TestDate), _objenqry.Mobile, (double)tb_ieltsresult.Overall, Convert.ToDateTime(enq.NextDate), enq.Remarks, enq.Enqstatus);
                        Status myobj = new Status();
                        string msg = $"Dear { _objenqry.FirstName}, your { coursname} test score is L: { tb_ieltsresult.LBand}, R: { tb_ieltsresult.RBand}, W: { tb_ieltsresult.WBand}, S: { tb_ieltsresult.SBand}, Overall: { tb_ieltsresult.Overall}. For result card & Visa, cont. SSC { num}";
                        //string msg = $"Dear {_objenqry.FirstName}, your IELTS/PTE test score is L: {tb_ieltsresult.LBand}, R: {tb_ieltsresult.RBand}, W: {tb_ieltsresult.WBand}, S: {tb_ieltsresult.SBand}, Overall: {tb_ieltsresult.Overall}. For result card & Visa, cont. SSC 9914411150";
                        myobj.Sendsms(_objenqry.Mobile, msg);
                        myobj.Sendsms(_objenqry.AlternateNo, msg);

                    }
                    else
                    {
                        if (tb_ieltsresult.Overall > 0)
                        {
                            MaAdmission _objadmission = db.MaAdmissions.Where(z => z.Id == tb_ieltsresult.StudentId).First();
                            // SendIELTSTestMail(_objadmission.FirstName + " " + _objadmission.LastName, _objadmission.RollNo.ToString(), tb_ieltsresult.CourseId, tb_ieltsresult.StudentType, tb_ieltsresult.Category, Convert.ToDateTime(tb_ieltsresult.TestDate), _objadmission.Mobile, (double)tb_ieltsresult.Overall, Convert.ToDateTime(enq.NextDate), enq.Remarks, enq.Enqstatus);
                            Status myobj = new Status();
                            string msg = $"Dear { _objadmission.FirstName}, your { coursname} test score is L: { tb_ieltsresult.LBand}, R: { tb_ieltsresult.RBand}, W: { tb_ieltsresult.WBand}, S: { tb_ieltsresult.SBand}, Overall: { tb_ieltsresult.Overall}. For result card & Visa, cont. SSC { num}";
                            //string msg = $"Dear {_objadmission.FirstName}, your IELTS/PTE test score is L: {tb_ieltsresult.LBand}, R: {tb_ieltsresult.RBand}, W: {tb_ieltsresult.WBand}, S: {tb_ieltsresult.SBand}, Overall: {tb_ieltsresult.Overall}. For result card & Visa, cont. SSC 9914411150";
                            //string msg = $"Dear {_objadmission.FirstName}, your IELTS/PTE test score is L: {tb_ieltsresult.LBand}, R {tb_ieltsresult.RBand}, W {tb_ieltsresult.WBand}, S {tb_ieltsresult.SBand}, Overall {tb_ieltsresult.Overall}. For result card & Visa, cont. SSC 9914411150";
                            myobj.Sendsms(_objadmission.Mobile, msg);
                            myobj.Sendsms(_objadmission.AlternateNo, msg);
                        }
                    }
                }
                //To Show Follow up button is Front of only one record

                int showbtncount = db.Tb_IELTSResult.Where(m => m.StudentId == tb_ieltsresult.StudentId).Where(m => m.isfollowbtn == true).Count();

                if (showbtncount == 0)
                {
                    Tb_IELTSResult objielts = db.Tb_IELTSResult.Find(tb_ieltsresult.Id);

                    objielts.isfollowbtn = true;
                    try
                    {
                        db.SaveChanges();

                    }
                    catch (DbEntityValidationException ex)
                    {
                        foreach (var eve in ex.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                ViewBag.ErrorMsg = ve.ErrorMessage;
                            }
                        }
                        return RedirectToAction("Index");
                    }
                }
                return RedirectToAction("Index");
            }

            DateTime datee = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["Datee"]);

            ViewBag.StudentId = new SelectList(cu.GetMockTestStudents(branchid), "Id", "fullname");
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_ieltsresult.BranchId);
            ViewBag.TimeId = new SelectList(db.MaTimes, "Id", "TimeIn", tb_ieltsresult.TimeId);
            ViewBag.UserId = new SelectList(cu.GetUserList(branchid, 1), "Id", "Password", tb_ieltsresult.UserId);
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName");
            ViewBag.BookingType = new SelectList(cu.GetBooking(), "Id", "BookingType", tb_ieltsresult.BookingType);

            return View(tb_ieltsresult);
        }

        private void UpdateMblnumbers(FormCollection frm, int studentid)
        {
            try
            {
                string studenttype = frm["StudentType"].ToString();
                if (studenttype == "Admission")
                {
                    MaAdmission objadmission = db.MaAdmissions.Where(z => z.Id == studentid).FirstOrDefault();
                    objadmission.Mobile = frm["mobileno"].ToString();
                    objadmission.AlternateNo = frm["alternateno"].ToString();
                    db.SaveChanges();

                    //here also update enquiry because both nombers comes from enquiry
                    int enquiryid = Convert.ToInt32(objadmission.EnquiryId);
                    UpdateEnquiry(frm, enquiryid);
                }
                if (studenttype == "Enquiry")
                {
                    UpdateEnquiry(frm, studentid);
                }

            }
            catch (Exception ex)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }


        }

        private void UpdateEnquiry(FormCollection frm, int id)
        {
            MaEnquiry objmaenquiry = db.MaEnquiries.Where(z => z.Id == id).First();
            objmaenquiry.Mobile = Convert.ToString(frm["mobileno"]);
            objmaenquiry.AlternateNo = Convert.ToString(frm["alternateno"]);
            db.SaveChanges();

        }

        //this is for to send the email.when ielts is created.
        public void SendIELTSTestMail(string studentname, string refno, string course, string studentype, string category, DateTime testdate, string mobile, double overallscre, DateTime nextfollwdate, string remarks, string enquirystatus)
        {
            try
            {
                //string from = "info@crm-ssceducation.org";
                //string Tomail = "ielts@ssceducation.org";
                string from = ConfigurationManager.AppSettings["Mail"];

                string Tomail = ConfigurationManager.AppSettings["IELTSmail"];

                MailMessage message = new MailMessage();

                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("<table border='1px'><tr> <td><b>Student Type</b></td> <td><b>REFERENCE NO</b></td> <td><b>Name</b></td> <td><b>Category</b></td> <td><b>Ielts Test Date</b></td> <td><b>Mobile No.</b></td><td><b>Overall Score</b></td> <td><b>Next FollowUp Date</b></td> <td><b>Remarks</b></td> <td><b>Enquiry Status</b></td></tr>");
                message.Subject = "IELTS Test Detail: ";
                sb.AppendFormat("<tr> <td>" + studentype + "</td> <td>" + refno + "</td> <td>" + studentname + "</td> <td>" + category + "</td> <td>" + string.Format("{0:dd MMM,yyyy}", testdate) + "</td> <td>" + mobile + "</td><td>" + overallscre + "</td> <td>" + string.Format("{0:dd MMM,yyyy}", nextfollwdate) + "</td> <td> " + remarks + "</td> <td>" + enquirystatus + "</td></tr>");
                sb.AppendFormat("</table>");
                message.Body = sb.ToString();
                message.From = new MailAddress(from);
                message.To.Add(new MailAddress(Tomail));
                message.IsBodyHtml = true;
                //string smtpserver = "mail.crm-ssceducation.org";
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                SmtpClient smtpClient = new SmtpClient(smtpserver);
                smtpClient.Port = Int32.Parse("8889");
                smtpClient.Credentials = new NetworkCredential(from, ConfigurationManager.AppSettings["credential"].ToString());
                smtpClient.EnableSsl = false;
                smtpClient.Send(message);

            }
            catch (Exception ex)
            { }

        }



        //
        // GET: /Tb_IELTSResult/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.IeltsPteTest, UserAction.Edit);
                string chbranch = Request.Cookies["chbranch"].Value;


                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            Tb_IELTSResult tb_ieltsresult = db.Tb_IELTSResult.Find(id);
            if (tb_ieltsresult == null)
            {
                return HttpNotFound();
            }
            ViewBag.sid = tb_ieltsresult.StudentId;
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_ieltsresult.BranchId);
            ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "Id", "BatchTime", tb_ieltsresult.TimeId);
            ViewBag.UserId = new SelectList(cu.GetUserList(branchid, 1), "Id", "Fullname", tb_ieltsresult.UserId);
            ViewBag.BookingType = new SelectList(cu.GetBooking(), "Id", "BookingType", tb_ieltsresult.BookingType
                );

            //var liststype1 = new List<ListItem>();
            //liststype1.Add(new ListItem { Text = "Select", Value = "Select" });
            //liststype1.Add(new ListItem { Text = "AC", Value = "AC" });
            //liststype1.Add(new ListItem { Text = "GT", Value = "GT" });

            //ViewBag.Category = new SelectList(liststype1, "Value", "Text", tb_ieltsresult.Category);

            if (tb_ieltsresult.StudentType == "Admission")
            {
                ViewBag.StudentId = new SelectList(cu.GetMockTestStudents(branchid), "Id", "fullname", tb_ieltsresult.StudentId);
            }
            else
            {
                ViewBag.StudentId = new SelectList(db.MaEnquiries.ToList(), "Id", "Firstname", tb_ieltsresult.StudentId);
                //ViewBag.StudentId = new SelectList(db.MaEnquiries.Where(z => z.EnquiryIELAC == true || z.EnquiryIELGT == true || z.EnquiryStyVisa == true || z.EnqAbt == "EXAM BOOKING").ToList(), "Id", "Firstname", tb_ieltsresult.StudentId);
            }
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName", tb_ieltsresult.CourseId);
            return View(tb_ieltsresult);
        }

        //
        // POST: /Tb_IELTSResult/Edit/5

        [HttpPost]
        public ActionResult Edit(Tb_IELTSResult tb_ieltsresult, FormCollection form)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.IeltsPteTest, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

            string[] myarray = form["StudentId"].ToString().Split(',');
            try
            {
                int usrid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);

                //if (myarray[1] != null)
                //{

                //    tb_ieltsresult.StudentId = Convert.ToInt32(myarray[1]);
                //}
                tb_ieltsresult.BranchId = branchid;

                db.Entry(tb_ieltsresult).State = System.Data.Entity.EntityState.Modified;
                tb_ieltsresult.CDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
                tb_ieltsresult.UserId = usrid;
                //if (tb_ieltsresult.Overall > 0)
                {
                    tb_ieltsresult.ResUpdTime = CommonUtil.IndianTime();

                }
                db.SaveChanges();
                string num = "9914411150";
                string coursname = "";
                if (tb_ieltsresult.ExamBody == "PTE")
                {
                    coursname = "PTE";
                }
                else
                {
                    coursname = "IELTS";
                }
                //update numbers 
                UpdateMblnumbers(form, Convert.ToInt32(tb_ieltsresult.StudentId));

                if (tb_ieltsresult.StudentType == "Enquiry" && tb_ieltsresult.Overall > 0)
                {
                    MaEnquiry _objenqry = db.MaEnquiries.Where(z => z.Id == tb_ieltsresult.StudentId).First();
                    //  SendIELTSTestMail(_objenqry.FirstName + " " + _objenqry.LastName, _objenqry.Id.ToString(), tb_ieltsresult.CourseId, tb_ieltsresult.StudentType, tb_ieltsresult.Category, Convert.ToDateTime(tb_ieltsresult.TestDate), _objenqry.Mobile, (double)tb_ieltsresult.Overall, Convert.ToDateTime(enq.NextDate), enq.Remarks, enq.Enqstatus);
                    Status myobj = new Status();
                    string msg = $"Dear { _objenqry.FirstName}, your { coursname} test score is L: { tb_ieltsresult.LBand}, R: { tb_ieltsresult.RBand}, W: { tb_ieltsresult.WBand}, S: { tb_ieltsresult.SBand}, Overall: { tb_ieltsresult.Overall}. For result card & Visa, cont. SSC { num}";
                    //string msg = $"Dear {_objenqry.FirstName}, your IELTS/PTE test score is L: {tb_ieltsresult.LBand}, R: {tb_ieltsresult.RBand}, W: {tb_ieltsresult.WBand}, S: {tb_ieltsresult.SBand}, Overall: {tb_ieltsresult.Overall}. For result card & Visa, cont. SSC 9914411150";
                    myobj.Sendsms(_objenqry.Mobile, msg);
                    myobj.Sendsms(_objenqry.AlternateNo, msg);

                }
                else
                {
                    if (tb_ieltsresult.Overall > 0)
                    {
                        MaAdmission _objadmission = db.MaAdmissions.Where(z => z.Id == tb_ieltsresult.StudentId).First();
                        // SendIELTSTestMail(_objadmission.FirstName + " " + _objadmission.LastName, _objadmission.RollNo.ToString(), tb_ieltsresult.CourseId, tb_ieltsresult.StudentType, tb_ieltsresult.Category, Convert.ToDateTime(tb_ieltsresult.TestDate), _objadmission.Mobile, (double)tb_ieltsresult.Overall, Convert.ToDateTime(enq.NextDate), enq.Remarks, enq.Enqstatus);
                        Status myobj = new Status();
                        string msg = $"Dear { _objadmission.FirstName}, your { coursname} test score is L: { tb_ieltsresult.LBand}, R: { tb_ieltsresult.RBand}, W: { tb_ieltsresult.WBand}, S: { tb_ieltsresult.SBand}, Overall: { tb_ieltsresult.Overall}. For result card & Visa, cont. SSC { num}";
                        //string msg = $"Dear {_objadmission.FirstName}, your IELTS/PTE test score is L: {tb_ieltsresult.LBand}, R: {tb_ieltsresult.RBand}, W: {tb_ieltsresult.WBand}, S: {tb_ieltsresult.SBand}, Overall: {tb_ieltsresult.Overall}. For result card & Visa, cont. SSC 9914411150";
                        //string msg = $"Dear {_objadmission.FirstName}, your IELTS/PTE test score is L: {tb_ieltsresult.LBand}, R {tb_ieltsresult.RBand}, W {tb_ieltsresult.WBand}, S {tb_ieltsresult.SBand}, Overall {tb_ieltsresult.Overall}. For result card & Visa, cont. SSC 9914411150";
                        myobj.Sendsms(_objadmission.Mobile, msg);
                        myobj.Sendsms(_objadmission.AlternateNo, msg);
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
            }
            var liststype1 = new List<ListItem>();
            liststype1.Add(new ListItem { Text = "Select", Value = "Select" });
            liststype1.Add(new ListItem { Text = "AC", Value = "AC" });
            liststype1.Add(new ListItem { Text = "GT", Value = "GT" });

            ViewBag.Category = new SelectList(liststype1, "Value", "Text", tb_ieltsresult.Category);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_ieltsresult.BranchId);
            ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "Id", "BatchTime", tb_ieltsresult.TimeId);
            ViewBag.UserId = new SelectList(cu.GetUserList(branchid, 1), "Id", "Fullname", tb_ieltsresult.UserId);
            if (tb_ieltsresult.StudentType == "Admission")
            {
                ViewBag.StudentId = new SelectList(cu.GetMockTestStudents(branchid), "Id", "fullname", tb_ieltsresult.StudentId);
            }
            else
            {
                ViewBag.StudentId = new SelectList(db.MaEnquiries.Where(z => z.EnquiryIELAC == true || z.EnquiryIELGT == true || z.EnquiryStyVisa == true).ToList(), "Id", "Firstname", tb_ieltsresult.StudentId);
            }
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName", tb_ieltsresult.CourseId);
            return View(tb_ieltsresult);
        }

        //
        // GET: /Tb_IELTSResult/Delete/5

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.IeltsPteTest, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            try
            {
                Tb_IELTSResult tb_ieltsresult = db.Tb_IELTSResult.Find(id);

                if (db.MaEnqFollows.Where(z => z.ReferId == id && z.FollowUpType == "IELTS").Count() > 0)
                {
                    List<MaEnqFollow> lst = db.MaEnqFollows.Where(z => z.FollowUpType == "IELTS" && z.ReferId == id).ToList();
                    foreach (MaEnqFollow obj in lst)
                    {
                        db.MaEnqFollows.Remove(obj);
                        db.SaveChanges();
                    }
                }

                db.Tb_IELTSResult.Remove(tb_ieltsresult);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        public ActionResult MultipleResult(string fdt = "", string tdt = "", string ReferNo = "", string sname = "", string examtype = "")
        {

            DateTime from = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["FromDate"]);
            from = from.AddMonths(6);
            string frodt = CommonUtil.IndianTime().ToShortDateString();
            string todt = CommonUtil.IndianTime().ToShortDateString();
            try
            {
                DateTime Fd1 = Convert.ToDateTime(frodt);
                DateTime Fd2 = Convert.ToDateTime(todt);
                ViewBag.fdt = Fd1;
                ViewBag.tdt = Fd2;
            }
            catch (Exception)
            {
            }
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            List<IELTSHelper> lst = new List<IELTSHelper>();
            List<IeltsResult_View> list2 = new List<IeltsResult_View>();
            if (ReferNo != "" || sname != "" || (fdt != "" && tdt != "") || examtype != "")
            {

                //lst = _ielthelper(fdt, tdt, branchid, ReferNo).Where(z => z.ReferNo > 0).ToList();
                if (Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeSuperAdmin)
                {
                    //lst = lst.Where(x=>x.Overall==0).OrderByDescending(z => z.Id).ToList();
                    list2 = db.IeltsResult_View.Where(x => x.Overall == 0).OrderByDescending(x => x.Id).ToList();
                }
                else
                {
                    int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                    //lst = lst.Where(z => z.branchid == branchid && z.Overall==0).OrderByDescending(z => z.Id).ToList();
                    list2 = db.IeltsResult_View.Where(x => x.BranchId == branchid && x.Overall == 0).OrderByDescending(x => x.Id).ToList();
                }
                ViewBag.fdt = fdt;
                ViewBag.tdt = tdt;
                ViewBag.sname = sname;
                ViewBag.ReferNo = ReferNo;
                //}
                try
                {
                    int refno = 0;
                    if (ReferNo != "")
                    {
                        refno = Convert.ToInt32(ReferNo);
                        list2 = list2.Where(x => x.RollNo == refno).ToList();
                        //if (refno > 0)
                        //{
                        //    //lst = _ielthelper(fdt,tdt, branchid,refno).Where(z => z.ReferNo > 0).ToList();
                        //    list2 = list2.Where(x => x.RollNo == refno).ToList();
                        //    //lst = lst.Where(m => m.rollnoid == refno).ToList();
                        //    ViewBag.TotalCount = lst.Count;
                        //    return View(lst);
                        //}
                    }

                    if (sname != "")
                    {
                        //lst = lst.Where(m => m.Name.ToUpper().Contains(sname.ToUpper())).ToList();
                        list2 = list2.Where(x => (x.FirstName.ToUpper().Contains(sname.ToUpper())) || x.LastName.ToUpper().Contains(sname.ToUpper())).ToList();

                    }
                    if (examtype != "")
                    {
                        if (examtype == "All")
                        {
                            //lst = lst.ToList();
                            list2 = list2.ToList();
                        }
                        else
                        {
                            //lst = lst.Where(z => z.exambody.ToUpper() == examtype.ToUpper()).ToList();
                            list2 = list2.Where(x => x.ExamBody == examtype).ToList();
                        }
                    }
                    if (fdt != "" && tdt != "")
                    {
                        try
                        {
                            DateTime dt1 = Convert.ToDateTime(fdt);
                            DateTime dt2 = Convert.ToDateTime(tdt);
                            //lst = lst.Where(z => z.TestDate >= dt1 && z.TestDate <= dt2).ToList();
                            list2 = list2.Where(z => z.Testdate >= dt1 && z.Testdate <= dt2).ToList();
                        }
                        catch (Exception ex)
                        {
                        }
                    }

                }
                catch (Exception ex) { }
            }
            //ViewBag.TotalCount = lst.Count;
            ViewBag.TotalCount = list2.Count;
            return View(list2);
        }

        [HttpPost]
        public ActionResult MultipleResult(FormCollection form)
        {
            try
            {
                var Listt = form["Id"].Split(',').Count();
                for (int i = 0; i <= Listt - 1; i++)
                {
                    try
                    {
                        int Idd = Convert.ToInt32(form["Id"].Split(',')[i]);
                        Tb_IELTSResult tb_ieltsresult = db.Tb_IELTSResult.Find(Idd);
                        double Lst = Convert.ToDouble(form["Lst"].Split(',')[i]);
                        tb_ieltsresult.LBand = Lst;
                        double Wrt = Convert.ToDouble(form["Wrt"].Split(',')[i]);
                        tb_ieltsresult.WBand = Wrt;
                        double Read = Convert.ToDouble(form["Read"].Split(',')[i]);
                        tb_ieltsresult.RBand = Read;
                        double Spk = Convert.ToDouble(form["Spk"].Split(',')[i]);
                        tb_ieltsresult.SBand = Spk;
                        double Ovr = Convert.ToDouble(form["Ovr"].Split(',')[i]);
                        tb_ieltsresult.Overall = Ovr;
                        if (tb_ieltsresult.Passport == null)
                        {
                            tb_ieltsresult.Passport = "-";
                        }
                        db.SaveChanges();
                        string num = "9876533888";
                        string coursname = "";
                        if (tb_ieltsresult.ExamBody == "PTE")
                        {
                            coursname = "PTE";
                        }
                        else
                        {
                            coursname = "IELTS";
                        }
                        if (tb_ieltsresult.StudentType == "Enquiry" && tb_ieltsresult.Overall > 0)
                        {
                            MaEnquiry _objenqry = db.MaEnquiries.Where(z => z.Id == tb_ieltsresult.StudentId).First();
                            Status myobj = new Status();
                            string msg = $"Dear { _objenqry.FirstName}, your { coursname} test score is L: { tb_ieltsresult.LBand}, R: { tb_ieltsresult.RBand}, W: { tb_ieltsresult.WBand}, S: { tb_ieltsresult.SBand}, Overall: { tb_ieltsresult.Overall}. For result card & Visa, cont. SSC { num}";
                            myobj.Sendsms(_objenqry.Mobile, msg);
                            myobj.Sendsms(_objenqry.AlternateNo, msg);
                        }
                        else
                        {
                            if (tb_ieltsresult.Overall > 0)
                            {
                                MaAdmission _objadmission = db.MaAdmissions.Where(z => z.Id == tb_ieltsresult.StudentId).First();
                                Status myobj = new Status();
                                string msg = $"Dear { _objadmission.FirstName}, your { coursname} test score is L: { tb_ieltsresult.LBand}, R: { tb_ieltsresult.RBand}, W: { tb_ieltsresult.WBand}, S: { tb_ieltsresult.SBand}, Overall: { tb_ieltsresult.Overall}. For result card & Visa, cont. SSC { num}";
                                myobj.Sendsms(_objadmission.Mobile, msg);
                                myobj.Sendsms(_objadmission.AlternateNo, msg);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                }
            }
            catch (Exception ex)
            {
            }
            return RedirectToAction("index");
        }

        //
        // POST: /Tb_IELTSResult/Delete/5

        ////[HttpPost, ActionName("Delete")]
        ////public ActionResult DeleteConfirmed(int id)
        ////{
        ////    Tb_IELTSResult tb_ieltsresult = db.Tb_IELTSResult.Find(id);
        ////    db.Tb_IELTSResult.Remove(tb_ieltsresult);
        ////    db.SaveChanges();
        ////    return RedirectToAction("Index");
        ////}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetStudentList(string st)
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            if (st == "Enquiry")
            {
                db = new SSCWebDBEntities1();
                DateTime datee = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["Datee"]);
                DateTime cdatee = DateTime.Now;

                if (cdatee > datee)
                {
                    // List<string> enqabout = new List<string>() { "IELTS", "SPOKEN ENGLISH", "VISA", "PTE", "EXAM BOOKING" };
                    DateTime enqdate = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["EnqDate"]);
                    //var courselist = db.MaEnquiries.Where(z => z.IsAdmission != true && (z.EnqAbt == "IELTS" || z.EnqAbt == "SPOKEN ENGLISH" || z.EnqAbt == "VISA" || z.EnqAbt == "PTE" || z.EnqAbt == "EXAM BOOKING") && z.Cdate >= enqdate).ToList();
                    var courselist = db.MaEnquiries.Where(z => z.IsAdmission != true && z.Cdate >= enqdate).ToList();
                    var colourData = courselist.Select(c => new SelectListItem()
                    {
                        Text = c.Id + " - " + c.FirstName + " " + c.LastName + " " + c.Mobile,
                        Value = c.Id.ToString(),

                    });
                    return Json(colourData, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var courselist = db.MaEnquiries.Where(z => z.EnquiryStyVisa == true || z.EnquiryIELGT == true || z.EnquiryIELAC == true || z.EnquirySpkEng == true || z.EnqPPE == true || z.IsExamBook == true).Where(z => z.IsAdmission != true).ToList();
                    var colourData = courselist.Select(c => new SelectListItem()
                    {
                        Text = c.Id + " - " + c.FirstName + " " + c.LastName + " " + c.Mobile,
                        Value = c.Id.ToString(),

                    });
                    return Json(colourData, JsonRequestBehavior.AllowGet);
                }
                // List<MaEnquiry> _lst = db.MaEnquiries.Where(z => z.EnquiryStyVisa == true || z.EnquiryIELGT == true || z.EnquiryIELAC == true || z.EnquirySpkEng == true).Where(z => z.IsAdmission != true && z.BranchId == branchid).ToList();
            }
            if (st == "Admission")
            {
                // var courselist = db.MockTestStudents.Where(z => z.BranchId == branchid).ToList();
                var courselist = db.MockTestStudents.Where(z => z.BranchId == branchid).ToList();
                //var courselist = db.MockTestStudents.Where(z => z.BranchId == branchid && (z.EnqAbt == "IELTS" || z.EnqAbt == "SPOKEN ENGLISH" || z.EnqAbt == "VISA" || z.EnqAbt == "PTE" || z.EnqAbt == "EXAM BOOKING")).ToList();

                var colourData = courselist.Select(c => new SelectListItem()
                {
                    Text = c.fullname,
                    Value = c.Id.ToString()
                });

                return Json(colourData, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetStudentListEnq(string st, int idd)
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            if (st == "Enquiry")
            {
                db = new SSCWebDBEntities1();
                DateTime datee = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["Datee"]);
                DateTime cdatee = DateTime.Now;

                if (cdatee > datee)
                {
                    DateTime enqdate = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["EnqDate"]);
                    var courselist = db.MaEnquiries.Where(z => z.IsAdmission != true && z.Id == idd && z.Cdate >= enqdate).ToList();
                    var colourData = courselist.Select(c => new SelectListItem()
                    {
                        Text = c.Id + " - " + c.FirstName + " " + c.LastName + " " + c.Mobile,
                        Value = c.Id.ToString(),

                    });
                    return Json(colourData, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var courselist = db.MaEnquiries.Where(z => z.EnquiryStyVisa == true || z.EnquiryIELGT == true || z.EnquiryIELAC == true || z.EnquirySpkEng == true || z.EnqPPE == true || z.IsExamBook == true).Where(z => z.IsAdmission != true).ToList();
                    var colourData = courselist.Select(c => new SelectListItem()
                    {
                        Text = c.Id + " - " + c.FirstName + " " + c.LastName + " " + c.Mobile,
                        Value = c.Id.ToString(),

                    });
                    return Json(colourData, JsonRequestBehavior.AllowGet);
                }
                // List<MaEnquiry> _lst = db.MaEnquiries.Where(z => z.EnquiryStyVisa == true || z.EnquiryIELGT == true || z.EnquiryIELAC == true || z.EnquirySpkEng == true).Where(z => z.IsAdmission != true && z.BranchId == branchid).ToList();

            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }



        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetBookingType(string st)
        {
            db = new SSCWebDBEntities1();
            var courselist = db.St_BookType.Where(z => z.studenttype == st).ToList();
            var colourData = courselist.Select(c => new SelectListItem()
            {
                Text = c.BookingType,
                Value = c.Id.ToString(),

            });
            return Json(colourData, JsonRequestBehavior.AllowGet);

        }



        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult CheckDuplicate(int id)
        {
            db = new SSCWebDBEntities1();
            bool res = false;
            int count = db.Tb_IELTSResult.Where(x => x.StudentId == id).Count();
            if (count > 0)
            {
                res = true;
            }
            return Json(res, JsonRequestBehavior.AllowGet);

        }


        public JsonResult GetStudentData(int? id, string st = "", string Vw = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            if (st == "Enquiry")
            {
                DateTime datee = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["Datee"]);
                DateTime cdatee = DateTime.Now;
                if (cdatee > datee)
                {
                    DateTime enqdate = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["EnqDate"]);
                    var courselist = db.MaEnquiries.Where(z => z.Cdate >= enqdate && z.Id == id && z.IsAdmission != true).ToList();

                    // var courselist = db.MaEnquiries.Where(z => z.EnquiryStyVisa == true || z.EnquiryIELGT == true || z.EnquiryIELAC == true || z.EnqPPE == true || z.IsExamBook == true).Where(z => z.IsAdmission != true && z.Id == id).ToList();
                    if (courselist.Count > 0)
                    {
                        MaEnquiry obj = courselist.Take(1).First();
                        string Course = "";

                        if (obj.EnqAbt != null)
                        {
                            Course = obj.EnqAbt;
                        }

                        var colourData = new
                        {

                            Timing = obj.EnqTimeSlot,
                            mobileno = obj.Mobile,
                            alternateno = obj.AlternateNo,
                            currentcourselevl = Course,
                        };

                        return Json(colourData, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            if (st == "Admission")
            {
                MaAdmission obj = new MaAdmission();
                if (Vw == "C")
                {
                    obj = db.MaAdmissions.Where(z => z.RollNo == id).FirstOrDefault();
                }
                else
                {
                    obj = db.MaAdmissions.Where(z => z.Id == id).FirstOrDefault();
                }

                Tb_CourseHist objhist = new Tb_CourseHist();
                try
                {
                    objhist = db.Tb_CourseHist.Where(z => z.AdmissionId == obj.Id).OrderByDescending(z => z.Id).FirstOrDefault();
                }
                catch (Exception ex)
                {
                }
                if (objhist != null && obj != null)
                {
                    string CourseId = "";
                    //if (objhist.currentcourselevel == "PTE")
                    //{
                    //    CourseId = "PTE";
                    //}
                    //else
                    //{
                    //    CourseId = "IELTS";
                    //}

                    CourseId = objhist.currentcourselevel;

                    var colourData = new
                    {
                        Course = CourseId,
                        Timing = objhist == null ? "" : objhist.MaTime.TCode + "-" + objhist.MaTime.TimeIn + " " + objhist.MaTime.Indesc + "-" + objhist.MaTime.TimeOut + " " + objhist.MaTime.OutDesc,
                        courselevel = objhist == null ? "" : (objhist.currentcourselevel == null ? "-" : objhist.currentcourselevel),
                        mobileno = obj.Mobile,
                        alternateno = obj.AlternateNo == null ? "-" : obj.AlternateNo,
                        currentcourselevl = objhist == null ? "" : objhist.currentcourselevel,
                    };

                    return Json(colourData, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }

            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AutoCompleteDest(string term)
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            //List<MockTestStudent> list = st.StudentList(term);
            //return Json(list, JsonRequestBehavior.AllowGet);
            return Json(db.MockTestStudents.Where(z => z.BranchId == branchid && z.fullname.Contains(term) && (z.EnqAbt == "IELTS" || z.EnqAbt == "SPOKEN ENGLISH" || z.EnqAbt == "VISA" || z.EnqAbt == "PTE" || z.EnqAbt == "EXAM BOOKING")).Select(x => x.fullname), JsonRequestBehavior.AllowGet);

        }
        public JsonResult AutoCompleteDest2(string term)
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            //List<MockTestStudent> list = st.StudentList(term);
            //return Json(list, JsonRequestBehavior.AllowGet);
            //return Json(db.MockTestStudentsEnqs.Where(z => z.BranchId == branchid && z.fullname.Contains(term)).Select(x => x.fullname), JsonRequestBehavior.AllowGet);
            //Commented on 13-Dec-2017 Enquiry of all over branch
            return Json(db.MockTestStudentsEnqs.Where(z => z.fullname.Contains(term)).Select(x => x.fullname), JsonRequestBehavior.AllowGet);

        }
        public JsonResult getid(string stype, int id)
        {
            int idd = 0;
            if (stype == "Admission")
            {
                idd = db.MockTestStudents.Where(x => x.RollNo == id).Select(x => x.Id).FirstOrDefault();
            }

            return Json(idd, JsonRequestBehavior.AllowGet);

        }




    }
}