﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using SSCMvc.Webutil;
namespace SSCMvc.Controllers
{
    [Authorize]
    public class MaBugReportController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /MaBugReport/

        public ActionResult Index()
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BugReport, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            ViewBag.UsersList = new SelectList(db.Users.OrderBy(z => z.Fullname).ToList(), "Id", "username");
            var mabugreports = db.MaBugReports.Include(m => m.MaBranch).Include(m => m.User).OrderByDescending(z => z.Id).ToList();
            ViewBag.TotalCount = mabugreports.Count();
            return View();
        }

        public JsonResult GetData()
        {

            List<MaBugReport> lis = db.MaBugReports.OrderByDescending(x => x.Id).ToList();
            var subCategoryToReturn = lis.Select(S => new {
                SerailNo = S.SrNo,
             Title = S.Title,
               UserName= S.User.Fullname,
               CDate = S.Cdate.Value.ToString("dd/MMM/yyyy hh:mm:ss tt"),
               ReplyStatus = S.ReplyStatus,
               PriorityType = S.PriorityType.Split(':')[0],
              lastupdate = S.LastUpdated.Value.ToString("dd/MMM/yyyy hh:mm:ss tt")
               ,
              
                Action = " <a target='_blanl' href='/MaBugReport/Details/" + S.Id + "'  title='Detail'><i class='icon-eye-open'></i> Detail </a> | <a target='_blanl' href='/MaBugReport/Edit/" + S.Id + "'  title='Edit'><i class='icon-edit'></i> Edit </a>|<a href='#' onclick='Delete("+S.Id+");'  title='Delete'  ><i class='icon-trash'></i> Delete </a> "
            }).ToList();
            //var jsonData = new
            //{
            //    data = from Soc in subCategoryToReturn.ToList() select Soc

            //};
            return Json(subCategoryToReturn, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /MaBugReport/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BugReport, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            MaBugReport mabugreport = db.MaBugReports.Find(id);
            if (mabugreport == null)
            {
                return HttpNotFound();
            }
            return View(mabugreport);
        }

        //
        // GET: /MaBugReport/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BugReport, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.UserId = new SelectList(db.Users, "Id", "Password");
            return View();
        }

        //
        // POST: /MaBugReport/Create

        [HttpPost]
        public ActionResult Create(MaBugReport mabugreport, HttpPostedFileBase File)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BugReport, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);

            int? maxsr = db.MaBugReports.Select(z => z.SrNo).Max();
            if (maxsr == null)
            {
                maxsr = 1;
            }
            else
            {
                maxsr = maxsr + 1;
            }
            mabugreport.SrNo = maxsr;
            mabugreport.BranchId = branchid;
            mabugreport.UserId = uid;
            mabugreport.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
            mabugreport.Status = true;
            mabugreport.LastUpdated = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
            mabugreport.ReplyStatus = CommonUtil.BugStatusOpen;
            if (ModelState.IsValid)
            {
                db.MaBugReports.Add(mabugreport);
                try
                {
                    if (File.ContentLength > 0)
                    {
                        string gid = Guid.NewGuid().ToString().Substring(0, 4);
                        string filename = gid + File.FileName;
                        filename = filename.Replace(" ", "_");
                        filename = filename.Replace("%", "_");
                        filename = filename.Replace("#", "_");
                        var image = System.Web.Helpers.WebImage.GetImageFromRequest();
                        image.Resize(700, 500, true, false);
                        var pic = filename;
                        image.Save(System.IO.Path.Combine(Server.MapPath("/Images/ScreenShot"), pic));
                        mabugreport.Screenshot = filename;
                    }
                    else
                    {
                        mabugreport.Screenshot = "NoImage.jpg";
                    }
                }
                catch (Exception ex)
                {
                    mabugreport.Screenshot = "NoImage.jpg";
                }
                db.SaveChanges();
                User u = db.Users.Where(z => z.Id == mabugreport.UserId).First();

                SendEmail(mabugreport, u);
                return RedirectToAction("Index");
            }

            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", mabugreport.BranchId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Password", mabugreport.UserId);
            return View(mabugreport);
        }

        //
        // GET: /MaBugReport/Edit/5

        public ActionResult Edit(int id = 0, int count = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BugReport, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            MaBugReport mabugreport = db.MaBugReports.Find(id);

            mabugreport.CountBug = count;
            if (mabugreport == null)
            {
                return HttpNotFound();
            }
            ViewBag.bugno = mabugreport.SrNo;
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", mabugreport.BranchId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Password", mabugreport.UserId);
            return View(mabugreport);
        }

        //
        // POST: /MaBugReport/Edit/5

        [HttpPost]
        public ActionResult Edit(MaBugReport mabugreport, HttpPostedFileBase File)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BugReport, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            mabugreport.BranchId = branchid;
            //mabugreport.UserId = uid;
            mabugreport.LastUpdated = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
            mabugreport.Status = true;
            if (ModelState.IsValid)
            {
                try
                {
                    if (File.ContentLength > 0)
                    {
                        string gid = Guid.NewGuid().ToString().Substring(0, 4);
                        string filename = gid + File.FileName;
                        filename = filename.Replace(" ", "_");
                        var image = System.Web.Helpers.WebImage.GetImageFromRequest();
                        image.Resize(700, 500, true, false);
                        var pic = filename;
                        image.Save(System.IO.Path.Combine(Server.MapPath("/Images/ScreenShot"), pic));
                        mabugreport.Screenshot = filename;
                    }
                }
                catch (Exception ex)
                {
                  
                }
                db.Entry(mabugreport).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                User u = db.Users.Where(z => z.Id == mabugreport.UserId).First();

                SendEmail(mabugreport, u);
                return RedirectToAction("Index");
            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", mabugreport.BranchId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Password", mabugreport.UserId);
            return View(mabugreport);
        }

        //
        // GET: /MaBugReport/Delete/5
       // [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BugReport, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            try
            {
                MaBugReport mabugreport = db.MaBugReports.Find(id);
                if (mabugreport == null)
                {
                    return HttpNotFound();
                }

                db.MaBugReports.Remove(mabugreport);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult SearchBug(string fdate = "", string tdate = "", string usr = "All", string rp = "All", string pd = "All")
        {
            ViewBag.UsersList = new SelectList(db.Users.OrderBy(z => z.Fullname).ToList(), "Id", "Fullname");
            List<MaBugReport> mabugreports = db.MaBugReports.Include(m => m.MaBranch).Include(m => m.User).OrderByDescending(z => z.Id).ToList();
            ViewBag.rpstatus = "All";
            ViewBag.pdstatus = "All";
            try
            {
                if (tdate != "" && fdate != "")
                {
                    DateTime dt1 = Convert.ToDateTime(fdate);
                    DateTime dt2 = Convert.ToDateTime(tdate);
                    mabugreports = mabugreports.Where(z => z.Cdate >= dt1 && z.Cdate <= dt2).ToList();
                    ViewBag.TDate = tdate;
                    ViewBag.FDate = fdate;
                }
            }
            catch (Exception ex)
            {
            }
            try
            {
                if (usr != "All")
                {
                    int id = Convert.ToInt32(usr);
                    mabugreports = mabugreports.Where(z => z.UserId == id).ToList();
                    ViewBag.UsersList = new SelectList(db.Users.OrderBy(z => z.Fullname).ToList(), "Id", "Fullname", id);
                }
            }
            catch (Exception ex)
            { }

            try
            {
                if (rp != "All")
                {
                    mabugreports = mabugreports.Where(z => z.ReplyStatus == rp).ToList();
                    ViewBag.rpstatus = rp;
                }
            }
            catch (Exception ex)
            {
            }


            try
            {
                if (pd != "All")
                {
                    mabugreports = mabugreports.Where(z => z.PriorityType.Contains(pd)).ToList();
                    ViewBag.pdstatus = pd;
                }
            }
            catch (Exception ex)
            {
            }


            ViewBag.TotalCount = mabugreports.Count();
            return View("Index", mabugreports);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public void SendEmail(MaBugReport mabug, User u)
        {
            try
            {
                string mail = "team@dstindia.com";

                string frommail = ConfigurationManager.AppSettings["Mail"];
                MailMessage message = new MailMessage();
                message.To.Add(new MailAddress(mail));

                if (u.Email != "" && u.Email != null)
                {
                    if (mabug.ReplyStatus != CommonUtil.BugStatusOpen)
                    {
                        message.CC.Add(new MailAddress(u.Email));
                    }
                }

                if (mabug.CountBug != null)
                {

                    message.Subject = "SSC Bug No:" + mabug.SrNo + " Title " + mabug.Title + " Status:" + mabug.ReplyStatus + " By " + u.Fullname;
                }
                else
                {
                    message.Subject = "SSC Bug " + mabug.Title + " Status:" + mabug.ReplyStatus + " By " + u.Fullname;
                }
                message.From = new MailAddress(frommail);
                message.Body = mabug.BugMessage;
                //string smtpserver = "smtp.gmail.com";

                //  smtpClient.Port = Int32.Parse("587");

                //smtpClient.Credentials = new NetworkCredential(mail, "Mail@123");
                //smtpClient.EnableSsl = true;
                //smtpClient.Send(message);

                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
                SmtpClient smtpClient = new SmtpClient(smtpserver);
                smtpClient.Port = Int32.Parse("25");
                smtpClient.Credentials = new NetworkCredential(mail, ConfigurationManager.AppSettings["credential"].ToString());
                smtpClient.EnableSsl = false;
                smtpClient.Send(message);

            }
            catch (Exception ex)
            {

            }
        }

    }
}