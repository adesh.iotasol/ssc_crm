﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class TbStudLeaveController : Controller
    {
        CommonUtil cu = new CommonUtil();
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /TbStudLeave/

        public ActionResult Index()
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentLeave, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            DateTime dt = DateTime.Now.AddDays(-2);
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            var tb_studleave = db.Tb_StudLeave.Where(z => z.BranchId == branchid).Where(m => m.Cdate > dt).OrderByDescending(z => z.Id);
            ViewBag.TotalCount = tb_studleave.Count();
            return View(tb_studleave.ToList());
        }

        //
        // GET: /TbStudLeave/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentLeave, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            Tb_StudLeave tb_studleave = db.Tb_StudLeave.Find(id);
            if (tb_studleave == null)
            {
                return HttpNotFound();
            }

          
            return View(tb_studleave);
        }

        //
        // GET: /TbStudLeave/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentLeave, UserAction.Add);
                string chbranch = Request.Cookies["chbranch"].Value;

                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.AdmissionId = new SelectList(cu.GetStudents(branchid), "Id", "fullname");
            return View();
        }

        //
        // POST: /TbStudLeave/Create

        [HttpPost]
        public ActionResult Create(Tb_StudLeave tb_studleave)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentLeave, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

            int Userid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            if (ModelState.IsValid)
            {
                tb_studleave.Cdate =  CommonUtil.IndianTime();
                tb_studleave.BranchId = branchid;
                TimeSpan daydiff = tb_studleave.ToDate.Value - tb_studleave.FromDate.Value;
                int days = daydiff.Days;
                tb_studleave.Status = true;
                tb_studleave.AffectDate = false;
                tb_studleave.UserId = Userid;
                tb_studleave.Remarks = tb_studleave.Remarks;
                db.Tb_StudLeave.Add(tb_studleave);
                db.SaveChanges();
                UpdateDueDate(tb_studleave);

                //----------------------Fee Due Date Updation-----------------------------------------
            
                //---------------------------------------------------------------------------------------
                //_printLeave(tb_studleave.Id);
                // return RedirectToAction("Create");
                return RedirectToAction("PrintLeave/" + tb_studleave.Id);
            }
            ViewBag.AdmissionId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_studleave.AdmissionId);

            return View(tb_studleave);
        }

        public void UpdateDueDate( Tb_StudLeave tb_studleave)
        {
            

            TimeSpan daydiff = tb_studleave.ToDate.Value - tb_studleave.FromDate.Value;
            int days = daydiff.Days;
            int admid = Convert.ToInt32(tb_studleave.AdmissionId);
            List<Tb_CourseHist> lsthistory = db.Tb_CourseHist.Where(z => z.AdmissionId == admid && z.IsActive==true).ToList();
            List<Tb_Installment> lstinstal = new List<Tb_Installment>();
            foreach (Tb_CourseHist obj in lsthistory)
            {
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                obj.DueDate = obj.DueDate.Value.AddDays(days + 1);
                lstinstal.AddRange(db.Tb_Installment.Where(z => z.CourseHistId == obj.Id).ToList());
                db.SaveChanges();
            }

            foreach (Tb_Installment obj1 in lstinstal)
            {
                db.Entry(obj1).State = System.Data.Entity.EntityState.Modified;
                obj1.DueDate = obj1.DueDate.Value.AddDays(days + 1);
                db.SaveChanges();
            }


            List<Tb_FeeReceipt> lstfeereceipt = db.Tb_FeeReceipt.Where(z => z.StudentId == tb_studleave.AdmissionId).ToList();
            if (lstfeereceipt.Count > 0)
            {

                try
                {
                    Tb_FeeReceipt objreceipt = lstfeereceipt.OrderByDescending(z => z.Id).First();

                    objreceipt.DueDate = objreceipt.DueDate.Value.Date.AddDays(days + 1);
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting  
                            // the current instance as InnerException  
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }

            }
        }

        ////this is for pop-up
        //public ActionResult _printLeave(int id)
        //{
        //    int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
        //    ViewBag.AdmissionId = new SelectList(cu.GetStudents(branchid), "Id", "fullname");
        //    Tb_StudLeave obj = db.Tb_StudLeave.Find(id);
        //    return PartialView(obj);
        //}

        public ActionResult PrintLeave(int id)
        {
            return View("PrintLeave");
        }

        //this is for receipt print
        public ActionResult PrintReport(int id)
        {
            //print of the course history during edit time

            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetStuLveReportList(id);

            var renderedBytes = reportViewModel.RenderReport();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
        }



        //
        // GET: /TbStudLeave/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentLeave, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            Tb_StudLeave tb_studleave = db.Tb_StudLeave.Find(id);
            if (tb_studleave == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdmissionId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_studleave.AdmissionId);

            int admissionid = (int)tb_studleave.AdmissionId;

            MaAdmission objadmiss = db.MaAdmissions.Find(admissionid);
            ViewBag.detail = admissionid;
            ViewBag.detail1 = db.Tb_StudLeave.Where(z => z.AdmissionId == admissionid).Where(z => z.BranchId == branchid).ToList();


            return View(tb_studleave);
        }

        //
        // POST: /TbStudLeave/Edit/5

        [HttpPost]
        public ActionResult Edit(Tb_StudLeave tb_studleave)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentLeave, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            int Userid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            if (ModelState.IsValid)
            {
                db.Entry(tb_studleave).State = System.Data.Entity.EntityState.Modified;
                tb_studleave.BranchId = branchid;
                tb_studleave.UserId = Userid;
                tb_studleave.Status = true;
                tb_studleave.Remarks = tb_studleave.Remarks;
                db.SaveChanges();
                //if (tb_studleave.AffectDate==true)
                //{
                //    UpdateDueDate(tb_studleave);
                //}


                ////----------------------Fee Due Date Updation-----------------------------------------
                //TimeSpan daydiff = tb_studleave.ToDate.Value - tb_studleave.FromDate.Value;
                //int days = daydiff.Days;
                //int admid = Convert.ToInt32(tb_studleave.AdmissionId);
                //List<Tb_CourseHist> lsthistory = db.Tb_CourseHist.Where(z => z.AdmissionId == admid).ToList();
                //foreach (Tb_CourseHist obj in lsthistory)
                //{
                //    db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                //    obj.DueDate = obj.DueDate.Value.AddDays(days + 1);
                //    db.SaveChanges();
                //}
                //List<Tb_Installment> lstinstal = new List<Tb_Installment>();
                //foreach (Tb_Installment obj1 in lstinstal)
                //{
                //    db.Entry(obj1).State = System.Data.Entity.EntityState.Modified;
                //    obj1.DueDate = obj1.DueDate.Value.AddDays(days + 1);
                //    db.SaveChanges();
                //}
                ////---------------------------------------------------------------------------------------
                return RedirectToAction("Index");
            }
            ViewBag.AdmissionId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_studleave.AdmissionId);
            return View(tb_studleave);
        }

        //
        // GET: /TbStudLeave/Delete/5

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Delete(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentLeave, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }




            try
            {
                Tb_StudLeave stuleave = db.Tb_StudLeave.Find(id);
                db.Tb_StudLeave.Remove(stuleave);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Search(string sname = "", string roll = "")
        {
            List<Tb_StudLeave> lst = db.Tb_StudLeave.ToList();
            ViewBag.SName = sname;
            ViewBag.ROLL = roll;
            if (sname != "")
            {
                sname = sname.ToUpper();
                lst = lst.Where(z => z.MaAdmission.FirstName.ToUpper().Contains(sname)).ToList();
            }
            if (roll != "")
            {
                try
                {
                    int rollno = Convert.ToInt32(roll);
                    lst = lst.Where(z => z.MaAdmission.RollNo == rollno).ToList();
                }
                catch (Exception ex)
                { }
            }
            return View(lst);
        }
        /// <summary>
        /// Used at Admission list index 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PartialViewResult _StudentLeaveList(int id)
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            var tb_studleave = db.Tb_StudLeave.Where(z => z.AdmissionId == id).OrderByDescending(z => z.Id).ToList();
            ViewBag.TotalCount = tb_studleave.Count();
            return PartialView(tb_studleave.ToList());
        }
    }
}