﻿using SSCMvc.Models;
using SSCMvc.Webutil;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSCMvc.Controllers
{
    public class HomeworkController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /Ma_Area/

        public ActionResult Index(string msg)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Homework, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (msg == "Updt")
            {
                ViewBag.message = "Record Updated Successfully";
            }
            if (msg == "sve")
            {
                ViewBag.message = "Record Added Successfully";
            }
            if (msg == "dup")
            {
                ViewBag.error = "Record With Same name already exists";
            }
            List<Tb_Homework> lst = new List<Tb_Homework>();
            lst = db.Tb_Homework.ToList();
            ViewBag.TotalCount = lst.Count();
         
            return View(lst);
        }

        //
        // GET: /Ma_Area/Details/5

        public ActionResult Details(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Homework, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            Tb_Homework home = db.Tb_Homework.Find(id);
            if (home == null)
            {
                return HttpNotFound();
            }
            return View(home);
        }

        //
        // GET: /Ma_Area/Create

        public ActionResult Create()
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Homework, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            return View();
        }

        //
        // POST: /Ma_Area/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tb_Homework home, HttpPostedFileBase File)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Homework, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (File.ContentLength > 0)
                    {
                        string gid = Guid.NewGuid().ToString().Substring(0, 4);
                        string filename = gid + File.FileName;
                        filename = filename.Replace(" ", "_");
                        filename = filename.Replace("%", "_");
                        filename = filename.Replace("#", "_");
                        File.SaveAs(System.IO.Path.Combine(Server.MapPath("/Download"),filename));
                        home.HFile = filename;
                    }
                    else
                    {
                        home.HFile = "NoImage.jpg";
                      
                    }
                }
                catch (Exception ex)
                {
                    home.HFile = "NoImage.jpg";
                }

                db.Tb_Homework.Add(home);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2601)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
                return RedirectToAction("Index", new { @msg = "sve" });
            }

            return View(home);
        }

        //
        // GET: /Ma_Area/Edit/5

        public ActionResult Edit(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Homework, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            Tb_Homework home = db.Tb_Homework.Find(id);
            if (home == null)
            {
                return HttpNotFound();
            }
            return View(home);
        }

        //
        // POST: /Ma_Area/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tb_Homework home, HttpPostedFileBase File)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Homework, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
                try
                {
                    if (File.ContentLength > 0)
                    {
                        string gid = Guid.NewGuid().ToString().Substring(0, 4);
                        string filename = gid + File.FileName;
                        filename = filename.Replace(" ", "_");
                        File.SaveAs(System.IO.Path.Combine(Server.MapPath("/Download"), filename));
                        home.HFile = filename;
                    }
                }
                catch (Exception ex)
                {
                  
                }
                db.Entry(home).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2601)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
                return RedirectToAction("Index", new { @msg = "Updt" });
            }
            return View(home);
        }

    
      
        public ActionResult Delete(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Homework, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            try
            {
                Tb_Homework home = db.Tb_Homework.Find(id);

                db.Tb_Homework.Remove(home);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /Ma_Area/Delete/5

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


    }
}