﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class Tb_StudentTransferController : Controller
    {
        CommonUtil cu = new CommonUtil();
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /Tb_StudentTransfer/

        //public ActionResult Index(string roll = "", string name = "", string branchto = "", string branchfrom = "")
        public ActionResult Index(string fdt = "", string tdt = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentTransfer, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            int userid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            string usertype = Request.Cookies[CommonUtil.CookieUsertype].Value;
            List<Tb_StudentTransfer> lst = new List<Tb_StudentTransfer>();
            lst = new List<Tb_StudentTransfer>();

            if (usertype == CommonUtil.UserTypeSuperAdmin)
            {
                lst = db.Tb_StudentTransfer.Where(z => z.BranchFrom == branchid).ToList();
            }
            else
            {
                lst = db.Tb_StudentTransfer.Where(z => z.TransferTo == userid).ToList();
            }
            if (fdt != "" && tdt != "")
            {
                DateTime fdate = Convert.ToDateTime(fdt);
                DateTime tdate = Convert.ToDateTime(tdt);
                ViewBag.Fdate = fdt;
                ViewBag.Tdate = tdt;
                lst = lst.Where(z => z.TransferDate >= fdate && z.TransferDate <= tdate).ToList();
            }

            ViewBag.TotalCount = lst.Count;

            return View(lst);
        }

        public ActionResult PrintData(string fdt = "", string tdt = "", string format = "")
        {
            int userid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            string usertype = Request.Cookies[CommonUtil.CookieUsertype].Value;
            List<Tb_StudentTransfer> lst = new List<Tb_StudentTransfer>();
            lst = new List<Tb_StudentTransfer>();

            if (usertype == CommonUtil.UserTypeSuperAdmin)
            {
                lst = db.Tb_StudentTransfer.Where(z => z.BranchFrom == branchid).ToList();
            }
            else
            {
                lst = db.Tb_StudentTransfer.Where(z => z.TransferTo == userid).ToList();
            }
            if (fdt != "" && tdt != "")
            {
                DateTime fdate = Convert.ToDateTime(fdt);
                DateTime tdate = Convert.ToDateTime(tdt);
                ViewBag.Fdate = fdt;
                ViewBag.Tdate = tdt;
                lst = lst.Where(z => z.TransferDate >= fdate && z.TransferDate <= tdate).ToList();
            }
            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetTransferStudent(lst, format);

            var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
        }

        //
        // GET: /Tb_StudentTransfer/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentTransfer, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            Tb_StudentTransfer tb_studenttransfer = db.Tb_StudentTransfer.Find(id);
            if (tb_studenttransfer == null)
            {
                return HttpNotFound();
            }
            ViewBag.StudentId = tb_studenttransfer.MaAdmission.RollNo + " - " + tb_studenttransfer.MaAdmission.FirstName + " " + tb_studenttransfer.MaAdmission.LastName;
            ViewBag.FatherName = tb_studenttransfer.MaAdmission.FatherName;
            ViewBag.DOB = string.Format("{0:dd MMM,yyyy}", tb_studenttransfer.MaAdmission.DOB);
            ViewBag.Mobile = tb_studenttransfer.MaAdmission.Mobile;


            ViewBag.BranchTo = tb_studenttransfer.MaBranch.BramchName;
            ViewBag.BranchName = tb_studenttransfer.MaBranch1.BramchName;

            ViewBag.TransferTo = tb_studenttransfer.User.Fullname;
            ViewBag.TransferBy = tb_studenttransfer.User1.Fullname;
            return View(tb_studenttransfer);
        }

        //
        // GET: /Tb_StudentTransfer/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentTransfer, UserAction.Add);
                string chbranch = Request.Cookies["chbranch"].Value;

                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname");
            ViewBag.BranchTo = new SelectList(db.MaBranches.Where(z => z.Id != branchid), "Id", "BramchName");
            ViewBag.BranchName = db.MaBranches.Where(z => z.Id == branchid).Select(z => z.BramchName).First();
            ViewBag.TransferTo = new SelectList(db.Users.Where(z => z.BranchId != branchid).ToList(), "Id", "FullName");
            ViewBag.TransferBy = new SelectList(db.Users.Where(z => z.BranchId == branchid).ToList(), "Id", "FullName");
            return View();
        }

        //
        // POST: /Tb_StudentTransfer/Create

        [HttpPost]
        public ActionResult Create(Tb_StudentTransfer tb_studenttransfer)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentTransfer, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            tb_studenttransfer.BranchFrom = branchid;
            tb_studenttransfer.TransferDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            tb_studenttransfer.CDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            try
            {
                db.Tb_StudentTransfer.Add(tb_studenttransfer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
            }
            ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_studenttransfer.StudentId);
            ViewBag.BranchTo = new SelectList(db.MaBranches.Where(z => z.Id != branchid), "Id", "BramchName", tb_studenttransfer.BranchTo);
            ViewBag.BranchName = db.MaBranches.Where(z => z.Id == branchid).Select(z => z.BramchName).First();
            ViewBag.TransferTo = new SelectList(db.Users.Where(z => z.BranchId != branchid).ToList(), "Id", "FullName", tb_studenttransfer.TransferTo);
            ViewBag.TransferBy = new SelectList(db.Users.Where(z => z.BranchId == branchid).ToList(), "Id", "FullName", tb_studenttransfer.TransferBy);
            return View(tb_studenttransfer);
        }

        //
        // GET: /Tb_StudentTransfer/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentTransfer, UserAction.Edit);
                string chbranch = Request.Cookies["chbranch"].Value;

                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            Tb_StudentTransfer tb_studenttransfer = db.Tb_StudentTransfer.Find(id);
            if (tb_studenttransfer == null)
            {
                return HttpNotFound();
            }

            ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_studenttransfer.StudentId);

            ViewBag.BranchTo = new SelectList(db.MaBranches.Where(z => z.Id != branchid), "Id", "BramchName", tb_studenttransfer.BranchTo);
            ViewBag.BranchName = db.MaBranches.Where(z => z.Id == branchid).Select(z => z.BramchName).First();

            ViewBag.TransferTo = new SelectList(db.Users.Where(z => z.BranchId != branchid).ToList(), "Id", "FullName", tb_studenttransfer.TransferTo);
            ViewBag.TransferBy = new SelectList(db.Users.Where(z => z.BranchId == branchid).ToList(), "Id", "FullName", tb_studenttransfer.TransferBy);
            ViewBag.UserName = tb_studenttransfer.TransferTo;
            return View(tb_studenttransfer);
        }

        //
        // POST: /Tb_StudentTransfer/Edit/5

        [HttpPost]
        public ActionResult Edit(Tb_StudentTransfer tb_studenttransfer)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentTransfer, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            tb_studenttransfer.BranchFrom = branchid;
            try
            {
                db.Entry(tb_studenttransfer).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

            }
            ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_studenttransfer.StudentId);

            ViewBag.BranchTo = new SelectList(db.MaBranches.Where(z => z.Id != branchid), "Id", "BramchName", tb_studenttransfer.BranchTo);
            ViewBag.BranchName = db.MaBranches.Where(z => z.Id == branchid).Select(z => z.BramchName).First();

            ViewBag.TransferTo = new SelectList(db.Users.Where(z => z.BranchId != branchid).ToList(), "Id", "FullName", tb_studenttransfer.TransferTo);
            ViewBag.TransferBy = new SelectList(db.Users.Where(z => z.BranchId == branchid).ToList(), "Id", "FullName", tb_studenttransfer.TransferBy);
            return View(tb_studenttransfer);
        }

        //
        // GET: /Tb_StudentTransfer/Delete/5

        public ActionResult Delete(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentTransfer, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            Tb_StudentTransfer tb_studenttransfer = db.Tb_StudentTransfer.Find(id);
            if (tb_studenttransfer == null)
            {
                return HttpNotFound();
            }
            return View(tb_studenttransfer);
        }

        //
        // POST: /Tb_StudentTransfer/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentTransfer, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            Tb_StudentTransfer tb_studenttransfer = db.Tb_StudentTransfer.Find(id);
            db.Tb_StudentTransfer.Remove(tb_studenttransfer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult GetStudentData(int admid)
        {
            MaAdmission maadmission = db.MaAdmissions.Where(z => z.Id == admid).First();
            string courseinfo = "No Course";
            try
            {
                Tb_CourseHist objCoursehist = db.Tb_CourseHist.Where(z => z.AdmissionId == admid && z.Status == true).First();
                courseinfo = objCoursehist.MaCourse.CourseName;
            }
            catch (Exception ex)
            {
            }
            var AdmisInfo = new
            {

                Mobile = maadmission.Mobile,
                Father = maadmission.FatherName,
                DOB = maadmission.DOB.Value.ToShortDateString(),
                CName = courseinfo,
            };


            return Json(AdmisInfo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUsersByBranch(int id)
        {
            var userlist = db.Users.Where(z => z.BranchId == id).ToList();

            var ulistdata = userlist.Select(c => new SelectListItem()
            {
                Text = c.Fullname,
                Value = c.Id.ToString(),

            });

            return Json(ulistdata, JsonRequestBehavior.AllowGet);
        }



        public ActionResult ManageStudentTransfer(int id)
        {
            Tb_StudentTransfer tb_studenttransfer = db.Tb_StudentTransfer.Find(id);
            if (tb_studenttransfer == null)
            {
                return HttpNotFound();
            }
            ViewBag.StudentId = tb_studenttransfer.MaAdmission.RollNo + " - " + tb_studenttransfer.MaAdmission.FirstName + " " + tb_studenttransfer.MaAdmission.LastName;
            ViewBag.FatherName = tb_studenttransfer.MaAdmission.FatherName;
            ViewBag.DOB = string.Format("{0:dd MMM,yyyy}", tb_studenttransfer.MaAdmission.DOB);
            ViewBag.Mobile = tb_studenttransfer.MaAdmission.Mobile;


            ViewBag.BranchTo = tb_studenttransfer.MaBranch.BramchName;
            ViewBag.BranchName = tb_studenttransfer.MaBranch1.BramchName;

            ViewBag.TransferTo = tb_studenttransfer.User.Fullname;
            ViewBag.TransferBy = tb_studenttransfer.User1.Fullname;

            return View(tb_studenttransfer);
        }
        [HttpPost]
        public ActionResult ManageStudentTransfer(Tb_StudentTransfer tb_studenttransfer)
        {

            try
            {
                string chbranch = Request.Cookies["chbranch"].Value;
                if (chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

                int userid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
                int stuid = Convert.ToInt32(tb_studenttransfer.StudentId);
                db.Entry(tb_studenttransfer).State = System.Data.Entity.EntityState.Modified;
                tb_studenttransfer.TransferDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                db.SaveChanges();
                string utype = Request.Cookies[CommonUtil.CookieUsertype].Value;
                string refadd = CommonUtil.HomeAddress(utype);
                int? OldLsNo = 0;
                int? NewLsNo = 0;

                Tb_FeeReceipt obj = db.Tb_FeeReceipt.Where(z => z.StudentId == stuid).OrderByDescending(a => a.Id).FirstOrDefault();
                OldLsNo = obj.LocSerialNo;

                try
                {
                    /*Update MaAdmission*/
                    cu.UpdateMaAdmission(stuid, branchid, userid);
                }
                catch (Exception ex)
                { }
                try
                {
                    /*Update fee receipt*/
                    cu.UpdateFeeReceipt(stuid, branchid, userid);
                }
                catch (Exception ex)
                { }
                try
                {
                    //update table old and new ls no here
                    SSCWebDBEntities1 db1 = new SSCWebDBEntities1();
                    Tb_FeeReceipt obj1 = db1.Tb_FeeReceipt.Where(z => z.StudentId == stuid).OrderByDescending(a => a.Id).FirstOrDefault();
                    NewLsNo = obj1.LocSerialNo;
                    tb_studenttransfer.OldLsNo = OldLsNo;
                    tb_studenttransfer.NewLsNo = NewLsNo;
                    db.SaveChanges();
                    //update table old and new ls no ends here
                }
                catch (Exception ex)
                { }
                try
                {
                    /*update tb_instalment*/
                    cu.UpdateInstalment(stuid, branchid);
                }
                catch (Exception ex)
                { }
                try
                {
                    /*update tb_studentleave*/
                    cu.UpdateStudentLeave(stuid, branchid);
                }
                catch (Exception ex)
                { }
                try
                {
                    /*update tb_fee refund*/
                    cu.UpdateFeeRefund(stuid, branchid);
                }
                catch (Exception ex)
                { }

                try
                {
                    /*update tb_fee adjustment*/
                    cu.UpdateFeeAdjustment(stuid, branchid);
                }
                catch (Exception ex)
                { }
                try
                {
                    /*update tb_fee adjustment*/
                    cu.UpdateMaEnquiry(stuid, branchid);
                }
                catch (Exception ex)
                { }
                return RedirectToAction("ListOfStudent");
            }
            catch (Exception ex)
            {
            }
            //ViewBag.StudentId = tb_studenttransfer.MaAdmission.FirstName + " " + tb_studenttransfer.MaAdmission.LastName;
            //ViewBag.FatherName = tb_studenttransfer.MaAdmission.FatherName;
            //ViewBag.DOB = string.Format("{0:dd MMM,yyyy}", tb_studenttransfer.MaAdmission.DOB);
            //ViewBag.Mobile = tb_studenttransfer.MaAdmission.Mobile;

            ViewBag.StudentId = tb_studenttransfer.GetStudentid();
            ViewBag.FatherName = tb_studenttransfer.GetFatherName();
            ViewBag.DOB = string.Format("{0:dd MMM,yyyy}", tb_studenttransfer.GetDOB());
            ViewBag.Mobile = tb_studenttransfer.GetMobile();


            ViewBag.BranchTo = tb_studenttransfer.GetBranchTo();
            ViewBag.BranchName = tb_studenttransfer.GetBranchName();

            ViewBag.TransferTo = tb_studenttransfer.GetTransferTo();
            ViewBag.TransferBy = tb_studenttransfer.GetTransferFrom();

            return RedirectToAction("Index");
        }

        public ActionResult ListOfStudent()
        {
            int userid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            string usrtype = Request.Cookies[CommonUtil.CookieUsertype].Value;
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            List<Tb_StudentTransfer> lst = new List<Tb_StudentTransfer>();
            if (usrtype == CommonUtil.UserTypeSuperAdmin || usrtype == CommonUtil.UserTypeAdmin)
            {
                lst = db.Tb_StudentTransfer.Where(z => z.Status == false).ToList();

            }
            else
            {
                lst = db.Tb_StudentTransfer.Where(z => z.TransferTo == userid && z.Status == false).ToList();
            }
            return View(lst);
        }

        public PartialViewResult _PendingStudentList(int id = 0)
        {
            List<Tb_StudentTransfer> lst = new List<Tb_StudentTransfer>();
            int userid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            string usrtype = Request.Cookies[CommonUtil.CookieUsertype].Value;
            if (usrtype == CommonUtil.UserTypeSuperAdmin || usrtype == CommonUtil.UserTypeAdmin)
            {

                lst = db.Tb_StudentTransfer.Where(z => z.Status == false).ToList();

            }
            else
            {
                lst = db.Tb_StudentTransfer.Where(z => z.TransferTo == userid && z.Status == false).ToList();
            }
            return PartialView(lst);
        }
    }
}