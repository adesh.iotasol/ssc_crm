﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using System.Data.SqlClient;
using SSCMvc.Webutil;


namespace SSCMvc.Controllers
{
    public class MaRoleController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /MaRole/

        public ActionResult Index(string msg)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;

                Prmsn = Webutil.Permission.CheckPermission(roleid, CommonUtil.AddRole, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (msg == "Updt")
            {
                ViewBag.message = "Record Updated Successfully";
            }
            if (msg == "sve")
            {
                ViewBag.message = "Record Added Successfully";
            }
            if (msg == "dup")
            {
                ViewBag.error = "Record With Same name already exists";
            }
            if (msg == "rolup")
            {
                ViewBag.message = "Permission's Updated Successfully";
            }
            List<MaRole> lst = new List<MaRole>();
            lst = db.MaRoles.ToList();
            ViewBag.TotalCount = lst.Count();
            return View(lst);
        }

        //
        // GET: /MaRole/Details/5

        public ActionResult Details(int id = 0)
        {

            MaRole marole = db.MaRoles.Find(id);
            if (marole == null)
            {
                return HttpNotFound();
            }
            return View(marole);
        }

        public ActionResult AddRights(int id = 0)
        {
            List<MaSecurity> lis = db.MaSecurities.ToList();
           
            foreach (MaSecurity item in lis)
            {
                if (db.Tb_Permission.ToList().Exists(x => x.SecurityId == item.Id && x.RollId == id) == false)
                {
                    Tb_Permission objper = new Tb_Permission();
                    objper.RollId = id;
                    objper.SecurityId = item.Id;

                    objper.IsAdd = true;
                    objper.IsView = true;
                    objper.IsEdit = true;
                    objper.IsDelete = true;
                    db.Tb_Permission.Add(objper);
                    db.SaveChanges();
                }
            }

            return RedirectToAction("Permission", new { @id = id });
        }

        //
        // GET: /MaRole/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;

                Prmsn = Webutil.Permission.CheckPermission(roleid, CommonUtil.AddRole, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            return View();
        }

        //
        // POST: /MaRole/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MaRole marole)
        {

            if (ModelState.IsValid)
            {
                marole.RollName = marole.RollName.ToUpper();
                marole.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                marole.Status = true;
                db.MaRoles.Add(marole);
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index", new { @msg = "sve" });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2627)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }

            }

            return View(marole);
        }

        //
        // GET: /MaRole/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;

                Prmsn = Webutil.Permission.CheckPermission(roleid, CommonUtil.AddRole, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            MaRole marole = db.MaRoles.Find(id);
            if (marole == null)
            {
                return HttpNotFound();
            }
            return View(marole);
        }

        //
        // POST: /MaRole/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MaRole marole)
        {
            if (ModelState.IsValid)
            {
                marole.RollName = marole.RollName.ToUpper();
                db.Entry(marole).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index", new { @msg = "Updt" });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2627)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
            }
            return View(marole);
        }

        //
        // GET: /MaRole/Delete/5
        
        public ActionResult Delete(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;

                Prmsn = Webutil.Permission.CheckPermission(roleid, CommonUtil.AddRole, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            MaRole marole = db.MaRoles.Find(id);
            try
            {
                db.MaRoles.Remove(marole);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        //
        // POST: /MaRole/Delete/5


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Permission(int id = 0,string name="")
        {

            List<Tb_Permission> lst = new List<Tb_Permission>();
            ViewBag.Idd = id;
            if (id == 0)
            {
                return RedirectToAction("Index");
            }

            try
            {
                lst = db.Tb_Permission.Where(a => a.RollId == id ).ToList();
                if (name=="given")
                {
                    lst = db.Tb_Permission.Where(a => a.RollId == id && (a.IsAdd == true || a.IsEdit == true || a.IsView == true || a.IsDelete == true)).ToList();
                 }
                else if(name=="all")
                {
                    lst = db.Tb_Permission.Where(a => a.RollId == id).ToList();
                }
               
            
                //lst = db.Tb_Permission.Where(a => a.RollId == id && (a.IsAdd == true || a.IsEdit == true || a.IsView == true || a.IsDelete == true)).ToList();

                string roll = db.MaRoles.Where(a => a.Id == id).Select(a => a.RollName).FirstOrDefault();

                ViewBag.Role = roll;

                if (lst.Count == 0)
                {
                    List<MaSecurity> seclst = new List<MaSecurity>();
                    seclst = db.MaSecurities.ToList();
                    foreach (var item in seclst)
                    {
                        Tb_Permission obj = new Tb_Permission();
                        obj.RollId = id;
                        obj.SecurityId = item.Id;
                        obj.IsAdd = true;
                        obj.IsEdit = true;
                        obj.IsView = true;
                        obj.IsDelete = false;
                        db.Tb_Permission.Add(obj);
                        db.SaveChanges();
                    }

                    lst = db.Tb_Permission.Where(a => a.RollId == id).ToList();
                }

                ViewBag.TotalCount = lst.Count;
            }
            catch (Exception)
            {
            }
            return View(lst);
        }

        [HttpPost]
        public ActionResult Permission(FormCollection frm)
        {
            try
            {
                string[] myhdnadd = frm["hadd"].ToString().Split(',');
                string[] myhdnedit = frm["hedit"].ToString().Split(',');
                string[] myview = frm["hview"].ToString().Split(',');
                string[] mydelete = frm["hdelete"].ToString().Split(',');
                string[] myidarray = frm["hdnid"].ToString().Split(',');


                if (myidarray.Length > 0)
                {
                    for (int j = 0; j < myidarray.Length; j++)
                    {
                        int id = Convert.ToInt32(myidarray[j]);

                        Tb_Permission objsecurity = db.Tb_Permission.Where(z => z.Id == id).FirstOrDefault();
                        try
                        {
                            string add = myhdnadd[j].ToString();
                            objsecurity.IsAdd = Convert.ToBoolean(add);
                        }
                        catch (Exception ex)
                        { }

                        try
                        {
                            string edit = myhdnedit[j].ToString();
                            objsecurity.IsEdit = Convert.ToBoolean(edit);
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            string delete = mydelete[j].ToString();
                            objsecurity.IsDelete = Convert.ToBoolean(delete);
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            string view = myview[j].ToString();
                            objsecurity.IsView = Convert.ToBoolean(view);
                        }
                        catch (Exception ex)
                        { }

                        db.SaveChanges();
                    }
                    return RedirectToAction("Index", new { @msg = "rolup" });
                }
            }
            catch (Exception)
            {
            }


            return View();
        }
    }
}