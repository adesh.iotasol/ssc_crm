﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class MaDiscountController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /MaDiscount/

        public ActionResult Index()
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Discount, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            var madiscounts = db.MaDiscounts.Include(m => m.MaBranch);
            ViewBag.TotalCount = madiscounts.Count();
            return View(madiscounts.ToList());
        }

        //
        // GET: /MaDiscount/Details/5

        public ActionResult Details(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Discount, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            MaDiscount madiscount = db.MaDiscounts.Find(id);
            if (madiscount == null)
            {
                return HttpNotFound();
            }
            return View(madiscount);
        }

        //
        // GET: /MaDiscount/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Discount, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            return View();
        }

        //
        // POST: /MaDiscount/Create

        [HttpPost]
        public ActionResult Create(MaDiscount madiscount)
        {
            if (ModelState.IsValid)
            {
                db.MaDiscounts.Add(madiscount);

                int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
                madiscount.BranchId = branchid;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", madiscount.BranchId);
            return View(madiscount);
        }

        //
        // GET: /MaDiscount/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Discount, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            MaDiscount madiscount = db.MaDiscounts.Find(id);
            if (madiscount == null)
            {
                return HttpNotFound();
            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", madiscount.BranchId);
            return View(madiscount);
        }

        //
        // POST: /MaDiscount/Edit/5

        [HttpPost]
        public ActionResult Edit(MaDiscount madiscount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(madiscount).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", madiscount.BranchId);
            return View(madiscount);
        }

        //
        // GET: /MaDiscount/Delete/5

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Delete(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Discount, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            try
            {
                MaDiscount madiscount = db.MaDiscounts.Find(id);

                db.MaDiscounts.Remove(madiscount);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}