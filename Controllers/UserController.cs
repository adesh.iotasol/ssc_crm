﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using System.Data.Entity.Validation;
using SSCMvc.Webutil;
namespace SSCMvc.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        CommonUtil util = new CommonUtil();
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        MasterListing listing = new MasterListing();
        //
        // GET: /User/

        public ActionResult Index(string msg)
        {
            Session["user"] = null;


            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.User, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (msg == "Y")
            {
                ViewBag.reset = "Your Password Has been Reset Successfully.Please Check Your Email.";
            }
            if (msg == "S")
            {
                ViewBag.reset = "Mail Sent Successfully";
            }
            int userid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            var users = db.Users.Where(a => a.Status == true).Include(u => u.MaDesignation).OrderByDescending(z => z.Id);
            string Utype = Request.Cookies[CommonUtil.CookieUsertype].Value;
            List<User> userlist = users.ToList();
            if (Utype == CommonUtil.UserTypeSuperAdmin)
            {
                userlist= userlist.ToList();
            }
            else if (Utype == CommonUtil.UserTypeAdmin)
            {
                userlist= userlist.Where(x => x.Usertype != CommonUtil.UserTypeSuperAdmin).ToList();
            }
            else if (Utype == CommonUtil.UserTypeManager)
            {
                userlist= userlist.Where(x => x.Usertype != CommonUtil.UserTypeSuperAdmin && x.Usertype != CommonUtil.UserTypeAdmin).ToList();
            }
            else
            {
                userlist= userlist.Where(x => x.Usertype != CommonUtil.UserTypeSuperAdmin && x.Usertype != CommonUtil.UserTypeAdmin && x.Usertype != CommonUtil.UserTypeManager).ToList();
            }

            ViewBag.BranchName = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.username = new SelectList(db.Users, "Id", "username");
            ViewBag.TotalCount = users.Count();
            return View(userlist.ToList());
            
        }

        public ActionResult changebranch(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.SwitchBranch, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            MaBranch bdetail = db.MaBranches.Find(id);

            int? branchid = id;
            HttpCookie cke = new HttpCookie(CommonUtil.CookieBranchid);
            cke.Value = branchid.ToString();
            Response.Cookies.Add(cke);

            string branchname = bdetail.BramchName;
            HttpCookie ckbranch = new HttpCookie(CommonUtil.BranchName);
            ckbranch.Value = branchname;
            Response.Cookies.Add(ckbranch);


            int uid = Convert.ToInt32(Request.Cookies["Userid"].Value);
            SSCMvc.Models.User user = db.Users.Find(uid);
            if(user.BranchId!=branchid)
            {
                HttpCookie chbranch = new HttpCookie("chbranch");
                chbranch.Value = "Y";
                Response.Cookies.Add(chbranch);
            }
            else
            {
                HttpCookie chbranch = new HttpCookie("chbranch");
                chbranch.Value = "N";
                Response.Cookies.Add(chbranch);
            }
           

            return  RedirectToAction("Index", "Home");
        }

        //
        // GET: /User/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.User, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }


        public ActionResult _FilterCust(int BranchName = 0, int username = 0, string txtfirstname = "", string Usertype = "")
        {
            try
            {
                int branchid = Convert.ToInt32(BranchName);
                List<User> lstusr = db.Users.ToList();
                if (branchid > 0)
                {
                    lstusr = lstusr.Where(z => z.BranchId == branchid).ToList();
                    ViewBag.branch = branchid;
                }

                int userid = Convert.ToInt32(username);
                if (userid > 0)
                {
                    lstusr = lstusr.Where(z => z.Id == userid).ToList();
                    ViewBag.userid = userid;
                }

                if (txtfirstname != "")
                {
                    lstusr = lstusr.Where(z => z.Fullname.ToUpper().Contains(txtfirstname.ToUpper())).ToList();
                    ViewBag.firstname = txtfirstname;
                }
                if (Usertype == "Active")
                {
                    lstusr = lstusr.Where(z => z.Status == true).ToList();
                    ViewBag.Usertype = Usertype;
                }
                if (Usertype == "InActive")
                {
                    lstusr = lstusr.Where(z => z.Status == false).ToList();
                    ViewBag.Usertype = Usertype;
                }

                ViewBag.username = new SelectList(db.Users, "Id", "username");
                ViewBag.BranchName = new SelectList(db.MaBranches, "Id", "BramchName");
                ViewBag.TotalCount = lstusr.Count;
                return View("Index", lstusr);
            }
            catch (Exception ex)
            {
                ViewBag.BranchName = new SelectList(db.MaBranches, "Id", "BramchName");
                List<User> lstusr = db.Users.OrderByDescending(z => z.Id).ToList();
                lstusr = lstusr.ToList();
                ViewBag.TotalCount = lstusr.Count;
                return View("Index", lstusr);
            }
        }

        //
        // GET: /User/Create

        public ActionResult Create(int id = 0, int bid = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.User, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (bid == 0)
            {
                ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            }
            else
            {

                ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", bid);
            }


            if (bid > 0)
            {
                ViewBag.branid = bid;
            }
            string Utype = Request.Cookies[CommonUtil.CookieUsertype].Value;
            List<MaRole> roles = db.MaRoles.ToList();
            if (Utype== CommonUtil.UserTypeSuperAdmin)
            {
                roles = roles.Where(x => x.RLevel >= 0).ToList();
            }
           else if (Utype == CommonUtil.UserTypeAdmin)
            {
                roles = roles.Where(x => x.RLevel >= 1).ToList();
            }
          else  if (Utype == CommonUtil.UserTypeManager)
            {
                roles = roles.Where(x => x.RLevel >= 2).ToList();
            }
            else
            {
                roles = roles.Where(x => x.RLevel >= 3).ToList();
            }
            ViewBag.DesignationId = new SelectList(db.MaDesignations, "Id", "Designation");

            ViewBag.RollId = new SelectList(roles, "Id", "RollName");

            ViewBag.Usertype = new SelectList(listing.GetUserTypeList, "Usertype", "Usertype");


            var SubRole = new List<SelectListItem>
            {

                new SelectListItem{Text = "Min" , Value= "Min"},
                                new SelectListItem{Text = "Moderate" , Value= "Moderate"},
                                                new SelectListItem{Text = "Max" , Value= "Max"}
            };


            ViewBag.SubRole = new SelectList(SubRole, "Value", "Text");

            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(User user, FormCollection frm, int bid = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.User, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            try
            {
                if (bid > 0)
                {
                    user.BranchId = bid;
                }
                else
                {
                    string[] value = frm["BranchId"].Split(',');
                    user.BranchId = Convert.ToInt32(value[1]);
                }
                user.Status = true;

                if (user.Password == user.ConfirmPassword)
                {
                    string password = Guid.NewGuid().ToString().Substring(0, 8);
                    user.Password = password;
                    user.ConfirmPassword = password;
                    user.IsNewUser = false;
                    user.expirydate = DateTime.Now.AddDays(90);
                    db.Users.Add(user);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        foreach (var eve in ex.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {

                            }
                        }
                        throw;
                    }



                    ViewBag.Message = "";

                    //here mail is send to a user
                    SendMailtoUser(user.Email, user.Password, user.username, user.Fullname);
                    if (Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeAdmin)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
                else
                {

                    ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", user.BranchId);
                    ViewBag.DesignationId = new SelectList(db.MaDesignations, "Id", "Designation", user.DesignationId);
                    ViewBag.Message = "Password Mismatch";
                    return View(user);
                }
            }
            catch (Exception ex)
            {
            }

            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", user.BranchId);
            ViewBag.DesignationId = new SelectList(db.MaDesignations, "Id", "Designation", user.DesignationId);
            ViewBag.RollId = new SelectList(listing.GetRoleList, "Id", "RollName", user.RollId);

            ViewBag.Usertype = new SelectList(listing.GetUserTypeList, "Usertype", "Usertype", user.Usertype);


            var SubRole = new List<SelectListItem>
            {

                new SelectListItem{Text = "Min" , Value= "Min"},
                                new SelectListItem{Text = "Moderate" , Value= "Moderate"},
                                                new SelectListItem{Text = "Max" , Value= "Max"}
            };


            ViewBag.SubRole = new SelectList(SubRole, "Value", "Text", user.SubRole);


            var errors = ModelState.Values.SelectMany(v => v.Errors);

            return View(user);
        }

        private void SendMailtoUser(string Emailid, string passwrd, string username, string fullname)
        {
            try
            {
                //string mail = "info@crm-ssceducation.org";
                string mail = ConfigurationManager.AppSettings["Mail"];
                MailMessage message = new MailMessage();
                message.Subject = " User ID created for SSC CRM Software : ";
                message.Body = "<p>Dear " + fullname + "</p> <br/> <p>To access SSC <b>CRM</b> software, please click on the link below:</p><p><a href='http://crm-ssceducation.org/'>crm-ssceducation.org</a></p> <br /> <p>Your access details are as follows:</p><br />User Name : " + username + " <br />Password : " + passwrd + "<br /> In case of any issue please contact at techteam@ssceducation.org";
                message.From = new MailAddress(mail);
                message.To.Add(new MailAddress(Emailid));
                message.IsBodyHtml = true;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
                SmtpClient smtpClient = new SmtpClient(smtpserver);
                smtpClient.Port = Int32.Parse("8889");
                smtpClient.Credentials = new NetworkCredential(mail, ConfigurationManager.AppSettings["credential"].ToString());
                smtpClient.EnableSsl = false;
                smtpClient.Send(message);
            }
            catch (Exception ex)
            { }
        }

        //
        // GET: /User/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.User, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            string Utype = Request.Cookies[CommonUtil.CookieUsertype].Value;
            List<MaRole> roles = db.MaRoles.ToList();
            if (Utype == CommonUtil.UserTypeSuperAdmin)
            {
                roles = roles.Where(x => x.RLevel >= 0).ToList();
            }
            else if (Utype == CommonUtil.UserTypeAdmin)
            {
                roles = roles.Where(x => x.RLevel >= 1).ToList();
            }
            else if (Utype == CommonUtil.UserTypeManager)
            {
                roles = roles.Where(x => x.RLevel >= 2).ToList();
            }
            else
            {
                roles = roles.Where(x => x.RLevel >= 3).ToList();
            }

            User user = db.Users.Find(id);
            user.ConfirmPassword = user.Password;
            ViewBag.usertpe = user.Usertype;

            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", user.BranchId);
            ViewBag.DesignationId = new SelectList(db.MaDesignations, "Id", "Designation", user.DesignationId);

            ViewBag.RollId = new SelectList(roles, "Id", "RollName", user.RollId);
            ViewBag.Usertype = new SelectList(listing.GetUserTypeList, "Usertype", "Usertype", user.Usertype);
            if (Utype== CommonUtil.UserTypeAdmin)
            {
                ViewBag.Usertype = new SelectList(listing.GetUserTypeList.Where(x=>x.UserType != "Super Admin"), "Usertype", "Usertype", user.Usertype);
            }
          


            var SubRole = new List<SelectListItem>
            {

                new SelectListItem{Text = "Min" , Value= "Min"},
                                new SelectListItem{Text = "Moderate" , Value= "Moderate"},
                                                new SelectListItem{Text = "Max" , Value= "Max"}
            };


            ViewBag.SubRole = new SelectList(SubRole, "Value", "Text", user.SubRole);


            return View(user);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        public ActionResult Edit(User user)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.User, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (ModelState.IsValid)
            {
                // it was by default true//But changed on 24 jun to make the deactivate client working
                // user.Status = true; 

                db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                user.expirydate = DateTime.Now.AddDays(60);
                if (user.ConfirmPassword == user.Password)
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", user.BranchId);
                    ViewBag.DesignationId = new SelectList(db.MaDesignations, "Id", "Designation", user.DesignationId);
                    ViewBag.Message = "Password Mismatch";
                    return View(user);
                }
            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", user.BranchId);
            ViewBag.DesignationId = new SelectList(db.MaDesignations, "Id", "Designation", user.DesignationId);
            ViewBag.RollId = new SelectList(listing.GetRoleList, "Id", "RollName", user.RollId);

            ViewBag.Usertype = new SelectList(listing.GetUserTypeList, "Usertype", "Usertype", user.Usertype);


            var SubRole = new List<SelectListItem>
            {

                new SelectListItem{Text = "Min" , Value= "Min"},
                                new SelectListItem{Text = "Moderate" , Value= "Moderate"},
                                                new SelectListItem{Text = "Max" , Value= "Max"}
            };


            ViewBag.SubRole = new SelectList(SubRole, "Value", "Text", user.SubRole);

            var errors = ModelState.Values.SelectMany(v => v.Errors);
            return View(user);
        }

        //
        // GET: /User/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    User user = db.Users.Find(id);
        //    if (user == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(user);
        //}

        //
        // POST: /User/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    User user = db.Users.Find(id);
        //    db.Users.Remove(user);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.User, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            try
            {
                User user = db.Users.Find(id);
                if (user == null)
                {
                    return HttpNotFound();
                }

                db.Users.Remove(user);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult ResetPassword(string username)
        {
            CommonUtil util = new CommonUtil();
            util.ResetPassword(username);

            return RedirectToAction("Index", new { @msg = "Y" });
        }


        public ActionResult SendMail(int id)
        {
            User us = db.Users.Find(id);
            util.SendMailtoUser(us.Email,us.Password,us.username,us.Fullname);
            return RedirectToAction("Index", new { @msg = "S" });
        }


        //for approval common Method
        // for update ststus
        public ActionResult UpdateUserStatus(FormCollection collection)
        {

            string[] myprodidArray = collection["item.id"].ToString().Split(',');

            string[] myprodstatus = collection["hdnStatus"].ToString().Split(',');

            List<UserApproval> lst = new List<UserApproval>();


            for (int i = 0; i < myprodidArray.Length; i++)
            {
                UserApproval approval = new UserApproval();

                try
                {
                    approval.id = Convert.ToInt32(myprodidArray[i]);
                }
                catch
                {

                }

                try
                {
                    approval.status = Convert.ToBoolean(myprodstatus[i]);
                }
                catch
                {

                }

                lst.Add(approval);

            }


            if (lst != null)
            {
                try
                {
                    foreach (UserApproval obj in lst)
                    {
                        if (db.Users.Where(z => z.Id == obj.id).Count() > 0)
                        {
                            User _obj = db.Users.Where(z => z.Id == obj.id).FirstOrDefault();
                            _obj.Status = obj.status;
                            db.Entry(_obj).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                    }
                }
                catch (Exception ex)
                { }
                Session["user"] = null;
                Session["i"] = null;
            }
            return RedirectToAction("Index");
            // return RedirectToAction("Index", new { @msg = "" });
        }



        // reset password  for single or all users 
        public ActionResult ResetUserPassword(FormCollection collection)
        {

            string[] myprodidArray = collection["item.id"].ToString().Split(',');

            string[] myprodstatus = collection["hdnStatus"].ToString().Split(',');

            List<UserApproval> lst = new List<UserApproval>();


            for (int i = 0; i < myprodidArray.Length; i++)
            {
                UserApproval approval = new UserApproval();

                try
                {
                    approval.id = Convert.ToInt32(myprodidArray[i]);
                }
                catch
                {

                }

                try
                {
                    approval.status = Convert.ToBoolean(myprodstatus[i]);
                }
                catch
                {

                }

                lst.Add(approval);

            }


            if (lst != null)
            {
                try
                {
                    foreach (UserApproval obj in lst)
                    {
                        if (db.Users.Where(z => z.Id == obj.id && obj.status == false).Count() > 0)
                        {
                            User _obj = db.Users.Where(z => z.Id == obj.id && obj.status == false).FirstOrDefault();
                            string pass = Guid.NewGuid().ToString().Substring(0, 8);
                            _obj.Password = pass;

                            //Add 60 days in expiry Days from previous expiry date code commented by gourav
                            //_obj.expirydate = _obj.expirydate.Value.AddDays(60);

                            //Add 60 days in expiry days from current date on 21-Dec-2015
                            _obj.expirydate = DateTime.Now.AddDays(60);

                            db.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            util.SendMailtoUser(_obj.Email, _obj.Password, _obj.username, _obj.Fullname);
                        }

                    }
                }
                catch (Exception ex)
                { }
                Session["user"] = null;
                Session["i"] = null;
            }
            return RedirectToAction("Index");


        }

    }
}