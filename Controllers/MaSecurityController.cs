﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using System.Data.SqlClient;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    public class MaSecurityController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        //
        // GET: /MaSecuirty/
        MasterListing listing = new MasterListing();

        public ActionResult Index(string msg)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddSecurity, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            if (msg == "Updt")
            {
                ViewBag.message = "Record Updated Successfully";
            }
            if (msg == "sve")
            {
                ViewBag.message = "Record Added Successfully";
            }
            if (msg == "dup")
            {
                ViewBag.error = "Record With Same name already exists";
            }

            List<MaSecurity> lst = new List<MaSecurity>();
            lst = db.MaSecurities.ToList();
            ViewBag.TotalCount = lst.Count();
            return View(lst);
        }

        //
        // GET: /MaSecurity/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddSecurity, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            MaSecurity masecurity = db.MaSecurities.Find(id);
            if (masecurity == null)
            {
                return HttpNotFound();
            }
            return View(masecurity);
        }

        //
        // GET: /MaSecurity/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddSecurity, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }





            return View();
        }

        //
        // POST: /MaSecurity/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MaSecurity masecurity)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddSecurity, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            
            if (ModelState.IsValid)
            {
                masecurity.RightName = masecurity.RightName.ToUpper();
                masecurity.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                db.MaSecurities.Add(masecurity);
                try
                {
                    db.SaveChanges();
                    List<MaRole> rolelist = new List<MaRole>();
                    rolelist = listing.GetRoleList;
                    foreach (var item in rolelist)
                    {
                        Tb_Permission PermissionObj = new Tb_Permission();
                        PermissionObj.RollId = item.Id;
                        PermissionObj.SecurityId = masecurity.Id;
                        PermissionObj.IsAdd = true;
                        PermissionObj.IsEdit = true;
                        PermissionObj.IsView = true;
                        PermissionObj.IsDelete = false;
                        db.Tb_Permission.Add(PermissionObj);
                        db.SaveChanges();
                    }
                    return RedirectToAction("Index", new { @msg = "sve" });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2627)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
            }

            return View(masecurity);
        }

        //
        // GET: /MaSecuirty/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddSecurity, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            MaSecurity masecurity = db.MaSecurities.Find(id);
            if (masecurity == null)
            {
                return HttpNotFound();
            }
            return View(masecurity);
        }

        //
        // POST: /MaSecuirty/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MaSecurity masecurity)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddSecurity, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }




            if (ModelState.IsValid)
            {
                masecurity.RightName = masecurity.RightName.ToUpper();
                db.Entry(masecurity).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index", new { @msg = "Updt" });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2627)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
            }
            return View(masecurity);
        }

        //
        // GET: /MaSecuirty/Delete/5

        public ActionResult Delete(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddSecurity, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }





            try
            {
                MaSecurity masecurity = db.MaSecurities.Find(id);
                db.MaSecurities.Remove(masecurity);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception)
            {

                return JavaScript("errormessage();");
            }
        }

        //
        // POST: /MaSecuirty/Delete/5


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}