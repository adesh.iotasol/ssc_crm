﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;
namespace SSCMvc.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        SSCWebDBEntities1 db = new SSCWebDBEntities1();
        CommonUtil cu = new CommonUtil();
     
        public ActionResult GroupTiming(string timecode = "All",string course="", string format = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.TimeWiseList, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            ViewBag.TimeCode = new SelectList(db.MaTime_View.OrderBy(z => z.tcode).ToList(), "Id", "BatchTime");
            ViewBag.Course = new SelectList(db.MaCourses, "Id", "CourseName");
            if (format != "")
            {
                try
                {
                    //DateTime fdt = Convert.ToDateTime(dt1);
                    //DateTime tdt = Convert.ToDateTime(dt2);
                    var Business = new RecieptBuisness();
                    //  var reportViewModel = Business.GetGroupTimingReport(fdt, tdt, timecode, format);

                    var reportViewModel = Business.GetGroupTimingReport(timecode, course, format,"E");

                    var renderedBytes = reportViewModel.RenderReport();
                    if (reportViewModel.ViewAsAttachment)
                        Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                    return File(renderedBytes, reportViewModel.LastmimeType);
                }
                catch (Exception ex)
                {
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult GroupTimingByStudent(FormCollection form ,string timecode = "All", string format = "PDF")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.TimeWiseList, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            updatecoursehist();
            string forms = form["stts"].ToString();
            var Listt = form["stts"].Split(',').Count();
            for (int i = 0; i <= Listt - 1; i++)
            {
                try
                {
                    int Idd = Convert.ToInt32(form["stts"].Split(',')[i]);
                    List<Tb_CourseHist> tb_corse = db.Tb_CourseHist.Where(x => x.TimeId == Idd).ToList();
                    Tb_CourseHist tb_cour = db.Database.SqlQuery<Tb_CourseHist>($"Update Tb_CourseHist set IsReport = 1 where TimeId ={Idd} ").FirstOrDefault();
                    //foreach (Tb_CourseHist item in tb_corse)
                    //{
                    //    Tb_CourseHist _obj = db.Tb_CourseHist.Find(item.Id);
                    //    _obj.IsReport = true;
                    //    db.SaveChanges();
                    //}
                }
                catch (Exception ex)
                {
                }
            }

            ViewBag.TimeCode = new SelectList(db.MaTime_View.OrderBy(z => z.tcode).ToList(), "Id", "BatchTime");
           
                try
                {
                    //DateTime fdt = Convert.ToDateTime(dt1);
                    //DateTime tdt = Convert.ToDateTime(dt2);
                    var Business = new RecieptBuisness();
                    //  var reportViewModel = Business.GetGroupTimingReport(fdt, tdt, timecode, format);

                    var reportViewModel = Business.GetGroupTimingReport(timecode,"", format,"R");

                    var renderedBytes = reportViewModel.RenderReport();
                    if (reportViewModel.ViewAsAttachment)
                        Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                    return File(renderedBytes, reportViewModel.LastmimeType);
                }
                catch (Exception ex)
                {
                }
           
            return View();
        }

        public ActionResult StudentDetail(string dt1 = "", string dt2 = "", string format = "")
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.StudentDetail, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            if (dt1 == "" && dt2 == "")
            {
                return View();
            }
            else
            {
                try
                {
                    DateTime fdt = Convert.ToDateTime(dt1);
                    DateTime tdt = Convert.ToDateTime(dt2);
                    var Business = new RecieptBuisness();
                    var reportViewModel = Business.GetStudentDetailReport(fdt, tdt, format);

                    var renderedBytes = reportViewModel.RenderReport();
                    if (reportViewModel.ViewAsAttachment)
                        Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                    return File(renderedBytes, reportViewModel.LastmimeType);
                }
                catch (Exception ex)
                {
                }
                return View();
            }
        }

        public ActionResult PendingFeeList()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.PendingFeeList, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetPendingFeeList();

            var renderedBytes = reportViewModel.RenderReport();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);

        }


        public ActionResult AreaReport()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AreaReport, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            ViewBag.AreaId = new SelectList(db.Ma_Area, "Id", "Area");
            return View();

        }

        public ActionResult PrintAreaReport(string dt1 = "", string dt2 = "", string format = "", int areaid = 0)
        {

            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetAreaReport(dt1, dt2, format, areaid);

            var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
        }

        public ActionResult EnqSearchReport(string dt1 = "", string dt2 = "", string dayname = "All", string timeslot = "All", string format = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.TimeSlot, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }





            if (dt1 == "" && dt2 == "" && dayname == "All" && timeslot == "All")
            {
                return View();
            }
            else
            {
                try
                {
                    DateTime fdt = Convert.ToDateTime(dt1);
                    DateTime tdt = Convert.ToDateTime(dt2);
                    var Business = new RecieptBuisness();
                    var reportViewModel = Business.GetPendingFeeList(fdt, tdt, dayname, timeslot, format);

                    var renderedBytes = reportViewModel.RenderReport();
                    if (reportViewModel.ViewAsAttachment)
                        Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                    return File(renderedBytes, reportViewModel.LastmimeType);

                }
                catch (Exception ex)
                { }
                return View();
            }
        }

        public ActionResult ReferenceReport(string dt1 = "", string dt2 = "", string refid = "", string format = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ReferenceReport, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            ViewBag.Reference = new SelectList(db.MaFrmReferences.Where(z => z.Status == true).ToList(), "Id", "CompName");
            if (dt1 == "" && dt2 == "" && refid == "")
            {
                return View();
            }
            else
            {
                var Business = new RecieptBuisness();
                var reportViewModel = Business.GetReferenceReportList(dt1, dt2, refid, format);

                var renderedBytes = reportViewModel.RenderReport();
                if (reportViewModel.ViewAsAttachment)
                    Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                return File(renderedBytes, reportViewModel.LastmimeType);
            }

        }

        public ActionResult InactiveCourseReport()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.InactiveCourse, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            return View();
        }

        public ActionResult PrintInactiveCourseReport(string dt1 = "", string dt2 = "", string format = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.InactiveCourse, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetInactivecourseReport(dt1, dt2, format);

            var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
        }

        public ActionResult IELTSReport(string dt1 = "", string dt2 = "", string format = "", string Ielts = "", string Brach = "",string Score="",string filteron="TestOn")
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.IeltsPteReport, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");


            if (dt1 == "" && dt2 == "")
            {
                return View();
            }
            else
            {
                var Business = new RecieptBuisness();
                var reportViewModel = Business.GetIELTSReportList(dt1, dt2, format, Ielts, Brach,Score,filteron);

                var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
                if (reportViewModel.ViewAsAttachment)
                    Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                return File(renderedBytes, reportViewModel.LastmimeType);
            }

        }




        //
        // GET: /Reports/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StudentTransferReport()
        {
            ViewBag.BranchTo = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.BranchFrom = new SelectList(db.MaBranches, "Id", "BramchName");
            return View();

        }

        public ActionResult PrintData(string fdt = "", string tdt = "",string BrTo="",string BrFrom ="", string format = "")
        {
            int userid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            string usertype = Request.Cookies[CommonUtil.CookieUsertype].Value;
            List<Tb_StudentTransfer> lst = new List<Tb_StudentTransfer>();
            lst = new List<Tb_StudentTransfer>();
                lst = db.Tb_StudentTransfer.ToList();
          
            if (fdt != "" && tdt != "")
            {
                DateTime fdate = Convert.ToDateTime(fdt);
                DateTime tdate = Convert.ToDateTime(tdt);
                ViewBag.Fdate = fdt;
                ViewBag.Tdate = tdt;
                lst = lst.Where(z => z.TransferDate >= fdate && z.TransferDate <= tdate).ToList();
            }
            if (BrTo !="")
            {
                int BrT = Convert.ToInt32(BrTo);
                lst = lst.Where(z => z.BranchTo == BrT).ToList();
            }
            if (BrFrom != "")
            {
                int BrF = Convert.ToInt32(BrFrom);
                lst = lst.Where(z => z.BranchFrom == BrF).ToList();
            }
            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetTransferStudent(lst, format);

            var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
        }


        //
        // GET: /Reports/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Reports/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Reports/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Reports/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Reports/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Reports/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        public ActionResult WithoutFee()
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.CourseGroup = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout");
            ViewBag.UserList = new SelectList(cu.GetUserList(branchid).OrderBy(x => x.username), "Id", "username");
            return View();
        }

        public ActionResult PrintWithoutFee(string dt1 = "", string dt2 = "", string format = "", string user = "", string course = "")
        {
            List<MaAdmission> maadd = new List<MaAdmission>();
            List<AdmWithoutFeeHelper> admList = new List<AdmWithoutFeeHelper>();

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            try
            {

             //   maadd = db.Database.SqlQuery<MaAdmission>("select * from maadmission where id not in (select studentid from Tb_FeeReceipt) order by Cdate").ToList();
                var query = from c in db.MaAdmissions where !(from o in db.Tb_FeeReceipt select o.StudentId) .Contains(c.Id) select c;
                maadd = query.ToList();
                admList = GetWithoutFeeList(maadd);
            }
            catch (Exception ex)
            {
            }
            if (dt1 != "" && dt2 != "")
            {
                try
                {
                    ViewBag.fdate = dt1;
                    ViewBag.tdate = dt2;
                    DateTime fdate = Convert.ToDateTime(dt1);
                    DateTime tdate = Convert.ToDateTime(dt2);
                    admList = admList.Where(z => z.BId == branchid && z.Cdate >= fdate && z.Cdate <= tdate).ToList();
                }
                catch (Exception ex)
                { }
            }
            if (user != "")
            {
                ViewBag.usrval = user;
                int userid = Convert.ToInt32(user);
                admList = admList.Where(z => z.EnqEnteredby == userid).ToList();
            }

            if (course != "All" && course != "")
            {

                admList = admList.Where(z => z.Enqabout == course).ToList();
                ViewBag.enquabt = course;
            }
            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetWithoutFeeList(admList, format);

            var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
        }

        public List<AdmWithoutFeeHelper> GetWithoutFeeList(List<MaAdmission> maAd)
        {
            List<AdmWithoutFeeHelper> admList = new List<AdmWithoutFeeHelper>();

            foreach (MaAdmission item in maAd)
            {
                string Cname = db.Users.Where(x => x.Id == item.CounselorName).Select(x => x.Fullname).FirstOrDefault();

                AdmWithoutFeeHelper obj = new AdmWithoutFeeHelper();
                obj.Roll = item.RollNo;
                obj.Name = item.FirstName;
                obj.EnquiryNum = item.EnquiryId;
                obj.Id = item.Id;
                obj.Mobile = item.Mobile;
                obj.AltNum = item.AlternateNo;
                obj.Enqabout = item.MaEnquiry.EnqAbt;
                obj.EnqEnteredby = item.MaEnquiry.EnqHandledBy;
                obj.Enqdate = item.MaEnquiry.WalkInDate;
                obj.Remarks = item.Remarks;
                obj.BId = item.BranchId;
                obj.Cdate = item.Cdate;
                obj.LDate = item.LeftDate;
                obj.Counsellor = Cname;
                admList.Add(obj);

            }
            return admList;
        }

        //
        // POST: /Reports/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult GetReprtbyTime()
        {
            return View(db.MaTime_View.OrderBy(z => z.tcode).ToList());
        }
        public void updatecoursehist()
        {

            try
            {
            Tb_CourseHist tb_cour = db.Database.SqlQuery<Tb_CourseHist>("Update Tb_CourseHist set IsReport = 0").FirstOrDefault();
            }
            catch (Exception ex)
            {
            }
        }

        //public JsonResult updateColumn(string va)
        //{
        //    try
        //    {
        //        if (va == "true")
        //        {
        //            Tb_CourseHist tb_cour = db.Database.SqlQuery<Tb_CourseHist>("Update Tb_CourseHist set IsReport = 1").FirstOrDefault();
        //        }
        //        if (va == "false")
        //        {
        //            Tb_CourseHist tb_cour = db.Database.SqlQuery<Tb_CourseHist>("Update Tb_CourseHist set IsReport = 0").FirstOrDefault();
        //        }

        //    }
        //    catch (Exception ex)
        //    { 
        //    }
        //    return Json(true,JsonRequestBehavior.AllowGet);
        //}



        //public JsonResult UpdateReportStatus(int id , string hdnStatus)
        //{
        //    bool stts = Convert.ToBoolean(hdnStatus);
        //        try
        //        {
        //        List<Tb_CourseHist> tb_corse = db.Tb_CourseHist.Where(x=>x.TimeId==id).ToList();
        //        foreach (Tb_CourseHist item in tb_corse)
        //        {
        //            Tb_CourseHist _obj = db.Tb_CourseHist.Find(item.Id);
        //            _obj.IsReport = stts;
        //            db.SaveChanges();
        //        }
                       
        //        }
        //        catch (Exception ex)
        //        { }
              
        //    //}
        //    return Json(null,JsonRequestBehavior.AllowGet);
          
        //}


    }
}
