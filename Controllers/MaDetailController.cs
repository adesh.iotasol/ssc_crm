﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class MaDetailController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /MaDetail/

        
        public ActionResult Index(int packid=0)
        {
            ViewBag.packid = packid;
            ViewBag.PackName = "Package: ";
            ViewBag.PackName += db.MaPackages.Where(z => z.Id == packid).Select(z => z.PackageName).First();
            var madetails = db.MaDetails.Include(m => m.MaCourse).Where(z=>z.PackageId==packid).ToList();
            ViewBag.TotalCount = madetails.Count;
            return View(madetails.ToList());
        }

        //
        // GET: /MaDetail/Details/5

        public ActionResult Details(int id = 0)
        {
            MaDetail madetail = db.MaDetails.Find(id);
            if (madetail == null)
            {
                return HttpNotFound();
            }
            return View(madetail);
        }

        //
        // GET: /MaDetail/Create

        public ActionResult Create(int packid)
        {
            MaDetail obj = new MaDetail();
            obj.CourseId = db.MaPackages.Where(z => z.Id == packid).Select(z => z.CourseId).First();
            obj.PackageId = packid;

            //auto generate Installment No
            int? instalno = 1;
            if (db.MaDetails.Where(z => z.PackageId == packid && z.CourseId == obj.CourseId).Count() > 0)
            {
                instalno = db.MaDetails.Where(z => z.PackageId == packid && z.CourseId == obj.CourseId).OrderByDescending(z => z.InstallmentNo).Select(z => z.InstallmentNo).First();
                instalno += 1;
            }
            obj.InstallmentNo = instalno;
            

            return View(obj);
        }

        //
        // POST: /MaDetail/Create

        [HttpPost]
        public ActionResult Create(MaDetail madetail)
        {
            
            if (ModelState.IsValid)
            {
                db.MaDetails.Add(madetail);
                madetail.EnterDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
                
                db.SaveChanges();
                int packid =(int) madetail.PackageId;
                UpdatePackageAmout(packid);
                return RedirectToAction("Index", new {@packid=packid });
            }

           
            return View(madetail);
        }

        //
        // GET: /MaDetail/Edit/5

        public ActionResult Edit(int id = 0)
        {
            MaDetail madetail = db.MaDetails.Find(id);
            if (madetail == null)
            {
                return HttpNotFound();
            }
            //ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", madetail.CourseId);
            //ViewBag.PackageId = new SelectList(db.MaPackages, "Id", "PackageName", madetail.PackageId);
            return View(madetail);
        }

        //
        // POST: /MaDetail/Edit/5

        [HttpPost]
        public ActionResult Edit(MaDetail madetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(madetail).State = System.Data.Entity.EntityState.Modified;
                madetail.EnterDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
                db.SaveChanges();
                int packid = (int)madetail.PackageId;
                UpdatePackageAmout(packid);
                return RedirectToAction("Index", new { @packid=packid});
            }
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", madetail.CourseId);
            ViewBag.PackageId = new SelectList(db.MaPackages, "Id", "PackageName", madetail.PackageId);
            return View(madetail);
        }

        //
        // GET: /MaDetail/Delete/5

        public ActionResult Delete(int id = 0)
        {
            MaDetail madetail = db.MaDetails.Find(id);
            if (madetail == null)
            {
                return HttpNotFound();
            }
            return View(madetail);
        }

        //
        // POST: /MaDetail/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            MaDetail madetail = db.MaDetails.Find(id);
            db.MaDetails.Remove(madetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        private void UpdatePackageAmout(int packid)
        {

            List<MaDetail> lst = db.MaDetails.Where(z => z.PackageId == packid).ToList();
            double? currentamt = 0;
            int? packageday = 0;
            foreach (MaDetail obj in lst)
            {
                packageday += obj.NextDurationDays; 
                currentamt += obj.InstallAmt;
            }
            MaPackage objPack = db.MaPackages.Where(z => z.Id == packid).First();
            objPack.Amount = currentamt;
            objPack.PackageDays = packageday;
            db.SaveChanges();
        }
    }
}