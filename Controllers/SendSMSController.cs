﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    public class SendSMSController : Controller
    {
        //
        // GET: /SendSMS/
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        MasterListing li = new MasterListing();

        public ActionResult Index(string msg)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.SendSMS, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (msg == "Updt")
            {
                ViewBag.message = "Record Updated Successfully";
            }
            if (msg == "sve")
            {
                ViewBag.message = "Record Added Successfully";
            }
           
            if (msg == "sent")
            {
                ViewBag.message = "Sms Sent Successfully.";
            }
            if (msg == "dup")
            {
                ViewBag.error = "Record With Same name already exists";
            }
            ViewBag.THeading = new SelectList(li.GetSmsTemp, "Id", "THeading");
            return View();
        }


        public ActionResult Create(string msg)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.SendSMS, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (msg == "del")
            {
                ViewBag.message = "Record Deleted Successfully";
            }
            if (msg == "updt")
            {
                ViewBag.message = "Record updated Successfully";
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(Tb_SmsTmp tb_sms)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.SendSMS, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
                try
                {
                    db.Tb_SmsTmp.Add(tb_sms);
                    db.SaveChanges();
                    return RedirectToAction("Index", new { @msg = "sve" });
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Index", new { @msg = "err" });
                }

            }
            return View();
        }

        public ActionResult Edit(int? id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.SendSMS, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            Tb_SmsTmp sms = db.Tb_SmsTmp.Find(id);

            return View(sms);
        }

        [HttpPost]
        public ActionResult Edit(Tb_SmsTmp tb_sms)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.SendSMS, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
                try
                {
                   db.Entry(tb_sms).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Create", new { @msg = "updt" });
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Create", new { @msg = "err" });
                }

            }
            return View();
        }


        public JsonResult GetFormat(int id)
        {
            Tb_SmsTmp sms = db.Tb_SmsTmp.Where(x => x.Id == id).FirstOrDefault();
            string messg = sms.TemplateTxt;
            return Json(messg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendMessage(string msgg = "")
        {
            Status myobj = new Status();
            List<MaAdmission> ma_add = db.MaAdmissions.Where(x => x.IsLeft == false).ToList();
            foreach (var item in ma_add)
            {
                myobj.Sendsms(item.Mobile, msgg);
                if (item.AlternateNo!="" && item.AlternateNo!= null)
                {
                    myobj.Sendsms(item.AlternateNo,msgg);
                }
            }
            return RedirectToAction("Index", new { @msg = "sent" });
        }

        public ActionResult Delete(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.SendSMS, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            try
            {
                Tb_SmsTmp tb_sms = db.Tb_SmsTmp.Find(id);
                db.Tb_SmsTmp.Remove(tb_sms);
                db.SaveChanges();
                return RedirectToAction("Create", new { @msg = "del" });
            }
            catch (Exception)
            {
                return RedirectToAction("Create", new { @msg = "delerror" });
            }
        }
    }
}
