﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Data.Linq.SqlClient;


namespace SSCMvc.Controllers
{
    [Authorize]
    public class MaEnqFollowController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        CommonUtil cu = new CommonUtil();
        //
        // GET: /MaEnqFollow/

        public ActionResult Index()
        {
            return View(db.MaEnqFollows.ToList());
        }
        public ActionResult _Index(int id, string reftype, int studid = 0)
        {
            if (reftype == "Enquiry")
            {
                ViewBag.etype = db.MaEnquiries.Find(id).EnqType;

            }
            else
            {
                ViewBag.etype = "";
            }
            return PartialView(db.MaEnqFollows.Where(z => (z.ReferId == id && z.FollowUpType == reftype) || z.AdmissionId == studid).OrderByDescending(z => z.Id).ToList());
        }

        public ActionResult _VisaIndex(int visaid, int id = 0)
        {
            //return PartialView(db.Tb_VisaFollowUp.Where(z => z.ReferId == id && z.Followtype == reftype).OrderByDescending(z => z.Id).ToList());
            //return PartialView(db.MaEnqFollows.Where(z => z.ReferId == visaid).OrderByDescending(z => z.Id).ToList());

            return PartialView(db.MaEnqFollows.Where(z => z.ReferId == id).OrderByDescending(z => z.Id).ToList());
        }


        //autocomplete textbox start here
        public ActionResult AutoCompleteCity(string term)
        {
            var db = new SSCWebDBEntities1();
            return Json(db.MaClosedReasons.Where(z => z.Remarks.StartsWith(term)).Select(z => z.Remarks), JsonRequestBehavior.AllowGet);
        }
        //autocomplete textbox end here

        #region
        public ActionResult IELTMOCKTestScore(string dt1 = "", string dt2 = "", string format = "")
        {
            if (dt1 == "" || dt2 == "")
            {
                ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
                ViewBag.tdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1.0).Date.ToShortDateString();
                return View();
            }
            else
            {
                //string usrtype = Request.Cookies[CommonUtil.CookieUsertype].Value;
                //if (usrtype == CommonUtil.UserTypeCounselor)
                //{
                //    try
                //    {
                //        DateTime fromdate = Convert.ToDateTime(dt1);
                //        if (fromdate < DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(-3))
                //        {
                //            return Content("You don't have permission to go to this date");
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        return Content("You don't have permission to go to this date");
                //    }
                //}

                var Business = new RecieptBuisness();
                var reportViewModel = Business.GetDiscountReportList(dt1, dt2, format);

                var renderedBytes = reportViewModel.RenderReport();
                if (reportViewModel.ViewAsAttachment)
                    Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                return File(renderedBytes, reportViewModel.LastmimeType);
            }
        }
        #endregion



        //closed reson
        #region

        public ActionResult ClosedReason(string type = "")
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ClosedReason, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ClosedReason, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            if (type != "")
            {
                //geting repot data from the business object
                var Business = new RecieptBuisness();
                var reportViewModel = Business.GetClosedreasoncountList(type);
                var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
                if (reportViewModel.ViewAsAttachment)
                    Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                return File(renderedBytes, reportViewModel.LastmimeType);

            }
            return View();
        }

        #endregion
        //closed reason end here

        //
        // GET: /MaEnqFollow/Details/5

        public ActionResult Details(int id = 0)
        {
            MaEnqFollow maenqfollow = db.MaEnqFollows.Find(id);
            if (maenqfollow == null)
            {
                return HttpNotFound();
            }
            return View(maenqfollow);
        }



        //calls report of PendingFollowUP
        public ActionResult PrintPendingFollowUpCountList()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.PendingFollowUp, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            //geting repot data from the business object
            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetPendingFollowUpcountList();

            var renderedBytes = reportViewModel.RenderReportWithoutPararmere();

            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);

        }


        //
        // GET: /MaEnqFollow/Create

        public ActionResult Create()
        {
            return View();
        }
        public ActionResult _Create(int id, string reftype, string name, int refno)
        {
            //int count = db.MaEnquiries.Where(z => z.Id == id).Select(z => z.WalkInDate).Count();
            //if (count > 0)
            //{
            //    //DateTime walkindate = Convert.ToDateTime(db.MaEnquiries.Where(z => z.Id == id).Select(z => z.WalkInDate).First());
            //    DateTime nextdate = DateTime.Now;
            //    ViewBag.nextdate = nextdate;

            //}

            MaEnqFollow obj = new MaEnqFollow();
            obj.ReferId = id;
            obj.FollowUpType = reftype;
            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            MaEnquiry enq = db.MaEnquiries.Find(id);
            var rlist = db.MaClosedReasons.OrderBy(o => o.Remarks);
            string uname = db.Users.Where(z => z.Id == uid).Select(z => z.Fullname).First();
            ViewBag.Username = uname;
            ViewBag.Name = name;
            ViewBag.rollno = id;
            ViewBag.refno = refno;
            ViewBag.Enqid = id;
            ViewBag.Etype = enq.EnqType;
            ViewBag.CloseReason = new SelectList(rlist, "Remarks", "Remarks");
            return View(obj);
        }



        ////
        // POST: /MaEnqFollow/Create

        [HttpPost]
        public ActionResult Create(MaEnqFollow maenqfollow, FormCollection form)
        {
            string chbranch = Request.Cookies["chbranch"].Value;

            if (chbranch == "Y")
            {
                return View("~/Views/Shared/AccessDenied.cshtml");
            }

            MaEnquiry enquery = new MaEnquiry();
            maenqfollow.CDate = CommonUtil.IndianTime();
            //  maenqfollow.NextDate=;

            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            int? referid = maenqfollow.ReferId;
            enquery = db.MaEnquiries.Where(a => a.Id == referid).FirstOrDefault();
            maenqfollow.EnqFollowBy = uid;
            if (ModelState.IsValid)
            {

                db.MaEnqFollows.Add(maenqfollow);

                enquery.FRemarks = maenqfollow.Remarks;
                enquery.FNextDate = maenqfollow.NextDate;
                enquery.FEnqstatus = maenqfollow.Enqstatus;
                enquery.FEnqFollowBy = maenqfollow.EnqFollowBy;
                enquery.EnqType = form["EnqType"];
                enquery.FCDate = CommonUtil.IndianTime();
                db.Entry(enquery).State = System.Data.Entity.EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                        }
                    }

                }


                return RedirectToAction("PendingFollowUp");
            }

            return Content("Invalid");
        }

        //
        // GET: /MaEnqFollow/Edit/5

        public ActionResult Edit(int id = 0)
        {
            MaEnqFollow maenqfollow = db.MaEnqFollows.Find(id);
            if (maenqfollow == null)
            {
                return HttpNotFound();
            }
            return View(maenqfollow);
        }

        //
        // POST: /MaEnqFollow/Edit/5

        [HttpPost]
        public ActionResult Edit(MaEnqFollow maenqfollow)
        {
            if (ModelState.IsValid)
            {
                db.Entry(maenqfollow).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(maenqfollow);
        }

        //
        // GET: /MaEnqFollow/Delete/5

        public ActionResult Delete(int id = 0)
        {
            MaEnqFollow maenqfollow = db.MaEnqFollows.Find(id);
            if (maenqfollow == null)
            {
                return HttpNotFound();
            }
            return View(maenqfollow);
        }

        //
        // POST: /MaEnqFollow/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            MaEnqFollow maenqfollow = db.MaEnqFollows.Find(id);
            db.MaEnqFollows.Remove(maenqfollow);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult UpdateEnqType(int Id, string EnqType)
        {
            MaEnquiry Enq = db.MaEnquiries.Find(Id);
            Enq.EnqType = EnqType;
            db.SaveChanges();
            int uid1 = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            return RedirectToAction("PendingFollowup", new { @uid = uid1 });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult PendingFollowUp(string name = "", string mobile = "", string dt1 = "", string dt2 = "", string uid = "", string dtype = "", string EnqType = "", string enquiryabout = "", string Report = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.UserList = new SelectList(db.Users.Where(z => z.BranchId == branchid && z.Status == true).OrderBy(x => x.Fullname).ToList(), "Id", "FullName");
            ViewBag.EnqType1 = new SelectList(db.St_EnquiryType.ToList(), "StatusName", "StatusName", EnqType);
            ViewBag.EnqType = new SelectList(db.St_EnquiryType.ToList(), "StatusName", "StatusName", EnqType);
            ViewBag.CourseGroup = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout", enquiryabout);
            Status st = new Status();

            DataTable dt = new DataTable();

            // For Enquiry FollowUp
            dt = st.GetPendingFollowComplete("Enquiry");
            int usid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            List<PendingEnqSummary> summary = db.Database.SqlQuery<PendingEnqSummary>($"select count(1) as Cnt, EnqType from Vw_EnquiryFollow as enq where enq.IsAdmission = 0 and  enq.nxdt <= dateadd(minute, 810, getdate()) and((enq.EnqHandledBy = {usid} and enq.transferid is null) or enq.transferid = {usid}) and enq.BranchId ={branchid} group by EnqType").ToList();

            ViewBag.Summ = summary;


            List<FollowUpEnqModel> lstenqfollow = new List<FollowUpEnqModel>();
            foreach (DataRow dr in dt.Rows)
            {
                int id = Convert.ToInt32(dr["Id"].ToString());
                // if (db.MaEnqFollow.Where(z => z.ReferId == id && z.FollowUpType == "Enquiry" && z.Enqstatus == "Close").Count() == 0)
                {
                    FollowUpEnqModel obj = new FollowUpEnqModel();
                    obj.FirstName = dr["FirstName"].ToString();
                    obj.Id = Convert.ToInt32(dr["Id"].ToString());
                    obj.LastName = dr["LastName"].ToString();
                    obj.Mobile = dr["Mobile"].ToString();
                    obj.GroupTime = dr["Grouptime"].ToString();
                    obj.EnqType = dr["EnqType"].ToString();
                    obj.VisaType = dr["VisaType"].ToString();
                    obj.GroupChoice = dr["GroupChoice"].ToString();
                    try
                    {
                        obj.nxdt = Convert.ToDateTime(dr["nxdt"].ToString());
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.EnqHandledBy = Convert.ToInt32(dr["EnqHandledBy"].ToString());
                    }
                    catch (Exception ex)
                    { }

                    try
                    {
                        obj.TransferId = Convert.ToInt32(dr["TransferId"].ToString());
                    }
                    catch (Exception ex)
                    { }


                    try
                    {
                        obj.Enqstatus = dr["enqstatus"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.AlternateMobile = dr["AlternateNo"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.enqab = dr["EnqAbt"].ToString();
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        obj.Cdate = Convert.ToDateTime(dr["cdate"]);
                    }
                    catch (Exception)
                    {

                    }
                    lstenqfollow.Add(obj);
                }
            }

            /* this is for search. To display list code only you can skip this */

            if (name != "")
            {
                ViewBag.name = name;
                name = name.ToUpper();
                lstenqfollow = lstenqfollow.Where(z => z.FirstName.ToUpper().Contains(name)).ToList();
            }
            if (mobile != "")
            {
                ViewBag.mobile = mobile;
                lstenqfollow = lstenqfollow.Where(z => z.Mobile == mobile).ToList();
            }
            if (EnqType != "")
            {

                lstenqfollow = lstenqfollow.Where(z => z.EnqType == EnqType).ToList();
            }
            if (enquiryabout != "")
            {

                lstenqfollow = lstenqfollow.Where(z => z.enqab == enquiryabout).ToList();
            }
            if (dt1 != "" && dt2 != "")
            {
                ViewBag.dt1 = dt1;
                ViewBag.dt2 = dt2;
                try
                {
                    DateTime fdt = Convert.ToDateTime(dt1);
                    DateTime tdt = Convert.ToDateTime(dt2);
                    if (dtype == "CD")
                    {
                        lstenqfollow = lstenqfollow.Where(z => z.Cdate >= fdt && z.Cdate <= tdt).ToList();
                    }
                    else
                    {
                        lstenqfollow = lstenqfollow.Where(z => z.nxdt >= fdt && z.nxdt <= tdt).ToList();
                    }

                }
                catch (Exception ex)
                {
                }
            }

            if (uid != "")
            {
                try
                {
                    ViewBag.usr = uid;
                    int usrid = Convert.ToInt32(uid);
                    lstenqfollow = lstenqfollow.Where(z => (z.EnqHandledBy == usrid && z.TransferId == null) || z.TransferId == usrid).ToList();
                }
                catch (Exception ex) { }

            }
            /*------------search code ends here--------*/
            ViewBag.TotalCount = lstenqfollow.Count;
            if (Report == "Excel")
            {
                var Business = new RecieptBuisness();
                var reportViewModel = Business.GetFollowupCreate(lstenqfollow);
                var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
                if (reportViewModel.ViewAsAttachment)
                    Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                return File(renderedBytes, reportViewModel.LastmimeType);
            }
            return View(lstenqfollow.OrderBy(z => z.nxdt).ToList());
        }

        public ActionResult TodayFollowUp(string name = "", string mobile = "", string dt1 = "", string dt2 = "", string uid = "", string dtype = "", string Report = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.UserList = new SelectList(db.Users.Where(z => z.BranchId == branchid && z.Status == true).OrderBy(x => x.Fullname).ToList(), "Id", "FullName");
            Status st = new Status();
            DataTable dt = new DataTable();
            // For Enquiry FollowUp
            dt = st.GetPendingFollowComplete("Enquiry");

            List<FollowUpEnqModel> lstenqfollow = new List<FollowUpEnqModel>();
            foreach (DataRow dr in dt.Rows)
            {
                int id = Convert.ToInt32(dr["Id"].ToString());

                {

                    FollowUpEnqModel obj = new FollowUpEnqModel();
                    obj.FirstName = dr["FirstName"].ToString();
                    obj.Id = Convert.ToInt32(dr["Id"].ToString());
                    obj.LastName = dr["LastName"].ToString();
                    obj.Mobile = dr["Mobile"].ToString();
                    obj.GroupTime = dr["Grouptime"].ToString();
                    obj.GroupChoice = dr["GroupChoice"].ToString();
                    try
                    {
                        obj.nxdt = Convert.ToDateTime(dr["nxdt"].ToString());
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.EnqHandledBy = Convert.ToInt32(dr["EnqHandledBy"].ToString());
                    }
                    catch (Exception ex)
                    { }

                    try
                    {
                        obj.TransferId = Convert.ToInt32(dr["TransferId"].ToString());
                    }
                    catch (Exception ex)
                    { }


                    try
                    {
                        obj.Enqstatus = dr["enqstatus"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.AlternateMobile = dr["AlternateNo"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.enqab = dr["EnqAbt"].ToString();
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        obj.Cdate = Convert.ToDateTime(dr["cdate"]);
                    }
                    catch (Exception)
                    {

                    }
                    lstenqfollow.Add(obj);
                }
            }

            /* this is for search. To display list code only you can skip this */

            if (name != "")
            {
                ViewBag.name = name;
                name = name.ToUpper();
                lstenqfollow = lstenqfollow.Where(z => z.FirstName.ToUpper().Contains(name)).ToList();
            }
            if (mobile != "")
            {
                ViewBag.mobile = mobile;
                lstenqfollow = lstenqfollow.Where(z => z.Mobile == mobile).ToList();
            }
            if (dt1 != "" && dt2 != "")
            {
                ViewBag.dt1 = dt1;
                ViewBag.dt2 = dt2;
                try
                {
                    DateTime fdt = Convert.ToDateTime(dt1);
                    DateTime tdt = Convert.ToDateTime(dt2);
                    if (dtype == "CD")
                    {
                        lstenqfollow = lstenqfollow.Where(z => z.Cdate >= fdt && z.Cdate <= tdt).ToList();
                    }
                    else
                    {
                        lstenqfollow = lstenqfollow.Where(z => z.nxdt >= fdt && z.nxdt <= tdt).ToList();
                    }

                }
                catch (Exception ex)
                {
                }
            }

            if (uid != "")
            {
                try
                {
                    ViewBag.usr = uid;
                    int usrid = Convert.ToInt32(uid);
                    lstenqfollow = lstenqfollow.Where(z => z.EnqHandledBy == usrid || z.TransferId == usrid).ToList();
                }
                catch (Exception ex) { }

            }
            /*------------search code ends here--------*/

            ViewBag.TotalCount = lstenqfollow.Count;

            return View(lstenqfollow.OrderBy(z => z.nxdt).ToList());
        }

        public ActionResult PendingLeftCounselFollowUp(string name = "", string mobile = "", string dt1 = "", string dt2 = "", string uid = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.UserList = new SelectList(db.Users.Where(z => z.BranchId == branchid && z.Status == false).OrderBy(x => x.Fullname).ToList(), "Id", "FullName");

            Status st = new Status();

            DataTable dt = new DataTable();

            // For Enquiry FollowUp
            dt = st.GetLeftCounselPendingFollow("Enquiry");



            List<FollowUpEnqModel> lstenqfollow = new List<FollowUpEnqModel>();
            foreach (DataRow dr in dt.Rows)
            {
                int id = Convert.ToInt32(dr["Id"].ToString());

                {

                    FollowUpEnqModel obj = new FollowUpEnqModel();
                    obj.FirstName = dr["FirstName"].ToString();
                    obj.Id = Convert.ToInt32(dr["Id"].ToString());
                    obj.LastName = dr["LastName"].ToString();
                    obj.Mobile = dr["Mobile"].ToString();
                    obj.GroupTime = dr["Grouptime"].ToString();
                    obj.GroupChoice = dr["GroupChoice"].ToString();
                    try
                    {
                        obj.nxdt = Convert.ToDateTime(dr["nxdt"].ToString());
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.EnqHandledBy = Convert.ToInt32(dr["EnqHandledBy"].ToString());
                    }
                    catch (Exception ex)
                    { }

                    try
                    {
                        obj.Enqstatus = dr["enqstatus"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.AlternateMobile = dr["AlternateNo"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.enqab = dr["EnqAbt"].ToString();
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        obj.Cdate = Convert.ToDateTime(dr["cdate"]);
                    }
                    catch (Exception)
                    {

                    }
                    lstenqfollow.Add(obj);
                }
            }

            /* this is for search. To display list code only you can skip this */

            if (name != "")
            {
                ViewBag.name = name;
                name = name.ToUpper();
                lstenqfollow = lstenqfollow.Where(z => z.FirstName.ToUpper().Contains(name)).ToList();
            }
            if (mobile != "")
            {
                ViewBag.mobile = mobile;
                lstenqfollow = lstenqfollow.Where(z => z.Mobile == mobile).ToList();
            }
            if (dt1 != "" && dt2 != "")
            {
                ViewBag.dt1 = dt1;
                ViewBag.dt2 = dt2;
                try
                {
                    DateTime fdt = Convert.ToDateTime(dt1);
                    DateTime tdt = Convert.ToDateTime(dt2);
                    lstenqfollow = lstenqfollow.Where(z => z.nxdt >= fdt && z.nxdt <= tdt).ToList();
                }
                catch (Exception ex)
                {
                }
            }

            if (uid != "")
            {
                try
                {
                    ViewBag.usr = uid;
                    int usrid = Convert.ToInt32(uid);
                    lstenqfollow = lstenqfollow.Where(z => z.EnqHandledBy == usrid).ToList();
                }
                catch (Exception ex) { }

            }


            /*------------search code ends here--------*/

            ViewBag.TotalCount = lstenqfollow.Count;

            return View(lstenqfollow.OrderBy(z => z.nxdt).ToList());
        }

        public JsonResult BranchUser(int id)
        {

            List<User> ub = db.Users.Where(z => z.BranchId == id && z.Status == true).ToList();

            var ItemUserData = ub.Select(c => new SelectListItem()
            {
                Text = c.Fullname,
                Value = c.Id.ToString(),

            });
            return Json(ItemUserData, JsonRequestBehavior.AllowGet);
        }


        public ActionResult PrintPendingFollowUpList(int branch, int uid, string date2, string date3)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.PendingFollowList, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            //geting repot data from the business object
            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetPendingFollowUpList("Enquiry", branch, uid, date2, date3);
            var renderedBytes = reportViewModel.RenderReportWithoutPararmere();

            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);

        }


        public ActionResult PendingFollowUpList()
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.PendingFollowList, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.BranchList = new SelectList(db.MaBranches.Where(a => a.Status == true).ToList(), "Id", "BramchName");
            ViewBag.UserList = new SelectList(db.Users.Where(z => z.BranchId == branchid && z.Status == true).OrderBy(x => x.Fullname).ToList(), "Id", "FullName");

            return View();
        }

        public ActionResult PendingFollowUpbycreatedate(string name = "", string mobile = "", string dt1 = "", string dt2 = "", string uid = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.UserList = new SelectList(db.Users.Where(z => z.BranchId == branchid && z.Status == true).OrderBy(x => x.Fullname).ToList(), "Id", "FullName");

            Status st = new Status();

            DataTable dt = new DataTable();




            if (Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeSuperAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeVisaCounselor)
            {
                int usrid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                dt = st.GetPendingFollowCompleteByCreateDate("Enquiry", usrid, dt1, dt2);
            }
            else
            {
                dt = st.GetPendingFollowCompleteByCreateDate("Enquiry", 0, dt1, dt2);
            }
            List<FollowUpEnqModel> lstenqfollow = new List<FollowUpEnqModel>();
            foreach (DataRow dr in dt.Rows)
            {
                int id = Convert.ToInt32(dr["Id"].ToString());
                // if (db.MaEnqFollow.Where(z => z.ReferId == id && z.FollowUpType == "Enquiry" && z.Enqstatus == "Close").Count() == 0)
                {

                    FollowUpEnqModel obj = new FollowUpEnqModel();
                    obj.FirstName = dr["FirstName"].ToString();
                    obj.Id = Convert.ToInt32(dr["Id"].ToString());
                    obj.LastName = dr["LastName"].ToString();
                    obj.Mobile = dr["Mobile"].ToString();
                    try
                    {
                        obj.nxdt = Convert.ToDateTime(dr["nxdt"].ToString());
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.CreateDate = Convert.ToDateTime(dr["cdate"].ToString());
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.EnqHandledBy = Convert.ToInt32(dr["EnqHandledBy"].ToString());
                    }
                    catch (Exception ex)
                    { }

                    try
                    {
                        obj.Enqstatus = dr["enqstatus"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    lstenqfollow.Add(obj);
                }
            }

            /* this is for search. To display list code only you can skip this */

            if (name != "")
            {
                ViewBag.name = name;
                name = name.ToUpper();
                lstenqfollow = lstenqfollow.Where(z => z.FirstName.ToUpper().Contains(name)).ToList();
            }
            if (mobile != "")
            {
                ViewBag.mobile = mobile;
                lstenqfollow = lstenqfollow.Where(z => z.Mobile == mobile).ToList();
            }
            //if (dt1 != "" && dt2 != "")
            //{
            //    ViewBag.dt1 = dt1;
            //    ViewBag.dt2 = dt2;
            //    try
            //    {
            //        DateTime fdt = Convert.ToDateTime(dt1);
            //        DateTime tdt = Convert.ToDateTime(dt2);
            //        lstenqfollow = lstenqfollow.Where(z => z.CreateDate >= fdt && z.CreateDate <= tdt).ToList();
            //    }
            //    catch (Exception ex)
            //    {
            //    }
            //}

            if (uid != "")
            {
                try
                {
                    ViewBag.usr = uid;
                    int usrid = Convert.ToInt32(uid);
                    lstenqfollow = lstenqfollow.Where(z => z.EnqHandledBy == usrid).ToList();
                }
                catch (Exception ex) { }

            }


            /*------------search code ends here--------*/

            ViewBag.TotalCount = lstenqfollow.Count;
            return View(lstenqfollow.OrderBy(z => z.nxdt).ToList());
        }



        #region Mock-Test-Functions
        public ActionResult _MockCreate(int id, string reftype, string name, string rollno)
        {
            MaEnqFollow obj = new MaEnqFollow();
            obj.ReferId = id;
            obj.FollowUpType = reftype;
            int Roll = Convert.ToInt32(rollno);
            int admid = db.MaAdmissions.Where(x => x.RollNo == Roll).Select(x => x.Id).FirstOrDefault();
            int? enqid = db.MaAdmissions.Where(x => x.RollNo == Roll).Select(x => x.EnquiryId).FirstOrDefault();
            obj.AdmissionId = admid;

            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);

            ViewBag.Remarks = new SelectList(db.MaClosedReasons, "Remarks", "Remarks");
            ViewBag.Enqid = enqid;
            string uname = db.Users.Where(z => z.Id == uid).Select(z => z.Fullname).First();
            ViewBag.Username = uname;
            ViewBag.referid = id;
            ViewBag.Name = name;
            ViewBag.rollno = rollno;

            return PartialView(obj);
        }

        [HttpPost]
        public ActionResult MockCreate(MaEnqFollow maenqfollow)
        {
            string chbranch = Request.Cookies["chbranch"].Value;

            if (chbranch == "Y")
            {
                return View("~/Views/Shared/AccessDenied.cshtml");
            }

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            maenqfollow.CDate = CommonUtil.IndianTime();
            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            int? stuid = db.Tb_MockTest.Where(z => z.Id == maenqfollow.ReferId).Select(z => z.StudentId).First();
            int _count = db.MaAdmissions.Where(z => z.Id == stuid).Count();

            if (_count > 0)
            {
                maenqfollow.Entertype = CommonUtil.Admission;
            }

            maenqfollow.EnqFollowBy = uid;
            maenqfollow.AdmissionId = stuid;
            db.MaEnqFollows.Add(maenqfollow);
            db.SaveChanges();

            if (maenqfollow.Enqstatus == "Move To Visa" && maenqfollow.FollowUpType == "MockTest")
            {
                //int? stuid = db.Tb_MockTest.Where(z => z.Id == maenqfollow.ReferId).Select(z => z.StudentId).First();
                double? bandoverall = db.Tb_MockTest.Where(z => z.StudentId == stuid).Select(z => z.Overall).Max();
                int mockid = db.Tb_MockTest.Where(z => z.StudentId == stuid && z.Overall == bandoverall).Select(z => z.Id).First();

                MaAdmission maadmission = db.MaAdmissions.Where(z => z.Id == stuid).First();
                maadmission.IsMoveToVisa = true;
                maadmission.MockTestId = mockid;
                db.SaveChanges();

                MaEnqFollow obj = new MaEnqFollow();
                obj.FollowUpType = "VisaMockTest";
                obj.Enqstatus = "Open";
                obj.ReferId = maenqfollow.ReferId;
                obj.Entertype = CommonUtil.Admission;
                obj.Remarks = maenqfollow.Remarks;
                obj.AdmissionId = stuid;
                obj.NextDate = CommonUtil.IndianTime().AddDays(2);
                obj.EnqFollowBy = maenqfollow.EnqFollowBy;
                obj.CDate = maenqfollow.CDate;
                db.MaEnqFollows.Add(obj);
                db.SaveChanges();

                InsertVisaFollow(maenqfollow, branchid, stuid, maadmission);
            }

            return RedirectToAction("PendingMockFollowUp");


        }

        //insert into visafollowup
        private void InsertVisaFollow(MaEnqFollow maenqfollow, int branchid, int? stuid, MaAdmission maadmission)
        {
            Tb_VisaFollowUp objvisa = new Tb_VisaFollowUp();
            objvisa.Cdate = CommonUtil.IndianTime();
            objvisa.FollowStatus = "Open";
            objvisa.Followtype = "VisaMockTest";
            objvisa.Remarks = maenqfollow.Remarks;
            objvisa.entertype = CommonUtil.Admission;
            objvisa.Branchid = branchid;
            objvisa.NextFollowdate = CommonUtil.IndianTime().AddDays(2);
            //objvisa.NextFollowdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(2);
            //objvisa.Admissionid = stuid;
            objvisa.LastFollowBy = maenqfollow.EnqFollowBy.ToString();
            objvisa.Name = maadmission.FirstName + " " + maadmission.LastName;
            objvisa.Referenceno = maadmission.RollNo;
            objvisa.ReferId = maenqfollow.ReferId;
            objvisa.Mobileno = maadmission.Mobile;
            db.Tb_VisaFollowUp.Add(objvisa);
            db.SaveChanges();
        }

        public ActionResult PendingMockFollowUp(string name = "", string mobile = "", string dt1 = "", string dt2 = "", string uid = "", string referenceno = "", string Print = "", string rangeon = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.UserList = new SelectList(db.Users.Where(z => z.BranchId == branchid && z.Status == true).OrderBy(x => x.Fullname).ToList(), "Id", "FullName");
            List<Mockfollowup_view> _lstenqfollow = _Getlst().ToList();
            /* this is for search. To display list code only you can skip this */
            try
            {
                if (name != "")
                {
                    ViewBag.name = name;

                    _lstenqfollow = _lstenqfollow.Where(z => String.Equals(z.FirstName, name, StringComparison.CurrentCultureIgnoreCase)).ToList();

                }
                if (mobile != "")
                {
                    ViewBag.mobile = mobile;
                    _lstenqfollow = _lstenqfollow.Where(z => z.Mobile == mobile).ToList();
                }
                if (dt1 != "" && dt2 != "")
                {
                    ViewBag.dt1 = dt1;
                    ViewBag.dt2 = dt2;
                    try
                    {
                        DateTime fdt = Convert.ToDateTime(dt1).Date;
                        DateTime tdt = Convert.ToDateTime(dt2).Date;
                        if (rangeon == "cdt")
                            _lstenqfollow = _lstenqfollow.Where(z => z.Afdate >= fdt && z.Afdate<= tdt).ToList();
                        else
                            _lstenqfollow = _lstenqfollow.Where(z => z.TestDate >= fdt && z.TestDate <= tdt).ToList();
                    }
                    catch (Exception ex)
                    {
                    }
                }

                if (uid != "")
                {
                    try
                    {
                        ViewBag.usr = uid;
                        int usrid = Convert.ToInt32(uid);
                        _lstenqfollow = _lstenqfollow.Where(z => z.EnqFollowBy == usrid).ToList();
                    }
                    catch (Exception ex) { }

                }

                if (referenceno != "")
                {
                    int refno = Convert.ToInt32(referenceno);
                    ViewBag.referenceno = referenceno;
                    _lstenqfollow = _lstenqfollow.Where(z => z.RollNo == refno).ToList();

                }

            }
            catch (Exception) { }


            /*------------search code ends here--------*/

            ViewBag.TotalCount = _lstenqfollow.Count;
            if (Print != "")
            {
                var Business = new RecieptBuisness();
                var reportViewModel = Business.GetPendingMockTestReport(_lstenqfollow.OrderBy(z => z.nxdt).ToList(), Print);

                var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
                if (reportViewModel.ViewAsAttachment)
                    Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                return File(renderedBytes, reportViewModel.LastmimeType);
            }
            return View(_lstenqfollow.OrderByDescending(z => z.nxdt).ToList());

            //return View(_lstenqfollow.Distinct().ToList());
        }






        public ActionResult PendingMockFollowUpByCreateDate(string name = "", string mobile = "", string dt1 = "", string dt2 = "", string uid = "", string referenceno = "", string Print = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.UserList = new SelectList(db.Users.Where(z => z.BranchId == branchid && z.Status == true).OrderBy(x => x.Fullname).ToList(), "Id", "FullName");
            List<FollowUpEnqModel> _lstenqfollow = _GetlstBycreatedate(uid, dt1, dt2).ToList();


            /* this is for search. To display list code only you can skip this */
            try
            {
                if (name != "")
                {
                    ViewBag.name = name;

                    _lstenqfollow = _lstenqfollow.Where(z => String.Equals(z.FirstName, name, StringComparison.CurrentCultureIgnoreCase)).ToList();

                }
                if (mobile != "")
                {
                    ViewBag.mobile = mobile;
                    _lstenqfollow = _lstenqfollow.Where(z => z.Mobile == mobile).ToList();
                }
                if (dt1 != "" && dt2 != "")
                {
                    ViewBag.dt1 = dt1;
                    ViewBag.dt2 = dt2;
                    try
                    {
                        DateTime fdt = Convert.ToDateTime(dt1);
                        DateTime tdt = Convert.ToDateTime(dt2);
                        _lstenqfollow = _lstenqfollow.Where(z => z.Cdate >= fdt && z.Cdate <= tdt).ToList();
                    }
                    catch (Exception ex)
                    {
                    }
                }

                if (uid != "")
                {
                    try
                    {
                        ViewBag.usr = uid;
                        int usrid = Convert.ToInt32(uid);
                        _lstenqfollow = _lstenqfollow.Where(z => z.EnqHandledBy == usrid).ToList();
                    }
                    catch (Exception ex) { }

                }

                if (referenceno != "")
                {
                    int refno = Convert.ToInt32(referenceno);
                    ViewBag.referenceno = referenceno;
                    _lstenqfollow = _lstenqfollow.Where(z => z.Rollno == refno).ToList();

                }

            }
            catch (Exception) { }


            /*------------search code ends here--------*/

            ViewBag.TotalCount = _lstenqfollow.Count;
            if (Print != "")
            {
                //var Business = new RecieptBuisness();
                //var reportViewModel = Business.GetPendingMockTestReport(_lstenqfollow.OrderBy(z => z.Cdate).ToList(), Print);

                //var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
                //if (reportViewModel.ViewAsAttachment)
                //    Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                //return File(renderedBytes, reportViewModel.LastmimeType);
            }
            return View(_lstenqfollow.OrderByDescending(z => z.Cdate).ToList());

            //return View(_lstenqfollow.Distinct().ToList());
        }

        public List<FollowUpEnqModel> _GetlstBycreatedate(string uid = "", string date1 = "", string date2 = "")
        {

            Status st = new Status();

            DataTable dt = new DataTable();
            //int userid = Convert.ToInt32(uid);

            if (Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeSuperAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeVisaCounselor)
            {
                int usrid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                dt = st.GetPendingMockCompletebycreatedate(usrid, date1, date2);
            }
            else
            {
                dt = st.GetPendingMockCompletebycreatedate(0, date1, date2);
            }
            List<FollowUpEnqModel> lstenqfollow = new List<FollowUpEnqModel>();

            foreach (DataRow dr in dt.Rows)
            {
                FollowUpEnqModel obj = new FollowUpEnqModel();

                int id = Convert.ToInt32(dr["Id"].ToString());


                Tb_MockTest mock = db.Tb_MockTest.Where(z => z.Id == id).OrderByDescending(m => m.Id).FirstOrDefault();
                MaAdmission adm = db.MaAdmissions.Where(z => z.Id == mock.StudentId).First();

                Tb_IELTSResult tb_ieltsresultobj = db.Tb_IELTSResult.Where(z => z.StudentId == adm.Id).OrderByDescending(m => m.Id).FirstOrDefault();
                MaEnquiry objenquiry = db.MaEnquiries.Where(z => z.Id == adm.EnquiryId).FirstOrDefault();

                DateTime CDate = CommonUtil.IndianTime().Date;

                MaEnqFollow maenq = db.MaEnqFollows.Where(z => z.ReferId == id).OrderByDescending(z => z.Id).FirstOrDefault();
                if (tb_ieltsresultobj == null)
                {
                    tb_ieltsresultobj = db.Tb_IELTSResult.Where(z => z.StudentId == adm.EnquiryId).OrderByDescending(z => z.Id).FirstOrDefault();
                }

                try
                {
                    obj.enqab = mock.Category;
                }
                catch (Exception)
                { }
                obj.AdmissionId = adm.Id;

                int cont = adm.Tb_CourseHist.Where(z => z.AdmissionId == mock.StudentId).Count();

                if (cont > 0)
                {
                    Tb_CourseHist _objcurrentcrselevl = adm.Tb_CourseHist.Where(z => z.AdmissionId == mock.StudentId).First();
                    obj.currentcourselevel = _objcurrentcrselevl.currentcourselevel;
                }
                try
                {
                    DateTime? maxtestdate = mock.TestDate;
                    obj.mocktestdate = maxtestdate;
                }
                catch (Exception ex)
                { }
                try
                {
                    double? overall = db.Tb_MockTest.Where(z => z.StudentId == adm.Id).Max(z => z.Overall);
                    obj.overallscore = overall.ToString();
                }
                catch (Exception ex)
                { }

                obj.Id = Convert.ToInt32(dr["Id"].ToString());
                obj.FirstName = adm.FirstName;
                obj.LastName = adm.LastName;
                obj.Mobile = adm.Mobile;
                obj.Cdate = Convert.ToDateTime(dr["cdate"].ToString());
                obj.EnqHandledBy = Convert.ToInt32(dr["EnqFollowBy"].ToString());
                obj.Rollno = (int)adm.RollNo;
                obj.Fullname = objenquiry.User.Fullname;
                obj.Enqstatus = "Open";

                try
                {
                    //string remarks = db.Tb_MockTest.Where(z => z.StudentId ==id).Max(z => z.Remarks);
                    obj.Remarks = maenq.Remarks;
                }
                catch (Exception ex)
                { }


                obj.CreateDate = Convert.ToDateTime(dr["cdate"].ToString());


                try
                {

                    obj.ieltstestdate = tb_ieltsresultobj.TestDate;
                }
                catch (Exception) { }

                lstenqfollow.Add(obj);
            }

            return lstenqfollow;
        }


        public List<Mockfollowup_view> _Getlst()
        {
            Status st = new Status();
            DataTable dt = new DataTable();
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            string userid = "%";
            if (Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeSuperAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeVisaCounselor)
            {
                userid = Request.Cookies[CommonUtil.CookieUserid].Value;

            }
            var list = db.Database.SqlQuery<Mockfollowup_view>($"select * from mockfollowup_view where branchid={branchid} and convert(varchar,EnqFollowBy) like '{userid}'").ToList();


            return list;
        }


        #endregion


        #region IELTS Test Function
        public ActionResult _IELTSCreate(int id, string reftype, string name, string refno, int enqid, int stuid)
        {
            MaEnqFollow obj = new MaEnqFollow();
            obj.ReferId = id;
            obj.FollowUpType = reftype;
            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);

            string uname = db.Users.Where(z => z.Id == uid).Select(z => z.Fullname).First();
            ViewBag.Username = uname;
            var rlist = db.MaClosedReasons.OrderBy(o => o.Remarks);
            ViewBag.Name = name;
            ViewBag.rollno = id;
            ViewBag.refno = refno;
            ViewBag.Enqid = enqid;
            ViewBag.stuid = stuid;
            ViewBag.Remarks = new SelectList(db.MaClosedReasons, "Remarks", "Remarks");
            ViewBag.CloseReason = new SelectList(rlist, "Remarks", "Remarks");
            return View(obj);
        }

        public ActionResult IELTSCreate(MaEnqFollow maenqfollow)
        {

            string chbranch = Request.Cookies["chbranch"].Value;

            if (chbranch == "Y")
            {
                return View("~/Views/Shared/AccessDenied.cshtml");
            }

            maenqfollow.CDate = CommonUtil.IndianTime();
            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            maenqfollow.EnqFollowBy = uid;

            if (ModelState.IsValid)
            {

                if (maenqfollow.FollowUpType == "IELTS")
                {
                    int? stuid = db.Tb_IELTSResult.Where(z => z.Id == maenqfollow.ReferId).Select(z => z.StudentId).First();
                    Tb_IELTSResult ieresult = db.Tb_IELTSResult.Where(z => z.Id == maenqfollow.ReferId).First();
                    if (ieresult.StudentType == "Admission")
                    {
                        MaAdmission maadmission = db.MaAdmissions.Where(z => z.Id == stuid).First();
                        maenqfollow.Entertype = CommonUtil.Admission;
                    }
                    if (ieresult.StudentType == "Enquiry")
                    {
                        MaEnquiry objenquiry = db.MaEnquiries.Where(z => z.Id == stuid).First();
                        maenqfollow.Entertype = CommonUtil.Enquiry;
                    }

                }

                db.MaEnqFollows.Add(maenqfollow);
                db.SaveChanges();

                if (maenqfollow.Enqstatus == "Move To Visa" && maenqfollow.FollowUpType == "IELTS")
                {
                    Tb_IELTSResult ieresult = db.Tb_IELTSResult.Where(z => z.Id == maenqfollow.ReferId).First();
                    if (ieresult.StudentType == "Admission")
                    {
                        int? stuid = db.Tb_IELTSResult.Where(z => z.Id == maenqfollow.ReferId).Select(z => z.StudentId).First();
                        double? bandoverall = db.Tb_IELTSResult.Where(z => z.StudentId == stuid).Select(z => z.Overall).Max();
                        int mockid = db.Tb_IELTSResult.Where(z => z.StudentId == stuid && z.Overall == bandoverall).Select(z => z.Id).First();

                        MaAdmission maadmission = db.MaAdmissions.Where(z => z.Id == stuid).First();
                        maadmission.IsMoveToVisa = true;
                        maadmission.IELTSTestId = mockid;
                        db.SaveChanges();


                        MaEnqFollow obj = new MaEnqFollow();
                        obj.FollowUpType = "VisaIELTSAdmission";
                        obj.Enqstatus = "Open";
                        obj.Entertype = CommonUtil.Admission;
                        obj.ReferId = maenqfollow.ReferId;
                        obj.Remarks = "Moved To Visa Successfully";
                        obj.NextDate = CommonUtil.IndianTime().AddDays(1).Date;
                        obj.EnqFollowBy = maenqfollow.EnqFollowBy;
                        obj.CDate = maenqfollow.CDate;
                        db.MaEnqFollows.Add(obj);
                        db.SaveChanges();

                        //insert into visafollowup
                        InsertintoVisaFollow(maenqfollow, stuid, maadmission);


                    }
                    if (ieresult.StudentType == "Enquiry")
                    {
                        try
                        {
                            int? stuid = db.Tb_IELTSResult.Where(z => z.Id == maenqfollow.ReferId).Select(z => z.StudentId).First();
                            double? bandoverall = db.Tb_IELTSResult.Where(z => z.StudentId == stuid).Select(z => z.Overall).Max();
                            int mockid = db.Tb_IELTSResult.Where(z => z.StudentId == stuid && z.Overall == bandoverall).Select(z => z.Id).First();

                            MaEnquiry objenquiry = db.MaEnquiries.Where(z => z.Id == stuid).First();
                            objenquiry.IELTSTestId = mockid;
                            objenquiry.IsMoveToVisa = true;
                            db.SaveChanges();

                            MaEnqFollow obj = new MaEnqFollow();
                            obj.FollowUpType = "VisaIELTS";
                            obj.Enqstatus = "Open";
                            obj.Entertype = CommonUtil.Enquiry;
                            obj.ReferId = maenqfollow.ReferId;
                            obj.Remarks = "Moved To Visa Successfully";
                            obj.NextDate = CommonUtil.IndianTime().AddDays(1).Date;
                            obj.EnqFollowBy = maenqfollow.EnqFollowBy;
                            obj.CDate = maenqfollow.CDate;
                            db.MaEnqFollows.Add(obj);
                            db.SaveChanges();


                            //insert into visafollowup
                            EnquiryVisaFollow(maenqfollow, stuid, objenquiry);

                        }
                        catch (DbEntityValidationException dbEx)
                        {
                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (var validationError in validationErrors.ValidationErrors)
                                {
                                    Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                }
                            }

                        }
                    }

                }

                if (maenqfollow.Enqstatus == "No" && maenqfollow.FollowUpType == "IELTS")
                {



                }



                return RedirectToAction("PendingIELTSFollowUp");
            }

            return Content("Invalid");
        }

        private void InsertintoVisaFollow(MaEnqFollow maenqfollow, int? stuid, MaAdmission maadmission)
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            Tb_VisaFollowUp objvisa = new Tb_VisaFollowUp();
            objvisa.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            objvisa.FollowStatus = "Open";
            objvisa.Followtype = "VisaIELTSAdmission";
            objvisa.entertype = "Admission";
            objvisa.Remarks = maenqfollow.Remarks;
            objvisa.NextFollowdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1);
            objvisa.Admissionid = stuid;
            objvisa.Branchid = branchid;
            objvisa.LastFollowBy = maenqfollow.EnqFollowBy.ToString();
            objvisa.Name = maadmission.FirstName + " " + maadmission.LastName;
            objvisa.Referenceno = maadmission.RollNo;
            objvisa.ReferId = maenqfollow.ReferId;
            objvisa.Mobileno = maadmission.Mobile;
            db.Tb_VisaFollowUp.Add(objvisa);
            db.SaveChanges();
        }

        private void EnquiryVisaFollow(MaEnqFollow maenqfollow, int? stuid, MaEnquiry maaenquiry)
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            Tb_VisaFollowUp objvisa = new Tb_VisaFollowUp();
            objvisa.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            objvisa.FollowStatus = "Open";
            objvisa.Followtype = "VisaIELTS";
            objvisa.entertype = "Enquiry";
            objvisa.Remarks = maenqfollow.Remarks;
            objvisa.NextFollowdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1);
            objvisa.Enquiryid = stuid;
            objvisa.Branchid = branchid;
            objvisa.LastFollowBy = maenqfollow.EnqFollowBy.ToString();
            objvisa.Name = maaenquiry.FirstName + " " + maaenquiry.LastName;
            objvisa.Referenceno = maaenquiry.Id;
            objvisa.ReferId = maenqfollow.ReferId;
            objvisa.Mobileno = maaenquiry.Mobile;
            db.Tb_VisaFollowUp.Add(objvisa);
            db.SaveChanges();

        }


        public ActionResult PendingIELTSFollowUp(string name = "", string mobile = "", string rangeon = "", string dt1 = "", string dt2 = "", string uid = "", string referenceno = "", string Score = "", string Print = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.UserList = new SelectList(db.Users.Where(z => z.BranchId == branchid && z.Status == true).OrderBy(x => x.Fullname).ToList(), "Id", "FullName");
            Status st = new Status();

            DataTable dt = new DataTable();

            if (Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeCounselor && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeSuperAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeVisaCounselor)
            {
                int usrid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                dt = st.GetPendingIELTSComplete(usrid);
            }
            else
            {
                dt = st.GetPendingIELTSComplete();
            }

            List<FollowUpEnqModel> lstenqfollow = new List<FollowUpEnqModel>();
            foreach (DataRow dr in dt.Rows)
            {
                int id = Convert.ToInt32(dr["Id"].ToString());
                Tb_IELTSResult mock = db.Tb_IELTSResult.Find(id);
                int? stuid = mock.StudentId;
                DateTime CDate = CommonUtil.IndianTime();
                if (dr["studenttype"].ToString() == "Admission")
                {
                    try
                    {
                        MaAdmission adm = db.MaAdmissions.Where(z => z.Id == stuid).First();
                        FollowUpEnqModel obj = new FollowUpEnqModel();

                        obj.Id = Convert.ToInt32(dr["Id"].ToString());
                        obj.FirstName = adm.FirstName;
                        obj.LastName = adm.LastName;
                        obj.Mobile = adm.Mobile;
                        obj.StudentType = "Admission";
                        obj.nxdt = Convert.ToDateTime(dr["nxdt"]);
                        obj.EnqHandledBy = Convert.ToInt32(dr["UserId"].ToString());
                        obj.AdmissionId = adm.Id;
                        obj.Rollno = adm.RollNo;
                        obj.EnqId = (int)adm.EnquiryId;
                        obj.StuId = (int)stuid;
                        try
                        {
                            obj.BookingType = Convert.ToInt32(dr["BookingType"].ToString());
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            obj.ieltstestdate = Convert.ToDateTime(dr["Testdate"]);
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.Cdate = Convert.ToDateTime(dr["cdate"]);
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.overallscore = dr["overall"].ToString();
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.overall = Convert.ToDouble(dr["overall"]);
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.Remarks = dr["remarks"].ToString();
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.Status = dr["enqstatus"].ToString();
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.enqab = dr["category"].ToString();
                        }
                        catch (Exception ex)
                        { }
                        lstenqfollow.Add(obj);
                    }
                    catch (Exception ex)
                    { }
                }
                else
                {
                    try
                    {
                        MaEnquiry adm = db.MaEnquiries.Where(z => z.Id == stuid).First();
                        FollowUpEnqModel obj = new FollowUpEnqModel();

                        obj.Id = Convert.ToInt32(dr["Id"].ToString());
                        obj.FirstName = adm.FirstName;
                        obj.LastName = adm.LastName;
                        obj.Mobile = adm.Mobile;
                        obj.StudentType = "Enquiry";
                        obj.nxdt = Convert.ToDateTime(dr["nxdt"]);
                        obj.EnqHandledBy = Convert.ToInt32(dr["UserId"].ToString());
                        obj.Rollno = adm.Id;
                        obj.EnqId = (int)stuid;
                        obj.StuId = (int)stuid;
                        try
                        {
                            obj.BookingType = Convert.ToInt32(dr["BookingType"].ToString());
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            obj.Cdate = Convert.ToDateTime(dr["cdate"]);
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.ieltstestdate = Convert.ToDateTime(dr["Testdate"]);
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.overallscore = dr["overall"].ToString();
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.overall = Convert.ToDouble(dr["overall"]);
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.Remarks = dr["remarks"].ToString();
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.Status = dr["enqstatus"].ToString();
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.enqab = dr["category"].ToString();
                        }
                        catch (Exception ex)
                        { }

                        lstenqfollow.Add(obj);
                    }
                    catch (Exception ex)
                    { }
                }
            }


            if (name != "")
            {
                ViewBag.name = name;
                name = name.ToUpper();
                lstenqfollow = lstenqfollow.Where(z => z.FirstName.ToUpper().Contains(name)).ToList();
            }
            if (mobile != "")
            {
                ViewBag.mobile = mobile;
                lstenqfollow = lstenqfollow.Where(z => z.Mobile == mobile).ToList();
            }

            if (dt1 != "" && dt2 != "")
            {
                ViewBag.dt1 = dt1;
                ViewBag.dt2 = dt2;
                try
                {
                    DateTime fdt = Convert.ToDateTime(dt1);
                    DateTime tdt = Convert.ToDateTime(dt2);
                    if (rangeon.Equals("fupdt"))
                        lstenqfollow = lstenqfollow.Where(z => z.nxdt >= fdt && z.nxdt <= tdt).ToList();
                    if (rangeon.Equals("cdt"))
                        lstenqfollow = lstenqfollow.Where(z => z.Cdate >= fdt && z.Cdate <= tdt).ToList();
                    if (rangeon.Equals("exdt"))
                        lstenqfollow = lstenqfollow.Where(z => z.ieltstestdate >= fdt && z.ieltstestdate <= tdt).ToList();
                }

                catch (Exception ex)
                {
                }
            }


            if (referenceno != "")
            {
                int refno = Convert.ToInt32(referenceno);
                ViewBag.referenceno = referenceno;
                lstenqfollow = lstenqfollow.Where(z => z.Rollno == refno).ToList();

            }
            if (Score != "")
            {
                if (Score == "All")
                {
                    //lst = lst.ToList();
                    lstenqfollow = lstenqfollow.ToList();
                }
                else
                {
                    //if (Score == "5.5")
                    //{
                    //    lstenqfollow = lstenqfollow.Where(x => x.overall == 5.5).ToList();
                    //}
                    if (Score == "A5")
                    {
                        lstenqfollow = lstenqfollow.Where(x => x.overall >= 6).ToList();
                    }
                    if (Score == "B5")
                    {
                        lstenqfollow = lstenqfollow.Where(x => x.overall <= 5.5).ToList();
                    }
                }
                ViewBag.TotalCount = lstenqfollow.Count;
            }

            if (uid != "")
            {
                try
                {
                    ViewBag.usr = uid;
                    int usrid = Convert.ToInt32(uid);
                    lstenqfollow = lstenqfollow.Where(z => z.EnqHandledBy == usrid).ToList();
                }
                catch (Exception ex) { }

            }


            /*------------search code ends here--------*/

            ViewBag.TotalCount = lstenqfollow.Count;
            if (Print != "")
            {
                var Business = new RecieptBuisness();
                var reportViewModel = Business.GetPendingIeltsReport(lstenqfollow.OrderBy(z => z.nxdt).ToList(), Print);

                var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
                if (reportViewModel.ViewAsAttachment)
                    Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                return File(renderedBytes, reportViewModel.LastmimeType);
            }
            return View(lstenqfollow.OrderBy(z => z.nxdt).ToList());
        }

        public ActionResult PendingIELTSFollowUpByCreateDate(string name = "", string mobile = "", string dt1 = "", string dt2 = "", string uid = "", string referenceno = "", string Print = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.UserList = new SelectList(db.Users.Where(z => z.BranchId == branchid && z.Status == true).OrderBy(x => x.Fullname).ToList(), "Id", "FullName");
            Status st = new Status();

            DataTable dt = new DataTable();

            if (Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeSuperAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeVisaCounselor)
            {
                int usrid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                dt = st.GetPendingIELTSCompleteByCdate(usrid, dt1, dt2);
            }
            else
            {
                dt = st.GetPendingIELTSCompleteByCdate(0, dt1, dt2);
            }

            List<FollowUpEnqModel> lstenqfollow = new List<FollowUpEnqModel>();
            foreach (DataRow dr in dt.Rows)
            {
                int id = Convert.ToInt32(dr["Id"].ToString());
                Tb_IELTSResult mock = db.Tb_IELTSResult.Where(z => z.Id == id).First();
                DateTime CDate = CommonUtil.IndianTime().Date;
                if (mock.StudentType == "Admission")
                {
                    try
                    {
                        MaAdmission adm = db.MaAdmissions.Where(z => z.Id == mock.StudentId).First();
                        MaEnqFollow enqfollow = db.MaEnqFollows.Where(z => z.ReferId == id && z.FollowUpType == "IELTS" && z.CDate <= CDate).OrderByDescending(z => z.Id).FirstOrDefault();

                        FollowUpEnqModel obj = new FollowUpEnqModel();

                        obj.Id = Convert.ToInt32(dr["Id"].ToString());
                        obj.FirstName = adm.FirstName;
                        obj.LastName = adm.LastName;
                        obj.Mobile = adm.Mobile;
                        obj.StudentType = "Admission";
                        obj.nxdt = enqfollow.NextDate;
                        obj.CreateDate = enqfollow.CDate;
                        obj.EnqHandledBy = Convert.ToInt32(dr["UserId"].ToString());
                        obj.AdmissionId = adm.Id;
                        obj.Rollno = adm.RollNo;
                        try
                        {
                            obj.ieltstestdate = mock.TestDate;
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.overallscore = mock.Overall.ToString();
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.Remarks = enqfollow.Remarks;
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.Status = enqfollow.Enqstatus;
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.enqab = mock.Category;
                        }
                        catch (Exception ex)
                        { }
                        lstenqfollow.Add(obj);
                    }
                    catch (Exception ex)
                    { }
                }
                else
                {
                    try
                    {
                        MaEnquiry adm = db.MaEnquiries.Where(z => z.Id == mock.StudentId).First();
                        MaEnqFollow enqfollow = db.MaEnqFollows.Where(z => z.ReferId == id && z.FollowUpType == "IELTS" && z.NextDate <= CDate).OrderByDescending(z => z.Id).FirstOrDefault();

                        FollowUpEnqModel obj = new FollowUpEnqModel();

                        obj.Id = Convert.ToInt32(dr["Id"].ToString());
                        obj.FirstName = adm.FirstName;
                        obj.LastName = adm.LastName;
                        obj.Mobile = adm.Mobile;
                        obj.StudentType = "Enquiry";
                        obj.CreateDate = enqfollow.CDate;
                        obj.EnqHandledBy = Convert.ToInt32(dr["UserId"].ToString());
                        obj.Rollno = adm.Id;
                        try
                        {
                            obj.ieltstestdate = mock.TestDate;
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.overallscore = mock.Overall.ToString();
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.AdmissionId = adm.Id;
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.Remarks = enqfollow.Remarks;
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.enqab = mock.Category;
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            obj.Status = enqfollow.Enqstatus;
                        }
                        catch (Exception ex)
                        { }
                        lstenqfollow.Add(obj);
                    }
                    catch (Exception ex)
                    { }
                }
            }
            //}
            /* this is for search. To display list code only you can skip this */

            if (name != "")
            {
                ViewBag.name = name;
                name = name.ToUpper();
                lstenqfollow = lstenqfollow.Where(z => z.FirstName.ToUpper().Contains(name)).ToList();
            }
            if (mobile != "")
            {
                ViewBag.mobile = mobile;
                lstenqfollow = lstenqfollow.Where(z => z.Mobile == mobile).ToList();
            }
            //if (dt1 != "" && dt2 != "")
            //{
            //    ViewBag.dt1 = dt1;
            //    ViewBag.dt2 = dt2;
            //    try
            //    {
            //        DateTime fdt = Convert.ToDateTime(dt1);
            //        DateTime tdt = Convert.ToDateTime(dt2);
            //        lstenqfollow = lstenqfollow.Where(z => z.CreateDate >= fdt && z.CreateDate <= tdt).ToList();
            //    }
            //    catch (Exception ex)
            //    {
            //    }
            //}


            if (referenceno != "")
            {
                int refno = Convert.ToInt32(referenceno);
                ViewBag.referenceno = referenceno;
                lstenqfollow = lstenqfollow.Where(z => z.Rollno == refno).ToList();

            }

            if (uid != "")
            {
                try
                {
                    ViewBag.usr = uid;
                    int usrid = Convert.ToInt32(uid);
                    lstenqfollow = lstenqfollow.Where(z => z.EnqHandledBy == usrid).ToList();
                }
                catch (Exception ex) { }

            }


            /*------------search code ends here--------*/

            ViewBag.TotalCount = lstenqfollow.Count;
            if (Print != "")
            {
                var Business = new RecieptBuisness();
                var reportViewModel = Business.GetPendingIeltsReport(lstenqfollow.OrderBy(z => z.Cdate).ToList(), Print);

                var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
                if (reportViewModel.ViewAsAttachment)
                    Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                return File(renderedBytes, reportViewModel.LastmimeType);
            }
            return View(lstenqfollow.OrderBy(z => z.Cdate).ToList());
        }


        #endregion

        #region Visa_Functions

        public ActionResult _VisaCreate(int id, string reftype, string name, string rollno, int visaid = 0, int vid = 0)
        {
            Tb_VisaFollowUp obj = new Tb_VisaFollowUp();
            obj.ReferId = id;
            obj.Followtype = reftype;
            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);

            string uname = db.Users.Where(z => z.Id == uid).Select(z => z.Fullname).First();
            ViewBag.Username = uname;
            ViewBag.Name = name;

            if (reftype.Equals("VisaMockTest"))
                ViewBag.Enqid = db.MaAdmissions.Find(vid).EnquiryId;
            else
                ViewBag.Enqid = vid;

            ViewBag.rollno = vid;

            ViewBag.visaid = visaid;


            return PartialView(obj);
        }

        public ActionResult VisaCreate(Tb_VisaFollowUp maenqfollow, FormCollection collection)
        {

            string chbranch = Request.Cookies["chbranch"].Value;

            if (chbranch == "Y")
            {
                return View("~/Views/Shared/AccessDenied.cshtml");
            }
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            maenqfollow.Cdate = CommonUtil.IndianTime();
            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            maenqfollow.LastFollowBy = uid.ToString();
            if (ModelState.IsValid)
            {
                Tb_VisaFollowUp visafollow = db.Tb_VisaFollowUp.Where(z => z.ReferId == maenqfollow.ReferId).FirstOrDefault();
                visafollow.Remarks = maenqfollow.Remarks;
                visafollow.NextFollowdate = maenqfollow.NextFollowdate;
                visafollow.FollowStatus = maenqfollow.FollowStatus;
                visafollow.LastFollowBy = uid.ToString();
                db.SaveChanges();
            }

            MaEnqFollow maenqfollow1 = new MaEnqFollow();

            int visaid = Convert.ToInt32(collection["hdnvisaid"]);
            //maenqfollow1.ReferId = visaid;
            maenqfollow1.ReferId = maenqfollow.ReferId;

            maenqfollow1.Remarks = maenqfollow.Remarks;
            maenqfollow1.NextDate = maenqfollow.NextFollowdate;
            if (maenqfollow.Followtype == "VisaIELTS")
            {
                //  maenqfollow1.FollowUpType = "VisaIELTS";
                maenqfollow1.FollowUpType = "VisaIELTS";
            }
            else if (maenqfollow.Followtype == "VisaIELTSAdmission")
            {
                maenqfollow1.FollowUpType = "VisaIELTSAdmission";
            }
            else if (maenqfollow.Followtype == "VisaEnquiry")
            {
                maenqfollow1.FollowUpType = "VisaEnquiry";
            }
            else if (maenqfollow.Followtype == "MockTest")
            {
                maenqfollow1.FollowUpType = "VisaMockTest";
            }

            maenqfollow1.CDate = CommonUtil.IndianTime();
            maenqfollow1.EnqFollowBy = uid;
            maenqfollow1.Enqstatus = maenqfollow.FollowStatus;
            db.MaEnqFollows.Add(maenqfollow1);
            db.SaveChanges();

            return RedirectToAction("PendingVisaFollowUp");
        }

        public PartialViewResult _PendingVisaFollow()
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            DataTable dt = new DataTable();
            List<PendingVisaStudentBean> lstenqfollow = new List<PendingVisaStudentBean>();

            List<Tb_VisaFollowUp> _lstfollow = new List<Tb_VisaFollowUp>();
            DateTime curntdate = CommonUtil.IndianTime().Date;

            if (Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeSuperAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeVisaCounselor && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeSuperAdmin)
            {
                _lstfollow = db.Tb_VisaFollowUp.ToList();
            }
            else
            {
                _lstfollow = db.Tb_VisaFollowUp.Where(z => z.NextFollowdate <= curntdate && z.FollowStatus == "Open" && z.Branchid == branchid).ToList();
            }
            //get list from admission table for MockTest start here
            foreach (Tb_VisaFollowUp obj in _lstfollow)
            {
                PendingVisaStudentBean objbean = new PendingVisaStudentBean();
                objbean.referid = obj.ReferId;
                objbean.rollno = Convert.ToInt32(obj.Referenceno);
                objbean.SName = obj.Name;
                objbean.Mobile = obj.Mobileno;
                objbean.remarks = obj.Remarks;
                objbean.status = obj.FollowStatus;
                objbean.followtype = obj.Followtype;
                objbean.type = obj.entertype;
                try
                {
                    int lastfollowid = Convert.ToInt32(obj.LastFollowBy);
                    objbean.lastupdated = db.Users.Where(z => z.Id == lastfollowid).Select(z => z.username).FirstOrDefault().ToString();
                }
                catch (Exception ex)
                { }
                objbean.followupdate = obj.NextFollowdate;
                objbean.EntryType = obj.Followtype;
                lstenqfollow.Add(objbean);

            }
            return PartialView(lstenqfollow.Take(3).ToList());
        }
        public ActionResult PendingVisaFollowUp(string name = "", string mobile = "", string txtentertype = "", string txtstatus = "", string txtlastupdated = "", string rollno = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            // ViewBag.UserList = new SelectList(db.Users.Where(z => z.BranchId == branchid).ToList(), "Id", "FullName");
            ViewBag.txtlastupdated = new SelectList(db.Users.Where(z => z.BranchId == branchid && z.Usertype == CommonUtil.UserTypeVisaCounselor || z.Usertype == CommonUtil.UserTypeAdmin).ToList(), "Id", "username");

            string uid = Request.Cookies[CommonUtil.CookieUserid].Value;

            List<PendingVisaStudentBean> lstenqfollow = new List<PendingVisaStudentBean>();

            List<Tb_VisaFollowUp> _lstfollow = new List<Tb_VisaFollowUp>();
            DateTime curntdate = CommonUtil.IndianTime().Date;

            if (Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeSuperAdmin || Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeVisaCounselor || Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeAdmin)
            {
                _lstfollow = db.Tb_VisaFollowUp.Where(z => z.NextFollowdate <= curntdate && z.FollowStatus == "Open" && z.Branchid == branchid).ToList();
            }
            else
            {
                _lstfollow = db.Tb_VisaFollowUp.Where(z => z.NextFollowdate <= curntdate && z.FollowStatus == "Open" && z.Branchid == branchid && z.LastFollowBy == uid).ToList();
            }

            //get list from admission table for MockTest start here
            foreach (Tb_VisaFollowUp obj in _lstfollow)
            {
                PendingVisaStudentBean objbean = new PendingVisaStudentBean();
                objbean.referid = obj.ReferId;
                objbean.rollno = Convert.ToInt32(obj.Referenceno);
                objbean.SName = obj.Name;
                objbean.Mobile = obj.Mobileno;
                objbean.remarks = obj.Remarks;
                objbean.status = obj.FollowStatus;
                objbean.Visaid = obj.Id;
                if (obj.Followtype == "MockTest" || obj.Followtype == "VisaMockTest")
                {
                    objbean.followtype = CommonUtil.AdmissionMcktst;
                }

                else if (obj.Followtype == "VisaEnquiry")
                {
                    objbean.followtype = CommonUtil.Enquiry;

                }
                else if (obj.Followtype == "IELTS" || obj.Followtype == "VisaIELTS")
                {
                    objbean.followtype = CommonUtil.EnquiryIELTS;
                }
                else if (obj.Followtype == "VisaIELTSAdmission")
                {
                    objbean.followtype = CommonUtil.ProspStudentsIELTS;
                }

                objbean.type = obj.entertype;
                try
                {
                    int lastfollowid = Convert.ToInt32(obj.LastFollowBy);
                    objbean.lastupdated = db.Users.Where(z => z.Id == lastfollowid).Select(z => z.username).FirstOrDefault().ToString();
                }
                catch (Exception ex)
                { }
                objbean.followupdate = obj.NextFollowdate;
                objbean.EntryType = obj.Followtype;
                lstenqfollow.Add(objbean);
            }



            if (name != "")
            {
                ViewBag.name = name;
                name = name.ToUpper();
                lstenqfollow = lstenqfollow.Where(z => z.SName.ToUpper().Contains(name)).ToList();
            }
            if (mobile != "")
            {
                ViewBag.mobile = mobile;
                lstenqfollow = lstenqfollow.Where(z => z.Mobile == mobile).ToList();
            }


            if (txtentertype != "")
            {
                ViewBag.entertype = txtentertype;
                lstenqfollow = lstenqfollow.Where(z => z.followtype.ToUpper().Contains(txtentertype.ToUpper())).ToList();
            }


            if (txtstatus != "")
            {
                ViewBag.txtstatus = txtstatus;
                lstenqfollow = lstenqfollow.Where(z => z.status == txtstatus).ToList();
            }

            if (txtlastupdated != "")
            {

                int lastfow = Convert.ToInt32(txtlastupdated);
                string lastupdateuser = db.Users.Where(z => z.Id == lastfow).Select(z => z.username).FirstOrDefault().ToString();

                ViewBag.lastupdated = txtlastupdated;
                lstenqfollow = lstenqfollow.Where(z => z.lastupdated.ToUpper().Contains(lastupdateuser.ToUpper())).ToList();
            }

            if (rollno != "")
            {
                int referenceno = Convert.ToInt32(rollno);
                ViewBag.rollno = rollno;
                lstenqfollow = lstenqfollow.Where(z => z.rollno == referenceno).ToList();
            }

            ViewBag.TotalCount = lstenqfollow.OrderBy(z => z.followupdate).Count();

            return View(lstenqfollow.OrderBy(z => z.followupdate).ToList());
        }
        #endregion

        #region Visa_Admission_Prospective_Functions

        public ActionResult _AdmissionCreate(int id, string reftype, string name)
        {
            MaEnqFollow obj = new MaEnqFollow();
            obj.ReferId = id;
            obj.FollowUpType = reftype;
            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);

            string uname = db.Users.Where(z => z.Id == uid).Select(z => z.Fullname).First();
            ViewBag.Username = uname;
            ViewBag.rollno = id;

            ViewBag.name = name;

            return PartialView(obj);
        }
        public ActionResult AdmissionCreate(MaEnqFollow maenqfollow)
        {
            string chbranch = Request.Cookies["chbranch"].Value;

            if (chbranch == "Y")
            {
                return View("~/Views/Shared/AccessDenied.cshtml");
            }

            maenqfollow.CDate = CommonUtil.IndianTime();
            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            maenqfollow.EnqFollowBy = uid;
            if (ModelState.IsValid)
            {
                db.MaEnqFollows.Add(maenqfollow);
                db.SaveChanges();

                if (maenqfollow.Enqstatus == "Move To Visa")
                {

                    // MaAdmission maadmission = db.MaAdmission.Where(z => z.Id == maenqfollow.ReferId).First();
                    //maadmission.IsMoveToVisa = true;

                    MaEnquiry maaenquiry = db.MaEnquiries.Where(z => z.Id == maenqfollow.ReferId).First();
                    maaenquiry.IsMoveToVisa = true;

                    db.SaveChanges();

                    MaEnqFollow obj = new MaEnqFollow();
                    obj.FollowUpType = "VisaEnquiry";
                    obj.Enqstatus = "Open";
                    obj.ReferId = maenqfollow.ReferId;
                    obj.Remarks = "Moved To Visa Successfully";
                    obj.NextDate = CommonUtil.IndianTime().AddDays(2);
                    obj.EnqFollowBy = maenqfollow.EnqFollowBy;
                    obj.CDate = maenqfollow.CDate;
                    db.MaEnqFollows.Add(obj);
                    db.SaveChanges();
                }
                return Content("Success");

            }



            return Content("Invalid");
        }


    }

    #endregion




}
