﻿using SSCMvc.Models;
using SSCMvc.Webutil;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSCMvc.Controllers
{
    public class CourseWiseDiscountController : Controller
    {
        // GET: CourseWiseDiscount
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        public ActionResult Index(string msg)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BranhDiscount, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            if (msg == "Updt")
            {
                ViewBag.message = "Record Updated Successfully";
            }
            if (msg == "sve")
            {
                ViewBag.message = "Record Sdded Successfully";
            }
            if (msg == "dup")
            {
                ViewBag.error = "Record With Same name already exists";
            }
            List<Tb_CoursewiseDiscount> lst = new List<Tb_CoursewiseDiscount>();
            lst = db.Tb_CoursewiseDiscount.ToList();
            ViewBag.TotalCount = lst.Count();
            return View(lst);
        }

        //
        // GET: /Ma_Area/Details/5

        public ActionResult Details(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BranhDiscount, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            Tb_CoursewiseDiscount Disc = db.Tb_CoursewiseDiscount.Find(id);
          
            return View(Disc);
        }

        //
        // GET: /Ma_Area/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BranhDiscount, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName");
            ViewBag.PackgId = new SelectList(db.MaPackages, "Id", "PackageName");
            ViewBag.PckHr = new SelectList(db.St_PackageHours, "PackageHours", "PackageHours");
            return View();
        }

        //
        // POST: /Ma_Area/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tb_CoursewiseDiscount discount)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BranhDiscount, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName",discount.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName",discount.CourseId);
            ViewBag.PackgId = new SelectList(db.MaPackages, "Id", "PackageName",discount.PackgId);
            ViewBag.PckHr = new SelectList(db.St_PackageHours, "PackageHours", "PackageHours",discount.PckHr);
            if (ModelState.IsValid)
            {
                discount.CDate = DateTime.Now;
                db.Tb_CoursewiseDiscount.Add(discount);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2601)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
                return RedirectToAction("Index", new { @msg = "sve" });
            }
            return View(discount);
        }

        //
        // GET: /Ma_Area/Edit/5

        public ActionResult Edit(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BranhDiscount, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            Tb_CoursewiseDiscount discount = db.Tb_CoursewiseDiscount.Find(id);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", discount.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", discount.CourseId);
            ViewBag.PackgId = new SelectList(db.MaPackages, "Id", "PackageName", discount.PackgId);
            ViewBag.PckHr = new SelectList(db.St_PackageHours, "PackageHours", "PackageHours", discount.PckHr);

            return View(discount);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetPackage(int id,string Pkhr)
        {
            db = new SSCWebDBEntities1();
            var courselist = db.MaPackages.Where(z => z.CourseId == id && z.PackageHours==Pkhr).ToList();
            var colourData = courselist.Select(c => new SelectListItem()
            {
                Text = c.PackageName,
                Value = c.Id.ToString(),

            });
            return Json(colourData, JsonRequestBehavior.AllowGet);

        }

        //
        // POST: /Ma_Area/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tb_CoursewiseDiscount discount)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BranhDiscount, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", discount.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", discount.CourseId);
            ViewBag.PackgId = new SelectList(db.MaPackages, "Id", "PackageName", discount.PackgId);
            ViewBag.PckHr = new SelectList(db.St_PackageHours, "PackageHours", "PackageHours", discount.PckHr);
            if (ModelState.IsValid)
            {
                db.Entry(discount).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2601)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
                return RedirectToAction("Index", new { @msg = "Updt" });
            }
            return View(discount);
        }

        //
        // GET: /Ma_Area/Delete/5
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.BranhDiscount, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            try
            {
                Tb_CoursewiseDiscount disount = db.Tb_CoursewiseDiscount.Find(id);

                db.Tb_CoursewiseDiscount.Remove(disount);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

    }
}