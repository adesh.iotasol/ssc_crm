﻿using SSCMvc.Models;
using SSCMvc.Webutil;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSCMvc.Controllers
{
    public class MaPackageHoursController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        //
        // GET: /MaPackageHours/

        public ActionResult Index(string msg)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddPackageHour, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (msg == "Updt")
            {
                ViewBag.message = "Record Updated Successfully";
            }
            if (msg == "sve")
            {
                ViewBag.message = "Record Sdded Successfully";
            }
            if (msg == "dup")
            {
                ViewBag.error = "Record With Same name already exists";
            }
            List<St_PackageHours> lst = new List<St_PackageHours>();
            lst = db.St_PackageHours.ToList();
            ViewBag.TotalCount = lst.Count();
            return View(lst);
        }

        public ActionResult Details(string name)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddPackageHour, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            St_PackageHours st_pck = db.St_PackageHours.Where(x => x.PackageHours == name).FirstOrDefault();
            if (st_pck == null)
            {
                return HttpNotFound();
            }
            return View(st_pck);
        }

        public ActionResult Create()
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddPackageHour, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(St_PackageHours st_pck)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddPackageHour, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (ModelState.IsValid)
            {
               
                db.St_PackageHours.Add(st_pck);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2601)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
                return RedirectToAction("Index", new { @msg = "sve" });
            }

            return View(st_pck);
        }

        public ActionResult Edit(string name)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddPackageHour, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            St_PackageHours st_pck = db.St_PackageHours.Where(x => x.PackageHours == name).FirstOrDefault();
            if (st_pck == null)
            {
                return HttpNotFound();
            }
            return View(st_pck);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(St_PackageHours st_pck)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddPackageHour, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
                st_pck.PackageHours = st_pck.PackageHours;
                db.Entry(st_pck).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2601)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
                return RedirectToAction("Index", new { @msg = "Updt" });
            }
            return View(st_pck);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(string name)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddPackageHour, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            try
            {
                St_PackageHours st_pck = db.St_PackageHours.Where(x => x.PackageHours == name).FirstOrDefault();

                db.St_PackageHours.Remove(st_pck);
                db.SaveChanges();
                return RedirectToAction("Index", new { @msg = "Del" });
                //return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

    }
}
