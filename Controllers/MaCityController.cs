﻿using SSCMvc.Models;
using SSCMvc.Webutil;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSCMvc.Controllers
{
    public class MaCityController : Controller
    {

        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /Ma_Area/

        public ActionResult Index(string msg)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (msg == "Updt")
            {
                ViewBag.message = "Record Updated Successfully";
            }
            if (msg == "sve")
            {
                ViewBag.message = "Record Sdded Successfully";
            }
            if (msg == "dup")
            {
                ViewBag.error = "Record With Same name already exists";
            }
            List<MaCity> lst = new List<MaCity>();
            lst = db.MaCities.ToList();
            ViewBag.TotalCount = lst.Count();
            return View(lst);
        }

        //
        // GET: /Ma_Area/Details/5

        public ActionResult Details(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            MaCity city = db.MaCities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        //
        // GET: /Ma_Area/Create

        public ActionResult Create()
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            return View();
        }

        //
        // POST: /Ma_Area/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MaCity city)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (ModelState.IsValid)
            {
               
                city.CityName = city.CityName.ToUpper();
                db.MaCities.Add(city);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2601)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
                return RedirectToAction("Index", new { @msg = "sve" });
            }

            return View(city);
        }

        //
        // GET: /Ma_Area/Edit/5

        public ActionResult Edit(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            MaCity city = db.MaCities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        //
        // POST: /Ma_Area/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MaCity city)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
                city.CityName = city.CityName.ToUpper();
                db.Entry(city).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2601)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
                return RedirectToAction("Index", new { @msg = "Updt" });
            }
            return View(city);
        }

        //
        // GET: /Ma_Area/Delete/5
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            try
            {
                MaCity city = db.MaCities.Find(id);

                db.MaCities.Remove(city);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        //
        // POST: /Ma_Area/Delete/5





        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}