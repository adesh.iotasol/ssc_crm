﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    public class Ma_AreaController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /Ma_Area/

        public ActionResult Index(string msg)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (msg == "Updt")
            {
                ViewBag.message = "Record Updated Successfully";
            }
            if (msg == "sve")
            {
                ViewBag.message = "Record Sdded Successfully";
            }
            if (msg == "dup")
            {
                ViewBag.error = "Record With Same name already exists";
            }
            List<Ma_Area> lst = new List<Ma_Area>();
            lst = db.Ma_Area.Where(a => a.Status == true).ToList();
            ViewBag.TotalCount = lst.Count();
            return View(lst);
        }

        //
        // GET: /Ma_Area/Details/5

        public ActionResult Details(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            Ma_Area ma_area = db.Ma_Area.Find(id);
            if (ma_area == null)
            {
                return HttpNotFound();
            }
            return View(ma_area);
        }

        //
        // GET: /Ma_Area/Create

        public ActionResult Create()
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            ViewBag.CityId = new SelectList(db.MaCities, "Id", "CityName");
            return View();
        }

        //
        // POST: /Ma_Area/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Ma_Area ma_area)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            ViewBag.CityId = new SelectList(db.MaCities, "Id", "CityName",ma_area.CityId);
            if (ModelState.IsValid)
            {
                ma_area.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                ma_area.Status = true;
                ma_area.Area = ma_area.Area.ToUpper();
                db.Ma_Area.Add(ma_area);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2601)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
                return RedirectToAction("Index", new { @msg = "sve" });
            }

            return View(ma_area);
        }

        //
        // GET: /Ma_Area/Edit/5

        public ActionResult Edit(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            Ma_Area ma_area = db.Ma_Area.Find(id);
            ViewBag.CityId = new SelectList(db.MaCities, "Id", "CityName", ma_area.CityId);
            if (ma_area == null)
            {
                return HttpNotFound();
            }
            return View(ma_area);
        }

        //
        // POST: /Ma_Area/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Ma_Area ma_area)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            ViewBag.CityId = new SelectList(db.MaCities, "Id", "CityName", ma_area.CityId);
            if (ModelState.IsValid)
            {
                ma_area.Area = ma_area.Area.ToUpper();
                db.Entry(ma_area).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2601)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
                return RedirectToAction("Index", new { @msg = "Updt" });
            }
            return View(ma_area);
        }

        //
        // GET: /Ma_Area/Delete/5
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AddArea, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            try
            {
                Ma_Area ma_area = db.Ma_Area.Find(id);

                db.Ma_Area.Remove(ma_area);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        //
        // POST: /Ma_Area/Delete/5





        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}