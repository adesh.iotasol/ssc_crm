﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class MaBranchController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /MaBranch/

        public ActionResult Index()
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Branches, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            var branchlist = db.MaBranches.OrderByDescending(z => z.Id).ToList();
            ViewBag.TotalCount = branchlist.Count;
            return View(branchlist.ToList());
        }

        //
        // GET: /MaBranch/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Branches, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            MaBranch mabranch = db.MaBranches.Find(id);
            if (mabranch == null)
            {
                return HttpNotFound();
            }
            return View(mabranch);
        }

        //
        // GET: /MaBranch/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Branches, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            return View();
        }

        //
        // POST: /MaBranch/Create

        [HttpPost]
        public ActionResult Create(MaBranch mabranch)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Branches, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
                mabranch.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                mabranch.Status = true;
                db.MaBranches.Add(mabranch);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mabranch);
        }

        //
        // GET: /MaBranch/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Branches, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            MaBranch mabranch = db.MaBranches.Find(id);
            if (mabranch == null)
            {
                return HttpNotFound();
            }
            return View(mabranch);
        }

        //
        // POST: /MaBranch/Edit/5

        [HttpPost]
        public ActionResult Edit(MaBranch mabranch)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Branches, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
                db.Entry(mabranch).State = System.Data.Entity.EntityState.Modified;
                //mabranch.Cdate = DateTime.Now.Date;
                mabranch.Status = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mabranch);
        }

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Delete(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Branches, UserAction.Delete);
                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            try
            {
                MaBranch mabranch = db.MaBranches.Find(id);

                db.MaBranches.Remove(mabranch);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}