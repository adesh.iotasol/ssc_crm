﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Webutil;
using SSCMvc.Models;
using System.Data;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class Tb_LedgerController : Controller
    {
        SSCWebDBEntities1 db = new SSCWebDBEntities1();
        CommonUtil cu = new CommonUtil();
        //
        // GET: /Tb_Ledger/
        public ActionResult GetLedger(int accid, string dt1 = "", string dt2 = "", string vtype = "")
        {

            


            ViewBag.RollNo = db.MaAdmissions.Where(z => z.Id == accid).Select(z => z.RollNo).First();
            string fname = db.MaAdmissions.Where(z => z.Id == accid).Select(z => z.FirstName).First();
            string lname = db.MaAdmissions.Where(z => z.Id == accid).Select(z => z.LastName).First();
            ViewBag.StudentName = fname + " " + lname;


            List<Tb_CourseHist> lsthis = db.Tb_CourseHist.Where(z => z.AdmissionId == accid).ToList();
            double opbal = 0;
            foreach (Tb_CourseHist obj in lsthis)
            {
                opbal += (double)obj.Amout;
            }
            ViewBag.opbal = opbal;


            Status st = new Status();

            int deptid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

            dt1 = db.MaAdmissions.Where(z => z.Id == accid).Select(z => z.Cdate).First().ToString();
            dt2 = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).ToString();
            DataTable dt = st.GetLedger(accid, dt1, dt2);

            List<Tb_Ledger> ledgerlst = new List<Tb_Ledger>();

            if (vtype == "")
            {
                vtype = "%";
            }

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Tb_Ledger lg = new Tb_Ledger();
                    try
                    {
                        lg.balance = double.Parse(dr["balance"].ToString());
                    }
                    catch (Exception ex)
                    {
                    }
                    lg.state = dr["state"].ToString();
                    lg.Vouchdate = DateTime.Parse(dr["Vouchdate"].ToString());
                    lg.VoucherType = dr["VoucherType"].ToString();
                    lg.VoucherNo = Convert.ToInt32(dr["VoucherNo"]);
                    lg.Narration = dr["Narration"].ToString();
                    try
                    {
                        lg.DebitAmt = Convert.ToDouble(dr["DebitAmt"]);
                    }
                    catch (Exception)
                    {

                    }

                    try
                    {
                        lg.CreditAmt = Convert.ToDouble(dr["CreditAmt"]);
                    }
                    catch (Exception)
                    {
                    }

                    ledgerlst.Add(lg);

                }
            }

            // apply filter with vtype
            if (vtype != "%")
            {
                ledgerlst = ledgerlst.Where(m => m.VoucherType.ToUpper().Contains(vtype.ToUpper())).ToList();
            }

            ViewBag.Debitsum = ledgerlst.Sum(m => m.DebitAmt);
            ViewBag.CreditSum = ledgerlst.Sum(m => m.CreditAmt);
            ViewBag.BalanceSum = ledgerlst.Select(m => m.balance).Last();
            ViewBag.State = ledgerlst.Select(m => m.state).Last();


            ViewBag.OpState = ledgerlst.Select(m => m.state).First();


            ViewBag.RecordCount = ledgerlst.Count;
            if (ledgerlst.Count == 0)
            {
                ViewBag.EmptyMessage = "No Ledger Records!!";
            }

            return View("GetLedger", ledgerlst);
        }

        public ActionResult DayBook(string dt1 = "", string dt2 = "", string vtype = "")
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Daybook, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            

            //ViewBag.dt1 = dt1;
            //ViewBag.dt2 = dt2;
            try
            {
                DateTime Fd1 = Convert.ToDateTime(dt1);
                DateTime Fd2 = Convert.ToDateTime(dt2);

                ViewBag.dt1 = cu.GetDateFormat(Fd1) ;
                ViewBag.dt2 = cu.GetDateFormat(Fd2);
            }
            catch (Exception)
            {
            }
            Status st = new Status();

            int deptid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            DataTable dt = st.GetDayBook(dt1, dt2);
            List<Tb_Ledger> ledgerlst = new List<Tb_Ledger>();

            if (vtype == "")
            {
                vtype = "%";
            }

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Tb_Ledger lg = new Tb_Ledger();
                    try
                    {
                        lg.balance = double.Parse(dr["balance"].ToString());
                    }
                    catch (Exception ex)
                    {
                    }
                    lg.state = dr["state"].ToString();
                    lg.Vouchdate = DateTime.Parse(dr["Vouchdate"].ToString());
                    lg.VoucherType = dr["VoucherType"].ToString();
                    lg.VoucherNo = Convert.ToInt32(dr["VoucherNo"]);
                    lg.Narration = dr["Narration"].ToString();
                    try
                    {
                        lg.DebitAmt = Convert.ToDouble(dr["DebitAmt"]);
                    }
                    catch (Exception)
                    {

                    }

                    try
                    {
                        lg.CreditAmt = Convert.ToDouble(dr["CreditAmt"]);
                    }
                    catch (Exception)
                    {
                    }

                    ledgerlst.Add(lg);

                }
            }

            // apply filter with vtype
            if (vtype != "%")
            {
                ledgerlst = ledgerlst.Where(m => m.VoucherType.ToUpper().Contains(vtype.ToUpper())).ToList();
            }

            ViewBag.Debitsum = ledgerlst.Sum(m => m.DebitAmt);
            ViewBag.CreditSum = ledgerlst.Sum(m => m.CreditAmt);
            // ViewBag.BalanceSum = ledgerlst.Select(m => m.balance).Last();
            //ViewBag.State = ledgerlst.Select(m => m.state).Last();


            //ViewBag.OpState = ledgerlst.Select(m => m.state).First();


            //ViewBag.RecordCount = ledgerlst.Count;

            if (dt1 != "")
            {
                try
                {
                    DateTime fromdt = Convert.ToDateTime(dt1);
                    List<Tb_Ledger> lst = db.Tb_Ledger.Where(z => z.Vouchdate < fromdt && z.VoucherType !="CourseFees").ToList();
                    double? opbal = 0;
                    foreach (Tb_Ledger obj in lst)
                    {
                        if (obj.DebitAmt != null && obj.CreditAmt !=null)
                        {
                            opbal += obj.DebitAmt - obj.CreditAmt;
                        }
                    }
                    ViewBag.OpeningBalance = opbal;
                }
                catch (Exception ex)
                { 
                
                }
            }


            if (ledgerlst.Count == 0)
            {
                ViewBag.EmptyMessage = "No Ledger Records!!";
            }

            return View("DayBook", ledgerlst);
        }
    }
}
