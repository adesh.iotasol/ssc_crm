﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using System.Web.UI.WebControls;
using SSCMvc.Webutil;
using System.Configuration;
using System.Data.SqlClient;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class Tb_MockTestController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        CommonUtil cu = new CommonUtil();
        //
        // GET: /Tb_MockTest/

        public ActionResult Index(string sname = "", string fdt = "", string tdt = "", string roll = "", string category = "", string EnqStatus = "", string score = "", string id = "", string Course = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.MockTest, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            ViewBag.sname = sname;
            //ViewBag.fdt = fdt;
            //ViewBag.tdt = tdt;
            ViewBag.roll = roll;

            ViewBag.EnqStatus = EnqStatus;
            ViewBag.category = category;
            ViewBag.Course = Course;

            List<Tb_MockTestHelper> tmocktest = new List<Tb_MockTestHelper>();
            try
            {

                //if (id > 0)
                //{
                //    tmocktest = Getlist(id);
                //}

                if (sname != "" || (fdt != "" && tdt != "") || roll != "" || score != "" || category != "" || EnqStatus != "")
                {
                    tmocktest = Getlist(sname, fdt, tdt, roll, category, EnqStatus, score, id, Course);

                    //if (Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeSuperAdmin)
                    //{
                    int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
                    tmocktest = tmocktest.Where(z => z.Branchid == branchid).ToList();
                    //}

                    if (sname != "")
                    {
                        tmocktest = tmocktest.Where(z => z.Name.ToUpper().Contains(sname.ToUpper())).ToList();
                    }
                    if (fdt != "" && tdt != "")
                    {
                        try
                        {
                            DateTime dt1 = Convert.ToDateTime(fdt);
                            DateTime dt2 = Convert.ToDateTime(tdt);
                            ViewBag.fdt = dt1.ToString("MM/dd/yyyy");
                            ViewBag.tdt = dt2.ToString("MM/dd/yyyy");
                            tmocktest = tmocktest.Where(z => z.Mocktestdate >= dt1 && z.Mocktestdate <= dt2).ToList();
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    if (roll != "")
                    {
                        try
                        {
                            int bnd = Convert.ToInt32(roll);
                            tmocktest = tmocktest.Where(z => z.Rollno == bnd).ToList();
                        }
                        catch (Exception ex)
                        { }
                    }
                    ViewBag.ieltsscore = score;
                    if (score != "")
                    {
                        tmocktest = tmocktest.Where(z => z.Overallscore >= 5.5).Where(m => m.isfollowbtn == true).ToList();

                    }
                    if (category != "")
                    {
                        if (category == "All")
                        {
                            tmocktest = tmocktest.ToList();
                        }
                        else
                        {
                            tmocktest = tmocktest.Where(z => z.Categ == category).ToList();
                        }
                    }
                    if (EnqStatus != "")
                    {
                        tmocktest = tmocktest.Where(z => z.EnqStatus.ToUpper().Contains(EnqStatus.ToUpper())).ToList();
                    }

                    if (Course != "")
                    {
                        if (Course == "All")
                        {
                            tmocktest = tmocktest.ToList();
                        }
                        else
                        {
                            tmocktest = tmocktest.Where(a => a.CourseId == Course).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            { }

            return View(tmocktest.ToList());
        }

        public ActionResult FollowupHistory(int Id)
        {
            return View(db.MaEnqFollows.Where(z => z.FollowUpType == "MockTest" && z.AdmissionId == Id).OrderByDescending(z => z.Id).ToList());
        }

        public ActionResult _MockCreate(int id, int admId)
        {
            MaEnqFollow obj = new MaEnqFollow();
            obj.ReferId = id;
            obj.FollowUpType = "MockTest";
            obj.AdmissionId = admId;
            int uid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);

            ViewBag.Remarks = new SelectList(db.MaClosedReasons, "Remarks", "Remarks");
            return PartialView(obj);
        }

        public ActionResult _Index(int id)
        {
            return PartialView(db.MaEnqFollows.Where(z => z.FollowUpType == "MockTest" && z.AdmissionId == id).OrderByDescending(z => z.Id).ToList());
        }



        public ActionResult PrintMockReport(string sname = "", string fdt = "", string tdt = "", string roll = "", string category = "", string EnqStatus = "", string score = "", string id = "", string Course = "", string Format = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.MockTest, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            ViewBag.sname = sname;
            ViewBag.fdt = fdt;
            ViewBag.tdt = tdt;
            ViewBag.roll = roll;

            ViewBag.EnqStatus = EnqStatus;
            ViewBag.category = category;
            ViewBag.Course = Course;

            List<Tb_MockTestHelper> tmocktest = new List<Tb_MockTestHelper>();
            try
            {

                //if (id > 0)
                //{
                //    tmocktest = Getlist(id);
                //}

                if (sname != "" || (fdt != "" && tdt != "") || roll != "" || score != "" || category != "" || EnqStatus != "")
                {
                    //tmocktest = Getlist(id);
                    tmocktest = Getlist(sname, fdt, tdt, roll, category, EnqStatus, score, id, Course);
                    //if (Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeSuperAdmin)
                    //{
                    int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
                    tmocktest = tmocktest.Where(z => z.Branchid == branchid).ToList();
                    //}

                    if (sname != "")
                    {
                        tmocktest = tmocktest.Where(z => z.Name.ToUpper().Contains(sname.ToUpper())).ToList();
                    }
                    if (fdt != "" && tdt != "")
                    {
                        try
                        {
                            DateTime dt1 = Convert.ToDateTime(fdt);
                            DateTime dt2 = Convert.ToDateTime(tdt);
                            tmocktest = tmocktest.Where(z => z.Mocktestdate >= dt1 && z.Mocktestdate <= dt2).ToList();
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    if (roll != "")
                    {
                        try
                        {
                            int bnd = Convert.ToInt32(roll);
                            tmocktest = tmocktest.Where(z => z.Rollno == bnd).ToList();
                        }
                        catch (Exception ex)
                        { }
                    }
                    ViewBag.ieltsscore = score;
                    if (score != "")
                    {
                        tmocktest = tmocktest.Where(z => z.Overallscore >= 5.5).Where(m => m.isfollowbtn == true).ToList();

                    }
                    if (category != "")
                    {
                        if (category == "All")
                        {
                            tmocktest = tmocktest.ToList();
                        }
                        else
                        {
                            tmocktest = tmocktest.Where(z => z.Categ == category).ToList();
                        }
                    }
                    if (EnqStatus != "")
                    {
                        tmocktest = tmocktest.Where(z => z.EnqStatus.ToUpper().Contains(EnqStatus.ToUpper())).ToList();
                    }

                    if (Course != "")
                    {
                        if (Course == "All")
                        {
                            tmocktest = tmocktest.ToList();
                        }
                        else
                        {
                            tmocktest = tmocktest.Where(a => a.CourseId == Course).ToList();
                        }

                    }
                }
            }
            catch (Exception ex)
            { }
            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetMockTestList(tmocktest, Format);

            var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
        }

        public ActionResult _HighestQuali(int id)
        {
            int? enqid = db.MaAdmissions.Where(x => x.Id == id).Select(x => x.EnquiryId).FirstOrDefault();
            return PartialView(db.Tb_Highestqual.Where(z => z.EnquiryId == enqid).OrderByDescending(z => z.EnquiryId).ToList());
        }

        public List<Tb_MockTestHelper> Getlist(string sname = "", string fdt = "", string tdt = "", string roll = "", string category = "", string EnqStatus = "", string score = "", string id = "", string Course = "")
        {
            List<Tb_MockTestHelper> _lstmocktest = new List<Tb_MockTestHelper>();
            List<Tb_MockTest> _lst = new List<Tb_MockTest>();
            if (roll == "")
            {
                roll = "%";
            }
            if (sname == "")
            {
                sname = "%";
            }
            if (category == "" || category == "All")
            {
                category = "%";
            }
            if (EnqStatus == "")
            {
                EnqStatus = "%";
            }
            if (score == "")
            {
                score = "%";
            }
            if (id == "")
            {
                id = "%";
            }
            if (Course == "" || Course == "All")
            {
                Course = "%";
            }
            var vtest = new List<Tb_MockTest>();
            if (fdt != "" && tdt != "")
            {
                if (sname != "")
                {
                    sname = sname.ToUpper();
                }
                string sql = $"select m.Id,m.StudentId,m.TestDate,m.Category,m.CourseId,m.TimeId,m.LBand,m.WBand,m.RBand,m.SBand,m.Overall      ,m.CDate,m.BranchId,m.UserId,m.Status,ad.Afremarks as Remarks,m.FollowAsgnTo,m.isfollowbtn,ad.Afdate as fdate,ad.Anextdate as nextdate,ad.Afremarks as fremarks,ad.Afstatus as fstatus,ad.Afollowupby as followupby,m.Courselevel from tb_mocktest m inner join MaAdmission ad on ad.Id = m.studentid where Upper(CONCAT(ad.FirstName,' ',ad.LastName)) like '%{sname}%' and ad.RollNo like '{roll}' and m.Studentid like '{id}' and Convert(date,m.TestDate) between '{fdt}' and '{tdt}' and  m.Category like '{category}' and isnull(m.fstatus,'-') like '{EnqStatus}' and m.Overall like '{score}' and m.CourseId like '{Course}' order by m.Testdate desc";

                vtest = db.Database.SqlQuery<Tb_MockTest>(sql).ToList();
            }
            else
            {
                if (sname != "")
                {
                    sname = sname.ToUpper();
                }
                string sql = $"select m.Id,m.StudentId,m.TestDate,m.Category,m.CourseId,m.TimeId,m.LBand,m.WBand,m.RBand,m.SBand,m.Overall      ,m.CDate,m.BranchId,m.UserId,m.Status,ad.afremarks as Remarks,m.FollowAsgnTo,m.isfollowbtn,ad.Afdate as fdate,ad.Anextdate as nextdate,ad.Afremarks as fremarks,ad.Afstatus as fstatus,ad.Afollowupby as followupby,m.Courselevel from tb_mocktest m inner join MaAdmission ad on ad.Id = m.studentid where Upper(CONCAT(ad.FirstName,' ',ad.LastName)) like '%{sname}%' and ad.RollNo like '{roll}' and m.Studentid like '{id}' and  m.Category like '{category}' and isnull(m.fstatus,'-') like '{EnqStatus}' and m.Overall like '{score}' and m.CourseId like '{Course}' and m.fdate is not null order by m.Testdate desc";
                vtest = db.Database.SqlQuery<Tb_MockTest>(sql).ToList();
            }
            try
            {

                //var vtest = from p in db.Tb_MockTest
                //            where p.isfollowbtn == true
                //            group p by p.StudentId into grp
                //            select grp.OrderByDescending(g => g.TestDate).FirstOrDefault();
                //var mstest = db.Database.SqlQuery<Tb_MockTest>($"select m.Id,m.StudentId,m.TestDate,m.Category,m.CourseId,m.TimeId,m.LBand,m.WBand,m.RBand,m.SBand,m.Overall      ,m.CDate,m.BranchId,m.UserId,m.Status,m.Remarks,m.FollowAsgnTo,m.isfollowbtn,m.fdate,m.nextdate,m.fremarks,m.fstatus,m.followupby,m.Courselevel from tb_mocktest m innerjoin MaAdmission ad on ad.Id = m.studentid where CONCAT(ad.FirstName,' ',ad.LastName) like '{sname}' and ad.RollNo like '{roll}' and m.Studentid like '{id}' and Convert(date,m.TestDate) between '{fdt}' and '{tdt}' and  m.Category like '{category}' and m.fstatus like '{EnqStatus}' and m.Overall like '{score}' and m.CourseId like '{Course}' group by m.studentId order by m.Testdate ").ToList();




                //if (id != null && id > 0)
                //{
                //    vtest = from p in db.Tb_MockTest
                //            where p.StudentId == id
                //            group p by p.StudentId into grp 
                //            select grp.OrderByDescending(g => g.TestDate).First();

                //}


                int count = vtest.Count();
                foreach (var item in vtest)
                {

                    MaAdmission obj = db.MaAdmissions.Where(z => z.Id == item.StudentId).First();
                    Tb_MockTestHelper _objmocktest = new Tb_MockTestHelper();
                    _objmocktest.Rollno = (int)obj.RollNo;
                    _objmocktest.Name = obj.FirstName + " " + obj.LastName;
                    _objmocktest.Phoneno = obj.Mobile;
                    _objmocktest.Categ = item.Category;
                    _objmocktest.TimeId = item.TimeId;
                    _objmocktest.Followupby = item.followupby;
                    _objmocktest.IsLeft = obj.IsLeft == true ? "Left" : "";

                    try
                    {
                        _objmocktest.Currentcrselvl = obj.Tb_CourseHist.OrderByDescending(o => o.Id).FirstOrDefault().currentcourselevel;


                    }
                    catch (Exception) { }

                    try
                    {
                        //DateTime? maxtestdate = db.Tb_MockTest.Where(z => z.StudentId == obj.Id).Max(z => z.TestDate);
                        _objmocktest.Mocktestdate = item.TestDate;
                    }
                    catch (Exception ex)
                    { }

                    try
                    {
                        //double? overall = db.Tb_MockTest.Where(z => z.StudentId == obj.Id).Max(z => z.Overall);
                        _objmocktest.Overallscore = item.Overall;
                    }
                    catch (Exception ex)
                    { }
                    _objmocktest.StudentId = item.StudentId;
                    _objmocktest.FollowAsgnTo = item.FollowAsgnTo;

                    _objmocktest.Id = item.Id;
                    _objmocktest.isfollowbtn = item.isfollowbtn;
                    _objmocktest.Branchid = item.BranchId;
                    _objmocktest.CourseId = item.CourseId;
                    _objmocktest.UserId = item.UserId;

                    // remove this section for performance

                    //if (db.MaEnqFollows.Where(z => z.ReferId == item.Id && z.FollowUpType == "MockTest").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                    //{
                    //    if (item.isfollowbtn == true)
                    //    {
                    //        DateTime dt = (DateTime)db.MaEnqFollows.Where(z => z.ReferId == item.Id && z.FollowUpType == "MockTest").OrderByDescending(z => z.Id).Select(z => z.NextDate).First();
                    //        _objmocktest.NextfollwDate = dt;

                    //        _objmocktest.FollowRemarks = db.MaEnqFollows.Where(z => z.ReferId == item.Id && z.FollowUpType == "MockTest").OrderByDescending(z => z.Id).Select(z => z.Remarks).First();

                    //        int _countcurntCrse = db.Tb_CourseHist.Where(z => z.AdmissionId == item.StudentId).Count();
                    //        if (_countcurntCrse > 0)
                    //        {
                    //            //Tb_CourseHist _objcurrentcrselevl = db.Tb_CourseHist.Where(z => z.AdmissionId == item.StudentId).First();
                    //            _objmocktest.Currentcrselvl = obj.Tb_CourseHist.First().currentcourselevel;
                    //        }

                    //        _objmocktest.EnqStatus = db.MaEnqFollows.Where(z => z.ReferId == item.Id && z.FollowUpType == "MockTest").OrderByDescending(z => z.Id).Select(z => z.Enqstatus).First();
                    //    }

                    //}
                    try
                    {
                        _objmocktest.NextfollwDate = (DateTime)item.nextdate;
                    }
                    catch (Exception ex)
                    { }


                    _objmocktest.FollowRemarks = item.fremarks;
                    _objmocktest.EnqStatus = item.fstatus;

                    Tb_IELTSResult tb_ieltsresultobj = db.Tb_IELTSResult.Where(m => m.StudentId == item.StudentId && m.StudentType == "Admission").OrderByDescending(m => m.Id).FirstOrDefault();
                    if (tb_ieltsresultobj != null)
                    {
                        _objmocktest.IeltsTestDate = String.Format("{0:MMM-dd-yyyy}", tb_ieltsresultobj.TestDate);
                    }
                    //if (db.Tb_IELTSResult.Where(m => m.StudentId == item.StudentId).Where(m => m.StudentType == "Admission").Count() > 0)
                    //{
                    //    Tb_IELTSResult tb_ieltsresultobj = db.Tb_IELTSResult.Where(m => m.StudentId == item.StudentId).OrderByDescending(m => m.Id).First();
                    //    _objmocktest.IeltsTestDate = String.Format("{0:MMM-dd-yyyy}", tb_ieltsresultobj.TestDate);

                    //}

                    if (!_lstmocktest.Exists(m => m.Rollno == obj.RollNo))
                        _lstmocktest.Add(_objmocktest);


                }

            }
            catch (Exception ex)
            { }

            DateTime fromdate = Convert.ToDateTime("06/01/2015");
            DateTime todate = Convert.ToDateTime("06/30/2015");

            var abc = _lstmocktest.Where(a => a.Mocktestdate > fromdate && a.Mocktestdate < todate).ToList();


            return _lstmocktest;
        }

        // GET: /Tb_MockTest/Details/5

        //here is to get remarks method
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetRemarks(string id)
        {
            //int referid = Convert.ToInt32(id);
            //  string remarks = db.MaEnqFollows.Where(z => z.ReferId == referid && z.FollowUpType == "MockTest").OrderByDescending(z => z.Id).Select(z => z.Remarks).First();

            string remarks = id;
            if (remarks.Length > 0)
            {
                var colourData = new
                {
                    showremarks = remarks
                };
                return Json(colourData, JsonRequestBehavior.AllowGet);

            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AllTestofStudent(int id)
        {

            List<Tb_MockTest> lst = db.Tb_MockTest.Where(m => m.StudentId == id).OrderByDescending(o => o.CDate).ToList();
            return PartialView("_AllTestofStudent", lst);

        }


        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.MockTest, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            Tb_MockTest tb_mocktest = db.Tb_MockTest.Find(id);
            if (tb_mocktest == null)
            {
                return HttpNotFound();
            }
            return View(tb_mocktest);
        }

        /// <summary>
        /// show ielts result in mock test detail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult IeltsResult(int id = 0)
        {
            Tb_MockTest tb_mocktest = db.Tb_MockTest.Find(id);
            int studentid = (int)tb_mocktest.StudentId;

            int ieltsid = db.Tb_IELTSResult.Where(m => m.StudentId == studentid).Where(m => m.StudentType == "Admission").Count();
            Tb_IELTSResult tb_ieltsResult = new Tb_IELTSResult();


            if (ieltsid > 0)
            {
                ieltsid = db.Tb_IELTSResult.Where(m => m.StudentId == studentid).Where(m => m.StudentType == "Admission").Select(m => m.Id).First();
                tb_ieltsResult = db.Tb_IELTSResult.Find(ieltsid);

                return PartialView("_ieltsResults", tb_ieltsResult);
            }
            else
            {
                return null;
            }

        }

        public void GetProcedureResult(string procname, string param1, string param2, string param3, string param4)
        {
            string cs = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ToString();
            var connection = new SqlConnection(cs);
            SqlCommand command = new SqlCommand(procname, connection);
            command.Parameters.Add(new SqlParameter("@branchid", param1));
            command.Parameters.Add(new SqlParameter("@date1", param2));
            command.Parameters.Add(new SqlParameter("@date2", param3));
            command.Parameters.Add(new SqlParameter("@userid", param4));
            command.CommandTimeout = 2000;
            command.CommandType = CommandType.StoredProcedure;
            connection.Open();
            command.CommandType = CommandType.StoredProcedure;
            command.ExecuteNonQuery();
            connection.Close();

        }

        public ActionResult ImportScore(string date1 = "", string date2 = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            int usrid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            if (date1 != "" && date2 != "")
            {
                GetProcedureResult("ImportScore", branchid.ToString(), Convert.ToDateTime(date1).ToString("yyyyMMdd"), Convert.ToDateTime(date2).ToString("yyyyMMdd"), usrid.ToString());
            }

            return RedirectToAction("Index", new { @fdt = date1, @tdt = date2, @category = "All", @course = "All" });

        }

        //
        // GET: /Tb_MockTest/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.MockTest, UserAction.Add);
                string chbranch = Request.Cookies["chbranch"].Value;

                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "Id", "BatchTime");
            ViewBag.UserId = new SelectList(cu.GetUserList(branchid), "Id", "Fullname");
            ViewBag.StudentId = new SelectList(cu.GetMockTestStudents(branchid), "Id", "fullname");
            //ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname");
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName");

            ViewBag.FollowAsgnTo = new SelectList(cu.GetUserList(branchid), "Id", "username");

            return View();
        }

        //
        // POST: /Tb_MockTest/Create

        [HttpPost]
        public ActionResult Create(Tb_MockTest tb_mocktest)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.MockTest, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.ErrorMsg = "";
            int usrid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            int? Count = db.MaEnqFollows.Where(z => z.AdmissionId == tb_mocktest.StudentId).Count();

            if (ModelState.IsValid)
            {
                //tb_mocktest.CDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
                tb_mocktest.CDate = CommonUtil.IndianTime();
                tb_mocktest.UserId = usrid;
                tb_mocktest.BranchId = branchid;
                tb_mocktest.isfollowbtn = false;
                db.Tb_MockTest.Add(tb_mocktest);
                db.SaveChanges();
                /*--------if overall bands are greater than 5.5 then send an enquiry follow up by default so that it should come in enquiry follow up list------*/
                if (tb_mocktest.Overall >= 5.5)
                {
                    if (Count == 0)
                    {
                        MaEnqFollow enq = new MaEnqFollow();
                        enq.ReferId = tb_mocktest.Id;
                        enq.FollowUpType = "MockTest";
                        enq.Enqstatus = "Open";
                        enq.EnqFollowBy = usrid;
                        enq.NextDate = tb_mocktest.CDate.Value.AddDays(1).Date;
                        enq.Remarks = "New Entry on " + tb_mocktest.CDate;
                        enq.CDate = tb_mocktest.CDate;
                        enq.Entertype = "Admission";
                        enq.AdmissionId = tb_mocktest.StudentId;
                        db.MaEnqFollows.Add(enq);
                        db.SaveChanges();
                    }
                    //To Show Follow up button is Front of only one record
                    Tb_MockTest mock = new Tb_MockTest();
                    int mocktestid = 0;

                    try
                    {
                        mocktestid = db.Tb_MockTest.Where(m => m.StudentId == tb_mocktest.StudentId).Where(m => m.isfollowbtn == true).Select(a => a.Id).FirstOrDefault();
                    }
                    catch (Exception ex)
                    { }

                    try
                    {
                        mock = db.Tb_MockTest.Where(a => a.Id == mocktestid).FirstOrDefault();
                        mock.isfollowbtn = false;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    { }

                    int showbtncount = db.Tb_MockTest.Where(m => m.StudentId == tb_mocktest.StudentId).Where(m => m.isfollowbtn == true).Count();

                    if (showbtncount == 0)
                    {
                        Tb_MockTest objmocktest = db.Tb_MockTest.Find(tb_mocktest.Id);

                        objmocktest.isfollowbtn = true;

                        db.SaveChanges();
                    }
                }
                /*--------------------------------------------------------------------------------------------------------*/
                return RedirectToAction("Index");
            }

            ViewBag.StudentId = new SelectList(cu.GetMockTestStudents(branchid), "Id", "fullname");
            //ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname");
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_mocktest.BranchId);
            ViewBag.TimeId = new SelectList(db.MaTimes, "Id", "TimeIn", tb_mocktest.TimeId);
            ViewBag.UserId = new SelectList(cu.GetUserList(branchid), "Id", "Password", tb_mocktest.UserId);
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName");
            ViewBag.FollowAsgnTo = new SelectList(cu.GetUserList(branchid), "Id", "username", tb_mocktest.FollowAsgnTo);
            return View(tb_mocktest);
        }

        //
        // GET: /Tb_MockTest/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.MockTest, UserAction.Edit);
                string chbranch = Request.Cookies["chbranch"].Value;


                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            Tb_MockTest tb_mocktest = db.Tb_MockTest.Find(id);
            if (tb_mocktest == null)
            {
                return HttpNotFound();
            }

            var liststype1 = new List<ListItem>();
            liststype1.Add(new ListItem { Text = "Select", Value = "Select" });
            liststype1.Add(new ListItem { Text = "AC", Value = "AC" });
            liststype1.Add(new ListItem { Text = "GT", Value = "GT" });

            ViewBag.Category = new SelectList(liststype1, "Value", "Text", tb_mocktest.Category);

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_mocktest.BranchId);
            ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "Id", "BatchTime", tb_mocktest.TimeId);
            ViewBag.UserId = new SelectList(cu.GetUserList(branchid), "Id", "Password", tb_mocktest.UserId);
            ViewBag.StudentId = new SelectList(cu.GetMockTestStudents(branchid), "Id", "fullname", tb_mocktest.StudentId);
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName", tb_mocktest.CourseId);
            ViewBag.FollowAsgnTo = new SelectList(cu.GetUserList(branchid), "Id", "username", tb_mocktest.FollowAsgnTo);

            return View(tb_mocktest);
        }

        //
        // POST: /Tb_MockTest/Edit/5

        [HttpPost]
        public ActionResult Edit(Tb_MockTest tb_mocktest)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.MockTest, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

            try
            {


                int usrid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);

                db.Entry(tb_mocktest).State = System.Data.Entity.EntityState.Modified;
                tb_mocktest.CDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
                tb_mocktest.UserId = usrid;
                tb_mocktest.BranchId = branchid;
                db.SaveChanges();


                if (tb_mocktest.Overall >= 5.5)
                {
                    //To Show Follow up button is Front of only one record

                    int showbtncount = db.Tb_MockTest.Where(m => m.StudentId == tb_mocktest.StudentId).Where(m => m.isfollowbtn == true).Count();

                    if (showbtncount == 0)
                    {
                        Tb_MockTest objmocktest = db.Tb_MockTest.Find(tb_mocktest.Id);

                        objmocktest.isfollowbtn = true;

                        db.SaveChanges();
                    }

                }
                /*--------------------------------------------------------------------------------------------------------*/

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
            }


            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_mocktest.BranchId);
            ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "Id", "BatchTime", tb_mocktest.TimeId);
            ViewBag.UserId = new SelectList(cu.GetUserList(branchid), "Id", "Password", tb_mocktest.UserId);
            ViewBag.StudentId = new SelectList(cu.GetMockTestStudents(branchid), "Id", "fullname", tb_mocktest.StudentId);
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName", tb_mocktest.CourseId);
            ViewBag.FollowAsgnTo = new SelectList(cu.GetUserList(branchid), "Id", "username", tb_mocktest.FollowAsgnTo);
            return View(tb_mocktest);
        }

        //
        // GET: /Tb_MockTest/Delete/5
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.MockTest, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            try
            {
                Tb_MockTest tb_mocktest = db.Tb_MockTest.Find(id);

                if (db.MaEnqFollows.Where(z => z.ReferId == id && z.FollowUpType == "MockTest").Count() > 0)
                {
                    List<MaEnqFollow> lst = db.MaEnqFollows.Where(z => z.FollowUpType == "MockTest" && z.ReferId == id).ToList();
                    foreach (MaEnqFollow obj in lst)
                    {
                        db.MaEnqFollows.Remove(obj);
                        db.SaveChanges();
                    }
                }

                db.Tb_MockTest.Remove(tb_mocktest);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        //
        // POST: /Tb_MockTest/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Tb_MockTest tb_mocktest = db.Tb_MockTest.Find(id);
        //    db.Tb_MockTest.Remove(tb_mocktest);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        /// <summary>
        ///  get courses  by student
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private IList<Tb_CourseHist> GetCourse(int id)
        {


            return db.Tb_CourseHist.Where(m => m.AdmissionId == id).ToList();

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadCourseByStudent(string id)
        {
            var courselist = this.GetCourse(Convert.ToInt32(id));

            var colourData = courselist.Select(c => new SelectListItem()
            {
                Text = c.MaCourse.CourseName,
                Value = c.CourseId.ToString(),

            });

            return Json(colourData, JsonRequestBehavior.AllowGet);
        }

        private IList<Tb_CourseHist> GetTime(int id, int cid)
        {


            return db.Tb_CourseHist.Where(m => m.AdmissionId == id && m.CourseId == cid).ToList();

        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadTimeByCourse(string id, string cid)
        {
            var courselist = this.GetTime(Convert.ToInt32(id), Convert.ToInt32(cid));

            var colourData = courselist.Select(c => new SelectListItem()
            {
                Text = c.MaTime.TCode + " " + c.MaTime.TimeIn + " " + c.MaTime.Indesc + "-" + c.MaTime.TimeOut + " " + c.MaTime.OutDesc,
                Value = c.TimeId.ToString(),

            });

            return Json(colourData, JsonRequestBehavior.AllowGet);
        }
    }
}