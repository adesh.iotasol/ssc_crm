﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class Tb_FeeRefundController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /Tb_FeeRefund/

        public ActionResult Index()
        {


            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeRefund, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            var tb_feerefund = db.Tb_FeeRefund.Include(t => t.MaAdmission).Include(t => t.MaCourse).Include(t => t.MaPackage).Where(z => z.BranchId == branchid);
            ViewBag.TotalCount = tb_feerefund.Count();
            return View(tb_feerefund.ToList());
        }

        //
        // GET: /Tb_FeeRefund/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeRefund, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            Tb_FeeRefund tb_feerefund = db.Tb_FeeRefund.Find(id);
            if (tb_feerefund == null)
            {
                return HttpNotFound();
            }
            return View(tb_feerefund);
        }

        //
        // GET: /Tb_FeeRefund/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeRefund, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.StudentId = new SelectList(db.AdmissionViews.ToList(), "Id", "fullname");
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName");
            ViewBag.PackageId = new SelectList(db.MaPackages.Where(z => z.BranchId == branchid).ToList(), "Id", "PackageName");
            return View();
        }

        //
        // POST: /Tb_FeeRefund/Create

        [HttpPost]
        public ActionResult Create(Tb_FeeRefund tb_feerefund)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeRefund, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            tb_feerefund.BranchId = branchid;
            tb_feerefund.Edate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
            int? srno = db.Tb_FeeRefund.Select(z => z.RefundNo).Max();
            if (srno == null || srno == 0)
            {
                srno = 1;
            }
            else
            {
                srno += 1;
            }

            tb_feerefund.RefundNo = srno;
            try
            {
                db.Tb_FeeRefund.Add(tb_feerefund);
                db.SaveChanges();

                ///Saves the value in ledger and debits the amount
                int id = tb_feerefund.Id;
                int sturoll =(int) db.MaAdmissions.Where(z => z.Id == tb_feerefund.StudentId).Select(z => z.RollNo).First();
                CommonUtil.InsertToLedger(id, tb_feerefund.RefundAmount, 0, String.Format("Narration={0} ", "Fee Refund"), sturoll, DateTime.Now.AddMinutes(StaticLogic.TimeDifference), CommonUtil.FeeRefundType);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
            }

            ViewBag.StudentId = new SelectList(db.AdmissionViews.ToList(), "Id", "fullname", tb_feerefund.StudentId);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_feerefund.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName", tb_feerefund.CourseId);
            ViewBag.PackageId = new SelectList(db.MaPackages.Where(z => z.BranchId == branchid).ToList(), "Id", "PackageName", tb_feerefund.PackageId);
            return View(tb_feerefund);
        }

        //
        // GET: /Tb_FeeRefund/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeRefund, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            Tb_FeeRefund tb_feerefund = db.Tb_FeeRefund.Find(id);
            if (tb_feerefund == null)
            {
                return HttpNotFound();
            }
            ViewBag.StudentId = new SelectList(db.AdmissionViews.ToList(), "Id", "fullname", tb_feerefund.StudentId);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_feerefund.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName");
            ViewBag.PackageId = new SelectList(db.MaPackages.ToList(), "Id", "PackageName");
            return View(tb_feerefund);
        }

        //
        // POST: /Tb_FeeRefund/Edit/5

        [HttpPost]
        public ActionResult Edit(Tb_FeeRefund tb_feerefund)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeRefund, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            
            try
            {
                tb_feerefund.BranchId = branchid;
                db.Entry(tb_feerefund).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                 ///Saves the value in ledger and debits the amount
                 int sturoll = (int)db.MaAdmissions.Where(z => z.Id == tb_feerefund.StudentId).Select(z => z.RollNo).First();
                 CommonUtil.UpdateToLedger(tb_feerefund.Id, tb_feerefund.RefundAmount, 0.0, string.Format("Narration={0} ", "Fee Refund"), DateTime.Now.AddMinutes(StaticLogic.TimeDifference), sturoll, CommonUtil.FeeRefundType);
                return RedirectToAction("Index");
                
            }
            catch (Exception ex)
            {
            }
            ViewBag.StudentId = new SelectList(db.AdmissionViews.ToList(), "Id", "fullname", tb_feerefund.StudentId);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_feerefund.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName", tb_feerefund.CourseId);
            ViewBag.PackageId = new SelectList(db.MaPackages.ToList(), "Id", "PackageName", tb_feerefund.PackageId);
            return View(tb_feerefund);
        }

        //
        // GET: /Tb_FeeRefund/Delete/5

        public ActionResult Delete(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeRefund, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            Tb_FeeRefund tb_feerefund = db.Tb_FeeRefund.Find(id);
            if (tb_feerefund == null)
            {
                return HttpNotFound();
            }
            return View(tb_feerefund);
        }

        //
        // POST: /Tb_FeeRefund/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeRefund, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            Tb_FeeRefund tb_feerefund = db.Tb_FeeRefund.Find(id);
            db.Tb_FeeRefund.Remove(tb_feerefund);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }



        //--------------------json methodds
        /// <summary>
        ///  get courses  by student
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private IList<Tb_CourseHist> GetCourse(int id)
        {


            return db.Tb_CourseHist.Where(m => m.AdmissionId == id && m.IsActive==true).ToList();

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadPackageByCourse(string id)
        {
            var courselist = this.GetCourse(Convert.ToInt32(id));

            var colourData = courselist.Select(c => new SelectListItem()
            {
                Text = c.MaCourse.CourseName,
                Value = c.CourseId.ToString(),

            });

            return Json(colourData, JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        ///  get packages by course
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private IList<MaPackage> GetPackages(int id,int stuid)
        {
            int packid = (int)db.Tb_CourseHist.Where(z => z.CourseId == id && z.AdmissionId == stuid).Select(z => z.PackageId).First();
            return db.MaPackages.Where(m => m.Id == packid).ToList();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadPackageBypackages(string id, string stuid)
        {

            var package = this.GetPackages(Convert.ToInt32(id),Convert.ToInt32(stuid));

            var colourData = package.Select(c => new SelectListItem()
            {
                Text = c.PackageName,
                Value = c.Id.ToString(),

            });




            return Json(colourData, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Search(string rollno="", string sname="", string scourse="")
        {
            List<Tb_FeeRefund> lstFeeRefund = db.Tb_FeeRefund.ToList();
            if (rollno != "")
            {
                try
                {
                    int roll = Convert.ToInt32(rollno);
                    lstFeeRefund = lstFeeRefund.Where(z => z.MaAdmission.RollNo == roll).ToList();
                    ViewBag.roll = roll;
                }
                catch (Exception ex)
                {
                }
            }
            if (sname != "")
            {
                ViewBag.sname = sname;
                sname = sname.ToUpper();
                lstFeeRefund = lstFeeRefund.Where(z => z.MaAdmission.FirstName.ToUpper().Contains(sname)).ToList();

            }
            if (scourse != "")
            {
                ViewBag.scourse = scourse;
                scourse = scourse.ToUpper();
                lstFeeRefund = lstFeeRefund.Where(z => z.MaCourse.CourseName.ToUpper().Contains(scourse)).ToList();
            }
            ViewBag.TotalCount = lstFeeRefund.Count;
            return View(lstFeeRefund);
        }

    }
}