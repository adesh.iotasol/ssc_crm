﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class MaReferenceController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /MaReference/

        public ActionResult Index()
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Reference, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            var reference = db.MaFrmReferences.Where(z => z.BranchId == branchid).OrderByDescending(z => z.Id);
            ViewBag.TotalCount = reference.Count();
            return View(reference.ToList());

        }

        //
        // GET: /MaReference/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Reference, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            MaFrmReference mafrmreference = db.MaFrmReferences.Find(id);
            if (mafrmreference == null)
            {
                return HttpNotFound();
            }
            return View(mafrmreference);
        }

        //
        // GET: /MaReference/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Reference, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            return View();
        }

        //
        // POST: /MaReference/Create

        [HttpPost]
        public ActionResult Create(MaFrmReference mafrmreference)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Reference, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            if (ModelState.IsValid)
            {
                mafrmreference.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                mafrmreference.BranchId = branchid;
                mafrmreference.Status = true;
                db.MaFrmReferences.Add(mafrmreference);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mafrmreference);
        }

        //
        // GET: /MaReference/Edit/5

        public ActionResult Edit(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Reference, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            MaFrmReference mafrmreference = db.MaFrmReferences.Find(id);
            if (mafrmreference == null)
            {
                return HttpNotFound();
            }
            return View(mafrmreference);
        }

        //
        // POST: /MaReference/Edit/5

        [HttpPost]
        public ActionResult Edit(MaFrmReference mafrmreference)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Reference, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
                mafrmreference.Status = true;
                db.Entry(mafrmreference).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mafrmreference);
        }

        //
        // GET: /MaReference/Delete/5

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Delete(int id)
        {


            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Reference, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            try
            {
                MaFrmReference marefer = db.MaFrmReferences.Find(id);

                db.MaFrmReferences.Remove(marefer);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        public ActionResult Search(string company = "", string mobile = "")
        {
            List<MaFrmReference> lst = db.MaFrmReferences.ToList();

            ViewBag.company = company;
            ViewBag.mobile = mobile;

            if (company != "")
            {
                company = company.ToUpper();
                lst = lst.Where(z => z.CompName.ToUpper().Contains(company)).ToList();
            }
            if (mobile != "")
            {
                lst = lst.Where(z => z.MobileNo == mobile).ToList();
            }

            ViewBag.TotalCount = lst.Count;
            return View(lst);
        }
    }
}