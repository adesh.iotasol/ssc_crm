﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            ViewBag.Active = "Active";


            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult TodayWork(string date1,string date2)
        {
            Models.CommonUtil util = new CommonUtil();
            if(date1=="" && date2=="")
            {
                date1 = DateTime.Now.ToString("yyyyMMdd");
                date2 = DateTime.Now.ToString("yyyyMMdd");

            }
            ViewBag.date1 = date1;
            ViewBag.date2 = date2;
            string sql = $"select followuptype as wtype,count(1) as count from MaEnqFollow where cdate between '{date1}' and '{date2}' and FollowUpType is not null group by FollowUpType";
            List<Todayworks> tw = db.Database.SqlQuery<Todayworks>(sql).ToList();
            return View(tw);

        }

    }
}
