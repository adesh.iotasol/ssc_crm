﻿using SSCMvc.Models;
using SSCMvc.Webutil;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSCMvc.Controllers
{
    public class PreferenceController : Controller
    {
        // GET: Preference
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        public ActionResult Index(string msg="")
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Preference, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (msg == "Updt")
            {
                ViewBag.message = "Record Updated Successfully";
            }
            if (msg == "sve")
            {
                ViewBag.message = "Record Sdded Successfully";
            }
            if (msg == "dup")
            {
                ViewBag.error = "Record With Same name already exists";
            }
            List<Tb_Prefrence> pref = db.Tb_Prefrence.ToList();
            return View(pref);
        }

        public ActionResult Edit(int Id)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Preference, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            Tb_Prefrence tb_pref = db.Tb_Prefrence.Find(Id);
            return View(tb_pref);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tb_Prefrence Prefer)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Preference, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
              
                db.Entry(Prefer).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException is SqlException)
                    {
                        SqlException sqlex = (SqlException)ex.InnerException.InnerException;
                        if (sqlex.Number == 2601)
                        {
                            return RedirectToAction("Index", new { @msg = "dup" });
                        }
                    }
                }
                return RedirectToAction("Index", new { @msg = "Updt" });
            }
            return View(Prefer);
        }

        #region Update from Techer Module

        public ActionResult UpdateMaadmission(int Id)
        {
            MaAdmission Ma_Add = db.MaAdmissions.Find(Id);
            Ma_Add.IsSync = true;
            db.SaveChanges();
            return Content("OK");
        }

        public ActionResult UpdateCourseHistory(int Id)
        {
            Tb_CourseHist course = db.Tb_CourseHist.Find(Id);
            course.IsSync = true;
            db.SaveChanges();
            return Content("OK");
        }
        #endregion
    }
}