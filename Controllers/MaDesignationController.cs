﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class MaDesignationController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /MaDesignation/

        public ActionResult Index()
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Designation, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            ViewBag.TotalCount = db.MaDesignations.Count();
            return View(db.MaDesignations.OrderByDescending(z => z.Id).ToList());
        }

        //
        // GET: /MaDesignation/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Designation, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            MaDesignation madesignation = db.MaDesignations.Find(id);
            if (madesignation == null)
            {
                return HttpNotFound();
            }
            return View(madesignation);
        }

        //
        // GET: /MaDesignation/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Designation, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            return View();
        }

        //
        // POST: /MaDesignation/Create

        [HttpPost]
        public ActionResult Create(MaDesignation madesignation)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Designation, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
                madesignation.Status = true;
                madesignation.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                db.MaDesignations.Add(madesignation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(madesignation);
        }

        //
        // GET: /MaDesignation/Edit/5

        public ActionResult Edit(int id)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Designation, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            MaDesignation madesignation = db.MaDesignations.Find(id);


            return View(madesignation);
        }

        //
        // POST: /MaDesignation/Edit/5

        [HttpPost]
        public ActionResult Edit(MaDesignation madesignation)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Designation, UserAction.Edit);
                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
                madesignation.Status = true;
                db.Entry(madesignation).State = System.Data.Entity.EntityState.Modified;
                //madesignation.Cdate = DateTime.Now.Date;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(madesignation);
        }


        //GET: /MaDesignation/Delete/5

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Delete(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Designation, UserAction.Delete);
                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            try
            {
                MaDesignation mabdesig = db.MaDesignations.Find(id);
                db.MaDesignations.Remove(mabdesig);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }




        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}