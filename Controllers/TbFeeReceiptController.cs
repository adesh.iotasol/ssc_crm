﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;
using SSCMvc.Webutil;
using Microsoft.Reporting.WebForms;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class TbFeeReceiptController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        SSCWebDBEntities1 sscdb = new SSCWebDBEntities1();
        CommonUtil cu = new CommonUtil();
        //
        // GET: /TbFeeReceipt/

        public ActionResult Index(string sve = "")
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeReceipt, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            //var tb_feereceipt = db.Tb_FeeReceipt.Include(t => t.MaAdmission).Include(t => t.MaBranch).Include(t => t.MaCourse).Include(t => t.MaPackage).Include(t => t.Tb_Installment);

            DateTime dt = DateTime.Now.AddDays(-2);
            var tb_feereceipt = db.Tb_FeeReceipt.Where(z => z.BranchId == branchid).Where(m => m.CDate > dt).OrderByDescending(z => z.ReceiptNo).ToList();

            // List<Tb_FeeReceipt> tb_feereceipt = new List<Tb_FeeReceipt>();
            ViewBag.TotalCount = tb_feereceipt.Count;
            return View(tb_feereceipt.ToList());  //Code done on 25 june to show no record when page load
        }

        public ActionResult DailyRecptPermission()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GenSlip(int sid)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.GSTslip, UserAction.View);

                string chbranch = Request.Cookies["chbranch"].Value;
                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            MaBranch bdetail = db.MaBranches.Find(branchid);
            string dt1 = bdetail.Startdate.Value.ToShortDateString();
            int? no = db.Database.SqlQuery<int>($"select isnull(max(fsrno),0)+1 from tb_feereceipt where branchid={branchid} and feedate>='{dt1}'").FirstOrDefault();
            Tb_FeeReceipt obj = db.Tb_FeeReceipt.Find(sid);
            obj.Fsrno = no;
            db.SaveChanges();
            return RedirectToAction("Index", new { @sve = "genslip" });
        }

        [HttpPost]
        public ActionResult DailyRecptPermission(User model, string password)
        {
            string username = User.Identity.Name;

            int count = (from p in db.Users where p.username == username && p.Password == password && p.Status == true select p).Count();

            if (count == 1)
            {

                FormsAuthentication.SetAuthCookie(username, false);
                //get user type
                int uid = db.Users.Where(z => z.username == username && z.Password == password).Select(z => z.Id).First();
                //int uid = db.Users.Where(z => z.Password == password).Select(z => z.Id).First();


                if (uid > 0)
                {

                    return Redirect("DailyFeeReceipt");
                }
            }
            ViewBag.message = "Password is incorrect";
            // RedirectToAction("DailyFeeReceipt", new { @msg = "" });
            return View();
        }


        //this is for admin only to see all receipts even with transfer students
        public ActionResult AllReceipts()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AllReceipt, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            var tb_feereceipt = db.Tb_FeeReceipt.OrderByDescending(z => z.ReceiptNo).ToList();
            ViewBag.TotalCount = tb_feereceipt.Count;
            ViewBag.BranchName = new SelectList(db.MaBranches.Where(z => z.Status == true), "Id", "BramchName");
            return View(tb_feereceipt.ToList());
        }



        //
        // GET: /TbFeeReceipt/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeReceipt, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            Tb_FeeReceipt tb_feereceipt = db.Tb_FeeReceipt.Find(id);
            if (tb_feereceipt == null)
            {
                return HttpNotFound();
            }
            return View(tb_feereceipt);
        }

        //
        // GET: /TbFeeReceipt/Create

        public ActionResult Create(int admid = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeReceipt, UserAction.Add);
                string chbranch = Request.Cookies["chbranch"].Value;
                
                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            int deptid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.DiscountId = new SelectList(db.MaDiscounts, "Id", "DiscountPercen");
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.PayMode = new SelectList(db.Ma_PayMode.ToList(), "PaymentMode", "PaymentMode");
            ViewBag.PayMode1 = new SelectList(db.Ma_PayMode.ToList(), "PaymentMode", "PaymentMode");
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == deptid).ToList(), "Id", "CourseName");
            ViewBag.PackageId = new SelectList(db.MaPackages.Where(z => z.BranchId == deptid), "Id", "PackageName");
            ViewBag.InstallmentNoId = new SelectList(db.Tb_Installment.Where(z => z.BranchId == deptid), "Id", "InstallmentNo");
            int userid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            ViewBag.Counselor = db.Users.Where(z => z.Id == userid && z.Status == true && z.BranchId == deptid).Select(z => z.Fullname).First();
            ViewBag.Date = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
            Tb_FeeReceipt objReceipt = new Tb_FeeReceipt();
            if (admid == 0)
            {
                ViewBag.StudentId = new SelectList(cu.GetStudents(deptid), "Id", "fullname");
                objReceipt.CounselorId = userid;
                return View(objReceipt);
            }
            else
            {
                MaAdmission obj = db.MaAdmissions.Where(z => z.Id == admid && z.IsLeft != true).First();
                ViewBag.StudentId = new SelectList(cu.GetStudents(deptid), "Id", "fullname", obj.Id);

                objReceipt.StudentId = obj.Id;
                return View(objReceipt);
            }
        }

        // POST: /TbFeeReceipt/Create
        //Returns The Courseid 

        //Actually in FeeReciept it should not be Courseid it should have been Coursehistid but the ssaving grace is that we are taking installment id  which is linked to coursehistid
        private int GetCourseId(int id)
        {
            int courseid = Convert.ToInt32(db.Tb_CourseHist.Where(z => z.Id == id).Select(z => z.CourseId).First());

            return courseid;
        }


        //[HttpPost]
        //public ActionResult Create(Tb_FeeReceipt tb_feereceipt, FormCollection frm)
        //{

        //    try
        //    {
        //        int roleid = StaticLogic.RoleId();
        //        bool Prmsn = false;
        //        Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeReceipt, UserAction.Add);

        //        if (Prmsn == false)
        //        {
        //            return View("~/Views/Shared/AccessDenied.cshtml");
        //        }

        //    }
        //    catch (Exception)
        //    {
        //    }


        //    int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
        //    tb_feereceipt.FeeDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);

        //    //-------------------------------Due date Updation------------------------
        //    int admid = Convert.ToInt32(tb_feereceipt.StudentId);

        //    //==========Commented on 3 july -2013 =======================
        //    //  int courseid = Convert.ToInt32(db.Tb_CourseHist.Where(z => z.Id == tb_feereceipt.CourseId).Select(z => z.CourseId).First());
        //    // tb_feereceipt.CourseId = courseid;
        //    //==============================================================
        //    int courseid = 0;
        //    //its the coursehistory id
        //    int coursehistid = (int)tb_feereceipt.CourseId;
        //    try
        //    {
        //        string dudate = "";
        //        try
        //        {
        //            dudate = frm["hdnFinalduedate"].ToString();
        //        }
        //        catch (Exception ex)
        //        { }

        //        //Tb_CourseHist obj = db.Tb_CourseHist.Where(z => z.AdmissionId == admid && z.CourseId == courseid).First();

        //        //------------------Insert Entry in fee receipt table
        //        # region fee_receiptEntry
        //        try
        //        {

        //            if (tb_feereceipt.Discount == null)
        //            {
        //                tb_feereceipt.Discount = 0.0;
        //            }
        //            if (tb_feereceipt.BalanceAmount == null)
        //            {
        //                tb_feereceipt.BalanceAmount = 0.0;
        //            }
        //            if (tb_feereceipt.PaidAmount == null)
        //            {
        //                tb_feereceipt.PaidAmount = 0.0;
        //            }
        //            tb_feereceipt.BranchId = branchid;
        //            tb_feereceipt.CDate = tb_feereceipt.FeeDate;
        //            int? maxrecno = db.Tb_FeeReceipt.Select(z => z.ReceiptNo).Max();

        //            DateTime startdate = Convert.ToDateTime(db.MaBranches.Where(z => z.Id == branchid).Select(z => z.Startdate).FirstOrDefault());
        //            SqlConnection con = new SqlConnection();
        //            con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
        //            SqlCommand cmd = new SqlCommand();
        //            cmd.CommandText = "select  max(LocSerialNo) from Tb_FeeReceipt where feedate>='" + startdate.Date.ToString("yyyy-MM-dd") + "' and branchid=" + branchid;
        //            cmd.Connection = con;
        //            con.Open();
        //            int maxlocal = 0;
        //            // int? maxlocal = db.Tb_FeeReceipt.Where(z => z.BranchId == branchid).Select(z => z.LocSerialNo).Max();

        //            try
        //            {
        //                maxlocal = Convert.ToInt32(cmd.ExecuteScalar());
        //                con.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                ViewBag.error = "error";
        //                cu.SendError(ex);
        //                return View(tb_feereceipt);
        //            }

        //            if (maxrecno == null || maxrecno == 0)
        //            {
        //                maxrecno = 1;
        //            }
        //            else
        //            {
        //                maxrecno = maxrecno + 1;
        //            }


        //            if (maxlocal == null || maxlocal == 0)
        //            {
        //                maxlocal = 1;
        //            }
        //            else
        //            {
        //                maxlocal = maxlocal + 1;
        //            }

        //            //===================Added 3 july 2013==================

        //            //======================================================
        //            tb_feereceipt.CourseId = courseid;
        //            tb_feereceipt.LocSerialNo = maxlocal;

        //            tb_feereceipt.ReceiptNo = maxrecno;
        //            try
        //            {
        //                tb_feereceipt.DueDate = Convert.ToDateTime(dudate);
        //            }
        //            catch (Exception ex)
        //            {
        //                ViewBag.error = "error";
        //                cu.SendError(ex);
        //                return View(tb_feereceipt);
        //            }


        //            string usrtype = Request.Cookies[CommonUtil.CookieUsertype].Value;
        //            if (usrtype == CommonUtil.UserTypeAdmin || usrtype == CommonUtil.UserTypeSuperAdmin)
        //            {
        //                tb_feereceipt.FeeDate = Convert.ToDateTime(frm["FeeDate"]);
        //            }
        //            else
        //            {
        //                tb_feereceipt.FeeDate = Convert.ToDateTime(frm["hdnfeedate"]);
        //            }

        //            tb_feereceipt.CounselorId = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
        //            tb_feereceipt.RTime = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).ToShortTimeString();



        //            DateTime? dtjoin = db.Tb_CourseHist.Where(z => z.AdmissionId == tb_feereceipt.StudentId && z.CourseId == tb_feereceipt.CourseId && z.PackageId == tb_feereceipt.PackageId && z.IsActive == true).Select(z => z.FromDate).First();
        //            tb_feereceipt.PackageDate = dtjoin;

        //            tb_feereceipt.PayMode2 = "Cash";
        //            db.Tb_FeeReceipt.Add(tb_feereceipt);
        //            db.SaveChanges();
        //            /*-------------------Updates due dates in Tb_installment table-----------------------------*/
        //            Tb_CourseHist obj = db.Tb_CourseHist.Where(z => z.AdmissionId == admid && z.Id == coursehistid).First();
        //            obj.DueDate = Convert.ToDateTime(dudate);
        //            db.SaveChanges();

        //            Tb_Installment objinstl = db.Tb_Installment.Where(z => z.Id == tb_feereceipt.InstallmentNoId).First();
        //            objinstl.DueDate = tb_feereceipt.DueDate;
        //            db.SaveChanges();

        //        }
        //        catch (Exception ex)
        //        {
        //            ViewBag.error = "error";
        //            cu.SendError(ex);
        //            return View(tb_feereceipt);
        //        }
        //        #endregion

        //        #region Ledger_Insert
        //        //---------------------insert record into ledger start here--------------------

        //        // int packageid = (int)tb_feereceipt.PackageId;
        //        // double balanceamount = (double)db.MaPackages.Where(z => z.Id == packageid).Select(z => z.Amount).First();

        //        int studentid = (int)tb_feereceipt.StudentId;
        //        int rollno = (int)db.MaAdmissions.Where(z => z.Id == studentid).Select(z => z.RollNo).First();

        //        CommonUtil.InsertToLedger(tb_feereceipt.Id, 0, tb_feereceipt.PaidAmount, String.Format("{0}", "Receipt No" + tb_feereceipt.ReceiptNo + ", Roll No" + rollno), rollno, DateTime.Now.AddMinutes(StaticLogic.TimeDifference), "FeeReceipt");

        //        //---------------------insert record into ledger end here-----------------------

        //        //in which if installment is paid full then isfeepaid is true and if one rupees balance is due then it will remain false.

        //        int installid = (int)tb_feereceipt.InstallmentNoId;//get the installment id .

        //        double amount = (double)tb_feereceipt.BalanceAmount;//get the balance amount.
        //        Tb_Installment ins = db.Tb_Installment.Find(installid);//
        //        ins.BalanceDueAmount = amount;
        //        db.SaveChanges();
        //        if (tb_feereceipt.BalanceAmount == 0)
        //        {
        //            ins.IsFeesPaid = true;

        //            //tb_feereceipt.DueDate = DateTime.Now.AddMonths(1);         
        //        }
        //        else
        //        {
        //            //tb_feereceipt.DueDate = DateTime.Now.AddDays(1);
        //            ins.IsFeesPaid = false;

        //        }

        //        db.SaveChanges();
        //        #endregion
        //        courseid = GetCourseId(coursehistid);


        //        int count = db.Tb_FeeReceipt.Where(z => z.StudentId == tb_feereceipt.StudentId).Count();
        //        if (count == 1)
        //        {
        //            // cu.SendEmailToReferene(tb_feereceipt); //inserts fee to reference on first time payment
        //        }
        //        return RedirectToAction("AftRectCreMess/" + tb_feereceipt.Id);
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.error = "error";
        //        cu.SendError(ex);
        //        return View(tb_feereceipt);

        //    }
        //    ViewBag.DiscountId = new SelectList(db.MaDiscounts.Where(z => z.BranchId == branchid).ToList(), "Id", "DiscountPercen", tb_feereceipt.DiscountId);
        //    ViewBag.PayMode = new SelectList(db.Ma_PayMode.ToList(), "PaymentMode", "PaymentMode", tb_feereceipt.PayMode);
        //    ViewBag.PayMode1 = new SelectList(db.Ma_PayMode.ToList(), "PaymentMode", "PaymentMode", tb_feereceipt.PayMode1);
        //    ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_feereceipt.StudentId);
        //    ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_feereceipt.BranchId);
        //    ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName", courseid);
        //    ViewBag.PackageId = new SelectList(db.MaPackages.ToList(), "Id", "PackageName", tb_feereceipt.PackageId);
        //    ViewBag.InstallmentNoId = new SelectList(db.Tb_Installment.Where(z => z.BranchId == branchid).ToList(), "Id", "InstallmentNo", tb_feereceipt.InstallmentNoId);
        //    ViewBag.Counselor = db.Users.Where(z => z.Id == tb_feereceipt.CounselorId && z.Status == true && z.BranchId == branchid).Select(z => z.Fullname).First();

        //    return View(tb_feereceipt);
        //}

        //
        // GET: /TbFeeReceipt/Edit/5

        [HttpPost]
        public ActionResult Create(Tb_FeeReceipt tb_feereceipt, FormCollection frm)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeReceipt, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = "error";
                cu.SendError(ex);
            }
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            tb_feereceipt.FeeDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);

            //-------------------------------Due date Updation------------------------
            int admid = Convert.ToInt32(tb_feereceipt.StudentId);

            //==========Commented on 3 july -2013 =======================
            //  int courseid = Convert.ToInt32(db.Tb_CourseHist.Where(z => z.Id == tb_feereceipt.CourseId).Select(z => z.CourseId).First());
            // tb_feereceipt.CourseId = courseid;
            //==============================================================
            int courseid = 0;
            //its the coursehistory id
            int coursehistid = (int)tb_feereceipt.CourseId;
            try
            {
                string dudate = "";
                try
                {
                    dudate = frm["hdnFinalduedate"].ToString();
                }
                catch (Exception ex)
                {
                    ViewBag.error = "error";
                    cu.SendError(ex);
                }
                //Tb_CourseHist obj = db.Tb_CourseHist.Where(z => z.AdmissionId == admid && z.CourseId == courseid).First();

                //------------------Insert Entry in fee receipt table
                #region fee_receiptEntry
                try
                {
                    if (tb_feereceipt.Discount == null)
                    {
                        tb_feereceipt.Discount = 0.0;
                    }
                    if (tb_feereceipt.BalanceAmount == null)
                    {
                        tb_feereceipt.BalanceAmount = 0.0;
                    }
                    if (tb_feereceipt.PaidAmount == null)
                    {
                        tb_feereceipt.PaidAmount = 0.0;
                    }
                    tb_feereceipt.BranchId = branchid;
                    tb_feereceipt.CDate = tb_feereceipt.FeeDate;
                    int? maxrecno = db.Tb_FeeReceipt.Select(z => z.ReceiptNo).Max();
                    DateTime startdate = Convert.ToDateTime(db.MaBranches.Where(z => z.Id == branchid).Select(z => z.Startdate).FirstOrDefault());
                    SqlConnection con = new SqlConnection();
                    con.ConnectionString = ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "select  isnull(max(LocSerialNo),0) from Tb_FeeReceipt where feedate>='" + startdate.Date.ToString("yyyy-MM-dd") + "' and branchid=" + branchid;
                    cmd.Connection = con;
                    con.Open();
                    int? maxlocal = 0;
                    // int? maxlocal = db.Tb_FeeReceipt.Where(z => z.BranchId == branchid).Select(z => z.LocSerialNo).Max();
                    try
                    {
                        maxlocal = Convert.ToInt32(cmd.ExecuteScalar());
                        con.Close();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        ViewBag.error = "error";
                        var errorMessages = ex.EntityValidationErrors
                                .SelectMany(x => x.ValidationErrors)
                                .Select(x => x.ErrorMessage);
                        var fullErrorMessage = string.Join("; ", errorMessages);
                        var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                        cu.SendError(new Exception(exceptionMessage));
                        ViewBag.mesg = cu.SessionError;
                    }
                    if (maxrecno == null || maxrecno == 0)
                    {
                        maxrecno = 1;
                    }
                    else
                    {
                        maxrecno = maxrecno + 1;
                    }
                    if (maxlocal == null || maxlocal == 0)
                    {
                        maxlocal = 1;
                    }
                    else
                    {
                        maxlocal = maxlocal + 1;
                    }

                    //===================Added 3 july 2013==================
                    courseid = GetCourseId(coursehistid);
                    //======================================================
                    tb_feereceipt.CourseId = courseid;
                    tb_feereceipt.LocSerialNo = maxlocal;

                    tb_feereceipt.ReceiptNo = maxrecno;
                    try
                    {
                        tb_feereceipt.DueDate = Convert.ToDateTime(dudate);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.error = "error";
                        cu.SendError(ex);
                        ViewBag.mesg = cu.SessionError;
                    }
                    string usrtype = Request.Cookies[CommonUtil.CookieUsertype].Value;
                    if (usrtype == CommonUtil.UserTypeAdmin || usrtype == CommonUtil.UserTypeSuperAdmin)
                    {
                        tb_feereceipt.FeeDate = Convert.ToDateTime(frm["FeeDate"]);
                    }
                    else
                    {
                        tb_feereceipt.FeeDate = Convert.ToDateTime(frm["hdnfeedate"]);
                    }
                    tb_feereceipt.CounselorId = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                    tb_feereceipt.RTime = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).ToShortTimeString();
                    DateTime? dtjoin = db.Tb_CourseHist.Where(z => z.AdmissionId == tb_feereceipt.StudentId && z.CourseId == tb_feereceipt.CourseId && z.PackageId == tb_feereceipt.PackageId && z.IsActive == true).Select(z => z.FromDate).First();
                    tb_feereceipt.PackageDate = dtjoin;
                    tb_feereceipt.PayMode2 = "Cash";
                    db.Tb_FeeReceipt.Add(tb_feereceipt);
                    //db.SaveChanges();
                    /*-------------------Updates due dates in Tb_installment table-----------------------------*/
                    Tb_CourseHist obj = db.Tb_CourseHist.Where(z => z.AdmissionId == admid && z.Id == coursehistid).First();
                    obj.DueDate = Convert.ToDateTime(dudate);
                    //db.SaveChanges();
                    Tb_Installment objinstl = db.Tb_Installment.Where(z => z.Id == tb_feereceipt.InstallmentNoId).First();
                    objinstl.DueDate = tb_feereceipt.DueDate;
                    //db.SaveChanges();

                }
                catch (DbEntityValidationException ex)
                {
                    ViewBag.error = "error";
                    var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);
                    var fullErrorMessage = string.Join("; ", errorMessages);
                    var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                    cu.SendError(new Exception(exceptionMessage));
                    ViewBag.mesg = cu.SessionError;
                }
                #endregion
                //try
                //{
                //    Tb_StudentTransfer tb_st = db.Tb_StudentTransfer.Where(x => x.StudentId == tb_feereceipt.StudentId).FirstOrDefault();
                //    if (tb_st != null)
                //    {
                //        tb_st.Status = true;
                //        //db.SaveChanges();
                //    }
                //}
                //catch (DbEntityValidationException ex)
                //{
                //    ViewBag.error = "error";
                //    var errorMessages = ex.EntityValidationErrors
                //            .SelectMany(x => x.ValidationErrors)
                //            .Select(x => x.ErrorMessage);
                //    var fullErrorMessage = string.Join("; ", errorMessages);
                //    var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                //    cu.SendError(new Exception(exceptionMessage));
                //    ViewBag.mesg = cu.SessionError;
                //}

                #region Ledger_Insert
                //---------------------insert record into ledger start here--------------------

                // int packageid = (int)tb_feereceipt.PackageId;
                // double balanceamount = (double)db.MaPackages.Where(z => z.Id == packageid).Select(z => z.Amount).First();

                int studentid = (int)tb_feereceipt.StudentId;
                int rollno = (int)db.MaAdmissions.Where(z => z.Id == studentid).Select(z => z.RollNo).First();

                CommonUtil.InsertToLedger(tb_feereceipt.Id, 0, tb_feereceipt.PaidAmount, String.Format("{0}", "Receipt No" + tb_feereceipt.ReceiptNo + ", Roll No" + rollno), rollno, DateTime.Now.AddMinutes(StaticLogic.TimeDifference), "FeeReceipt");

                //---------------------insert record into ledger end here-----------------------

                //in which if installment is paid full then isfeepaid is true and if one rupees balance is due then it will remain false.

                int installid = (int)tb_feereceipt.InstallmentNoId;//get the installment id .
                double amount = (double)tb_feereceipt.BalanceAmount;//get the balance amount.
                Tb_Installment ins = db.Tb_Installment.Find(installid);//
                ins.BalanceDueAmount = amount;
                //db.SaveChanges();
                if (tb_feereceipt.BalanceAmount == 0)
                {
                    ins.IsFeesPaid = true;
                    //tb_feereceipt.DueDate = DateTime.Now.AddMonths(1);         
                }
                else
                {
                    //tb_feereceipt.DueDate = DateTime.Now.AddDays(1);
                    ins.IsFeesPaid = false;
                }
                db.SaveChanges();
                #endregion
                int count = db.Tb_FeeReceipt.Where(z => z.StudentId == tb_feereceipt.StudentId).Count();
                if (count == 1)
                {
                    // cu.SendEmailToReferene(tb_feereceipt); //inserts fee to reference on first time payment
                }
                return RedirectToAction("AftRectCreMess/" + tb_feereceipt.Id);
            }
            catch (DbEntityValidationException ex)
            {
                ViewBag.error = "error";
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);
                var fullErrorMessage = string.Join("; ", errorMessages);
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                cu.SendError(new Exception(exceptionMessage));
                ViewBag.mesg = cu.SessionError;
            }
            ViewBag.DiscountId = new SelectList(db.MaDiscounts.Where(z => z.BranchId == branchid).ToList(), "Id", "DiscountPercen", tb_feereceipt.DiscountId);
            ViewBag.PayMode = new SelectList(db.Ma_PayMode.ToList(), "PaymentMode", "PaymentMode", tb_feereceipt.PayMode);
            ViewBag.PayMode1 = new SelectList(db.Ma_PayMode.ToList(), "PaymentMode", "PaymentMode", tb_feereceipt.PayMode1);
            ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_feereceipt.StudentId);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_feereceipt.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName", courseid);
            ViewBag.PackageId = new SelectList(db.MaPackages.ToList(), "Id", "PackageName", tb_feereceipt.PackageId);
            ViewBag.InstallmentNoId = new SelectList(db.Tb_Installment.Where(z => z.BranchId == branchid).ToList(), "Id", "InstallmentNo", tb_feereceipt.InstallmentNoId);
            ViewBag.Counselor = db.Users.Where(z => z.Id == tb_feereceipt.CounselorId && z.Status == true && z.BranchId == branchid).Select(z => z.Fullname).First();
            return View(tb_feereceipt);
        }

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeReceipt, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            Tb_FeeReceipt tb_feereceipt = db.Tb_FeeReceipt.Find(id);
            tb_feereceipt.FeeDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
            ViewBag.cdate = tb_feereceipt.FeeDate;
            ViewBag.receiptno = tb_feereceipt.ReceiptNo;

            int insatId = (int)tb_feereceipt.InstallmentNoId;
            ViewBag.InstallmentNo = db.Tb_Installment.Where(z => z.Id == insatId).Select(z => z.InstallmentNo).First();

            ViewBag.NextDay = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;

            DateTime nextdate = (DateTime)db.Tb_Installment.Where(z => z.Id == insatId).Select(z => z.DueDate).First();
            int? days = db.Tb_Installment.Where(z => z.Id == insatId).Select(z => z.NextDayDuration).First();


            ViewBag.NextDueDate = nextdate.AddDays(Convert.ToDouble(days));

            if (tb_feereceipt == null)
            {
                return HttpNotFound();
            }
            ViewBag.PayMode = new SelectList(db.Ma_PayMode.ToList(), "PaymentMode", "PaymentMode", tb_feereceipt.PayMode);
            ViewBag.PayMode1 = new SelectList(db.Ma_PayMode.ToList(), "PaymentMode", "PaymentMode", tb_feereceipt.PayMode1);
            ViewBag.DiscountId = new SelectList(db.MaDiscounts.Where(z => z.BranchId == branchid).ToList(), "Id", "DiscountPercen", tb_feereceipt.DiscountId);
            ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_feereceipt.StudentId);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_feereceipt.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName", tb_feereceipt.CourseId);
            ViewBag.PackageId = new SelectList(db.MaPackages.ToList(), "Id", "PackageName", tb_feereceipt.PackageId);
            ViewBag.InstallmentNoId = new SelectList(db.Tb_Installment.Where(z => z.BranchId == branchid).ToList(), "Id", "InstallmentNo", tb_feereceipt.InstallmentNoId);
            ViewBag.Counselor = db.Users.Where(z => z.Id == tb_feereceipt.CounselorId && z.Status == true && z.BranchId == branchid).Select(z => z.Fullname).First();
            return View(tb_feereceipt);
        }

        //
        // POST: /TbFeeReceipt/Edit/5

        [HttpPost]
        public ActionResult Edit(Tb_FeeReceipt tb_feereceipt, FormCollection fc)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeReceipt, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

            //-------------------------------Due date Updation------------------------
            int admid = Convert.ToInt32(tb_feereceipt.StudentId);

            //==========Commented on 3 july -2013 =======================
            // int courseid = Convert.ToInt32(db.Tb_CourseHist.Where(z => z.Id == tb_feereceipt.CourseId).Select(z => z.CourseId).First());
            // tb_feereceipt.CourseId = courseid;
            //========================================================

            int courseid = 0;
            //its the coursehistory id
            int coursehistid = (int)tb_feereceipt.CourseId;
            try
            {

                //Tb_CourseHist obj = db.Tb_CourseHist.Where(z => z.AdmissionId == admid && z.Id == coursehistid).First();
                //if (tb_feereceipt.BalanceDueAmt == 0)
                //{
                //    obj.DueDate = obj.DueDate.Value.AddMonths(1);
                //}
                //else
                //{
                //    if (obj.DueDate <= DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date)
                //    {
                //        obj.DueDate = obj.DueDate.Value.AddDays(1);
                //    }
                //}
                //db.SaveChanges();


                //in which if installment is paid full then isfeepaid is true and if one rupees balance is due then it will remain false.

                int installid = (int)tb_feereceipt.InstallmentNoId;//get the installment id .

                double amount = (double)tb_feereceipt.BalanceAmount;//get the balance amount.
                Tb_Installment ins = db.Tb_Installment.Find(installid);//
                ins.DueDate = tb_feereceipt.DueDate;
                ins.BalanceDueAmount = amount;


                db.SaveChanges();
                //====================Added 3 july 2013====================
                courseid = GetCourseId(coursehistid);
                tb_feereceipt.CourseId = courseid;
                //=========================================================

                if (ModelState.IsValid)
                {
                    string usrtype = Request.Cookies[CommonUtil.CookieUsertype].Value;
                    if (usrtype == CommonUtil.UserTypeAdmin || usrtype == CommonUtil.UserTypeSuperAdmin)
                    {
                        tb_feereceipt.FeeDate = Convert.ToDateTime(fc["FeeDate"]);
                    }
                    else
                    {
                        tb_feereceipt.FeeDate = Convert.ToDateTime(fc["hdnfeedate"]);
                    }

                    tb_feereceipt.RTime = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).ToShortTimeString();

                    db.Entry(tb_feereceipt).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    int studentid = (int)tb_feereceipt.StudentId;
                    int rollno = (int)db.MaAdmissions.Where(z => z.Id == studentid).Select(z => z.RollNo).First();


                    CommonUtil.UpdateToLedger(tb_feereceipt.Id, 0.0, tb_feereceipt.PaidAmount, String.Format("{0}", "Receipt No" + tb_feereceipt.ReceiptNo + ", Roll No" + rollno), DateTime.Now.AddMinutes(StaticLogic.TimeDifference), rollno, "FeeReceipt");
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
            }
            //---------------------------------Due date Updation ends here 

            ViewBag.DiscountId = new SelectList(db.MaDiscounts.Where(z => z.BranchId == branchid).ToList(), "Id", "DiscountPercen", tb_feereceipt.DiscountId);
            ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_feereceipt.StudentId);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_feereceipt.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses.Where(z => z.BranchId == branchid).ToList(), "Id", "CourseName", tb_feereceipt.CourseId);
            ViewBag.PackageId = new SelectList(db.MaPackages.ToList(), "Id", "PackageName", tb_feereceipt.PackageId);
            ViewBag.InstallmentNoId = new SelectList(db.Tb_Installment.Where(z => z.BranchId == branchid).ToList(), "Id", "InstallmentNo", tb_feereceipt.InstallmentNoId);
            ViewBag.Counselor = db.Users.Where(z => z.Id == tb_feereceipt.CounselorId && z.Status == true && z.BranchId == branchid).Select(z => z.Fullname).First();
            return View(tb_feereceipt);
        }

        //
        // GET: /TbFeeReceipt/Delete/5

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Delete(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeReceipt, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            try
            {
                Tb_FeeReceipt feeareceipt = db.Tb_FeeReceipt.Find(id);

                db.Tb_FeeReceipt.Remove(feeareceipt);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }



        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        ///  get courses  by student
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private IList<Tb_CourseHist> GetCourse(int id)
        {


            return db.Tb_CourseHist.Where(m => m.AdmissionId == id && m.IsActive == true).ToList();


        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadPackageByCourse(string id)
        {

            try
            {
                var courselist = this.GetCourse(Convert.ToInt32(id));

                var colourData = courselist.Select(c => new SelectListItem()
                {
                    Text = c.MaCourse.CourseName + '-' + c.GroupName + "-(" + c.MaTime.TCode + "-" + c.MaTime.TimeIn + " " + c.MaTime.Indesc + "-" + c.MaTime.TimeOut + " " + c.MaTime.OutDesc + ")",
                    Value = c.Id.ToString(),

                });

                return Json(colourData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(null, JsonRequestBehavior.AllowGet);

            }

        }



        /// <summary>
        ///  get packages by course
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private IList<MaPackage> GetPackages(int id, int stuid)
        {
            int packid = (int)db.Tb_CourseHist.Where(z => z.Id == id && z.AdmissionId == stuid).Select(z => z.PackageId).First();



            return db.MaPackages.Where(m => m.Id == packid).ToList();

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadPackageBypackages(string id, string stuid)
        {

            var package = this.GetPackages(Convert.ToInt32(id), Convert.ToInt32(stuid));

            var colourData = package.Select(c => new SelectListItem()
            {
                Text = c.PackageName,
                Value = c.Id.ToString(),

            });




            return Json(colourData, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult Getonlynextdate(int id, int stdid, int coursehistid, int balanceamount)
        {
            string nextdate = "";
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            Tb_Installment obj = db.Tb_Installment.Where(m => m.Tb_CourseHist.IsActive == true && m.CourseHistId == coursehistid && m.BranchId == branchid && m.IsFeesPaid == false).OrderBy(m => m.Id).Take(1).SingleOrDefault();
            double? dueamount = 0;

            Tb_CourseHist courseobj = new Tb_CourseHist();
            try
            {
                courseobj = db.Tb_CourseHist.Where(a => a.Id == coursehistid).FirstOrDefault();
            }
            catch (Exception ex)
            { }

            DateTime nxtday = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
            if (balanceamount == 0)
            {
                dueamount = obj.Amount;
                int? days = obj.NextDayDuration;
                DateTime dtnext = courseobj.DueDate.Value;
                nxtday = dtnext.AddDays(Convert.ToDouble(days - 1));
                nextdate = nxtday.ToString("MM/dd/yyyy");
            }
            else
            {
                dueamount = obj.BalanceDueAmount;
                nxtday.AddDays(1);
                if (nxtday.ToLongDateString().Contains("Sunday"))
                {
                    nxtday = nxtday.AddDays(1);
                }
                nextdate = nxtday.ToString("MM/dd/yyyy");
            }

            return Json(nextdate, JsonRequestBehavior.AllowGet);
        }



        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetInstallmentNo(int id, int stdid, int coursehistid)
        {
            //.Where(m => m.brnchid = id).
            int maxinstallment = 0;
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

            //  int coursehistid = db.Tb_CourseHist.Where(z => z.PackageId == id && z.AdmissionId == stdid).Select(z => z.Id).First();
            //double? installdueamount = db.Tb_FeeReceipt.Where(z => z.PackageId == id && z.InstallmentNoId == instid).Select(z => z.BalanceDueAmt).First();

            Tb_Installment obj = db.Tb_Installment.Where(m => m.Tb_CourseHist.IsActive == true && m.CourseHistId == coursehistid && m.BranchId == branchid && m.IsFeesPaid == false).OrderBy(m => m.Id).Take(1).SingleOrDefault();

            try
            {
                maxinstallment = db.Tb_Installment.Where(m => m.Tb_CourseHist.IsActive == true && m.CourseHistId == coursehistid && m.BranchId == branchid && m.IsFeesPaid == false).OrderBy(m => m.Id).Max(a => a.InstallmentNo).Value;
            }
            catch (Exception ex)
            { }

            double? dueamount = 0;
            Tb_CourseHist courseobj = new Tb_CourseHist();
            try
            {
                courseobj = db.Tb_CourseHist.Where(a => a.Id == coursehistid).FirstOrDefault();
            }
            catch (Exception ex)
            { }

            if (obj != null)
            {
                DateTime nxtday;
                if (courseobj != null)
                {
                    nxtday = Convert.ToDateTime(courseobj.DueDate);
                }
                else
                {
                    nxtday = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
                }
                double? feeamount = obj.Amount;
                if (obj.BalanceDueAmount > 0)
                {
                    dueamount = obj.BalanceDueAmount;
                    feeamount = dueamount;
                    nxtday.AddDays(1);
                    if (nxtday.ToLongDateString().Contains("Sunday"))
                    {
                        nxtday = nxtday.AddDays(1);
                    }
                }

                else if (obj.Amount > 0)
                {
                    dueamount = obj.Amount;
                    if (obj.BalanceDueAmount != null && obj.BalanceDueAmount > 0)
                    {
                        feeamount = obj.BalanceDueAmount;
                    }
                    double? BranchDisAmt = 0;
                    try
                    {
                        BranchDisAmt = db.Tb_CoursewiseDiscount.Where(x => x.BranchId == branchid && x.PackgId == id).Select(x => x.DiscAmt).FirstOrDefault();
                        if (BranchDisAmt == null)
                        {
                            BranchDisAmt = 0;
                        }
                    }
                    catch (Exception)
                    {
                    }
                    if (BranchDisAmt < feeamount && obj.InstallmentNo == 1)
                    {
                        feeamount = feeamount - BranchDisAmt;
                    }
                    int? days = obj.NextDayDuration;
                    DateTime dtnext = courseobj.DueDate.Value;
                    nxtday = dtnext.AddDays(Convert.ToDouble(days));
                }


                DateTime nextmonthdate = DateTime.Now;
                try
                {
                    if (obj.Tb_CourseHist.DueDate == null)
                    {
                        nextmonthdate = Convert.ToDateTime(obj.Tb_CourseHist.FromDate.Value.AddMonths(1).ToShortDateString());
                    }
                }
                catch (Exception ex)
                { }
                //obj.Tb_CourseHist.DueDate.Value.AddMonths(1).ToShortDateString(),
                var NewInstallment = new
                {
                    installmentno = obj.InstallmentNo,
                    installmentamount = feeamount,
                    id = obj.Id,
                    duedate = nxtday,
                    NextMonthDate = nextmonthdate.AddMonths(1).ToShortDateString(),
                    NextDayDate = nxtday.ToShortDateString(),
                    GroupName = obj.Tb_CourseHist.GroupName,
                    doj = obj.Tb_CourseHist.FromDate.Value.ToShortDateString(),
                };
                return Json(NewInstallment, JsonRequestBehavior.AllowGet);
            }

            else
            {

                var NewInstallment = new
                {

                    installmentno = 0,
                    installmentamount = 0,
                    id = 0,
                    duedate = 0,
                    doj = "fee finished",
                };

                return Json(NewInstallment, JsonRequestBehavior.AllowGet);
            }

        }



        public ActionResult AftRectCreMess()
        {

            return View("AftRectCreMess");
        }


        public ActionResult Search(string receiptno = "0", string name = "", string course = "", string rollno = "0")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            List<Tb_FeeReceipt> lst = new List<Tb_FeeReceipt>();
            if (receiptno != "")
            {
                try
                {
                    int receipt = Convert.ToInt32(receiptno);

                    //lst = lst.Where(z => z.ReceiptNo == receipt).ToList();
                    lst = db.Tb_FeeReceipt.Where(z => z.LocSerialNo == receipt && z.BranchId == branchid).ToList();
                    ViewBag.receiptno = receiptno;
                }
                catch (Exception ex)
                {
                }
            }

            if (rollno != "")
            {
                try
                {
                    int rolno = Convert.ToInt32(rollno);
                    lst = db.Tb_FeeReceipt.Where(z => z.MaAdmission.RollNo == rolno && z.BranchId == branchid).ToList();
                    ViewBag.rolno = rolno;
                }
                catch (Exception ex)
                {
                }
            }

            if (name != "")
            {

                lst = lst.Where(z => z.MaAdmission.FirstName.ToUpper().Contains(name.ToUpper())).ToList();
                ViewBag.name = name;
            }


            if (course != "")
            {
                lst = lst.Where(z => z.MaCourse.CourseName.ToUpper().Contains(course.ToUpper())).ToList();
                ViewBag.course = course;

            }


            if (course == "" && name == "" && rollno == "" && receiptno == "")
            {
                lst = db.Tb_FeeReceipt.Where(z => z.BranchId == branchid).Take(25).OrderByDescending(z => z.Id).ToList();

            }

            ViewBag.TotalCount = lst.Count;

            return View("Index", lst);
        }



        /*----searchall is used for admin searchlist where admin can show receipts of all branches and even if transfer student------*/


        public ActionResult SearchAll(string receiptno = "0", string name = "", string course = "", string rollno = "0", string branc = "")
        {


            List<Tb_FeeReceipt> lst = db.Tb_FeeReceipt.OrderByDescending(z => z.Id).ToList();


            if (receiptno != "")
            {
                try
                {
                    int receipt = Convert.ToInt32(receiptno);

                    lst = lst.Where(z => z.ReceiptNo == receipt).ToList();
                    ViewBag.receiptno = receiptno;
                }
                catch (Exception ex)
                {
                }
            }

            if (rollno != "")
            {
                try
                {
                    int rolno = Convert.ToInt32(rollno);
                    lst = lst.Where(z => z.MaAdmission.RollNo == rolno).ToList();
                    ViewBag.rolno = rolno;
                }
                catch (Exception ex)
                {
                }
            }

            if (name != "")
            {

                lst = lst.Where(z => z.MaAdmission.FirstName.ToUpper().Contains(name.ToUpper())).ToList();
                ViewBag.name = name;
            }


            if (course != "")
            {
                lst = lst.Where(z => z.MaCourse.CourseName.ToUpper().Contains(course.ToUpper())).ToList();
                ViewBag.course = course;

            }

            ViewBag.brname = branc;
            if (branc != "")
            {
                int bid = Convert.ToInt32(branc);
                lst = lst.Where(z => z.BranchId == bid).ToList();
            }


            ViewBag.TotalCount = lst.Count;
            ViewBag.BranchName = new SelectList(db.MaBranches.Where(z => z.Status == true), "Id", "BramchName");

            return View("AllReceipts", lst);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Getnextdate()
        {
            DateTime nxtday = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1);
            if (nxtday.ToLongDateString().Contains("Sunday"))
            {
                nxtday = nxtday.AddDays(1);
            }

            var NewInstallment = new
            {

                installmentno = 0,
                installmentamount = 0,
                id = 0,
                duedate = 0,
                NextMonthDate = 0,

                NextDayDate = nxtday.ToShortDateString(),
                GroupName = 0,


            };
            return Json(NewInstallment, JsonRequestBehavior.AllowGet);
        }

        //calls report of fee receipt
        public ActionResult PrintReport(int id)
        {
            //geting repot data from the business object
            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetFeeRecieptView(id);

            var renderedBytes = reportViewModel.RenderReport();

            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);

        }

        public ActionResult GetFeeRecieptView2(int id)
        {
           
            ReportViewer viwer = new ReportViewer();
            byte[] renderedBytes = null;
            //string PDF = "PDF";
            Warning[] warnings = null;
            string[] streamIds = null;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            string filetype = string.Empty;
            DataTable dt = new DataTable();

            string sql = string.Empty;
            Models.SSCDataSetTableAdapters.FeeReceGstTableAdapter r = new Models.SSCDataSetTableAdapters.FeeReceGstTableAdapter();
            dt = r.GetData(id);
            ReportDataSource rds = new ReportDataSource("DataSet1", (object)dt);
            viwer.SizeToReportContent = true;
            viwer.LocalReport.ReportPath = "Reports/gstreceipt.rdlc";
            viwer.LocalReport.Refresh();
            viwer.LocalReport.DataSources.Add(rds);
            renderedBytes = viwer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(renderedBytes, mimeType);

        }

        public ActionResult GetFeeRecieptView3(string date1, string date2, string format)
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ReportViewer viwer = new ReportViewer();
            byte[] renderedBytes = null;
            //string PDF = "PDF";
            Warning[] warnings = null;
            string[] streamIds = null;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            string filetype = string.Empty;
            DataTable dt = new DataTable();

            string sql = string.Empty;
            Models.SSCDataSetTableAdapters.FeeReceGstTableAdapter r = new Models.SSCDataSetTableAdapters.FeeReceGstTableAdapter();
            dt = r.GetDataByDate(DateTime.Parse(date1), DateTime.Parse(date2),branchid);
            ReportDataSource rds = new ReportDataSource("DataSet1", (object)dt);
            viwer.SizeToReportContent = true;
            viwer.LocalReport.ReportPath = "Reports/gstreg.rdlc";
            viwer.LocalReport.Refresh();
            viwer.LocalReport.DataSources.Add(rds);
            renderedBytes = viwer.LocalReport.Render(format, null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(renderedBytes, mimeType);

        }
        public ActionResult PrintReport2(int id)
        {

            return GetFeeRecieptView2(id);

        }

        //calls report of daily fee receipt
        public ActionResult DailyFeeReceipt(string dt1 = "", string dt2 = "", string format = "", string reptype = "")
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.DailyFeeReprot, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            try
            {
                if (reptype == "GST")
                {
                    int roleid = StaticLogic.RoleId();
                    bool Prmsn = false;
                    Prmsn = Permission.CheckPermission(roleid, CommonUtil.GSTReport, UserAction.View);

                    if (Prmsn == false)
                    {
                        return View("~/Views/Shared/AccessDenied.cshtml");
                    }
                }
            }
            catch (Exception)
            {
            }

            if (dt1 == "" || dt2 == "")
            {
                ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
                ViewBag.tdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1.0).Date.ToShortDateString();
                return View();
            }
            else
            {

                string usrtype = Request.Cookies[CommonUtil.CookieUsertype].Value;
                if (usrtype == CommonUtil.UserTypeCounselor || usrtype == CommonUtil.UserTypeAdmin || usrtype == CommonUtil.UserTypeVisaCounselor)
                {
                    try
                    {
                        DateTime fromdate = Convert.ToDateTime(dt1);
                        if (fromdate < DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(-3))
                        {
                            return Content("You don't have permission to go to this date");
                        }
                    }
                    catch (Exception ex)
                    {
                        return Content("You don't have permission to go to this date");
                    }
                }

                if (reptype.Equals("DFR"))
                {
                    var Business = new RecieptBuisness();
                    var reportViewModel = Business.GetDailyFeeReceiptView(dt1, dt2, format);

                    var renderedBytes = reportViewModel.RenderReport();
                    if (reportViewModel.ViewAsAttachment)
                        Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                    return File(renderedBytes, reportViewModel.LastmimeType);
                }
                else
                {
                    return GetFeeRecieptView3(dt1, dt2, format);
                }
            }
        }



        #region

        //calls report of Discount Receipt 
        public ActionResult DiscountReport(string dt1 = "", string dt2 = "", string format = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.DiscountReport, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            if (dt1 == "" || dt2 == "")
            {
                ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
                ViewBag.tdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1.0).Date.ToShortDateString();
                return View();
            }
            else
            {

                string usrtype = Request.Cookies[CommonUtil.CookieUsertype].Value;
                if (usrtype == CommonUtil.UserTypeCounselor)
                {
                    try
                    {
                        DateTime fromdate = Convert.ToDateTime(dt1);
                        if (fromdate < DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(-3))
                        {
                            return Content("You don't have permission to go to this date");
                        }
                    }
                    catch (Exception ex)
                    {
                        return Content("You don't have permission to go to this date");
                    }
                }

                var Business = new RecieptBuisness();
                var reportViewModel = Business.GetDiscountReportList(dt1, dt2, format);

                var renderedBytes = reportViewModel.RenderReport();
                if (reportViewModel.ViewAsAttachment)
                    Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                return File(renderedBytes, reportViewModel.LastmimeType);
            }
        }
        #endregion

        #region Package Course Report
        public ActionResult CoursePackageReport(string dt1 = "", string dt2 = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.CoursePackageReport, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            List<CoursePackageHelper> lst = new List<CoursePackageHelper>();
            if (lst == null)
            {
                lst = new List<CoursePackageHelper>();
            }


            if (dt1 == "" || dt2 == "")
            {
                ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
                ViewBag.tdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1.0).Date.ToShortDateString();
                return View("CoursePackageReport", lst);
            }
            else
            {
                string connectionstring = ConfigurationManager.ConnectionStrings["SSCWebDBConnectionString"].ConnectionString;
                string sql = "";

                DateTime fromdate;
                DateTime todate;

                if (dt1 != "" && dt2 != "")
                {
                    ViewBag.fdate = dt1;
                    ViewBag.tdate = dt2;
                    fromdate = Convert.ToDateTime(dt1);
                    todate = Convert.ToDateTime(dt2);

                    sql = @"select CourseId, PackageId, count(1) as total from Tb_CourseHist where Convert(date, FromDate) BETWEEN '" + dt1 + "' AND '" + dt2 + "' group by CourseId , PackageId with rollup order by CourseId";

                    using (SqlConnection con = new SqlConnection(connectionstring))
                    {
                        SqlDataAdapter adp = new SqlDataAdapter(sql, con);
                        con.Open();
                        DataTable dt = new DataTable();
                        adp.Fill(dt);



                        foreach (DataRow dr in dt.Rows)
                        {
                            CoursePackageHelper obj = new CoursePackageHelper();

                            try
                            {
                                obj.CourseId = Convert.ToInt32(dr["CourseId"]);
                            }
                            catch (Exception ex)
                            { }
                            try
                            {
                                obj.PackageID = Convert.ToInt32(dr["PackageId"]);
                            }
                            catch (Exception ex)
                            { }
                            try
                            {
                                obj.Total = Convert.ToInt32(dr["total"]);
                            }
                            catch (Exception ex)
                            { }
                            lst.Add(obj);
                        }

                    }
                }


            }

            return View("CoursePackageReport", lst);
        }
        #endregion

        public ActionResult PackagewiseStudentList(int PkgId, string FDate = "", string TDate = "")
        {
            DateTime Dt1 = Convert.ToDateTime(FDate).Date;
            DateTime Dt2 = Convert.ToDateTime(TDate).Date;
            List<Tb_CourseHist> course = db.Tb_CourseHist.Where(x => x.PackageId == PkgId && x.FromDate >= Dt1 && x.FromDate <= Dt2).ToList();

            return View(course);
        }



        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetGroupName(int id, int stu)
        {

            var package = db.Tb_CourseHist.Where(z => z.Id == id && z.AdmissionId == stu).ToList();

            var colourData = package.Select(c => new SelectListItem()
            {
                Text = c.GroupName,
                Value = c.GroupName,

            });




            return Json(colourData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult OpenDefaulterList()
        {
            return View();
        }

        public DateTime IndianTime()
        {
            TimeZone time2 = TimeZone.CurrentTimeZone;
            DateTime test = time2.ToUniversalTime(DateTime.Now);
            var indiatime = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            var india = TimeZoneInfo.ConvertTimeFromUtc(test, indiatime);
            return india;
        }


        public ActionResult PrintDefaulterList(string date1)
        {
            DateTime date = Convert.ToDateTime(date1);
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.DefaultureList, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            var Business = new RecieptBuisness();

            var reportViewModel = Business.GetDefaulterList(date);
            var renderedBytes = reportViewModel.RenderReport();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
        }

        public ActionResult GetReportByDOJ()
        {
            return View();
        }

        public ActionResult GetDOJExcel(string dt1, string dt2, string format)
        {
            DateTime date1 = Convert.ToDateTime(dt1);
            DateTime date2 = Convert.ToDateTime(dt2);
            //try
            //{
            //    int roleid = StaticLogic.RoleId();
            //    bool Prmsn = false;
            //    Prmsn = Permission.CheckPermission(roleid, CommonUtil.DefaultureList, UserAction.View);

            //    if (Prmsn == false)
            //    {
            //        return View("~/Views/Shared/AccessDenied.cshtml");
            //    }

            //}
            //catch (Exception)
            //{
            //}


            var Business = new RecieptBuisness();

            var reportViewModel = Business.GetDOJList(date1, date2, format);
            var renderedBytes = reportViewModel.RenderReport();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
        }



        public ActionResult StudentFeeDetail(int admid)
        {
            MaAdmission objAdm = db.MaAdmissions.Where(z => z.Id == admid).First();
            return View(objAdm);
        }

        /*This gives the record of fee paid*/
        public PartialViewResult _FeeList(int admid)
        {
            List<Tb_FeeReceipt> lst = db.Tb_FeeReceipt.Where(z => z.StudentId == admid).OrderByDescending(z => z.Id).ToList();
            ViewBag.Idd = admid;
            return PartialView(lst);
        }
        /*This gives the pending fee report*/
        public PartialViewResult _FeePending(int admid)
        {
            List<Tb_CourseHist> lstcourse = db.Tb_CourseHist.Where(z => z.AdmissionId == admid).ToList();
            List<Tb_Installment> lstinstal = new List<Tb_Installment>();
            if (lstcourse.Count > 0)
            {
                foreach (Tb_CourseHist objcourse in lstcourse)
                {
                    try
                    {
                        //Tb_Installment objinstal = db.Tb_Installment.Where(z => z.CourseHistId == objcourse.Id && z.IsFeesPaid == false).First();
                        //lstinstal.Add(objinstal);
                        lstinstal.AddRange(db.Tb_Installment.Where(z => z.CourseHistId == objcourse.Id && z.IsFeesPaid == false).ToList());
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }


            return PartialView(lstinstal);
        }
    }
}