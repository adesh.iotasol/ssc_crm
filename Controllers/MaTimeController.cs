﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class MaTimeController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        MasterListing listing = new MasterListing();

        //
        // GET: /MaTime/

        public ActionResult Index()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Time, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            

            var matimes = db.MaTimes.Include(m => m.MaBranch).OrderByDescending(z => z.Id);
            ViewBag.TotalCount = matimes.Count();
            return View(matimes.ToList());
        }

        //
        // GET: /MaTime/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Time, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            MaTime matime = db.MaTimes.Find(id);
            if (matime == null)
            {
                return HttpNotFound();
            }
            return View(matime);
        }

        //
        // GET: /MaTime/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Time, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            ViewBag.PackageHours = new SelectList(listing.GetPackageHours, "PackageHours", "PackageHours");
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            return View();
        }

        //
        // POST: /MaTime/Create

        [HttpPost]
        public ActionResult Create(MaTime matime)
        {
            ViewBag.PackageHours = new SelectList(listing.GetPackageHours, "PackageHours", "PackageHours",matime.PackageHours);

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Time, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            try
            {

                int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
                matime.BranchId = branchid;
                matime.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
               // matime.PackageHours = ViewBag.PackageHours;
                matime.Status = true;
                db.MaTimes.Add(matime);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }

            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", matime.BranchId);
            return View(matime);
        }

        //
        // GET: /MaTime/Edit/5

        public ActionResult Edit(int id)
        {
           
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Time, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            MaTime matime = db.MaTimes.Find(id);

            if (matime == null)
            {
                return HttpNotFound();
            }
            ViewBag.PackageHours = new SelectList(listing.GetPackageHours, "PackageHours", "PackageHours",matime.PackageHours);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", matime.BranchId);
            return View(matime);
        }

        //
        // POST: /MaTime/Edit/5

        [HttpPost]
        public ActionResult Edit(MaTime matime)
        {
            ViewBag.PackageHours = new SelectList(listing.GetPackageHours, "PackageHours", "PackageHours", matime.PackageHours);

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Time, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            try
            {

                int id = matime.Id;
                MaTime objtime = db.MaTimes.Where(z => z.Id == id).First();
                //List<MaGroupAssign> lstgroup = db.MaGroupAssign.Where(z => z.TimeIn == objtime.TimeIn && z.TimeOut == objtime.TimeOut && z.Indesc == objtime.Indesc && z.OutDesc == objtime.OutDesc).ToList();

                //foreach (MaGroupAssign obj in lstgroup)
                //{
                //    obj.Indesc = matime.Indesc;
                //    obj.OutDesc = matime.OutDesc;
                //    obj.TimeIn = matime.TimeIn;
                //    obj.TimeOut = matime.TimeOut;
                //    db.SaveChanges();
                //}



                // db.Entry(matime).State = System.Data.Entity.EntityState.Modified;
                db.Entry(objtime).CurrentValues.SetValues(matime);
                int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
                matime.BranchId = branchid;
                matime.Status = true;
                //matime.Cdate = DateTime.Now.Date;

                db.SaveChanges();


                return RedirectToAction("Index");
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }

            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", matime.BranchId);
            return View(matime);
        }

        //
        // GET: /MaTime/Delete/5

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Delete(int id)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Time, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            try
            {
                MaTime matime = db.MaTimes.Find(id);



                db.MaTimes.Remove(matime);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}