﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class Tb_VisaPaymentController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /Tb_VisaPayment/

        public ActionResult Index()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.VisaStudent, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            var tb_visapayment = db.Tb_VisaPayment.Include(t => t.MaBranch).Include(t => t.User);
            return View(tb_visapayment.ToList());
        }

        //
        // GET: /Tb_VisaPayment/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.VisaStudent, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }




            Tb_VisaPayment tb_visapayment = db.Tb_VisaPayment.Find(id);
            if (tb_visapayment == null)
            {
                return HttpNotFound();
            }
            return View(tb_visapayment);
        }

        //
        // GET: /Tb_VisaPayment/Create

        public ActionResult Create(int id, string enqtype, string stype)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.VisaStudent, UserAction.Add);
                string chbranch = Request.Cookies["chbranch"].Value;

                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            Tb_VisaPayment objVisa = new Tb_VisaPayment();
            objVisa.StudentId = id;
            objVisa.StudentType = stype;

            objVisa.Status = true;
            objVisa.EnqType = enqtype;


            ViewBag.enqtype = enqtype;
            ViewBag.stype = stype;
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.UserId = new SelectList(db.Users, "Id", "FullName");
            return View(objVisa);
        }

        //
        // POST: /Tb_VisaPayment/Create

        [HttpPost]
        public ActionResult Create(Tb_VisaPayment tb_visapayment)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.VisaStudent, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }




            string enqtype = Request.QueryString["enqtype"];
            string stype = Request.QueryString["stype"];
            int usrid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            tb_visapayment.BranchId = branchid;
            tb_visapayment.UserId = usrid;

            if (tb_visapayment.PaymentMode != "Cash")
            {
                if (tb_visapayment.BankName == "" || tb_visapayment.ChequeDate == null || tb_visapayment.ChequeNo == "")
                {
                    ViewBag.enqtype = enqtype;
                    ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_visapayment.BranchId);
                    ViewBag.UserId = new SelectList(db.Users, "Id", "Password", tb_visapayment.UserId);
                    ViewBag.err = "Please Fill FullDetail of Cheque/DD";
                    return View(tb_visapayment);

                    // return RedirectToAction("Create", new { @id = tb_visapayment.StudentId, @enqtype = enqtype, @stype = stype, @err = "Please Fill FullDetail of Cheque/DD" });
                }
            }


            try
            {
                db.Tb_VisaPayment.Add(tb_visapayment);
                db.SaveChanges();
                return RedirectToAction("VisaStudents");
            }

            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        ViewBag.err = "Please Fill FullDetail of Cheque/DD";
                        // return RedirectToAction("Create", new { @id = tb_visapayment.StudentId, @enqtype = enqtype, @stype = stype, @err = "Please Fill FullDetail of Cheque/DD" });
                    }
                }

            }


            ViewBag.enqtype = enqtype;
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_visapayment.BranchId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Password", tb_visapayment.UserId);
            return View(tb_visapayment);
        }

        //
        // GET: /Tb_VisaPayment/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.VisaStudent, UserAction.Edit);
                string chbranch = Request.Cookies["chbranch"].Value;

                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }




            Tb_VisaPayment tb_visapayment = db.Tb_VisaPayment.Find(id);
            if (tb_visapayment == null)
            {
                return HttpNotFound();
            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_visapayment.BranchId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Password", tb_visapayment.UserId);
            return View(tb_visapayment);
        }

        //
        // POST: /Tb_VisaPayment/Edit/5

        [HttpPost]
        public ActionResult Edit(Tb_VisaPayment tb_visapayment)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.VisaStudent, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }




            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            int userid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            tb_visapayment.BranchId = branchid;
            tb_visapayment.UserId = userid;
            if (ModelState.IsValid)
            {
                db.Entry(tb_visapayment).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Content("Success");
            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_visapayment.BranchId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Password", tb_visapayment.UserId);
            return View(tb_visapayment);
        }

        //
        // GET: /Tb_VisaPayment/Delete/5
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.VisaStudent, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            try
            {
                Tb_VisaPayment tb_visapayment = db.Tb_VisaPayment.Find(id);
                db.Tb_VisaPayment.Remove(tb_visapayment);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        //
        // POST: /Tb_VisaPayment/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Tb_VisaPayment tb_visapayment = db.Tb_VisaPayment.Find(id);
        //    db.Tb_VisaPayment.Remove(tb_visapayment);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult VisaStudents(string roll = "", string sname = "", string mob = "", string entertype = "", string status = "", string lastupdated = "")
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.VisaStudent, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }





            List<VisaStudentBean> lstbean = new List<VisaStudentBean>();
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

            ViewBag.txtlastupdated = new SelectList(db.Users.Where(z => z.BranchId == branchid && z.Usertype == CommonUtil.UserTypeVisaCounselor || z.Usertype == CommonUtil.UserTypeAdmin).ToList(), "Id", "username");

            if (roll != "" || sname != "" || mob != "" || entertype != "" || status != "" || lastupdated != "")
            {
                List<Tb_VisaFollowUp> _lstfollow = new List<Tb_VisaFollowUp>();

                if (Request.Cookies[CommonUtil.CookieUsertype].Value == CommonUtil.UserTypeSuperAdmin && Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeVisaCounselor)
                {
                    _lstfollow = db.Tb_VisaFollowUp.ToList();
                }
                else
                {
                    _lstfollow = db.Tb_VisaFollowUp.Where(z => z.Branchid == branchid).ToList();
                }
                //get list from admission table for MockTest start here
                foreach (Tb_VisaFollowUp obj in _lstfollow)
                {

                    VisaStudentBean objbean = new VisaStudentBean();
                    objbean.MockIELTSid = obj.ReferId;
                    objbean.StudentId = Convert.ToInt32(obj.Referenceno);
                    objbean.SName = obj.Name;
                    objbean.Mobile = obj.Mobileno;
                    objbean.remarks = obj.Remarks;
                    objbean.status = obj.FollowStatus;

                    //objbean.MockIELTSid = obj.ReferId;
                    objbean.followtype = obj.Followtype;
                    objbean.type = obj.entertype;
                    objbean.Visaid = obj.Id;
                    try
                    {
                        int lastfollowid = Convert.ToInt32(obj.LastFollowBy);
                        objbean.lastupdated = db.Users.Where(z => z.Id == lastfollowid).Select(z => z.username).FirstOrDefault().ToString();
                    }
                    catch (Exception ex)
                    { }
                    objbean.followupdate = obj.NextFollowdate;

                    if (obj.Followtype == "MockTest" || obj.Followtype == "VisaMockTest")
                    {
                        objbean.EntryType = CommonUtil.AdmissionMcktst;
                    }
                    else if (obj.Followtype == "VisaEnquiry")
                    {
                        objbean.EntryType = CommonUtil.Enquiry;
                    }
                    else if (obj.Followtype == "IELTS" || obj.Followtype == "VisaIELTS")
                    {
                        objbean.EntryType = CommonUtil.EnquiryIELTS;
                    }
                    else if (obj.Followtype == "VisaIELTSAdmission")
                    {

                        objbean.EntryType = CommonUtil.ProspStudentsIELTS;
                    }
                    //                    objbean.EntryType = obj.Followtype;
                    lstbean.Add(objbean);
                }


            }
            /*-----------------------------Search Filter starts from here---------------------------------------*/

            if (roll != "")
            {
                ViewBag.roll = roll;
                try
                {
                    int rl = Convert.ToInt32(roll);
                    lstbean = lstbean.Where(z => z.StudentId == rl).ToList();
                }
                catch (Exception ex)
                { }
            }

            if (sname != "")
            {
                ViewBag.sname = sname;
                sname = sname.ToUpper();
                lstbean = lstbean.Where(z => z.SName.ToUpper().Contains(sname)).ToList();
            }

            if (mob != "")
            {
                ViewBag.mob = mob;
                lstbean = lstbean.Where(z => z.Mobile == mob).ToList();
            }

            if (entertype != "")
            {
                ViewBag.entertype = entertype;
                lstbean = lstbean.Where(z => z.EntryType.ToUpper().Contains(entertype.ToUpper())).ToList();
            }

            if (status != "All")
            {
                ViewBag.status = status;
                lstbean = lstbean.Where(z => z.status == status).ToList();
            }

            if (lastupdated != "")
            {
                int lastfow = Convert.ToInt32(lastupdated);
                string lastupdateuser = db.Users.Where(z => z.Id == lastfow).Select(z => z.username).FirstOrDefault().ToString();

                ViewBag.lastupdated = lastupdated;
                lstbean = lstbean.Where(z => z.lastupdated.ToUpper().Contains(lastupdateuser.ToUpper())).ToList();
            }


            ViewBag.TotalCount = lstbean.Count;
            return View(lstbean);
        }

        public PartialViewResult _VisaFeeDetail(int id = 0)
        {
            List<Tb_VisaPayment> lstvisa = new List<Tb_VisaPayment>();
            if (id != 0)
            {
                lstvisa = db.Tb_VisaPayment.Where(z => z.StudentId == id).ToList();
            }
            return PartialView(lstvisa);
        }

        public PartialViewResult _Edit(int id)
        {
            Tb_VisaPayment tb_visapayment = db.Tb_VisaPayment.Find(id);
            if (tb_visapayment == null)
            {

            }
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_visapayment.BranchId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Password", tb_visapayment.UserId);
            return PartialView(tb_visapayment);
        }




        public ActionResult TransferVisaStudent()
        {
            List<VisaStudentBean> lstbean = GetVisaList();

            ViewBag.TotalCount = lstbean.Count;
            return View(lstbean);
        }

        private List<VisaStudentBean> GetVisaList()
        {
            List<MaEnquiry> lstenq = db.MaEnquiries.Where(z => z.IsMoveToVisa == true && z.IELTSTestId != null).ToList();
            List<VisaStudentBean> lstbean = new List<VisaStudentBean>();

            //get list from enquiry table for IELTS students
            foreach (MaEnquiry obj in lstenq)
            {
                VisaStudentBean objbean = new VisaStudentBean();
                objbean.EntryType = "Enquiry";
                objbean.MockIELTSid = obj.IELTSTestId;
                objbean.StudentId = obj.Id;
                objbean.enquiryid = obj.Id;
                objbean.TestType = "IELTS";
                objbean.rollno = obj.Id;
                objbean.SName = obj.FirstName + " " + obj.LastName;
                objbean.Mobile = obj.Mobile;
                if (db.Tb_IELTSResult.Where(z => z.Id == obj.IELTSTestId).Select(z => z.UserId).Count() > 0)
                {
                    objbean.Cid = db.Tb_IELTSResult.Where(z => z.Id == obj.IELTSTestId).Select(z => z.UserId).First();

                    objbean.branchid = db.Tb_IELTSResult.Where(z => z.Id == obj.IELTSTestId).Select(z => z.BranchId).First();

                    objbean.Counselor = db.Users.Where(z => z.Id == objbean.Cid).Select(z => z.Fullname).First();
                }
                if (objbean.TestType == "IELTS")
                {
                    if (objbean.EntryType == "Enquiry")
                    {
                        if (db.MaEnqFollows.Where(z => z.ReferId == obj.IELTSTestId && z.FollowUpType == "VisaIELTS").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                        {
                            DateTime? dt = db.MaEnqFollows.Where(z => z.ReferId == obj.IELTSTestId && z.FollowUpType == "VisaIELTS").OrderByDescending(z => z.Id).Select(z => z.NextDate).First();
                            objbean.followupdate = dt;
                        }

                        if (db.MaEnqFollows.Where(z => z.ReferId == obj.IELTSTestId && z.FollowUpType == "VisaIELTS").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                        {
                            string remarks = db.MaEnqFollows.Where(z => z.ReferId == obj.IELTSTestId && z.FollowUpType == "VisaIELTS").OrderByDescending(z => z.Id).Select(z => z.Remarks).First();
                            objbean.remarks = remarks;
                            objbean.status = db.MaEnqFollows.Where(z => z.ReferId == obj.IELTSTestId && z.FollowUpType == "VisaIELTS").OrderByDescending(z => z.Id).Select(z => z.Enqstatus).First();
                        }

                    }
                }

                lstbean.Add(objbean);

            }


            List<MaEnquiry> lstenqvisaprosepect = db.MaEnquiries.Where(z => z.IsMoveToVisa == true && z.IELTSTestId == null).ToList();
            // get list from enquiry table for  visa prospective students


            foreach (MaEnquiry obj in lstenqvisaprosepect)
            {
                VisaStudentBean objbean = new VisaStudentBean();
                objbean.EntryType = "Enquiry";
                objbean.MockIELTSid = obj.Id;
                objbean.StudentId = obj.Id;
                objbean.enquiryid = obj.Id;
                objbean.TestType = "Enquiry";
                objbean.rollno = obj.Id;
                objbean.SName = obj.FirstName + " " + obj.LastName;
                objbean.Mobile = obj.Mobile;
                objbean.Cid = obj.CounselorName;
                objbean.branchid = obj.BranchId;
                if (db.Users.Where(z => z.Id == objbean.Cid).Select(z => z.Fullname).Count() > 0)
                {
                    objbean.Counselor = db.Users.Where(z => z.Id == objbean.Cid).Select(z => z.Fullname).First();
                }
                if (objbean.TestType == "Enquiry")
                {
                    if (objbean.EntryType == "Enquiry")
                    {

                        if (db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "VisaIELTS").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                        {
                            DateTime? dt = db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "VisaIELTS").OrderByDescending(z => z.Id).Select(z => z.NextDate).First();
                            objbean.followupdate = dt;
                        }

                        if (db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "VisaIELTS").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                        {
                            string remarks = db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "VisaIELTS").OrderByDescending(z => z.Id).Select(z => z.Remarks).First();
                            objbean.remarks = remarks;
                            objbean.status = db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "VisaIELTS").OrderByDescending(z => z.Id).Select(z => z.Enqstatus).First();
                        }

                    }
                }

                lstbean.Add(objbean);

            }


            //here data is shown only when is visa prospective is true start here(29-08-2013)
            List<MaEnquiry> lstenqvisaprospctve = db.MaEnquiries.Where(z => z.IsVisaProspect == true).ToList();

            foreach (MaEnquiry obj in lstenqvisaprospctve)
            {
                VisaStudentBean objbean = new VisaStudentBean();
                objbean.EntryType = "Enquiry";
                objbean.MockIELTSid = obj.Id;
                objbean.StudentId = obj.Id;
                objbean.TestType = "Any";
                objbean.enquiryid = obj.Id;
                objbean.rollno = obj.Id;
                objbean.EnqryOvlscr = obj.IELTSExmOverall;
                objbean.SName = obj.FirstName + " " + obj.LastName;
                objbean.Mobile = obj.Mobile;
                objbean.Cid = obj.CounselorName;
                objbean.branchid = obj.BranchId;

                if (objbean.TestType == "Any")
                {
                    if (objbean.EntryType == "Enquiry")
                    {
                        if (db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "VisaEnquiry").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                        {
                            string remarks = db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "VisaEnquiry").OrderByDescending(z => z.Id).Select(z => z.Remarks).First();
                            objbean.remarks = remarks;
                            objbean.status = db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "VisaEnquiry").OrderByDescending(z => z.Id).Select(z => z.Enqstatus).First();
                            objbean.followupdate = db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "VisaEnquiry").OrderByDescending(z => z.Id).Select(z => z.NextDate).First();
                        }
                    }

                }
                objbean.Counselor = db.Users.Where(z => z.Id == objbean.Cid).Select(z => z.Fullname).First();
                lstbean.Add(objbean);

            }
            //here data is shown only when is visa prospective is true end here



            //get list from Admission Table for IELTS students

            List<MaAdmission> lstadminielts = db.MaAdmissions.Where(z => z.IsMoveToVisa == true && z.IELTSTestId != null).ToList();

            foreach (MaAdmission obj in lstadminielts)
            {
                VisaStudentBean objbean = new VisaStudentBean();
                objbean.EntryType = "Admission";
                objbean.MockIELTSid = obj.IELTSTestId;
                objbean.admissionid = obj.Id;
                objbean.StudentId = Convert.ToInt32(obj.RollNo);
                objbean.TestType = "IELTS";
                objbean.rollno = obj.RollNo;
                objbean.SName = obj.FirstName + " " + obj.LastName;
                objbean.Mobile = obj.Mobile;

                if (db.Tb_IELTSResult.Where(z => z.Id == obj.IELTSTestId).Select(z => z.UserId).Count() > 0)
                {
                    objbean.Cid = db.Tb_IELTSResult.Where(z => z.Id == obj.IELTSTestId).Select(z => z.UserId).First();

                    objbean.branchid = db.Tb_IELTSResult.Where(z => z.Id == obj.IELTSTestId).Select(z => z.BranchId).First();

                    objbean.Counselor = db.Users.Where(z => z.Id == objbean.Cid).Select(z => z.Fullname).First();
                }

                if (objbean.TestType == "IELTS")
                {
                    if (objbean.EntryType == "Admission")
                    {

                        if (db.MaEnqFollows.Where(z => z.ReferId == obj.IELTSTestId && z.FollowUpType == "IELTS").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                        {
                            DateTime? dt = db.MaEnqFollows.Where(z => z.ReferId == obj.IELTSTestId && z.FollowUpType == "IELTS").OrderByDescending(z => z.Id).Select(z => z.NextDate).First();
                            objbean.followupdate = dt;
                        }

                        if (db.MaEnqFollows.Where(z => z.ReferId == obj.IELTSTestId && z.FollowUpType == "IELTS").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                        {
                            string remarks = db.MaEnqFollows.Where(z => z.ReferId == obj.IELTSTestId && z.FollowUpType == "IELTS").OrderByDescending(z => z.Id).Select(z => z.Remarks).First();
                            objbean.remarks = remarks;
                            objbean.status = db.MaEnqFollows.Where(z => z.ReferId == obj.IELTSTestId && z.FollowUpType == "IELTS").OrderByDescending(z => z.Id).Select(z => z.Enqstatus).First();
                        }

                    }
                }

                lstbean.Add(objbean);
            }


            //get list from admission table for MockTest
            List<MaAdmission> lstadminmock = db.MaAdmissions.Where(z => z.IsMoveToVisa == true && z.MockTestId != null).ToList();

            foreach (MaAdmission obj in lstadminmock)
            {
                VisaStudentBean objbean = new VisaStudentBean();
                objbean.EntryType = "Admission";
                objbean.MockIELTSid = obj.MockTestId;
                objbean.StudentId = Convert.ToInt32(obj.RollNo);
                objbean.rollno = obj.RollNo;
                objbean.admissionid = obj.Id;
                objbean.TestType = "MockTest";
                objbean.SName = obj.FirstName + " " + obj.LastName;
                objbean.Mobile = obj.Mobile;
                if (db.Tb_MockTest.Where(z => z.Id == obj.MockTestId).Select(z => z.UserId).Count() > 0)
                {
                    objbean.Cid = db.Tb_MockTest.Where(z => z.Id == obj.MockTestId).Select(z => z.UserId).First();

                    objbean.branchid = db.Tb_MockTest.Where(z => z.Id == obj.MockTestId).Select(z => z.BranchId).First();

                    objbean.Counselor = db.Users.Where(z => z.Id == objbean.Cid).Select(z => z.Fullname).First();
                }
                if (objbean.TestType == "MockTest")
                {
                    if (objbean.EntryType == "Admission")
                    {

                        if (db.MaEnqFollows.Where(z => z.ReferId == obj.MockTestId && z.FollowUpType == "VisaMockTest").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                        {
                            DateTime? dt = db.MaEnqFollows.Where(z => z.ReferId == obj.MockTestId && z.FollowUpType == "VisaMockTest").OrderByDescending(z => z.Id).Select(z => z.NextDate).First();
                            objbean.followupdate = dt;
                        }

                        if (db.MaEnqFollows.Where(z => z.ReferId == obj.MockTestId && z.FollowUpType == "VisaMockTest").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
                        {
                            string remarks = db.MaEnqFollows.Where(z => z.ReferId == obj.MockTestId && z.FollowUpType == "VisaMockTest").OrderByDescending(z => z.Id).Select(z => z.Remarks).First();
                            objbean.remarks = remarks;

                            objbean.status = db.MaEnqFollows.Where(z => z.ReferId == obj.MockTestId && z.FollowUpType == "VisaMockTest").OrderByDescending(z => z.Id).Select(z => z.Enqstatus).First();
                        }


                    }
                }
                lstbean.Add(objbean);
            }
            return lstbean;
        }

        [HttpPost]
        public ActionResult TransferVisaStudent(Tb_VisaFollowUp objvisafollow)
        {
            List<VisaStudentBean> lstbean = GetVisaList();

            List<Tb_VisaFollowUp> _lstvisafollow = new List<Tb_VisaFollowUp>();
            foreach (var obj in lstbean)
            {
                objvisafollow = new Tb_VisaFollowUp();
                objvisafollow.LastFollowBy = obj.Cid.ToString();
                objvisafollow.Mobileno = obj.Mobile;
                objvisafollow.Name = obj.SName;
                objvisafollow.NextFollowdate = obj.followupdate;
                objvisafollow.Referenceno = obj.rollno;
                objvisafollow.ReferId = obj.MockIELTSid;
                objvisafollow.Remarks = obj.remarks;
                objvisafollow.FollowStatus = obj.status;

                objvisafollow.Branchid = obj.branchid;
                objvisafollow.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;

                objvisafollow.entertype = obj.EntryType;

                if (obj.TestType == "Any")
                {
                    objvisafollow.Followtype = "VisaEnquiry";
                }
                else
                {
                    objvisafollow.Followtype = obj.TestType;
                }
                try
                {
                    objvisafollow.Admissionid = obj.admissionid;
                }
                catch (Exception ex)
                { }

                try
                {
                    objvisafollow.Enquiryid = obj.enquiryid;
                }
                catch (Exception ex)
                { }

                db.Tb_VisaFollowUp.Add(objvisafollow);
                db.SaveChanges();
            }

            return RedirectToAction("TransferVisaStudent");
        }


    }
}