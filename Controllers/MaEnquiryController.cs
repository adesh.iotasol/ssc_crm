﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Configuration;
using System.Web.Http;
using System.Data.SqlClient;

namespace SSCMvc.Controllers
{
    [System.Web.Mvc.Authorize]
    public class MaEnquiryController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        CommonUtil cu = new CommonUtil();
        //
        // GET: /MaEnquiry/

        public ActionResult Index(int enqid = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Enquiry, UserAction.View);
                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            //enquiry made global on 25 june removed check of where
            //.Where(m => m.BranchId == branchid)

            DateTime dt = DateTime.Now.AddDays(-1);

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            List<MaEnquiry> maenquiries = new List<MaEnquiry>();
            if (enqid > 0)
            {
                maenquiries = db.MaEnquiries.Where(z => z.Id == enqid).OrderByDescending(z => z.Id).ToList();

            }
            else
            {
                if (Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeNewCounselor)
                {
                    maenquiries = db.MaEnquiries.Where(m => m.Cdate > dt).OrderByDescending(z => z.Id).ToList();
                }
            }
            //List<MaEnquiry> maenquiries = new List<MaEnquiry>();

            ViewBag.Enqst = "All";
            // ViewBag.isadmission = db.MaEnquiries.Where(z => z.IsAdmission == true).ToList();
            ViewBag.TotalCount = maenquiries.Count;
            ViewBag.UserList = new SelectList(cu.GetUserList(branchid, 1), "Id", "username");
            ViewBag.CampusList = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.CourseGroup = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout");
            return View(maenquiries.ToList());
        }

        public ActionResult UpdateConversion(string sve = "")
        {
            if (sve == "cte")
            {
                ViewBag.message = "Successfully Done";
            }
            if (sve == "error")
            {
                ViewBag.error = "Error Occured";
            }
            return View();
        }

        public ActionResult UpdateConversionPost()
        {
            try
            {
                string cs = System.Configuration.ConfigurationManager.ConnectionStrings["SSCWebQuery"].ConnectionString;
                var connection = new SqlConnection(cs);
                SqlCommand command = new SqlCommand("UpdateConversion", connection);
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                return RedirectToAction("UpdateConversion", new { @sve = "cte" });
            }
            catch (Exception)
            {
                return RedirectToAction("UpdateConversion", new { @sve = "error" });
            }

        }

        public ActionResult TransferOldCounselorEnq(string sve = "", string leftuser = "", string EnqAbt = "")
        {
            int? branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.UserListLeft = new SelectList(cu.GetUserListLeft(branchid, 0), "Id", "username", leftuser);
            ViewBag.UserListExist = new SelectList(cu.GetUserList(branchid, 0), "Id", "username");
            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout", EnqAbt);
            List<MaEnquiry> lis = new List<MaEnquiry>();
            if (sve != "")
            {
                ViewBag.Mesg = "Enquiries Successfully transfered";
            }
            if (leftuser != "")
            {
                int LftId = Convert.ToInt32(leftuser);
                lis = db.MaEnquiries.Where(x => x.EnqHandledBy == LftId && x.FEnqstatus == "Open" && x.EnqAbt.Contains(EnqAbt) && x.IsAdmission == false).ToList();
            }
            else
            {
                lis = db.MaEnquiries.Where(x => x.FEnqstatus == "Open" && x.EnqAbt.Contains(EnqAbt) && x.IsAdmission == false).ToList();
            }
            return View(lis);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult TransferOldEnq(FormCollection form)
        {
            try
            {
                string chbranch = Request.Cookies["chbranch"].Value;

                if (chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

                int user = Convert.ToInt32(form["UserListExist"]);
                string forms = form["Status"].ToString();

                var Listt = form["Status"].ToString().Split(',').Count();
                for (int i = 0; i <= Listt - 1; i++)
                {
                    try
                    {
                        int Idd = Convert.ToInt32(form["Status"].ToString().Split(',')[i]);
                        MaEnquiry enq = db.MaEnquiries.Find(Idd);
                        enq.TransferId = user;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return RedirectToAction("TransferOldCounselorEnq", new { @sve = "trsf" });
        }


        [System.Web.Mvc.AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetAreaList(string city)
        {
            db = new SSCWebDBEntities1();
            int cid = Convert.ToInt32(city);
            var courselist = db.Ma_Area.Where(z => z.CityId == cid).OrderBy(x => x.Area).ToList();
            var colourData = courselist.Select(c => new SelectListItem()
            {
                Text = c.Area,
                Value = c.Id.ToString(),

            });
            return Json(colourData, JsonRequestBehavior.AllowGet);

        }

        //here is to get remarks method
        [System.Web.Mvc.AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetRemarks(string id)
        {
            //int referid = Convert.ToInt32(id);
            //  string remarks = db.MaEnqFollows.Where(z => z.ReferId == referid && z.FollowUpType == "MockTest").OrderByDescending(z => z.Id).Select(z => z.Remarks).First();

            string remarks = id;
            if (remarks.Length > 0)
            {
                var colourData = new
                {
                    showremarks = remarks
                };
                return Json(colourData, JsonRequestBehavior.AllowGet);

            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }



        public ActionResult DemoConversion(string dt1 = "", string dt2 = "", string type = "", string groupchoice = "", string Printtype = "", string Status = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.DemoConversion, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }




            if (dt1 == "" || dt2 == "")
            {

                ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
                ViewBag.tdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1.0).Date.ToShortDateString();
                return View();
            }

            var Business = new RecieptBuisness();
            var reportViewModel = Business.Getdemoconversion(dt1, dt2, type, groupchoice, Printtype, Status);

            var renderedBytes = reportViewModel.RenderReport();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);


        }


        //
        // GET: /MaEnquiry/Details/5

        public ActionResult Details(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Enquiry, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            MaEnquiry maenquiry = db.MaEnquiries.Find(id);
            if (maenquiry == null)
            {
                return HttpNotFound();
            }
            return View(maenquiry);
        }

        public ActionResult HighestQualifi(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        public ActionResult _EnqEduDetail(int id)
        {

            return PartialView(db.Tb_Highestqual.Where(z => z.EnquiryId == id).OrderByDescending(z => z.YearOfPassing).ToList());
        }

        public ActionResult _HighestQuali(int id)
        {

            return PartialView(db.Tb_Highestqual.Where(z => z.EnquiryId == id).OrderByDescending(z => z.Id).ToList());
        }

        public ActionResult EmplHistCreate(int id)
        {
            ViewBag.Id = id;
            return View("EmplHist");
        }

        public ActionResult _EmplHistList(int id)
        {
            return PartialView(db.Tb_EmployHist.Where(z => z.EnquiryId == id).OrderByDescending(z => z.Id).ToList());
        }


        /// <summary>
        /// creation for Tb_Highestqual starts here.
        /// </summary>
        /// <param name="highestqual"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        public ActionResult HighestQualifi(Tb_Highestqual highestquali, int id)
        {
            MaEnquiry enquir = db.MaEnquiries.Find(id);
            highestquali.EnquiryId = enquir.Id;
            highestquali.Status = true;
            highestquali.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;

            int? srno = db.Tb_Highestqual.Where(z => z.EnquiryId == enquir.Id).Select(z => z.SrNo).Max();
            if (srno == null || srno == 0)
            {
                srno = 1;
            }
            else
            {
                srno = srno + 1;
            }
            highestquali.SrNo = srno;

            db.Tb_Highestqual.Add(highestquali);
            db.SaveChanges();
            if (highestquali.GoToAdm == false)
            {
                return RedirectToAction("HighestQualifi");
            }
            else
            {
                return RedirectToAction("Create", "MaAdmission", new { @id = highestquali.EnquiryId });
            }
        }
        // creation for Tb_Highestqual ends here.


        /// <summary>
        /// edit for Tb_Highestqual start here.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult HighestQualifiEdit(int id = 0)
        {
            Tb_Highestqual highestquali = db.Tb_Highestqual.Find(id);
            return View(highestquali);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult HighestQualifiEdit(Tb_Highestqual highestquali, int id, string course = "No")
        {
            if (ModelState.IsValid)
            {
                db.Entry(highestquali).State = System.Data.Entity.EntityState.Modified;
                highestquali.Status = true;
                int enqid = (int)db.Tb_Highestqual.Where(z => z.Id == id).Select(z => z.EnquiryId).First();
                highestquali.EnquiryId = enqid;
                db.SaveChanges();
                if (course != "No")
                {
                    return RedirectToAction("Edit", new { @id = enqid });
                }
                return RedirectToAction("Index");
            }

            return View(highestquali);
        }
        // edit for Tb_Highestqual end here.


        // delete for Tb_Highestqual start here 
        [System.Web.Mvc.AcceptVerbs(HttpVerbs.Post)]

        public ActionResult DelHighestQualifi(int id)
        {
            try
            {
                Tb_Highestqual highestquali = db.Tb_Highestqual.Find(id);
                db.Tb_Highestqual.Remove(highestquali);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }
        //delete for Tb_Highestqual end here  




        //creation for Tb_EmployHist  starts here.

        [System.Web.Mvc.HttpPost]
        public ActionResult EmplHistCreate(Tb_EmployHist EmployHist, int id)
        {
            EmployHist.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            EmployHist.Status = true;
            EmployHist.EnquiryId = id;
            int? srno = db.Tb_EmployHist.Where(z => z.EnquiryId == id).Select(z => z.SrNo).Max();
            if (srno == null || srno == 0)
            {
                srno = 1;
            }
            else
            {
                srno = srno + 1;
            }
            EmployHist.SrNo = srno;
            db.Tb_EmployHist.Add(EmployHist);
            db.SaveChanges();
            return RedirectToAction("EmplHistCreate");

        }
        //creation for Tb_EmployHist  ends here.


        //edit for Tb_EmployHist starts here
        public ActionResult EmplHistedit(int id = 0)
        {
            Tb_EmployHist employhis = db.Tb_EmployHist.Find(id);

            return View(employhis);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult EmplHistedit(Tb_EmployHist employhis, string course = "No")
        {
            try
            {
                db.Entry(employhis).State = System.Data.Entity.EntityState.Modified;
                employhis.Status = true;
                db.SaveChanges();

                if (course != "No")
                {
                    return RedirectToAction("Edit", new { @id = employhis.EnquiryId });
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex) { }

            return View(employhis);
        }
        //edit for Tb_EmployHist ends here



        #region Partial_View_Lists

        public ActionResult _EmplHistEdit(int id = 0)
        {
            Tb_EmployHist employhis = db.Tb_EmployHist.Find(id);

            return PartialView(employhis);
        }

        public ActionResult _DemoHistory(int id = 0)
        {
            List<Tb_DemoHistory> obj = db.Tb_DemoHistory.Where(z => z.EnquiryId == id).ToList();
            return PartialView(obj);
        }


        [System.Web.Mvc.HttpPost]
        public ActionResult _EmplHistEdit(Tb_EmployHist employhis, int Id)
        {
            try
            {
                employhis.EnquiryId = Id;
                int? srno = db.Tb_EmployHist.Where(z => z.EnquiryId == Id).Select(z => z.SrNo).Max();
                if (srno == null || srno == 0)
                {
                    srno = 1;
                }
                else
                {
                    srno = srno + 1;
                }
                employhis.SrNo = srno;
                employhis.Status = true;
                employhis.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                db.Tb_EmployHist.Add(employhis);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex) { }
            return View(employhis);
        }




        public ActionResult _HighestQualiEdit(int id = 0)
        {
            Tb_Highestqual highestquali = db.Tb_Highestqual.Find(id);
            return PartialView(highestquali);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult _HighestQualiEdit(Tb_Highestqual highestquali, int enqid)
        {
            try
            {

                highestquali.EnquiryId = enqid;
                highestquali.Status = true;
                int? srno = db.Tb_Highestqual.Where(z => z.EnquiryId == enqid).Select(z => z.SrNo).Max();
                if (srno == null || srno == 0)
                {
                    srno = 1;
                }
                else
                {
                    srno = srno + 1;
                }
                highestquali.SrNo = srno;
                db.Tb_Highestqual.Add(highestquali);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex) { }
            return PartialView(highestquali);
        }



        #endregion


        public ActionResult CopyEnq(int id)
        {

            int newid = 0;
            MaEnquiry enq = db.MaEnquiries.Find(id);
            MaEnquiry newcopy = enq;
            newcopy.CopyFrom = enq.Id;
            db.MaEnquiries.Add(newcopy);
            db.SaveChanges();
            List<Tb_Highestqual> q = db.Tb_Highestqual.Where(w => w.EnquiryId == id).ToList();
            foreach (Tb_Highestqual item in q)
            {
                Tb_Highestqual obj = new Tb_Highestqual();
                obj.EnquiryId = newcopy.Id;
                obj.Qualification = item.Qualification;
                obj.PassingYear = item.PassingYear;
                obj.YearOfPassing = item.YearOfPassing;
                obj.Board = item.Board;
                obj.CGPA = item.CGPA;
                obj.Cdate = item.Cdate;
                db.Tb_Highestqual.Add(obj);
            }
            db.SaveChanges();

            newid = newcopy.Id;
            return Redirect($"/MaEnquiry/edit/{newid}?uaction=copy");
        }


        // GET: /MaEnquiry/Create
        public ActionResult Create()
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Enquiry, UserAction.Add);
                string chbranch = Request.Cookies["chbranch"].Value;


                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            int id = db.Users.Where(m => m.username == User.Identity.Name).Select(m => m.Id).Single();
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            //ViewBag.CounselorName = new SelectList(db.Users, "Id", "Fullname", id);
            MaEnquiry objEnq = new MaEnquiry();
            ViewBag.CounselorName = db.Users.Where(z => z.Id == id).Select(z => z.Fullname).First();
            objEnq.CounselorName = id;
            ViewBag.ReferenceId = new SelectList(db.MaFrmReferences, "Id", "ContactPerson");
            ViewBag.EnqType = new SelectList(db.St_EnquiryType, "StatusName", "StatusName");
            ViewBag.EnqHandle = new SelectList(cu.GetUserList(branchid), "Id", "username");
            //ViewBag.EnqHandle = new SelectList(cu.GetUserList(branchid), "Id", "username");
            ViewBag.Grouptime = new SelectList(db.MaGroupTime_View, "Timeid", "BatchTime");
            ViewBag.Demobranchid = new SelectList(db.MaBranches, "Id", "BramchName", branchid);

            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout");
            ViewBag.GroupChoice = new SelectList(db.St_CourseGroup, "GroupName", "GroupName");

            ViewBag.AreaId = new SelectList(db.Ma_Area.OrderBy(x => x.Area), "Id", "Area");
            ViewBag.CityId = new SelectList(db.MaCities, "Id", "CityName");
            ViewBag.VisaType = new SelectList(db.St_VisaType.Where(w => w.VisaType == "-"), "VisaType", "VisaType");
            return View(objEnq);
        }

        //
        // POST: /MaEnquiry/Create

        [System.Web.Mvc.HttpPost]
        public ActionResult Create(MaEnquiry maenquiry, FormCollection form, HttpPostedFileBase File)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Enquiry, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            string brachname = db.MaBranches.Where(z => z.Id == branchid).Select(z => z.BramchName).First();
            //if (ModelState.IsValid)
            //{

            // commented by gourav for changes Checkbox to Dropdown in Enq. About   at 21-Dec-2015
            //if (maenquiry.EnquiryIELAC == true || maenquiry.EnquiryIELGT == true || maenquiry.EnquirySpkEng == true || maenquiry.EnquiryStyVisa == true || maenquiry.EnqPPE == true || maenquiry.IsExamBook)

            // New Condition apply by Gourav at 21-Dec-2015
            if (maenquiry.EnqAbt != "" || maenquiry.EnqAbt != null)
            {

                try
                {
                    maenquiry.AdmDate = CommonUtil.IndianTime();
                    maenquiry.LastUpdatedDate = CommonUtil.IndianTime();
                    maenquiry.Cdate = CommonUtil.IndianTime();
                    maenquiry.Status = true;

                    try
                    {
                        maenquiry.FirstName = maenquiry.FirstName.ToUpper();
                    }
                    catch (Exception ewx)
                    { }
                    try
                    {
                        maenquiry.LastName = maenquiry.LastName.ToUpper();
                    }

                    catch (Exception ex)
                    { }
                    try
                    {
                        maenquiry.FatherName = maenquiry.FatherName.ToUpper();
                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        maenquiry.Address = maenquiry.Address.ToUpper();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        maenquiry.EmailId = maenquiry.EmailId.ToLower();
                        //maenquiry.EmailId = maenquiry.EmailId;
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        maenquiry.FacebookId = maenquiry.FacebookId;
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        maenquiry.Remarks = maenquiry.Remarks.ToUpper();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        maenquiry.Other = maenquiry.Other.ToUpper();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        maenquiry.CityPreference = maenquiry.CityPreference.ToUpper();
                    }
                    catch (Exception erxz)
                    { }


                    int enqhandleby = 0;

                    try
                    {

                        enqhandleby = Convert.ToInt32(form["EnqHandle"].ToString());
                    }
                    catch (Exception)
                    {
                    }
                    maenquiry.EnqHandledBy = enqhandleby;
                    try
                    {
                        if (File.ContentLength > 0)
                        {
                            string gid = Guid.NewGuid().ToString().Substring(0, 4);
                            string filename = gid + File.FileName;
                            filename = filename.Replace(" ", "_");
                            filename = filename.Replace("%", "_");
                            filename = filename.Replace("#", "_");
                            File.SaveAs(System.IO.Path.Combine(Server.MapPath("/Uploads/Enquiry"), filename));
                            maenquiry.UFile = filename;
                        }
                        else
                        {
                            maenquiry.UFile = "NoImage.jpg";
                        }
                    }
                    catch (Exception ex)
                    {
                        maenquiry.UFile = "NoImage.jpg";
                    }
                    db.MaEnquiries.Add(maenquiry);
                    maenquiry.BranchId = branchid;
                    db.SaveChanges();

                    Tb_Highestqual tb_Highestqual = new Tb_Highestqual();
                    try
                    {
                        tb_Highestqual.EnquiryId = maenquiry.Id;
                        tb_Highestqual.SrNo = 1;
                        tb_Highestqual.Qualification = form["Qualification"];
                        tb_Highestqual.Board = form["Board"];
                        try
                        {
                            tb_Highestqual.CGPA = Convert.ToDouble(form["CGPA"]);
                        }
                        catch (Exception) { }

                        tb_Highestqual.YearOfPassing = form["YearOfPassing"];
                        tb_Highestqual.Cdate = DateTime.Now;
                        tb_Highestqual.Status = true;

                        db.Tb_Highestqual.Add(tb_Highestqual);
                        db.SaveChanges();
                    }
                    catch (Exception) { }

                    try
                    {
                        Status myobj = new Status();
                        string msg = $"Dear {maenquiry.FirstName}, thanx for visiting SSC. Your enquiry number is {maenquiry.Id}. For further information %26 exam registration, contact: 9876533888";
                        myobj.Sendsms(maenquiry.Mobile, msg);
                        if (maenquiry.AlternateNo != "" && maenquiry.AlternateNo != null)
                        {
                            myobj.Sendsms(maenquiry.AlternateNo, msg);
                        }
                    }
                    catch (Exception) { }


                    AddEnqFollow(maenquiry);

                    try
                    {
                        string usertype = Request.Cookies[CommonUtil.CookieUsertype].Value;

                        if (maenquiry.DemoFeeAmt >= 0)
                        {
                            Tb_DemoHistory demohistory = new Tb_DemoHistory();
                            demohistory.EnquiryId = maenquiry.Id;
                            demohistory.ClassDate = maenquiry.ClassDate;
                            demohistory.Demobranchid = maenquiry.Demobranchid;
                            demohistory.DemoBy = usertype;
                            demohistory.DemoFeeAmount = maenquiry.DemoFeeAmt;
                            demohistory.GroupChoice = maenquiry.GroupChoice;
                            demohistory.Grouptime = maenquiry.Grouptime;
                            demohistory.FeeDate = maenquiry.DemoFeeDate;
                            db.Tb_DemoHistory.Add(demohistory);
                            db.SaveChanges();
                        }

                    }
                    catch (Exception ex)
                    { }

                    /*Send email to user and reference*/
                    string situation = ConfigurationManager.AppSettings["Switch"];
                    if (situation == "On")
                    {
                        cu.SendEmailToUser(maenquiry, true);

                    }

                    //this is call when visa prospective is true
                    if (maenquiry.IsVisaProspect == true)
                    {
                        //insert into visafollowup
                        EnquiryVisaFollow(maenquiry);
                    }


                    if (maenquiry.GoToAdm == false)
                    {
                        return RedirectToAction("NextAction", new { @id = maenquiry.Id });
                    }
                    else
                    {
                        return RedirectToAction("Create", "MaAdmission", new { @id = maenquiry.Id });
                    }
                    //}
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
                ViewBag.ErrorMessage = "Plz Select Enquiry About";
            }

            int id = db.Users.Where(m => m.username == User.Identity.Name).Select(m => m.Id).Single();
            ViewBag.CounselorName = db.Users.Where(z => z.Id == id).Select(z => z.Fullname).First();
            ViewBag.ReferenceId = new SelectList(db.MaFrmReferences, "Id", "ContactPerson");
            ViewBag.EnqType = new SelectList(db.St_EnquiryType, "StatusName", "StatusName");
            ViewBag.EnqHandle = new SelectList(cu.GetUserList(branchid), "Id", "username");
            ViewBag.Grouptime = new SelectList(db.MaGroupTime_View, "Timeid", "BatchTime");
            ViewBag.Demobranchid = new SelectList(db.MaBranches, "Id", "BramchName", branchid);
            ViewBag.AreaId = new SelectList(db.Ma_Area.OrderBy(x => x.Area), "Id", "Area");
            ViewBag.CityId = new SelectList(db.MaCities.OrderBy(x => x.CityName), "Id", "CityName");
            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout");
            ViewBag.GroupChoice = new SelectList(db.St_CourseGroup, "GroupName", "GroupName");
            ViewBag.VisaType = new SelectList(db.St_VisaType.Where(w => w.VisaType == "-"), "VisaType", "VisaType");
            return View(maenquiry);
        }
        //insert entry into maenqfollow
        private void AddEnqFollow(MaEnquiry maenquiry)
        {
            try
            {
                MaEnqFollow obj = new MaEnqFollow();
                obj.NextDate = maenquiry.WalkInDate.Value.AddDays(1);
                obj.CDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
                obj.EnqFollowBy = maenquiry.CounselorName;
                obj.ReferId = maenquiry.Id;
                obj.FollowUpType = "Enquiry";
                obj.Enqstatus = "Open";
                if (maenquiry.EnqAbt == "EXAM BOOKING")
                {
                    obj.Enqstatus = "Close";
                }


                if (maenquiry.Remarks == "" || maenquiry.Remarks == null)
                {
                    obj.Remarks = "New Enquiry On" + string.Format("{0: dd MMM,yyyy HH:mm tt}", maenquiry.WalkInDate);
                }
                else
                {
                    obj.Remarks = maenquiry.Remarks;
                }

                db.MaEnqFollows.Add(obj);
                db.SaveChanges();
                maenquiry.FEnqstatus = obj.Enqstatus;
                maenquiry.FNextDate = maenquiry.WalkInDate.Value.AddDays(1);
                maenquiry.FRemarks = obj.Remarks;
                maenquiry.FCDate = maenquiry.WalkInDate;
                maenquiry.FEnqFollowBy = maenquiry.CounselorName;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                System.IO.StreamWriter sw = System.IO.File.AppendText(Server.MapPath("//Uploads/error.txt"));
                sw.WriteLine($"Error on {DateTime.Now} {ex.Message} Enq. No {maenquiry.Id} ");
                sw.Close();
            }

        }

        //insert entry into visafollow
        private void EnquiryVisaFollow(MaEnquiry maaenquiry)
        {
            try
            {
                int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

                Tb_VisaFollowUp objvisa = new Tb_VisaFollowUp();
                objvisa.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(2);
                objvisa.FollowStatus = "Open";
                objvisa.Followtype = "VisaEnquiry";
                objvisa.entertype = "Enquiry";
                objvisa.Remarks = "New Enquiry On" + string.Format("{0: dd MMM,yyyy HH:mm tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference));
                objvisa.NextFollowdate = maaenquiry.WalkInDate.Value.AddDays(1);
                objvisa.Branchid = branchid;
                objvisa.Enquiryid = maaenquiry.Id;
                objvisa.LastFollowBy = maaenquiry.CounselorName.ToString();
                objvisa.Name = maaenquiry.FirstName + " " + maaenquiry.LastName;
                objvisa.Referenceno = maaenquiry.Id;
                objvisa.ReferId = maaenquiry.Id;
                objvisa.Mobileno = maaenquiry.Mobile;
                db.Tb_VisaFollowUp.Add(objvisa);
                db.SaveChanges();
            }
            catch (Exception) { }


        }

        [System.Web.Mvc.AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetVisaType(string enqabt)
        {

            var itemlist = db.St_VisaType.Where(w => w.EnqAbt == enqabt);

            var ItemcaytData = itemlist.Select(c => new SelectListItem()
            {
                Text = c.VisaType,
                Value = c.VisaType
            });
            return Json(ItemcaytData, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /MaEnquiry/Edit/5

        public ActionResult Edit(int id = 0, string uaction = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Enquiry, UserAction.Edit);
                string chbranch = Request.Cookies["chbranch"].Value;


                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (uaction == "copy")
            {
                ViewBag.Iscopy = "Y";
            }
            else
            {
                ViewBag.Iscopy = "N";
            }

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            MaEnquiry maenquiry = db.MaEnquiries.Find(id);
            if (maenquiry == null)
            {
                return HttpNotFound();
            }
            _DemoHistory(id);
            //here check the visaprospective value is true or false
            TempData["isprospective"] = maenquiry.IsVisaProspect;


            ViewBag.ReferenceId = new SelectList(db.MaFrmReferences, "Id", "ContactPerson", maenquiry.ReferenceId);
            ViewBag.CounselorName = db.Users.Where(z => z.Id == maenquiry.CounselorName).Select(z => z.Fullname).First();

            User objuser = Getenqhandledby(Convert.ToInt32(maenquiry.EnqHandledBy));

            ViewBag.EnqHandle = objuser.username;
            if (uaction == "copy")
            {
                ViewBag.EnqHandledBy = new SelectList(cu.GetUserList(0), "Id", "username");
                maenquiry.EnqHandledBy = 0;
            }
            else
            {
                ViewBag.EnqHandledBy = new SelectList(cu.GetUserList(0), "Id", "username", maenquiry.EnqHandledBy);
            }

            ViewBag.VisaType = new SelectList(db.St_VisaType, "VisaType", "VisaType", maenquiry.VisaType);

            ViewBag.Grouptime = new SelectList(db.MaGroupTime_View, "Timeid", "BatchTime", maenquiry.Grouptime);
            ViewBag.EnqType = new SelectList(db.St_EnquiryType, "StatusName", "StatusName", maenquiry.EnqType);
            ViewBag.Demobranchid = new SelectList(db.MaBranches, "Id", "BramchName", maenquiry.Demobranchid);
            ViewBag.AreaId = new SelectList(db.Ma_Area.OrderBy(x => x.Area), "Id", "Area", maenquiry.AreaId);
            ViewBag.CityId = new SelectList(db.MaCities, "Id", "CityName", maenquiry.CityId);
            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout", maenquiry.EnqAbt);
            ViewBag.GroupChoice = new SelectList(db.St_CourseGroup, "GroupName", "GroupName", maenquiry.GroupChoice);
            return View(maenquiry);
        }

        public User Getenqhandledby(int id)
        {
            return db.Users.Where(z => z.Id == id).FirstOrDefault();
        }

        //
        // POST: /MaEnquiry/Edit/5

        [System.Web.Mvc.HttpPost]
        public ActionResult Edit(MaEnquiry maenquiry, FormCollection form, HttpPostedFileBase File)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Enquiry, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            //if (ModelState.IsValid)
            //{
            try
            {
                if (File.ContentLength > 0)
                {
                    string gid = Guid.NewGuid().ToString().Substring(0, 4);
                    string filename = gid + File.FileName;
                    filename = filename.Replace(" ", "_");
                    filename = filename.Replace("%", "_");
                    filename = filename.Replace("#", "_");
                    File.SaveAs(System.IO.Path.Combine(Server.MapPath("/Uploads/Enquiry"), filename));
                    maenquiry.UFile = filename;
                }
            }
            catch (Exception ex)
            {
            }
            try
            {
                db.Entry(maenquiry).State = System.Data.Entity.EntityState.Modified;
                maenquiry.Status = true;

                try
                {
                    maenquiry.FirstName = maenquiry.FirstName.ToUpper();
                }
                catch (Exception ewx)
                { }
                try
                {
                    maenquiry.FatherName = maenquiry.FatherName.ToUpper();
                }
                catch (Exception)
                {
                }
                try
                {
                    maenquiry.LastName = maenquiry.LastName.ToUpper();
                }
                catch (Exception ex)
                { }
                try
                {
                    maenquiry.Address = maenquiry.Address.ToUpper();
                }
                catch (Exception ex)
                { }
                try
                {
                    maenquiry.EmailId = maenquiry.EmailId.ToLower();
                    //maenquiry.EmailId = maenquiry.EmailId;
                }
                catch (Exception ex)
                { }
                try
                {
                    maenquiry.FacebookId = maenquiry.FacebookId;
                }
                catch (Exception ex)
                { }
                try
                {
                    maenquiry.Remarks = maenquiry.Remarks.ToUpper();
                }
                catch (Exception ex)
                { }
                try
                {
                    maenquiry.Other = maenquiry.Other.ToUpper();
                }
                catch (Exception ex)
                { }
                try
                {
                    maenquiry.CityPreference = maenquiry.CityPreference.ToUpper();
                }
                catch (Exception erxz)
                { }
                //maenquiry.Cdate = DateTime.Now.Date;
                maenquiry.LastUpdatedDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                // int enqhandleby = Convert.ToInt32(form["EnqHandle"].ToString());
                //  User objuser = Getenqhandledby(Convert.ToInt32(maenquiry.EnqHandledBy));

                // maenquiry.EnqHandledBy = objuser.Id;

                db.SaveChanges();
                if (db.MaAdmissions.Where(z => z.EnquiryId == maenquiry.Id).Count() == 0)
                {
                    UpdateEnqFollow(maenquiry);
                }
                try
                {
                    Tb_DemoHistory obj = new Tb_DemoHistory();
                    obj.ClassDate = maenquiry.ClassDate;
                    obj.Demobranchid = maenquiry.Demobranchid;
                    obj.DemoBy = maenquiry.DemoBy;
                    obj.DemoFeeAmount = maenquiry.DemoFeeAmt;
                    obj.EnquiryId = maenquiry.Id;
                    obj.FeeDate = maenquiry.DemoFeeDate;
                    obj.GroupChoice = maenquiry.GroupChoice;
                    obj.Grouptime = maenquiry.Grouptime;
                    db.Tb_DemoHistory.Add(obj);
                    db.SaveChanges();

                }
                catch (Exception ex)
                { }
                bool IeltsACValue = maenquiry.EnquiryIELAC;
                bool IeltsGTValue = maenquiry.EnquiryIELGT;
                bool StudyVisa = maenquiry.EnquiryStyVisa;

                bool chck = Convert.ToBoolean(TempData["isprospective"]);
                if (chck == false)
                {
                    if (maenquiry.IsVisaProspect == true)
                    {
                        //AddEnqFollowVisa(maenquiry);
                        EnquiryVisaFollow(maenquiry);
                    }
                }


                if (IeltsACValue)
                {
                    return RedirectToAction("CallHighest", new { @id = maenquiry.Id });
                }
                else if (IeltsGTValue)
                {
                    return RedirectToAction("CallHighest", new { @id = maenquiry.Id });
                }
                else if (StudyVisa)
                {
                    return RedirectToAction("CallHighest", new { @id = maenquiry.Id });
                }
                if (maenquiry.GoToAdm == false)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Create", "MaAdmission", new { @id = maenquiry.Id });
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var validationErrors in e.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
            }

            ViewBag.VisaType = new SelectList(db.St_VisaType, "VisaType", "VisaType", maenquiry.VisaType);
            ViewBag.Grouptime = new SelectList(db.MaGroupTime_View, "Timeid", "BatchTime", maenquiry.Grouptime);
            ViewBag.EnqType = new SelectList(db.St_EnquiryType, "StatusName", "StatusName", maenquiry.EnqType);
            ViewBag.Demobranchid = new SelectList(db.MaBranches, "Id", "BramchName", maenquiry.Demobranchid);
            ViewBag.AreaId = new SelectList(db.Ma_Area.OrderBy(x => x.Area), "Id", "Area", maenquiry.AreaId);
            ViewBag.CityId = new SelectList(db.MaCities, "Id", "CityName", maenquiry.CityId);
            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout", maenquiry.EnqAbt);
            ViewBag.GroupChoice = new SelectList(db.St_CourseGroup, "GroupName", "GroupName", maenquiry.GroupChoice);
            ViewBag.EnqHandledBy = new SelectList(cu.GetUserList(branchid), "Id", "username");
            return View(maenquiry);
        }

        private void UpdateEnqFollow(MaEnquiry maenquiry)
        {
            if (db.MaEnqFollows.Where(z => z.ReferId == maenquiry.Id && z.FollowUpType == "Enquiry").Count() > 0)
            {
                MaEnqFollow obj = db.MaEnqFollows.Where(z => z.ReferId == maenquiry.Id && z.FollowUpType == "Enquiry").First();
                obj.NextDate = maenquiry.WalkInDate.Value.AddDays(1);
                obj.CDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
                obj.EnqFollowBy = maenquiry.CounselorName;
                obj.ReferId = maenquiry.Id;
                obj.FollowUpType = "Enquiry";
                obj.Enqstatus = "Open";
                obj.Remarks = "New Enquiry On" + string.Format("{0: dd MMM,yyyy HH:mm tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference));

                db.SaveChanges();
            }
        }
        //
        // GET: /MaEnquiry/Delete/5

        [System.Web.Mvc.AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Delete(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Enquiry, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            try
            {
                MaDesignation mabdesig = db.MaDesignations.Find(id);
                db.MaDesignations.Remove(mabdesig);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }


        [System.Web.Mvc.AcceptVerbs(HttpVerbs.Post)]

        public ActionResult DeleteEmpHist(int id)
        {
            try
            {
                Tb_EmployHist emphist = db.Tb_EmployHist.Find(id);
                db.Tb_EmployHist.Remove(emphist);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        public ActionResult _EnquiryDetail(int id)
        {
            MaEnquiry maenq = db.MaEnquiries.Find(id);

            return PartialView(maenq);
        }

        public ActionResult CallHighest(int id)
        {


            return RedirectToAction("HighestQualifi", new { @id = id });
        }

        //public ActionResult Searchbyenquiryno(string enqno)
        //{
        //    int no = 0;
        //    try
        //    {
        //        no = Convert.ToInt32(enqno);
        //    }
        //    catch (Exception ex)
        //    {
        //        no = 0;
        //    }

        //    List<MaEnquiry> lst = db.MaEnquiries.Where(m => m.Id == no).OrderByDescending(z => z.Id).ToList();
        //    ViewBag.TotalCount = lst.Count;
        //    int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
        //    ViewBag.EnqNo = no;
        //    ViewBag.UserList = new SelectList(cu.GetUserList(branchid, 1), "Id", "username");

        //    return View("Searchforenquiry", lst);
        //}



        //Previously working on search enquiry with fully control and it change into new filters on 21-Dec-2015 by Gourav
        //public ActionResult Searchforenquiry(string enqno, string name, string mobile, string date = "", string tdate = "", string usr = "", string enqstatus = "", string enquiryabout = "")
        //{
        //    //enquiry made global on 25 june removed check of where
        //    int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

        //    string utype = Request.Cookies[CommonUtil.CookieUsertype].Value;

        //    List<MaEnquiry> lst = db.MaEnquiries.OrderByDescending(z => z.Id).ToList();
        //    ViewBag.UserList = new SelectList(cu.GetUserList(branchid, 1), "Id", "username");

        //    if (enqno != "")
        //    {
        //        int no = 0;
        //        try
        //        {
        //            no = Convert.ToInt32(enqno);
        //            lst = lst.Where(m => m.Id == no).OrderByDescending(z => z.Id).ToList();
        //        }
        //        catch (Exception ex)
        //        {
        //            no = 0;
        //        }
        //        ViewBag.EnqNo = no;
        //    }

        //    if (name != "")
        //    {
        //        lst = lst.Where(z => z.FirstName.ToUpper().Contains(name.ToUpper())).ToList();
        //        ViewBag.name = name;
        //    }

        //    if (mobile != "")
        //    {
        //        lst = lst.Where(z => z.Mobile.Contains(mobile)).ToList();
        //        ViewBag.mobile = mobile;
        //    }

        //    if (!utype.Equals(CommonUtil.UserTypeNewCounselor))
        //    {

        //        if (date != "" && tdate != "")
        //        {
        //            try
        //            {
        //                DateTime dat = Convert.ToDateTime(date);
        //                DateTime todate = Convert.ToDateTime(tdate);
        //                lst = lst.Where(z => z.WalkInDate >= dat && z.WalkInDate <= todate).ToList();
        //                ViewBag.fdate = date;
        //                ViewBag.tdate = tdate;

        //            }
        //            catch (Exception ex)
        //            {
        //            }
        //        }
        //        if (usr != "")
        //        {
        //            int id = Convert.ToInt32(usr);
        //            lst = lst.Where(z => z.EnqHandledBy == id).ToList();
        //            ViewBag.UserList = new SelectList(cu.GetUserList(branchid, 1), "Id", "username", id);
        //            ViewBag.usr = id;
        //        }


        //        if (enqstatus != "All" && enqstatus != "")
        //        {
        //            List<MaEnquiry> filterenq = new List<MaEnquiry>();
        //            foreach (MaEnquiry obj in lst)
        //            {
        //                if (db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).Select(z => z.NextDate).Count() > 0)
        //                {
        //                    string enqst = db.MaEnqFollows.Where(z => z.ReferId == obj.Id && z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).Select(z => z.Enqstatus).First();
        //                    if (enqstatus == enqst)
        //                    {
        //                        filterenq.Add(obj);
        //                    }

        //                }
        //            }
        //            lst = filterenq;

        //            ViewBag.Enqst = enqstatus;
        //        }


        //        if (enquiryabout != "All" && enquiryabout != "")
        //        {
        //            if (enquiryabout == "IELTS")
        //            {
        //                lst = lst.Where(z => z.EnquiryIELAC == true || z.EnquiryIELGT == true).ToList();
        //                ViewBag.enquabt = enquiryabout;
        //            }
        //            else if (enquiryabout == "SE")
        //            {
        //                lst = lst.Where(z => z._EnquirySpkEng == true).ToList();
        //                ViewBag.enquabt = enquiryabout;
        //            }
        //            else if (enquiryabout == "SV")
        //            {
        //                lst = lst.Where(z => z._EnquiryStyVisa == true).ToList();
        //                ViewBag.enquabt = enquiryabout;
        //            }
        //            else if (enquiryabout == "PTE")
        //            {
        //                lst = lst.Where(z => z.EnqPPE == true).ToList();
        //                ViewBag.enquabt = enquiryabout;
        //            }
        //            else
        //            {
        //                lst = lst.ToList();
        //                ViewBag.enquabt = "All";
        //            }
        //        }
        //    }

        //    ViewBag.TotalCount = lst.Count;

        //    return View("Searchforenquiry", lst);

        //}

        //New enqiry for search with update filters by Gourav on 21-Dec-2015

        public ActionResult Searchforenquiry(string enqno, string name, string mobile, string father = "", string usr = "", int camp = 0, string enqstatus = "", string enquiryabout = "")
        {
            //enquiry made global on 25 june removed check of where
            string camps = "%";
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            string utype = Request.Cookies[CommonUtil.CookieUsertype].Value;

            if (camp > 0)
            {
                camps = camp.ToString();
            }

            if (enqno == "")
            {
                enqno = "%";
            }

            if (name == "")
                name = "%";

            if (father == "")
                father = "%";

            if (mobile == "")
                mobile = "%";

            if (enqstatus.ToLower() == "all" || enqstatus.ToLower() == "undefined")
            {
                enqstatus = "%";
            }

            if (enquiryabout == "" || enquiryabout.ToLower() == "undefined")
            {
                enquiryabout = "%";
            }

            if (usr == "" || usr == "undefined")
                usr = "%";

            string sql = $"select * from maenquiry where convert(varchar,id) like '{enqno}' and isnull(firstname,'-') like '{name}' and isnull(fathername,'-') like '{father}' and (isnull(mobile,'-') like '{mobile}' or isnull(alternateno,'-') like '{mobile}') and isnull(convert(varchar,branchid),'-') like '{camps}' and isnull(convert(varchar,EnqHandledBy),'-') like '{usr}' and isnull(EnqAbt,'-') like '{enquiryabout}' and isnull(FEnqstatus,'-') like '{enqstatus}' order by cdate desc";

            List<MaEnquiry> lst = db.Database.SqlQuery<MaEnquiry>(sql).ToList();

            ViewBag.UserList = new SelectList(cu.GetUserList(branchid, 1), "Id", "username", usr);
            ViewBag.CampusList = new SelectList(db.MaBranches, "Id", "BramchName", camp);
            ViewBag.CourseGroup = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout", enquiryabout);

            if (enqno != "")
            {
                ViewBag.EnqNo = enqno;
            }

            if (name != "")
            {

                ViewBag.name = name;
            }

            if (father != "")
            {

                ViewBag.fname = father;
            }

            if (mobile != "")
            {

                ViewBag.mobile = mobile;
            }


            if (StaticLogic.SubRole() == "Moderate" || StaticLogic.SubRole() == "Max")
            {

                if (usr != "%")
                {
                    int id = Convert.ToInt32(usr);
                    ViewBag.UserList = new SelectList(cu.GetUserList(branchid, 1), "Id", "username", id);
                    ViewBag.usr = id;
                }
            }
            if (StaticLogic.SubRole() == "Max")
            {

                if (enqstatus != "All" && enqstatus != "undefine")
                {
                    ViewBag.Enqst = enqstatus;
                }


            }

            ViewBag.TotalCount = lst.Count;

            return View("Searchforenquiry", lst);

        }



        /*----Used for visa payment to show enquiry detail there ------*/

        public PartialViewResult _VisaEnqDetail(int id = 0)
        {
            MaEnquiry maenquiry = db.MaEnquiries.Find(id);
            if (maenquiry == null)
            {
                return PartialView();
            }
            return PartialView(maenquiry);
        }

        public ActionResult EnquiryAnalysis(string date = "", string tdate = "", string usr = "", string enqstatus = "", string enquiryabout = "", string Status = "", string enqno = "", string mobileno = "")
        {

            try
            {

                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.EnquiryAnalusis, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            List<MaEnquiry> lstenquiry = EnquiryList(date, tdate, usr, enqstatus, enquiryabout, branchid, Status, enqno, mobileno);
            ViewBag.totalenq = lstenquiry.Count;
            ViewBag.Admission = lstenquiry.Where(z => z.IsAdmission == true).Count();
            ViewBag.NonInterested = lstenquiry.Where(z => z.IsAdmission != true).Count();
            ViewBag.UserList = new SelectList(cu.GetUserList(branchid).OrderBy(x => x.username), "Id", "username");
            ViewBag.EnqListTotal = lstenquiry.Count;
            ViewBag.CourseGroup = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout", enquiryabout);
            ViewBag.fdate = date;
            ViewBag.tdate = tdate;
            return View(lstenquiry);
        }


        private List<MaEnquiry> EnquiryList(string date, string tdate, string usr, string enqstatus, string enquiryabout, int branchid, string Status, string enqno, string mobileno)
        {
            List<MaEnquiry> lstenquiry = new List<MaEnquiry>();
            MaEnqFollow foll = new MaEnqFollow();

            if (date != "" && tdate != "" || enqstatus != "All" && enqstatus != "" || usr != "" || enquiryabout != "")
            {
                //Code added by Gourav For skip record where enquiry about is Exam Booking ar 22-Dec-2015
                //lstenquiry = db.MaEnquiries.Where(z => z.BranchId == branchid && z.EnqAbt != "EXAM BOOKING").ToList();

                /*--------------------------------------Search Functionality--------------------------------*/
                if (date != "" && tdate != "")
                {
                    try
                    {

                        DateTime dt1 = Convert.ToDateTime(date);
                        DateTime dt2 = Convert.ToDateTime(tdate);
                        // ViewBag.fdate = dt1.ToString("MM/dd/yyyy");
                        // ViewBag.tdate = dt2.ToString("MM/dd/yyyy");
                        string FDt = dt1.ToString("yyyy-MM-dd");
                        string TDt = dt2.ToString("yyyy-MM-dd");
                        lstenquiry = db.MaEnquiries.Where(z => z.BranchId == branchid && DbFunctions.TruncateTime(z.AdmDate) >= dt1 && DbFunctions.TruncateTime(z.AdmDate) <= dt2).ToList();
                    }
                    catch (Exception ex)
                    { }
                }

                if (enqno != "")
                {
                    int id = Convert.ToInt32(enqno);
                    ViewBag.enqno = enqno;
                    lstenquiry = lstenquiry.Where(z => z.Id == id).ToList();
                }

                if (mobileno != "")
                {
                    ViewBag.enqno = mobileno;
                    lstenquiry = lstenquiry.Where(z => z.Mobile == mobileno || z.AlternateNo == mobileno).ToList();
                }

                if (usr != "")
                {
                    ViewBag.usrval = usr;
                    int userid = Convert.ToInt32(usr);
                    lstenquiry = lstenquiry.Where(z => z.EnqHandledBy == userid).ToList();
                }

                if (enqstatus != "")
                {
                    if (enqstatus == "Converted")
                    {
                        lstenquiry = lstenquiry.Where(z => z.IsAdmission == true).ToList();
                        ViewBag.Enqst = enqstatus;
                    }
                    else if (enqstatus == "UnConverted")
                    {
                        lstenquiry = lstenquiry.Where(z => z.IsAdmission == false).ToList();
                        ViewBag.Enqst = enqstatus;
                    }

                    else if (enqstatus == "All")
                    {
                        ViewBag.Enqst = "All";
                        lstenquiry = lstenquiry.ToList();
                    }

                }
                if (Status != "All")
                {
                    if (Status == "Open")
                    {
                        lstenquiry = lstenquiry.Where(z => z.FEnqstatus == "Open").ToList();
                    }
                    else if (Status == "Close")
                    {
                        lstenquiry = lstenquiry.Where(z => z.FEnqstatus == "Close").ToList();
                    }
                    ViewBag.Status = Status;
                }


                if (enquiryabout != "All" && enquiryabout != "")
                {
                    lstenquiry = lstenquiry.Where(z => z.EnqAbt == enquiryabout).ToList();
                    ViewBag.enquabt = enquiryabout;

                }
            }
            return lstenquiry;
        }


        public ActionResult EnquiryAnalysisReport(string date = "", string tdate = "", string usr = "", string enqstatus = "", string enquiryabout = "", string format = "", string Status = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

            List<MaEnquiry> lstenquiry = EnquiryList(date, tdate, usr, enqstatus, enquiryabout, branchid, Status, "", "");

            //if (date != "" || tdate != "")
            //{
            //    ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
            //    ViewBag.tdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1.0).Date.ToShortDateString();
            //    return View();
            //}
            //else
            //{

            string usrtype = Request.Cookies[CommonUtil.CookieUsertype].Value;


            if (usrtype == CommonUtil.UserTypeCounselor)
            {
                try
                {
                    DateTime fromdate = Convert.ToDateTime(date);
                    if (fromdate < DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(-3))
                    {
                        return Content("You don't have permission to go to this date");
                    }
                }
                catch (Exception ex)
                {
                    return Content("You don't have permission to go to this date");
                }
            }

            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetEnquiryAnanlysis(lstenquiry, date, tdate, format);

            var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
            //}
        }


        /*----------------------Used for Lazy Loading--------------------------------*/

        #region Lazy-Load-Data

        public ActionResult LoadData(int id)
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            var maenquiries = db.MaEnquiries.Where(m => m.BranchId == branchid).Where(z => z.Id < id).Take(25).OrderByDescending(z => z.Id).ToList();

            ViewBag.Enqst = "All";
            // ViewBag.isadmission = db.MaEnquiries.Where(z => z.IsAdmission == true).ToList();
            ViewBag.TotalCount = maenquiries.Count;
            ViewBag.UserList = new SelectList(cu.GetUserList(branchid), "Id", "Fullname");

            return PartialView("_LazyLoadData", maenquiries.ToList());
        }

        #endregion

        //======show enquiry detail test

        //===used in visa payment when type is enquiry because in ielts entry can be from enquiry
        public ActionResult IeltsLastScore(int id = 0)
        {
            List<Tb_IELTSResult> lstIeltsResult = db.Tb_IELTSResult.Where(m => m.StudentType == "Enquiry").Where(m => m.StudentId == id).OrderByDescending(m => m.Id).ToList();

            if (lstIeltsResult.Count > 0)
            {
                Tb_IELTSResult obj = lstIeltsResult.First();

                return PartialView("_IeltsScoreDetail", obj);
            }


            return HttpNotFound();


        }


        //=== All Remark History (mock test,ielts)
        public ActionResult _RemarkHistory(int id = 0, string enqtype = "", string reftype = "", string stype = "", string name = "", int referid = 0, int visaid = 0, string rolno = "")
        {
            List<RemarkHistory> sturemarks = new List<Models.RemarkHistory>();
            RemarkHistory remrkhis = new RemarkHistory();
            if (enqtype == "Enquiry")
            {
                remrkhis.enqtype = reftype;
                remrkhis.id = referid;
                remrkhis.visaid = visaid;
                remrkhis.rollno = Convert.ToInt32(rolno);
            }
            else
            {
                remrkhis.enqtype = reftype;
                remrkhis.id = id;
            }


            remrkhis.name = name;
            int _countmocktst = db.Tb_MockTest.Where(m => m.Id == id).Select(m => m.Id).Count();

            if (_countmocktst > 0)
            {
                int _mocktstid = db.Tb_MockTest.Where(m => m.Id == id).Select(z => z.Id).First();
                remrkhis.mockid = _mocktstid;

                //remrkhis.mocktype = "VisaMockTest";
                remrkhis.mocktype = "MockTest";
                remrkhis.mockid = id;
                remrkhis.rollno = Convert.ToInt32(rolno);
            }


            int count = db.Tb_IELTSResult.Where(m => m.StudentId == id).Select(m => m.Id).Count();
            if (count > 0)
            {

                int ieltsid = db.Tb_IELTSResult.Where(m => m.StudentId == id).Select(m => m.Id).First();
                if (ieltsid > 0)
                {
                    //remrkhis.ielttype = reftype;
                    remrkhis.ielttype = "IELTS";
                    remrkhis.ieltsid = ieltsid;
                }
                else
                {
                    remrkhis.ieltsid = 0;
                }
            }

            sturemarks.Add(remrkhis);
            return PartialView("_RemarkHistory", sturemarks);

        }



        //this is for to get list of group time by group choice start here
        private IList<MaGroupTime_View> GetGroupTime(string id)
        {
            return db.MaGroupTime_View.Where(m => m.GroupName == id && m.Status == true).OrderBy(z => z.BatchTime).ToList();

        }

        [System.Web.Mvc.AcceptVerbs(HttpVerbs.Get)]
        public JsonResult Loadgrouptme(string id)
        {
            var PackageList = this.GetGroupTime(id);

            var colourData = PackageList.Select(c => new SelectListItem()
            {
                Text = c.BatchTime,
                Value = c.Timeid.ToString(),

            });

            return Json(colourData, JsonRequestBehavior.AllowGet);
        }

        //this is for to get list of group time by group choice end here

        [System.Web.Mvc.AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChewckDuplicate(string Mobile)
        {
            string Result = "";
            int count = 0;
            try
            {
                count = db.MaEnquiries.Where(a => a.Mobile == Mobile || a.AlternateNo == Mobile).Count();
            }
            catch (Exception ex)
            { }
            if (count > 0)
            {
                Result = "Y";
            }
            else
            {
                Result = "N";
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChewckDuplicateAlt(string Mobile)
        {
            string Result = "";
            int count = 0;
            try
            {
                count = db.MaEnquiries.Where(a => a.Mobile == Mobile || a.AlternateNo == Mobile).Count();
            }
            catch (Exception ex)
            { }
            if (count > 0)
            {
                Result = "Y";
            }
            else
            {
                Result = "N";
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult NextAction(int id)
        {
            ViewBag.Idd = id;

            return View();
        }

        public ActionResult DoubleEntry()
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.CampusList = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.UserList = new SelectList(cu.GetUserList(branchid, 1), "Id", "username");
            return View();
        }
        public ActionResult DoubleEntryTab(string dt1, string dt2, string branch, string user)
        {


            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetDoubleEntryEnq(dt1, dt2, branch, user);

            var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
        }
        public ActionResult _Index1(int id)
        {
            return PartialView(db.Tb_DemoHistory.Where(z => z.EnquiryId == id).ToList());
        }



    }

}