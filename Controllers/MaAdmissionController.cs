﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using System.IO;
using SSCMvc.Webutil;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class MaAdmissionController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        CommonUtil cu = new CommonUtil();
        MasterListing listing = new MasterListing();

        #region Print Score
        //[AcceptVerbs(HttpVerbs.Get)]
        //public JsonResult GetStudentRollnoList(string st)
        //{
        //    int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

        //    var courselist = db.MockTestStudents.Where(z => z.BranchId == branchid).ToList();

        //    var colourData = new List<SelectListItem> { new SelectListItem { Text = "Jason", Value = "Jason" } };

        //    //var colourData = courselist.Select(c => new List<SelectListItem> { new SelectListItem() { Text = "Jason", Value = "Jason" }, new SelectListItem() { Text = c.fullname, Value = c.Id.ToString() } });

        //    return Json(colourData, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult CompareScores(string dt1 = "", string dt2 = "", string stuid = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ScoreReport, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
                 
            }


            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            if (dt1 == "" || dt2 == "")
            {
                ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
                ViewBag.tdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1.0).Date.ToShortDateString();
                //ViewBag.val =-1;

                ViewBag.StudentId = new SelectList(cu.GetMockTestStudents(branchid), "Id", "fullname");
                return View("CompareScores");
            }

            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetCompareScoreList(dt1, dt2, stuid);

            var renderedBytes = reportViewModel.RenderReport();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
        }

        #endregion

        public ActionResult _FolloupHistoryMocktest(int id)
        {
            return PartialView(db.MaEnqFollows.Where(z => z.AdmissionId == id && z.FollowUpType == "MockTest").OrderByDescending(z => z.Id).ToList());
        }

        public ActionResult _FolloupHistoryIelts(int id)
        {
            var IList = from Iel in db.Tb_IELTSResult join Fol in db.MaEnqFollows on Iel.Id equals Fol.ReferId where Fol.FollowUpType == "IELTS" && Iel.StudentId == id orderby Fol.Id descending select Fol;
            return PartialView(IList.ToList());
        }

        public ActionResult _FolloupHistoryEnquiry(int id)
        {
            return PartialView(db.MaEnqFollows.Where(z => z.ReferId == id && z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).ToList());
        }

        //--------------------------MaAdmission start here-----------------------/

        #region MaAdmission

        public ActionResult _searchadmiss(string name, string mobile, string rollno, string fdate = "", string tdate = "", string fathername = "", string Course = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            int courseid = 0;
            try
            {
                courseid = Convert.ToInt32(Course);
            }
            catch (Exception ex)
            { }

            //List<MaAdmission> lst = db.MaAdmissions.Where(z => z.BranchId == branchid).OrderByDescending(z => z.Id).ToList();
            List<ProsStudentHelper> lst = ProsStudentList(fdate, tdate, courseid, fathername, name, rollno, mobile);

            //if (fathername != "")
            //{
            //    lst = lst.Where(z => z.FatherName.ToUpper().Contains(fathername.ToUpper())).ToList();

            //}

            //if (name != "")
            //{
            //    lst = lst.Where(z => z.FirstName.ToUpper().Contains(name.ToUpper())).ToList();

            //}

            //if (mobile != "")
            //{

            //    lst = lst.Where(z => z.Mobile.Contains(mobile)).ToList();

            //}

            //if (rollno != "")
            //{
            //    try
            //    {
            //        int rno = Convert.ToInt32(rollno);
            //        lst = lst.Where(z => z.RollNo == rno).ToList();
            //    }
            //    catch (Exception ex)
            //    {
            //    }

            //}
            //if (Course != null)
            //{
            //    try
            //    {
            //        int courseid1 = Convert.ToInt32(Course);
            //        lst = lst.Where(a => a.CourseId == courseid1).ToList();
            //    }
            //    catch (Exception ex)
            //    { }


            //}

            //if (Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeNewCounselor)
            //{
            //    if (fdate != "" && tdate != "")
            //    {

            //        try
            //        {
            //            DateTime dt1 = Convert.ToDateTime(fdate);
            //            DateTime dt2 = Convert.ToDateTime(tdate);
            //            lst = lst.Where(z => z.Cdate >= dt1 && z.Cdate <= dt2).ToList();
            //        }
            //        catch (Exception ex)
            //        {
            //        }

            //    }
            //}
            ViewBag.Course = new SelectList(db.MaCourses, "Id", "CourseName", Course);
            ViewBag.fdate = fdate;
            ViewBag.tdate = tdate;
            ViewBag.mobile = mobile;
            ViewBag.fathername = fathername;
            ViewBag.name = name;
            ViewBag.rollno = rollno;
            ViewBag.TotalCount = lst.Count;
            return View("Index", lst);

        }


        public List<ProsStudentHelper> ProsStudentList(string date1, string date2, int courseid, string fname, string sname, string rollno, string mobleno)
        {
            List<ProsStudentHelper> lst = new List<ProsStudentHelper>();

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

            string connectionstring = ConfigurationManager.ConnectionStrings["SSCWebDBConnectionString"].ConnectionString;
            string sql = "";

            if (rollno.Length > 0)
                sql = String.Format("SELECT A.Id,A.EnquiryId,A.FirstName,A.LastName,A.RollNo,A.FatherName,A.Mobile,A.Cdate,A.StudPhoto FROM MaAdmission A WHERE A.BranchId ={0} and  RollNo = {1} and A.RollNo is not null", branchid, rollno);
            else if (sname.Length > 0)
                sql = String.Format("SELECT A.Id,A.EnquiryId,A.FirstName,A.LastName,A.RollNo,A.FatherName,A.Mobile,A.Cdate,A.StudPhoto FROM MaAdmission A WHERE A.BranchId ={0} and  FirstName like '%{1}%' and A.RollNo is not null", branchid, sname);
            else if (fname.Length > 0)
                sql = String.Format("SELECT A.Id,A.EnquiryId,A.FirstName,A.LastName,A.RollNo,A.FatherName,A.Mobile,A.Cdate,A.StudPhoto FROM MaAdmission A WHERE A.BranchId ={0} and  fathername like '{1}' and A.RollNo is not null ", branchid, fname);
            else if (mobleno.Length > 0)
                sql = String.Format("SELECT A.Id,A.EnquiryId,A.FirstName,A.LastName,A.RollNo,A.FatherName,A.Mobile,A.Cdate,A.StudPhoto FROM MaAdmission A WHERE A.BranchId ={0} and  A.RollNo is not null and  ( Mobile = '{1}' or AlternateNo='{1}' )", branchid, mobleno);

            else if (StaticLogic.SubRole() == "Max")
            {
                if (date1.Length > 0 && date2.Length > 0 && courseid > 0)
                {
                    sql = String.Format("SELECT A.Id,A.EnquiryId,A.FirstName,A.LastName,A.RollNo,A.FatherName,A.Mobile,A.Cdate,A.StudPhoto FROM MaAdmission A WHERE A.BranchId = {0} and cdate between '{1}' and '{2}' and A.RollNo is not null  and id in(select admissionid from tb_coursehist where CourseId={3})", branchid, date1, date2, courseid);
                }
                else
                {
                    sql = String.Format("SELECT A.Id,A.EnquiryId,A.FirstName,A.LastName,A.RollNo,A.FatherName,A.Mobile,A.Cdate,A.StudPhoto FROM MaAdmission A WHERE A.BranchId ={0} and cdate between '{1}' and '{2}' and A.RollNo is not null", branchid, date1, date2);
                }
            }
            else
            {

                sql = String.Format("SELECT A.Id,A.EnquiryId,A.FirstName,A.LastName,A.RollNo,A.FatherName,A.Mobile,A.Cdate,A.StudPhoto FROM MaAdmission A WHERE A.BranchId ={0} and cdate between '{1}' and '{2}' and A.RollNo is not null", branchid, date1, date2);
            }
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlDataAdapter adp = new SqlDataAdapter(sql, con);
                con.Open();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    ProsStudentHelper obj = new ProsStudentHelper();
                    try
                    {
                        obj.Id = Convert.ToInt32(dr["Id"]);
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.FirstName = dr["FirstName"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.LastName = dr["LastName"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.StudentPhoto = dr["StudPhoto"].ToString();
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    try
                    {
                        obj.RollNo = Convert.ToInt32(dr["RollNo"]);
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.FatherName = dr["FatherName"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.Mobile = dr["Mobile"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.Cdate = Convert.ToDateTime(dr["Cdate"]);
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        obj.Mobile = dr["Mobile"].ToString();
                    }
                    catch (Exception ex)
                    { }


                    try
                    {
                        obj.EnquiryId = Convert.ToInt32(dr["EnquiryId"]);
                    }
                    catch (Exception ex)
                    { }

                    lst.Add(obj);
                }
            }
            return lst;
        }


        public ActionResult _searchadmissLeft(string name, string mobile, string rollno, string fromdate = "", string todate = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

            List<MaAdmission> lst = db.MaAdmissions.Where(z => z.BranchId == branchid && z.IsLeft == true).ToList();

            if (name != "")
            {
                lst = lst.Where(z => z.FirstName.ToUpper().Contains(name.ToUpper())).ToList();

            }

            if (mobile != "")
            {

                lst = lst.Where(z => z.Mobile.Contains(mobile)).ToList();

            }

            if (rollno != "")
            {

                int rno = Convert.ToInt32(rollno);
                lst = lst.Where(z => z.RollNo == rno).ToList();
            }
            DateTime? fdate = null;
            DateTime? tdate = null;

            if (fromdate != "" && todate != "")
            {
                fdate = Convert.ToDateTime(fromdate);
                tdate = Convert.ToDateTime(todate);

                lst = lst.Where(z => z.LastUpdatedDate >= fdate && z.LastUpdatedDate <= tdate).ToList();
            }
            ViewBag.TotalCount = lst.Count;
            return View("LeftStudent", lst);

        }



        public ActionResult Index()
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ProspStudent, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            DateTime dt = DateTime.Now.AddDays(-2);
            string Fdate = dt.ToShortDateString();
            string Todate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).ToShortDateString();

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            List<ProsStudentHelper> lst = new List<ProsStudentHelper>();
            ViewBag.Course = new SelectList(db.MaCourses, "Id", "CourseName");
            if (Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeNewCounselor)
            {
                //var maadmissions = db.MaAdmissions.Where(z => z.BranchId == branchid).Where(m => m.Cdate > dt).OrderByDescending(z => z.Id).ToList();
                lst = ProsStudentList(Fdate, Todate, 0, "", "", "", "");
                ViewBag.TotalCount = lst.Count();//db.MaAdmission.Where(z => z.BranchId == branchid).OrderByDescending(z => z.Id).Count();
                return View(lst);
            }

            return View(lst);

        }

        public ActionResult LeftStudent1()
        {


            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
            ViewBag.tdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1.0).Date.ToShortDateString();

            var maadmissions = db.MaAdmissions.Where(z => z.BranchId == branchid && z.IsLeft == true).OrderByDescending(z => z.Id).ToList();
            ViewBag.TotalCount = maadmissions.Count;
            return View(maadmissions);
        }




        public ActionResult LeftStudent()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.LeftStuentReport, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
            ViewBag.tdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1.0).Date.ToShortDateString();

            var maadmissions = db.MaAdmissions.Where(z => z.BranchId == branchid && z.IsLeft == true).OrderByDescending(z => z.Id).ToList();
            //ViewBag.TotalCount = maadmissions.Count;

            List<MaAdmission> lst = new List<MaAdmission>();
            if (lst == null)
            {
                lst = new List<MaAdmission>();
                ViewBag.TotalCount = lst.Count;
            }

            //List<MaAdmission> maadmissions = new List<MaAdmission>();
            return View(lst);
        }


        public ActionResult PrintTransfer(int id)
        {
            try
            {
                //int roleid = StaticLogic.RoleId();
                //bool Prmsn = false;
                //Prmsn = Permission.CheckPermission(roleid, CommonUtil.LeftStuentReport, UserAction.View);

                //if (Prmsn == false)
                //{
                //    return View("~/Views/Shared/AccessDenied.cshtml");
                //}

                try
                {
                    var Business = new RecieptBuisness();
                    var reportViewModel = Business.GetPartTransStuReport(id);

                    var renderedBytes = reportViewModel.RenderReportWithoutPararmere();
                    if (reportViewModel.ViewAsAttachment)
                        Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                    return File(renderedBytes, reportViewModel.LastmimeType);
                }
                catch (Exception ex)
                {
                }

            }
            catch (Exception)
            {
            }
            return View();
        }



        public ActionResult SearchLeftStudent(string FromDate = "", string ToDate = "", int RollNo = 0, string Name = "")
        {
            List<MaAdmission> lst = new List<MaAdmission>();
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

            if (FromDate == "" || ToDate == "")
            {
                ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
                ViewBag.tdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1.0).Date.ToShortDateString();
            }
            ViewBag.fdate = FromDate;
            ViewBag.tdate = ToDate;

            lst = db.MaAdmissions.Where(z => z.BranchId == branchid && z.IsLeft == true).OrderByDescending(z => z.Id).ToList();
            ViewBag.TotalCount = lst.Count;

            if (FromDate != " " && ToDate != " ")
            {
                DateTime fdate = Convert.ToDateTime(FromDate);
                DateTime TDate = Convert.ToDateTime(ToDate);
                lst = db.MaAdmissions.Where(z => z.BranchId == branchid && z.IsLeft == true && z.LeftDate > fdate && z.LeftDate < TDate).OrderByDescending(z => z.Id).ToList();
                ViewBag.TotalCount = lst.Count;
            }
            if (RollNo != 0)
            {
                lst = db.MaAdmissions.Where(z => z.BranchId == branchid && z.IsLeft == true && z.RollNo == RollNo).OrderByDescending(z => z.Id).ToList();
                ViewBag.TotalCount = lst.Count;
            }
            if (Name != "")
            {
                lst = db.MaAdmissions.Where(z => z.BranchId == branchid && z.IsLeft == true && z.FirstName == Name).OrderByDescending(z => z.Id).ToList();
                ViewBag.TotalCount = lst.Count;
            }

            return View("LeftStudent", lst);
        }
        public ActionResult StudentTransferIndex()
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ProspStudent, UserAction.View);
                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            DateTime dt = DateTime.Now.AddDays(-2);
            string Fdate = dt.ToShortDateString();
            string Todate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).ToShortDateString();

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            List<ProsStudentHelper> lst = new List<ProsStudentHelper>();
            ViewBag.Course = new SelectList(db.MaCourses, "Id", "CourseName");
            if (Request.Cookies[CommonUtil.CookieUsertype].Value != CommonUtil.UserTypeNewCounselor)
            {
                lst = ProsStudentList(Fdate, Todate, 0, "", "", "", "");
                ViewBag.TotalCount = lst.Count();
                return View(lst);
            }
            return View(lst);
        }

        public ActionResult _searchStudentTrnasfer(string name, string mobile, string rollno, string fdate = "", string tdate = "", string fathername = "", string Course = "")
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            int courseid = 0;
            try
            {
                courseid = Convert.ToInt32(Course);
            }
            catch (Exception ex)
            { }
            List<ProsStudentHelper> lst = ProsStudentList(fdate, tdate, courseid, fathername, name, rollno, mobile);

            ViewBag.Course = new SelectList(db.MaCourses, "Id", "CourseName", Course);
            ViewBag.fdate = fdate;
            ViewBag.tdate = tdate;
            ViewBag.mobile = mobile;
            ViewBag.fathername = fathername;
            ViewBag.name = name;
            ViewBag.rollno = rollno;
            ViewBag.TotalCount = lst.Count;
            return View("StudentTransferIndex", lst);
        }

        public ActionResult TransferStudent(int id = 0)
        {
            MaAdmission maadmissions = db.MaAdmissions.Where(z => z.Id == id).FirstOrDefault();
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.StudentId = new SelectList(cu.GetAdmStudent(), "Id", "Rollfullname", id);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.BranchIdF = new SelectList(db.MaBranches, "Id", "BramchName", branchid);
            ViewBag.UserId = new SelectList(cu.GetUserList(0), "Id", "Fullname");
            ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
            ViewBag.Name = maadmissions.FirstName + " " + maadmissions.LastName;
            try
            {
                int? Coursed = db.Tb_CourseHist.Where(m => m.AdmissionId == id && m.IsActive == true).OrderByDescending(x => x.Id).Select(x => x.Id).FirstOrDefault();
                ViewBag.Couse = db.Tb_CourseHist.Where(x => x.Id == Coursed).Select(x => x.MaCourse.CourseName + "-" + x.GroupName + "-(" + x.MaTime.TimeIn + " " + x.MaTime.Indesc + "-" + x.MaTime.TimeOut + " " + x.MaTime.OutDesc + ")").FirstOrDefault();
                int packid = (int)db.Tb_CourseHist.Where(z => z.Id == Coursed && z.AdmissionId == id).Select(z => z.PackageId).First();
                ViewBag.Packg = db.MaPackages.Where(x => x.Id == packid).Select(x => x.PackageName).FirstOrDefault();
                Tb_Installment obj = db.Tb_Installment.Where(m => m.Tb_CourseHist.IsActive == true && m.CourseHistId == Coursed && m.BranchId == branchid && m.IsFeesPaid == false).OrderBy(m => m.Id).Take(1).SingleOrDefault();
                double? feeamount = obj.Amount;
                if (obj.BalanceDueAmount != null && obj.BalanceDueAmount > 0)
                {
                    feeamount = obj.BalanceDueAmount;
                }
                ViewBag.DueAmt = feeamount;
            }
            catch (Exception)
            {
            }
            return View();
        }

        [HttpPost]
        public ActionResult TransferStudent(int id, FormCollection form)
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            int users = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.BranchIdF = new SelectList(db.MaBranches, "Id", "BramchName", branchid);
            ViewBag.UserId = new SelectList(cu.GetUserList(0), "Id", "Fullname");
            ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
            MaAdmission maadmissions = db.MaAdmissions.Where(z => z.Id == id).FirstOrDefault();
            //ViewBag.Student = maadmissions.RollNo + "-" + maadmissions.FirstName + " " + maadmissions.LastName;
            //ViewBag.StId = maadmissions.Id;
            MaAdmission mad = new MaAdmission();
            int CnId = Convert.ToInt32(form["UserId"]);
            int OldSid = Convert.ToInt32(form["StudentId"]);
            int ToBranch = Convert.ToInt32(form["BranchId"]);
            MaBranch mabrnch = db.MaBranches.Find(ToBranch);
            MaBranch mabrnch2 = db.MaBranches.Find(branchid);
            try
            {
                Tb_StudentTransfer st = new Tb_StudentTransfer();
                st.StudentId = Convert.ToInt32(form["StudentId"]);
                st.BranchTo = Convert.ToInt32(form["BranchId"]);
                st.BranchFrom = Convert.ToInt32(form["BranchIdF"]);
                st.TransferTo = Convert.ToInt32(form["UserId"]);
                try
                {
                    st.BalanceAmt = Convert.ToDouble(form["Balance"]);
                }
                catch (Exception)
                {
                }
                st.OldRollNo = maadmissions.RollNo;
                st.Course = form["course"];
                st.Package = form["pkg"];
                st.TransferBy = users;
                st.Remarks = form["Remarks"];
                st.OldStudId = maadmissions.Id;
                st.TransferDate = Convert.ToDateTime(form["Tdate"]);
                st.Status = false;
                st.CDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
                st.TransStatus = "Transfer";
                db.Tb_StudentTransfer.Add(st);
                db.SaveChanges();
                maadmissions.IsLeft = true;
                maadmissions.LeftDate = CommonUtil.IndianTime();
                maadmissions.Remarks = "Transfered to " + mabrnch.BramchName;
                db.SaveChanges();
                mad.EnquiryId = maadmissions.EnquiryId;
                mad.FirstName = maadmissions.FirstName;
                mad.LastName = maadmissions.LastName;
                mad.FatherName = maadmissions.FatherName;
                mad.DOB = maadmissions.DOB;
                mad.Gender = maadmissions.Gender;
                mad.Address = maadmissions.Address;
                mad.Mobile = maadmissions.Mobile;
                mad.AlternateNo = maadmissions.AlternateNo;
                mad.FacebookId = maadmissions.FacebookId;
                mad.EmailId = maadmissions.EmailId;
                mad.Source = maadmissions.Source;
                mad.WalkInType = maadmissions.WalkInType;
                mad.WalkInDate = maadmissions.WalkInDate;
                mad.Remarks = "System Generated Remarks :Transfered from " + mabrnch2.BramchName;
                mad.CounselorName = Convert.ToInt32(form["UserId"]);
                mad.CityPreference = maadmissions.CityPreference;
                mad.DateOfExam = maadmissions.DateOfExam;
                mad.Status = maadmissions.Status;
                mad.EnquiryIELAC = maadmissions.EnquiryIELAC;
                mad.EnquiryIELGT = maadmissions.EnquiryIELGT;
                mad.EnquiryPTE = maadmissions.EnquiryPTE;
                mad.EnquirySpkEng = maadmissions.EnquirySpkEng;
                mad.EnquiryStyVisa = maadmissions.EnquiryStyVisa;
                mad.CountryInterested = maadmissions.CountryInterested;
                mad.IELTSExmLearn = maadmissions.IELTSExmLearn;
                mad.IELTSExmOverall = maadmissions.IELTSExmOverall;
                mad.IELTSExmRead = maadmissions.IELTSExmRead;
                mad.IELTSExmSpkn = maadmissions.IELTSExmSpkn;
                mad.IELTSExmWrit = maadmissions.IELTSExmWrit;
                mad.IELTSTestId = maadmissions.IELTSTestId;
                mad.Cdate = maadmissions.Cdate;
                mad.LastUpdatedDate = maadmissions.LastUpdatedDate;
                //int? maxRoll = db.MaAdmissions.Max(x => x.RollNo);
                //mad.RollNo = maxRoll + 1;
                mad.BranchId = Convert.ToInt32(form["BranchId"]);
                mad.IsLeft = false;
                mad.ReferenceId = maadmissions.ReferenceId;
                mad.StudPhoto = maadmissions.StudPhoto;
                mad.Other = maadmissions.Other;
                mad.IsMoveToVisa = maadmissions.IsMoveToVisa;
                mad.MockTestId = maadmissions.MockTestId;
                mad.LeftDate = maadmissions.LeftDate;
                mad.AreaId = maadmissions.AreaId;
                mad.EnqAbt = maadmissions.EnqAbt;
                mad.LoginCounselr = maadmissions.LoginCounselr;
                db.MaAdmissions.Add(mad);
                db.SaveChanges();
                Tb_StudentTransfer tb_st = db.Tb_StudentTransfer.Find(st.Id);
                tb_st.StudentId = mad.Id;
                db.SaveChanges();
                SendMailtoUser(mad.Id, CnId, maadmissions.RollNo, maadmissions.FirstName, maadmissions.LastName, OldSid);
                return RedirectToAction("StudentTransferIndex");
            }
            catch (DbEntityValidationException ex)
            {
                ViewBag.error = "error";
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);
                var fullErrorMessage = string.Join("; ", errorMessages);
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                cu.SendError(new Exception(exceptionMessage));
                ViewBag.mesg = cu.SessionError;
            }
            return View();
        }

        private void SendMailtoUser(int StId, int CnId, int? roll, string FName, string LName, int osid)
        {
            try
            {
                //string mail = "info@crm-ssceducation.org";
                int idd = 0;
                Tb_FeeReceipt tb_fee = db.Tb_FeeReceipt.Where(x => x.StudentId == osid).OrderByDescending(x => x.Id).FirstOrDefault();
                try
                {
                    idd = tb_fee.Id;
                }
                catch (Exception)
                {
                }

                User user = new User();
                user = db.Users.Where(x => x.Id == CnId).FirstOrDefault();
                string mail = ConfigurationManager.AppSettings["Mail"];
                MailMessage message = new MailMessage();
                message.Subject = "Student Transfer ";
                message.Body = "<p>Dear " + user.Fullname + "</p> <br/> <p> Following Student has been transfered to your Branch</p> <br />Student Name : " + FName + " " + LName + " <br />RollNo " + roll;
                message.From = new MailAddress(mail);
                message.To.Add(new MailAddress(user.Email));
                message.IsBodyHtml = true;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
                SmtpClient smtpClient = new SmtpClient(smtpserver);
                smtpClient.Port = Int32.Parse("8889");
                smtpClient.Credentials = new NetworkCredential(mail, ConfigurationManager.AppSettings["credential"].ToString());
                smtpClient.EnableSsl = false;
                smtpClient.Send(message);
            }
            catch (Exception ex)
            { }
        }

        public ActionResult AcceptTransfer(int Id = 0, int AId = 0)
        {
            Tb_StudentTransfer st = db.Tb_StudentTransfer.Find(Id);
            MaAdmission mad = db.MaAdmissions.Find(AId);
            if (st.Status == true)
            {
                return RedirectToAction("CourseHistory", new { @Id = AId });
            }
            st.Status = true;
            int? maxRoll = db.MaAdmissions.Max(x => x.RollNo);
            mad.RollNo = maxRoll + 1;
            st.AcctDate = CommonUtil.IndianTime();
            db.SaveChanges();
            return RedirectToAction("CourseHistory", new { @Id = AId });
        }

        public ActionResult DeclineTransfer(int Id = 0, int AId = 0)
        {
            Tb_StudentTransfer st = db.Tb_StudentTransfer.Find(Id);
            st.Status = true;
            st.Remarks = $"Decline on {DateTime.Now} by {User.Identity.Name}";
            MaAdmission mad = db.MaAdmissions.Find(AId);
            mad.IsLeft = false;
            mad.LeftDate = null;
            db.SaveChanges();

            return RedirectToAction("Index","Home");
        }

        public ActionResult EditTransferStudent(int id)
        {
            Tb_StudentTransfer st = db.Tb_StudentTransfer.Find(id);
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            ViewBag.StudentId = new SelectList(cu.GetAdmStudent(), "Id", "Rollfullname", st.StudentId);
            //ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", st.StudentId);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", st.BranchTo);
            ViewBag.BranchIdF = new SelectList(db.MaBranches, "Id", "BramchName", st.BranchFrom);
            ViewBag.UserId = new SelectList(cu.GetUserList(st.BranchTo), "Id", "Fullname", st.TransferTo);
            ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
            MaAdmission maadmissions = db.MaAdmissions.Where(z => z.Id == st.StudentId).FirstOrDefault();
            //ViewBag.Student = maadmissions.RollNo + "-" + maadmissions.FirstName + " " + maadmissions.LastName;
            //ViewBag.StId = maadmissions.Id;
            return View(st);
        }

        [HttpPost]
        public ActionResult EditTransferStudent(int id, Tb_StudentTransfer st, FormCollection form)
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            int users = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", st.BranchTo);
            ViewBag.BranchIdF = new SelectList(db.MaBranches, "Id", "BramchName", st.BranchFrom);
            ViewBag.UserId = new SelectList(cu.GetUserList(0), "Id", "Fullname", st.TransferTo);

            try
            {
                st.StudentId = Convert.ToInt32(form["StudentId"]);
                st.BranchTo = Convert.ToInt32(form["BranchId"]);
                st.BranchFrom = Convert.ToInt32(form["BranchIdF"]);
                st.TransferTo = Convert.ToInt32(form["UserId"]);
                st.Remarks = form["Remarks"];
                db.Entry(st).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index", "Tb_StudentTransfer");
            }
            catch (DbEntityValidationException ex)
            {
                ViewBag.error = "error";
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);
                var fullErrorMessage = string.Join("; ", errorMessages);
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                cu.SendError(new Exception(exceptionMessage));
                ViewBag.mesg = cu.SessionError;
            }
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadCousellorByBranch(int bid)
        {
            var user = db.Users.Where(z => z.Status == true && z.BranchId == bid).ToList();
            var colourData = user.Select(c => new SelectListItem()
            {
                Text = c.Fullname,
                Value = c.Id.ToString(),

            });

            return Json(colourData, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /MaAdmission/Details/5

        public ActionResult Details(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ProspStudent, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            MaAdmission maadmission = db.MaAdmissions.Find(id);
            if (maadmission == null)
            {
                return HttpNotFound();
            }
            return View(maadmission);
        }

        //
        // GET: /MaAdmission/Create

        public ActionResult Create(int id = 0)
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ProspStudent, UserAction.Add);
                string chbranch = Request.Cookies["chbranch"].Value;


                if (Prmsn == false || chbranch=="Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            if (id > 0)
            {
                MaEnquiry enquiry = db.MaEnquiries.Find(id);

                ViewBag.Sid = id;
                MaAdmission adm = new MaAdmission();

                adm.FatherName = enquiry.FatherName;
                adm.Address = enquiry.Address;
                adm.AlternateNo = enquiry.AlternateNo;
                adm.Cdate = enquiry.Cdate;
                adm.CityPreference = enquiry.CityPreference;
                adm.EnquiryPTE = enquiry.EnqPPE;
                adm.CountryInterested = enquiry.CountryInterested;
                adm.DateOfExam = enquiry.DateOfExam;
                adm.EmailId = enquiry.EmailId;
                adm.EnquiryIELAC = enquiry.EnquiryIELAC;
                adm.EnquiryIELGT = enquiry.EnquiryIELGT;
                adm.EnquirySpkEng = enquiry.EnquirySpkEng;
                adm.EnquiryStyVisa = enquiry.EnquiryStyVisa;
                adm.FacebookId = enquiry.FacebookId;
                adm.FirstName = enquiry.FirstName;
                adm.Gender = enquiry.Gender;
                adm.IELTSExmLearn = enquiry.IELTSExmLearn;
                adm.IELTSExmOverall = enquiry.IELTSExmOverall;
                adm.IELTSExmRead = enquiry.IELTSExmRead;
                adm.IELTSExmSpkn = enquiry.IELTSExmSpkn;
                adm.IELTSExmWrit = enquiry.IELTSExmWrit;
                adm.LastName = enquiry.LastName;
                adm.Remarks = enquiry.Remarks;
                adm.Source = enquiry.Source;
                adm.Status = enquiry.Status;
                adm.Mobile = enquiry.Mobile;
                adm.AlternateNo = enquiry.AlternateNo;
                adm.WalkInDate = enquiry.WalkInDate;
                adm.WalkInType = enquiry.WalkInType;
                adm.ReferenceId = enquiry.ReferenceId;
                adm.Other = enquiry.Other;
                adm.BranchId = branchid;

                ViewBag.EnquiryId = new SelectList(db.MaEnquiries, "Id", "FirstName");
                ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout", enquiry.EnqAbt);
                ViewBag.CounselorName = new SelectList(cu.GetUserList(branchid), "Id", "Fullname", enquiry.CounselorName); // changed on 24 june problem like user coming all
                ViewBag.AreaId = new SelectList(db.Ma_Area, "Id", "Area", enquiry.AreaId);

                if (enquiry.BranchId == branchid)
                {
                    ViewBag.CounselorName = new SelectList(cu.GetUserList(branchid), "Id", "Fullname", enquiry.CounselorName); // changed on 24 june problem like user coming all
                    ViewBag.branch = "Y";
                }
                else
                {

                    adm.CounselorName = enquiry.CounselorName;
                    ViewBag.branch = "N";
                }
                ViewBag.ReferenceId = new SelectList(db.MaFrmReferences, "Id", "ContactPerson", enquiry.ReferenceId);
                adm.IsLeft = false;
                return View(adm);

            }

            ViewBag.EnquiryId = new SelectList(db.MaEnquiries, "Id", "FirstName");
            ViewBag.CounselorName = new SelectList(cu.GetUserList(branchid), "Id", "Fullname");// changed on 24 june problem like user coming all
            ViewBag.ReferenceId = new SelectList(db.MaFrmReferences, "Id", "ContactPerson");
            return View();

        }



        //
        // POST: /MaAdmission/Create

        [HttpPost]
        public ActionResult Create(MaAdmission maadmission, int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ProspStudent, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            //   if (maadmission.EnquiryIELAC == true || maadmission.EnquiryIELGT == true || maadmission.EnquirySpkEng == true || maadmission.EnquiryStyVisa == true || maadmission.EnquiryPTE == true)

            ViewBag.AreaId = new SelectList(db.Ma_Area, "Id", "Area", maadmission.AreaId);
            ViewBag.EnquiryId = new SelectList(db.MaEnquiries, "Id", "FirstName", maadmission.EnquiryId);
            /*--*/
            ViewBag.CounselorName = new SelectList(cu.GetUserList(branchid), "Id", "Fullname", maadmission.CounselorName);
            ViewBag.ReferenceId = new SelectList(db.MaFrmReferences, "Id", "ContactPerson", maadmission.ReferenceId);

            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout");
            int Count = db.MaAdmissions.Where(x => x.FirstName == maadmission.FirstName && x.LastName == maadmission.LastName && x.Mobile == maadmission.Mobile && x.FatherName == maadmission.FatherName && x.DOB == maadmission.DOB).Count();
            if (Count > 0)
            {
                ViewBag.Msg = "Student already Registered";
                return View(maadmission);
            }

            if (maadmission.EnqAbt != "" || maadmission.EnqAbt != null)
            {
                try
                {
                    string filename = "NoImage.PNG";
                    if (ModelState.IsValid)
                    {
                        foreach (string inputTagName in Request.Files)
                        {
                            HttpPostedFileBase file = Request.Files[inputTagName];
                            if (file.ContentLength > 0)
                            {
                                filename = Guid.NewGuid().ToString().Substring(0, 5) + file.FileName;
                                string filePath = Path.Combine(Server.MapPath("~//Content//themes//base//images//Documents//"), filename);
                                file.SaveAs(filePath);

                            }

                        }
                        if (id > 0)
                        {
                            MaEnquiry enquiry = db.MaEnquiries.Find(id);
                            /*some time double admission saves so to stop it i am checking if is admission is true in maequiry
                             becauce one admission is made it will update IsAdmission column in MaEnquiry. so next time if it try to
                             * save same admission again then control will redirect to index page without saving it
                             */
                            //if (enquiry.IsAdmission == true)
                            //{
                            //    return RedirectToAction("Index");
                            //}
                            //update enquery table here update by amar on 10/20/2015

                            //enquiry.IsAdmission = true;
                            //enquiry.Status = true;
                            //enquiry.AdmDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                            //db.SaveChanges();
                            //update Enquiry Followup Table Here
                            if (enquiry.FEnqstatus == null || enquiry.FEnqstatus == "Open")
                            {
                                try
                                {
                                    //Enquiry
                                    MaEnqFollow enqfollowup = new MaEnqFollow();
                                    enqfollowup.CDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                                    enqfollowup.NextDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                                    enqfollowup.ReferId = enquiry.Id;
                                    enqfollowup.Remarks = "Converted on " + DateTime.Now.AddMinutes(StaticLogic.TimeDifference).ToShortDateString();
                                    enqfollowup.Entertype = "Admission";
                                    enqfollowup.Enqstatus = "Close";
                                    enqfollowup.EnqFollowBy = enquiry.CounselorName;
                                    enqfollowup.FollowUpType = "Enquiry";
                                    db.MaEnqFollows.Add(enqfollowup);
                                    db.SaveChanges();

                                }
                                catch (Exception ex)
                                { }

                                try
                                {
                                    enquiry.FEnqstatus = "Close";
                                    enquiry.FEnqFollowBy = enquiry.CounselorName;
                                    enquiry.FRemarks = "Converted on " + DateTime.Now.AddMinutes(StaticLogic.TimeDifference).ToShortDateString();
                                    enquiry.FCDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                                    enquiry.FNextDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                                    db.SaveChanges();
                                }
                                catch (Exception ex)
                                { }
                            }

                            //update Enquiry Followup Table Ends Here


                            //maadmission.BranchId = branchid;
                            maadmission.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                            maadmission.LastUpdatedDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                            maadmission.StudPhoto = filename;
                            int? rollno = db.MaAdmissions.Select(z => z.RollNo).Max();
                            if (rollno == null || rollno == 0)
                            {
                                rollno = 1;
                            }
                            else
                            {
                                rollno = rollno + 1;
                            }
                            maadmission.RollNo = rollno;

                            try
                            {
                                maadmission.FatherName = maadmission.FatherName.ToUpper();
                            }
                            catch (Exception ex)
                            { }
                            maadmission.CounselorName = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                            maadmission.IsSync = false;
                            maadmission.SPassword = "PASS" + Guid.NewGuid().ToString().Substring(0, 4);
                            maadmission.ExpireOn = DateTime.Now;

                            db.MaAdmissions.Add(maadmission);

                        }

                        db.SaveChanges();
                        Status myobj = new Status();
                        string HomeworkURL = "http://sscstudentlogin.com";
                        string msg1 = "",res="";

                        if (maadmission.EnqAbt == "IELTS")
                        {
                            //msg1 = $"Dear {maadmission.FirstName}, welcome to SSC. Your roll number is {maadmission.RollNo}. For Homework, go to {HomeworkURL}. Username {maadmission.RollNo} Password {maadmission.SPassword} For further information & exam registration, contact: 9876533888";
                            msg1 = $"Dear Student {maadmission.FirstName}, welcome to SSC. Your roll number is {maadmission.RollNo}.For Homework, go to {HomeworkURL}. Username {maadmission.RollNo} Password {maadmission.SPassword}. For further information & exam registration, contact: 9876533888";
                            try
                            {
                                res=myobj.SendsmsWithResponse(maadmission.Mobile + "," + maadmission.AlternateNo, msg1);
                            }
                            catch (Exception)
                            {
                                res=myobj.SendsmsWithResponse(maadmission.Mobile, msg1);
                            }
                         
                        }
                        else
                        {
                            msg1 = $"Dear {maadmission.FirstName}, welcome to SSC. Your roll number is {maadmission.RollNo}. For further information & exam registration, contact: 9876533888";
                            try
                            {
                                res=myobj.SendsmsWithResponse(maadmission.Mobile + "," + maadmission.AlternateNo, msg1);
                            }
                            catch (Exception)
                            {
                                res=myobj.SendsmsWithResponse(maadmission.Mobile, msg1);
                            }

                        }
                        maadmission.SMSResponse = res;
                        maadmission.SmsSent = "Y";
                        db.SaveChanges();

                        int admissid = maadmission.Id;

                        // update in maenquiry table in which button is hide when isadmission is true. 
                        if (id > 0)
                        {
                            MaEnquiry enquiry = db.MaEnquiries.Find(id);

                            MaEnqFollow objenqfollow = db.MaEnqFollows.Where(z => z.ReferId == id && z.FollowUpType == "Enquiry").OrderByDescending(z => z.Id).First();
                            objenqfollow.Enqstatus = CommonUtil.CloseEnqFollow;

                            db.SaveChanges();


                            //added on 2 july 2013 to open visa prospective automatically is admisfsion is true
                            //=========================================================================
                            if (enquiry.IsVisaProspect)
                            {
                                AddEnqFollow(enquiry);
                            }
                            //=============================================================================

                            //set enquiry follow up status to close

                            List<MaEnqFollow> lstenq = db.MaEnqFollows.Where(z => z.ReferId == id && z.FollowUpType == "Enquiry").ToList();
                            if (lstenq.Count > 0)
                            {
                                foreach (MaEnqFollow obj in lstenq)
                                {
                                    obj.Enqstatus = CommonUtil.CloseEnqFollow;
                                    db.SaveChanges();
                                }
                            }

                            //insert higest qualification value of enquiry time to admission higest qualification value because two separate tables exist for both enquiry and admission time entry for highest qualicaion

                            List<Tb_Highestqual> lsthighestqual = new List<Tb_Highestqual>();
                            if (db.Tb_Highestqual.Where(z => z.EnquiryId == id).Count() > 0)
                            {
                                lsthighestqual = db.Tb_Highestqual.Where(z => z.EnquiryId == id).ToList();
                                foreach (Tb_Highestqual obj in lsthighestqual)
                                {
                                    Tb_AdmissHigQual objadmin = new Tb_AdmissHigQual();
                                    objadmin.AdmissionId = maadmission.Id;
                                    objadmin.SrNo = obj.SrNo;
                                    objadmin.Board = obj.Board;
                                    objadmin.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
                                    objadmin.CGPA = obj.CGPA;
                                    objadmin.Qualification = obj.Qualification;
                                    objadmin.Status = obj.Status;
                                    objadmin.YearOfPassing = obj.YearOfPassing;
                                    db.Tb_AdmissHigQual.Add(objadmin);
                                    db.SaveChanges();
                                }
                            }


                            //insert Employment value of enquiry time to admission Employment value because two separate tables exist for both enquiry and admission time entry for Employmenthistory
                            List<Tb_EmployHist> lstemphist = new List<Tb_EmployHist>();
                            if (db.Tb_EmployHist.Where(z => z.EnquiryId == id).Count() > 0)
                            {
                                lstemphist = db.Tb_EmployHist.Where(z => z.EnquiryId == id).ToList();
                                foreach (Tb_EmployHist obj in lstemphist)
                                {
                                    Tb_AdmissEmpHist objadmin = new Tb_AdmissEmpHist();
                                    objadmin.AdmissionId = maadmission.Id;
                                    objadmin.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
                                    objadmin.Company = obj.Company;
                                    objadmin.DurationOfExp = obj.DurationOfExp;
                                    objadmin.Fdate = obj.Fdate;
                                    objadmin.JobResponsibility = obj.JobResponsibility;
                                    objadmin.PositionHeld = obj.PositionHeld;
                                    objadmin.SrNo = obj.SrNo;
                                    objadmin.Status = obj.Status;
                                    objadmin.Tdate = obj.Tdate;
                                    db.Tb_AdmissEmpHist.Add(objadmin);
                                    db.SaveChanges();
                                }
                            }
                        }
                        cu.SendEmailToUser(maadmission, true);
                        /*----send msg on mobile----------*/
                        string msg = "Dear " + maadmission.FirstName.ToUpper() + " " + maadmission.LastName.ToUpper() + " Welcome To SSC.Your Roll No is " + maadmission.RollNo + " We Wish You Happy Learning \n SSC TEAM";
                        cu.SendMessage(maadmission.Mobile, msg);
                        /*-------------------------------------*/

                        return RedirectToAction("CourseHistory/" + admissid);
                    }
                    else
                    {

                        ViewBag.ErrorMessage = "Form Not Filled Correctly. To remove please Fill all (*) Marked Fields";
                    }


                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {

                        }
                    }
                }

            }
            else
            {
                ViewBag.ErrorMessage = "Plz Select Enquiry About";
            }

            //Code added 24 june 2013==================================

            ViewBag.AreaId = new SelectList(db.Ma_Area, "Id", "Area", maadmission.AreaId);
            ViewBag.EnquiryId = new SelectList(db.MaEnquiries, "Id", "FirstName", maadmission.EnquiryId);
            /*--*/
            ViewBag.CounselorName = new SelectList(cu.GetUserList(branchid), "Id", "Fullname", maadmission.CounselorName);
            ViewBag.ReferenceId = new SelectList(db.MaFrmReferences, "Id", "ContactPerson", maadmission.ReferenceId);

            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout");
            return View(maadmission);
        }

        //added 2 july 2013
        //insert entry into maenqfollow if isvsaprospective is true
        private void AddEnqFollow(MaEnquiry maenquiry)
        {
            MaEnqFollow obj = new MaEnqFollow();
            obj.NextDate = maenquiry.WalkInDate.Value.AddDays(2);
            obj.CDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);
            obj.EnqFollowBy = maenquiry.CounselorName;
            obj.ReferId = maenquiry.Id;
            obj.FollowUpType = "VisaEnquiry";
            obj.Enqstatus = "Open";
            obj.Remarks = "Visa Prospective follow up opened on" + string.Format("{0: dd MMM,yyyy HH:mm tt}", DateTime.Now.AddMinutes(StaticLogic.TimeDifference));
            db.MaEnqFollows.Add(obj);
            db.SaveChanges();
        }


        //
        // GET: /MaAdmission/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ProspStudent, UserAction.Edit);
                string chbranch = Request.Cookies["chbranch"].Value;


                if (Prmsn == false || chbranch == "Y")
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            //Code added 24 june==================================
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            MaAdmission maadmission = db.MaAdmissions.Find(id);
            try
            {

                if (maadmission == null)
                {
                    return HttpNotFound();
                }
                ViewBag.EnquiryId = new SelectList(db.MaEnquiries, "Id", "FirstName", maadmission.EnquiryId);
                if (maadmission.BranchId == branchid)
                {
                    ViewBag.CounselorName = new SelectList(cu.GetUserList(branchid), "Id", "Fullname", maadmission.CounselorName); // changed on 24 june problem like user coming all
                    ViewBag.branch = "Y";
                }
                else
                {
                    //maadmission.BranchId = maadmission.BranchId;
                    maadmission.CounselorName = maadmission.CounselorName;
                    ViewBag.branch = "N";
                }
            }
            catch (Exception ex)
            { }
            ViewBag.AreaId = new SelectList(db.Ma_Area, "Id", "Area", maadmission.AreaId);
            //ViewBag.CounselorName = new SelectList(cu.GetUserList(branchid), "Id", "Fullname", maadmission.CounselorName);
            ViewBag.ReferenceId = new SelectList(db.MaFrmReferences, "Id", "ContactPerson", maadmission.ReferenceId);

            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout", maadmission.EnqAbt);
            return View(maadmission);
        }

        //
        // POST: /MaAdmission/Edit/5

        [HttpPost]
        public ActionResult Edit(MaAdmission maadmission)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ProspStudent, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            //Code added 24 june==================================
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            string filename = maadmission.StudPhoto;
            string EnqAbt = db.MaAdmissions.Where(x => x.Id == maadmission.Id).Select(x => x.EnqAbt).FirstOrDefault();
            if (ModelState.IsValid)
            {

                try
                {
                    foreach (string inputTagName in Request.Files)
                    {
                        HttpPostedFileBase file = Request.Files[inputTagName];
                        if (file.ContentLength > 0)
                        {
                            filename = Guid.NewGuid().ToString().Substring(0, 5) + file.FileName;
                            string year = Convert.ToString(DateTime.Now.Year);
                            string filePath = Path.Combine(Server.MapPath("~/Content/themes/base/images/Documents/" + year + "/"), filename);
                            file.SaveAs(filePath);

                        }

                    }
                }
                catch (Exception ex)
                {
                }
                if (maadmission.IsLeft == true)
                {
                    maadmission.IsLeft = true;
                    maadmission.LeftDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                    maadmission.CounselorName = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                }
                maadmission.IsSync = false;
                if (maadmission.EnqAbt == "IELTS" && EnqAbt != "IELTS")
                {
                    maadmission.SmsSent = "Y";
                }

                db.Entry(maadmission).State = System.Data.Entity.EntityState.Modified;
                //maadmission.Cdate = DateTime.Now.Date;
                maadmission.LastUpdatedDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                maadmission.Status = true;
                if (filename != "")
                {
                    maadmission.StudPhoto = filename;
                }

                db.SaveChanges();
                Status myobj = new Status();
                string HomeworkURL = "http://sscstudentlogin.com";
                string msg1 = "";
                if (maadmission.EnqAbt == "IELTS" && EnqAbt != "IELTS")
                {
                    msg1 = $"Dear Student {maadmission.FirstName}, welcome to SSC. Your roll number is {maadmission.RollNo}.For Homework, go to {HomeworkURL}. Username {maadmission.RollNo} Password {maadmission.SPassword}. For further information & exam registration, contact: 9876533888";
                    myobj.Sendsms(maadmission.Mobile, msg1);
                    if (maadmission.AlternateNo != "" && maadmission.AlternateNo != null)
                    {
                        myobj.Sendsms(maadmission.AlternateNo, msg1);
                    }
                }
                return RedirectToAction("Index");
            }
            ViewBag.EnquiryId = new SelectList(db.MaEnquiries, "Id", "FirstName", maadmission.EnquiryId);
            ViewBag.CounselorName = new SelectList(cu.GetUserList(branchid), "Id", "Fullname", maadmission.CounselorName);
            ViewBag.ReferenceId = new SelectList(db.MaFrmReferences, "Id", "ContactPerson", maadmission.ReferenceId);
            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout", maadmission.EnqAbt);
            ViewBag.AreaId = new SelectList(db.Ma_Area, "Id", "Area", maadmission.AreaId);
            return View(maadmission);
        }




        //
        // GET: /MaAdmission/Delete/5


        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Delete(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ProspStudent, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            try
            {
                MaAdmission admission = db.MaAdmissions.Find(id);

                db.MaAdmissions.Remove(admission);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        #endregion
        //------------------------MaAdmission end here---------------------------/


        //-------------------------course history start here------------------------------------.


        #region CourseHistory
        /// <summary>
        /// it return the list of course history during page load
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult _ListOfCourseHistory(int id = 0, int admisid = 0)
        {
            ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "Id", "BatchTime");
            if (admisid > 0)
            {
                //return PartialView(db.Tb_CourseHist.Where(z => z.IsActive == true && z.AdmissionId == admisid).OrderByDescending(z => z.Id).ToList());
                return PartialView(db.Tb_CourseHist.Where(z => z.AdmissionId == admisid).OrderByDescending(z => z.Id).ToList());
            }
            //return PartialView(db.Tb_CourseHist.Where(z => z.IsActive == true && z.AdmissionId == id).OrderByDescending(z => z.Id).ToList());
            return PartialView(db.Tb_CourseHist.Where(z => z.AdmissionId == id).OrderByDescending(z => z.Id).ToList());
        }



        //course history detail

        public ActionResult _CourseHisDetail(int id, int admisid = 0)
        {
            ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "Id", "BatchTime");
            if (admisid > 0)
            {
                return PartialView(db.Tb_CourseHist.Where(z => z.IsActive == true && z.AdmissionId == admisid && z.MaCourse.CourseName == "IELTS").OrderByDescending(z => z.Id).ToList());
                //  return PartialView(db.Tb_CourseHist.Where(z => z.AdmissionId == admisid).OrderByDescending(z => z.Id).ToList());
            }
            if (db.Tb_CourseHist.Where(z => z.AdmissionId == id).OrderByDescending(z => z.Id).Count() > 0)
            {
                return PartialView(db.Tb_CourseHist.Where(z => z.IsActive == true && z.AdmissionId == id && z.MaCourse.CourseName == "IELTS").OrderByDescending(z => z.Id).ToList());
                //return PartialView(db.Tb_CourseHist.Where(z => z.AdmissionId == id).OrderByDescending(z => z.Id).ToList());
            }
            List<Tb_CourseHist> lst = new List<Tb_CourseHist>();
            return PartialView(lst);

        }


        public ActionResult CourseHistory(int id = 0, string validatnMsg = "")
        {
            if (id > 0)
            {
                MaAdmission objadmission = db.MaAdmissions.Find(id);
                ViewBag.Sid = id;
                // TempData["Sidd"] = id;
            }

            if (validatnMsg == "Y")
            {
                ViewBag.msg = "All * marked fields are required";
            }

            if (validatnMsg == "dup")
            {
                ViewBag.msg = "Package With same name and same date is already exist";
            }
            //ViewBag.AdmissionId = new SelectList(db.MaAdmission, "Id", "FirstName");

            ViewBag.ie = new SelectList("", "", "");
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName");
            ViewBag.PackageId = new SelectList(db.MaPackages.Where(z => z.PackageDays != null).ToList(), "Id", "PackageName");
            ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "Id", "BatchTime");
            ViewBag.PackageHours = new SelectList(listing.GetPackageHours, "PackageHours", "PackageHours");


            return View();

        }




        public ActionResult _CourseHistoryCreate(int id = 0)
        {
            if (id > 0)
            {
                MaAdmission objadmission = db.MaAdmissions.Find(id);
                ViewBag.Sid = id;
            }

            //ViewBag.AdmissionId = new SelectList(db.MaAdmission, "Id", "FirstName");
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName");
            ViewBag.PackageId = new SelectList(db.MaPackages.Where(z => z.PackageDays != null).ToList(), "Id", "PackageName");
            ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "Id", "BatchTime");
            Tb_CourseHist history = new Tb_CourseHist();
            //string date =

            history.FromDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            return PartialView(history);

        }

        //Create Course History 
        [HttpPost]
        public ActionResult _CourseHistoryCreate(Tb_CourseHist coursehis, int Course, int Package, string Group, int Time, DateTime FromDate, bool IsActive, int id, string remarks)
        {
            string chbranch = Request.Cookies["chbranch"].Value;
            if (chbranch == "Y")
            {
                return View("~/Views/Shared/AccessDenied.cshtml");
            }

            try
            {
                db.Tb_CourseHist.Add(coursehis);
                coursehis.Status = true;
                coursehis.CourseId = Course;
                coursehis.PackageId = Package;
                coursehis.TimeId = Time;
                coursehis.FromDate = FromDate;
                coursehis.GroupName = Group;
                double todates = (double)db.MaPackages.Where(z => z.Id == Package && z.CourseId == Course).Select(z => z.PackageDays).First();
                coursehis.Amout = (double)db.MaPackages.Where(z => z.Id == Package).Select(z => z.Amount).First();
                coursehis.Todate = FromDate.AddDays(todates);
                coursehis.IsActive = IsActive;
                coursehis.AdmissionId = id;
                coursehis.DueDate = FromDate;
                coursehis.currentcourselevel = Group;
                coursehis.CourseRemarks = remarks;
                db.SaveChanges();
                int courseid = coursehis.Id;
                try
                {
                    Tb_CourseHist tb_cst = db.Tb_CourseHist.Find(courseid);
                    MaCourse crs = db.MaCourses.Where(x => x.Id == tb_cst.CourseId).FirstOrDefault();
                    MaAdmission maad = db.MaAdmissions.Where(x => x.Id == tb_cst.AdmissionId).FirstOrDefault();
                    string Crse = crs.CourseName;
                    string Mobile = maad.Mobile;
                    string FName = maad.FirstName;
                    int? Roll = maad.RollNo;
                    string Pass = maad.SPassword;
                    Status myobj = new Status();
                    string HomeworkURL = "http://sscstudentlogin.com";
                    string msg1 = "";
                    if (Crse == "IELTS")
                    {

                        msg1 = $"Dear Student {FName}, welcome to SSC. Your roll number is {Roll}.For Homework, go to {HomeworkURL}. Username {Roll} Password {Pass}. For further information & exam registration, contact: 9876533888";
                        myobj.Sendsms(Mobile, msg1);
                        if (maad.AlternateNo != "" && maad.AlternateNo != null)
                        {
                            myobj.Sendsms(maad.AlternateNo, msg1);
                        }
                        maad.SmsSent = "Y";
                        db.SaveChanges();
                    }



                }
                catch (Exception ex)
                {

                    throw;
                }
                //----------------------------insert to ledger--------------------

                int studentid = (int)coursehis.AdmissionId;
                int rollno = (int)db.MaAdmissions.Where(z => z.Id == studentid).Select(z => z.RollNo).First();


                CommonUtil.InsertToLedger(coursehis.Id, coursehis.Amout, 0.0, String.Format("{0}", "Course Fee Against Admission"), rollno, DateTime.Now.AddMinutes(StaticLogic.TimeDifference), "CourseFees");

                //--------------------code to insert installment plan-------------------------
                int packid = (int)coursehis.PackageId;


                // int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

                List<MaDetail> detail = db.MaDetails.Where(z => z.PackageId == packid).ToList();
                int i = 1;
                MaAdmission admissid = db.MaAdmissions.Find(id);
                double dd = 0;
                DateTime? dtprev = null;
                foreach (MaDetail obj in detail)
                {
                    Tb_Installment objins = new Tb_Installment();
                    objins.Amount = obj.InstallAmt;
                    objins.InstallmentNo = i;
                    objins.CourseId = coursehis.CourseId;
                    objins.CourseHistId = coursehis.Id;
                    objins.NextDayDuration = obj.NextDurationDays;

                    /*because pehli entry tah addmission day (Today)+ first installment days from master
                te next installment date hougi tb_installment d pichli duedate + installment days from master plan 
                */


                    if (i == 1)
                    {
                        objins.DueDate = FromDate;
                    }
                    else
                    {
                        objins.DueDate = dtprev.Value.AddDays(dd);
                    }
                    dd = Convert.ToDouble(obj.NextDurationDays);
                    dtprev = objins.DueDate;
                    objins.Status = true;
                    objins.IsFeesPaid = false;
                    objins.BranchId = admissid.BranchId;
                    db.Tb_Installment.Add(objins);
                    db.SaveChanges();
                    i++;
                }
                return RedirectToAction("GoToReceipt", new { admid = coursehis.AdmissionId });
            }
            catch (Exception ex)
            {
                return RedirectToAction("GoToReceipt", new { admid = coursehis.AdmissionId });
            }
        }



        /// <summary>
        /// Tb_CourseHist create is statrts here.
        /// </summary>
        /// <param name="coursehis"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CourseHistory(Tb_CourseHist coursehis, int id, FormCollection frm)
        {

            string chbranch = Request.Cookies["chbranch"].Value;

            if (chbranch == "Y")
            {
                return View("~/Views/Shared/AccessDenied.cshtml");
            }

            if (ModelState.IsValid)
            {
                int callerid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                string fullname = db.Users.Where(a => a.Id == callerid).Select(a => a.Fullname).FirstOrDefault();

                string group = frm["ie"].ToString();
                coursehis.EnterBy = fullname;
                db.Tb_CourseHist.Add(coursehis);
                coursehis.Status = true;
                coursehis.DueDate = coursehis.FromDate;
                coursehis.GroupName = group;
                coursehis.CourseRemarks = coursehis.CourseRemarks;
                coursehis.currentcourselevel = group;
                double todates = (double)db.MaPackages.Where(z => z.Id == coursehis.PackageId).Select(z => z.PackageDays).First();
                coursehis.Todate = coursehis.FromDate.Value.AddDays(todates);
                int? packageid = coursehis.PackageId;
                double? amount = db.MaPackages.Where(z => z.Id == packageid).Select(z => z.Amount).First();
                coursehis.Amout = amount;

                db.SaveChanges();
                studenthistory(coursehis, id, "");
                int courseid = coursehis.Id;
                try
                {
                    Tb_CourseHist tb_cst = db.Tb_CourseHist.Find(courseid);
                    MaCourse crs = db.MaCourses.Where(x => x.Id == tb_cst.CourseId).FirstOrDefault();
                    MaAdmission maad = db.MaAdmissions.Where(x => x.Id == tb_cst.AdmissionId).FirstOrDefault();
                    string Crse = crs.CourseName;
                    string Mobile = maad.Mobile;
                    string FName = maad.FirstName;
                    int? Roll = maad.RollNo;
                    string Pass = maad.SPassword;
                    Status myobj = new Status();
                    string HomeworkURL = "http://sscstudentlogin.com";
                    string msg1 = "";
                    if (Crse == "IELTS")
                    {
                        msg1 = $"Dear Student {FName}, welcome to SSC. Your roll number is {Roll}.For Homework, go to {HomeworkURL}. Username {Roll} Password {Pass}. For further information & exam registration, contact: 9876533888";
                        myobj.Sendsms(Mobile, msg1);
                        if (maad.AlternateNo != "" && maad.AlternateNo != null)
                        {
                            myobj.Sendsms(maad.AlternateNo, msg1);
                        }
                        maad.SmsSent = "Y";
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }



                //----------------------------insert to ledger--------------------




                int studentid = (int)coursehis.AdmissionId;
                int rollno = (int)db.MaAdmissions.Where(z => z.Id == studentid).Select(z => z.RollNo).First();


                CommonUtil.InsertToLedger(coursehis.Id, coursehis.Amout, 0.0, String.Format("{0}", "Course Fee Against Admission"), rollno, DateTime.Now.AddMinutes(StaticLogic.TimeDifference), "CourseFees");


                //--------------------code to insert installment plan-------------------------
                int packid = (int)coursehis.PackageId;


                // int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);




                List<MaDetail> detail = db.MaDetails.Where(z => z.PackageId == packid).ToList();
                int i = 1;
                MaAdmission admissid = db.MaAdmissions.Find(id);
                double dd = 0;
                DateTime? dtprev = null;
                foreach (MaDetail obj in detail)
                {
                    Tb_Installment objins = new Tb_Installment();
                    objins.Amount = obj.InstallAmt;
                    objins.InstallmentNo = i;
                    objins.CourseId = coursehis.CourseId;
                    objins.CourseHistId = coursehis.Id;
                    objins.NextDayDuration = obj.NextDurationDays;

                    /*because pehli entry tah addmission day (Today)+ first installment days from master
                te next installment date hougi tb_installment d pichli duedate + installment days from master plan 
                */


                    if (i == 1)
                    {
                        objins.DueDate = coursehis.FromDate;
                    }
                    else
                    {
                        objins.DueDate = dtprev.Value.AddDays(dd);
                    }
                    dd = Convert.ToDouble(obj.NextDurationDays);
                    dtprev = objins.DueDate;
                    objins.Status = true;
                    objins.IsFeesPaid = false;
                    objins.BranchId = admissid.BranchId;
                    db.Tb_Installment.Add(objins);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException.InnerException is SqlException)
                        {
                            SqlException sql = (SqlException)ex.InnerException.InnerException;
                            if (sql.Number == 2627)
                            {
                                return RedirectToAction("CourseHistory", new { id = coursehis.AdmissionId, validatnMsg = "dup" });
                            }
                        }
                    }

                    i++;
                }
            }

            if (ModelState.IsValid == false)
            {

                return RedirectToAction("CourseHistory/" + id, new { @validatnMsg = "Y" });
            }

            return RedirectToAction("GoToReceipt", new { admid = coursehis.AdmissionId });

        }
        /// <summary>
        /// Student Course history 
        /// </summary>
        /// <param name="coursehis"></param>
        /// <param name="id"></param>
        private void studenthistory(Tb_CourseHist coursehis, int id, string grup)
        {
            try
            {
                StudentCourseHisorty student = new StudentCourseHisorty();
                student.Package = Convert.ToString(coursehis.PackageId);
                student.CourseHistId = coursehis.Id;
                student.StudentId = id;
                student.Time = Convert.ToString(coursehis.TimeId);
                student.Course = Convert.ToString(coursehis.CourseId);
                if (grup != "")
                {

                    student.CGroup = Convert.ToString(grup);
                }
                else
                {
                    student.CGroup = Convert.ToString(coursehis.GroupName);
                }

                student.CDate = coursehis.FromDate;
                student.Status = coursehis.Status;

                student.IsActive = coursehis.IsActive;
                student.LastUpdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                student.EnterBy = coursehis.EnterBy;

                student.CurrentCourse = coursehis.currentcourselevel;
                db.StudentCourseHisorties.Add(student);
                db.SaveChanges();
            }
            catch (Exception ex)
            {


            }


        }

        private bool CheckIfFeePaid(int coursehistid)
        {
            List<Tb_Installment> lstinstallment = db.Tb_Installment.Where(m => m.CourseHistId == coursehistid).Where(m => m.IsFeesPaid == true).ToList();


            if (lstinstallment.Count > 0)
            {
                return true;
            }

            return false;

        }


        /// <summary>
        /// Edit for Tb_CourseHist
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CourseHistoryEdit(int id = 0, int admisid = 0, string course = "No", string validatnMsg = "")
        {
           

            Tb_CourseHist tb_coursehist = db.Tb_CourseHist.Find(id);
            if (tb_coursehist == null)
            {
                return HttpNotFound();
            }
            if (validatnMsg == "Y")
            {
                ViewBag.msg = "All * marked fields are required";
            }
            ViewBag.disabled = CheckIfFeePaid(id);
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", tb_coursehist.CourseId);
            ViewBag.PackageId = new SelectList(db.MaPackages.Where(z => z.PackageDays != null).ToList(), "Id", "PackageName", tb_coursehist.PackageId);
            ViewBag.currentcourselevel = new SelectList(db.St_CourseGroup, "GroupName", "GroupName", tb_coursehist.currentcourselevel);
            ViewBag.ie = new SelectList(db.St_CourseGroup.Where(a => a.CourseId == tb_coursehist.CourseId), "GroupName", "GroupName", tb_coursehist.GroupName);
            ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "TimeId", "BatchTime", tb_coursehist.TimeId);
            ViewBag.PackageHours = new SelectList(listing.GetPackageHours, "PackageHours", "PackageHours");
            EditStudentCourseHisotry(admisid, tb_coursehist);
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            return View(tb_coursehist);
        }

        private void EditStudentCourseHisotry(int admisid, Tb_CourseHist tb_coursehist)
        {
            try
            {
                StudentCourseHisorty objstudent = db.StudentCourseHisorties.Where(z => z.StudentId == admisid).FirstOrDefault();
                objstudent.StudentId = admisid;
                objstudent.Package = Convert.ToString(tb_coursehist.PackageId);
                objstudent.Time = Convert.ToString(tb_coursehist.TimeId);
                objstudent.Course = Convert.ToString(tb_coursehist.CourseId);
                objstudent.CGroup = Convert.ToString(tb_coursehist.GroupName);
                objstudent.CDate = tb_coursehist.FromDate;
                objstudent.Status = tb_coursehist.Status;


                //Code Added by Gourav for add colms and show last update on 22-dec-2015
                objstudent.EnterBy = tb_coursehist.EnterBy;
                objstudent.IsActive = tb_coursehist.IsActive;
                objstudent.LastUpdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;


                objstudent.CurrentCourse = tb_coursehist.currentcourselevel;
                db.SaveChanges();
            }
            catch (Exception ex) { }
        }


        [HttpPost]
        public ActionResult CourseHistoryEdit(Tb_CourseHist tb_coursehist, int admisid, FormCollection frm, string course = "No")
        {
            int course_hist_id = Convert.ToInt32(tb_coursehist.Id); // this is actually coursehistid;

            // if fees is paid then we donnot need to create installment again,just we can edit time and active ,inactive
            if (CheckIfFeePaid(course_hist_id))
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        Tb_CourseHist newcoursehistobj = db.Tb_CourseHist.Find(course_hist_id);
                        newcoursehistobj.TimeId = tb_coursehist.TimeId;

                        string usrtype = Request.Cookies[CommonUtil.CookieUsertype].Value;
                        if (usrtype == CommonUtil.UserTypeAdmin || usrtype == CommonUtil.UserTypeSuperAdmin || Request.Cookies[SSCMvc.Models.CommonUtil.CookieRoleid].Value == "1008")
                        {
                            try
                            {
                                newcoursehistobj.DueDate = Convert.ToDateTime(frm["DueDate"]);
                            }
                            catch (Exception ex)
                            { }
                            if (newcoursehistobj.DueDate == null)
                            {
                                newcoursehistobj.DueDate = newcoursehistobj.FromDate;
                            }
                        }
                        else
                        {
                            //tb_coursehist.DueDate = tb_coursehist.DueDate;
                            try
                            {
                                string hdnvalue = (frm["hdnduedate"]);

                                if (hdnvalue == null)
                                {
                                    newcoursehistobj.DueDate = null;
                                }
                                else
                                {
                                    newcoursehistobj.DueDate = Convert.ToDateTime(hdnvalue);
                                }
                            }
                            catch (Exception ex)
                            { }
                            if (newcoursehistobj.DueDate == null)
                            {
                                newcoursehistobj.DueDate = newcoursehistobj.FromDate;
                            }
                        }


                        newcoursehistobj.IsActive = tb_coursehist.IsActive;
                        newcoursehistobj.currentcourselevel = tb_coursehist.currentcourselevel;
                        newcoursehistobj.CourseRemarks = tb_coursehist.CourseRemarks;
                        newcoursehistobj.IsSync = false;
                        db.SaveChanges();
                        studenthistory(newcoursehistobj, admisid, "");
                    }

                    if (ModelState.IsValid == false)
                    {
                        return RedirectToAction("CourseHistoryEdit/" + course_hist_id, new { @admisid = admisid, @course = "No", @validatnMsg = "Y" });
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.error = "error";
                    cu.SendError(ex);
                }

                //}
                //else
                //{
                //    ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", tb_coursehist.CourseId);
                //    ViewBag.PackageId = new SelectList(db.MaPackages.Where(z => z.PackageDays != null).ToList(), "Id", "PackageName", tb_coursehist.PackageId);
                //    ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "Id", "BatchTime", tb_coursehist.TimeId);
                //    //return View(tb_coursehist);

                //    return RedirectToAction("CourseHistoryEdit/" + course_hist_id, new { @admisid = admisid, @course = course });
                //}
            }
            else
            {
                try
                {

                    if (ModelState.IsValid)
                    {
                        string group = frm["ie"].ToString();
                        db.Entry(tb_coursehist).State = System.Data.Entity.EntityState.Modified;

                        tb_coursehist.GroupName = group;
                        tb_coursehist.Status = true;


                        string usrtype = Request.Cookies[CommonUtil.CookieUsertype].Value;
                        if (usrtype == CommonUtil.UserTypeAdmin || usrtype == CommonUtil.UserTypeSuperAdmin)
                        {
                            try
                            {
                                tb_coursehist.DueDate = Convert.ToDateTime(frm["DueDate"]);
                            }
                            catch (Exception ex)
                            { }
                            if (tb_coursehist.DueDate == null)
                            {
                                tb_coursehist.DueDate = tb_coursehist.FromDate;
                            }
                        }
                        else
                        {
                            //tb_coursehist.DueDate = tb_coursehist.DueDate;
                            try
                            {
                                tb_coursehist.DueDate = Convert.ToDateTime(frm["hdnduedate"]);
                            }
                            catch (Exception ex)
                            {
                                ViewBag.error = "error";
                                cu.SendError(ex);
                            }
                            if (tb_coursehist.DueDate == null)
                            {
                                tb_coursehist.DueDate = tb_coursehist.FromDate;
                            }
                        }
                        int callerid = 0;
                        string fullname = "";
                        try
                        {
                            callerid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                        }
                        catch (Exception ex)
                        {
                            ViewBag.error = "error";
                            cu.SendError(ex);
                        }
                        try
                        {
                            fullname = db.Users.Where(a => a.Id == callerid).Select(a => a.Fullname).FirstOrDefault();
                        }
                        catch (Exception ex)
                        {
                            ViewBag.error = "error";
                            cu.SendError(ex);
                        }

                        double todates = (double)db.MaPackages.Where(z => z.Id == tb_coursehist.PackageId).Select(z => z.PackageDays).First();
                        tb_coursehist.Todate = tb_coursehist.FromDate.Value.AddDays(todates);

                        int? packageid = tb_coursehist.PackageId;
                        double? amount = db.MaPackages.Where(z => z.Id == packageid).Select(z => z.Amount).First();
                        tb_coursehist.currentcourselevel = group;
                        tb_coursehist.Amout = amount;
                        tb_coursehist.CourseRemarks = tb_coursehist.CourseRemarks;
                        tb_coursehist.EnterBy = fullname;
                        db.SaveChanges();

                        /*update installment in the table tb_installment*/
                        List<Tb_Installment> lstinstal = db.Tb_Installment.Where(z => z.CourseHistId == tb_coursehist.Id).ToList();
                        foreach (Tb_Installment objin in lstinstal)
                        {
                            db.Tb_Installment.Remove(objin);
                            db.SaveChanges();
                        }

                        //--------------------code to insert installment plan-------------------------
                        int packid = (int)tb_coursehist.PackageId;
                        // int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
                        List<MaDetail> detail = db.MaDetails.Where(z => z.PackageId == packid).ToList();
                        int i = 1;
                        MaAdmission admissid = db.MaAdmissions.Find(tb_coursehist.AdmissionId);
                        double dd = 0;
                        DateTime? dtprev = null;
                        foreach (MaDetail obj in detail)
                        {
                            Tb_Installment objins = new Tb_Installment();
                            objins.Amount = obj.InstallAmt;
                            objins.InstallmentNo = i;
                            objins.CourseId = tb_coursehist.CourseId;
                            objins.CourseHistId = tb_coursehist.Id;
                            objins.NextDayDuration = obj.NextDurationDays;

                            /*because pehli entry tah addmission day (Today)+ first installment days from master
                        te next installment date hougi tb_installment d pichli duedate + installment days from master plan 
                        */


                            if (i == 1)
                            {
                                objins.DueDate = tb_coursehist.FromDate;
                            }
                            else
                            {
                                objins.DueDate = dtprev.Value.AddDays(dd);
                            }
                            dd = Convert.ToDouble(obj.NextDurationDays);
                            dtprev = objins.DueDate;
                            objins.Status = true;
                            objins.IsFeesPaid = false;
                            objins.BranchId = admissid.BranchId;
                            db.Tb_Installment.Add(objins);
                            db.SaveChanges();
                            i++;
                        }

                        //update in to ledger table

                        int studentid = (int)tb_coursehist.AdmissionId;
                        int rollno = (int)db.MaAdmissions.Where(z => z.Id == studentid).Select(z => z.RollNo).First();

                        CommonUtil.UpdateToLedger(tb_coursehist.Id, tb_coursehist.Amout, 0.0, String.Format("{0}", "Course Fee Against Admission"), DateTime.Now.AddMinutes(StaticLogic.TimeDifference), rollno, "CourseFees");

                        //------------------------------------------------
                        studenthistory(tb_coursehist, admisid, group);
                        //------------------------------------------------
                    }

                    if (ModelState.IsValid == false)
                    {
                        //string address = "CourseHistoryEdit/" + course_hist_id + "?admisid=" + admisid + "&course=No";

                        return RedirectToAction("CourseHistoryEdit/" + course_hist_id, new { @admisid = admisid, @course = "No", @validatnMsg = "Y" });
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.error = "error";
                    cu.SendError(ex);
                    return View(tb_coursehist);
                }
            }
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", tb_coursehist.CourseId);
            ViewBag.PackageId = new SelectList(db.MaPackages.Where(z => z.PackageDays != null).ToList(), "Id", "PackageName", tb_coursehist.PackageId);
            ViewBag.TimeId = new SelectList(db.MaGroupTime_View, "Id", "BatchTime", tb_coursehist.TimeId);
            if (course == "Edit")
            {
                //return RedirectToAction("Edit", new { @id = tb_coursehist.AdmissionId });

                return RedirectToAction("PrintTransferStdnt", new { @id = tb_coursehist.Id, @CrsId = tb_coursehist.AdmissionId, @course = course });
            }
            else
            {
                return RedirectToAction("PrintTransferStdnt", new { @id = tb_coursehist.Id, @CrsId = tb_coursehist.AdmissionId, @course = course });
                //return RedirectToAction("CourseHistory", new { @id = tb_coursehist.AdmissionId });
            }
            return View(tb_coursehist);
        }


        public ActionResult PrintTransferStdnt()
        {
            return View("PrintTransferStdnt");

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PrintTransferStdnt(string CrsId)
        {
            if (CrsId != null)
            {
            }
            return View("PrintTransferStdnt");

        }



        //this is for receipt print
        public ActionResult PrintCrseHist(int id)
        {
            //print of the course history during edit time

            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetCrsHisReportList(id);

            var renderedBytes = reportViewModel.RenderReport();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);
        }


        //course history delete
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult _CourseHisDelete(int id)
        {
            try
            {
                Tb_CourseHist courhis = db.Tb_CourseHist.Find(id);

                db.Tb_CourseHist.Remove(courhis);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }

        }

        #endregion

        //-------------------------------------course history end here---------------------.


        #region EmploymentHistory

        //-----------------------------employmenthistory start here.--------------------------





        public ActionResult _EmploymentHisCreate(int id)
        {
            ViewBag.Id = id;
            return PartialView("_EmploymentHisCreate");
        }

        /// <summary>
        /// list of employment history.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult _ListEmploymentHist(int id, int admisid = 0)
        {
            ViewBag.Id = id;
            if (admisid > 0)
            {
                return PartialView(db.Tb_AdmissEmpHist.Where(z => z.AdmissionId == admisid).OrderByDescending(z => z.Id).ToList());
            }
            return PartialView(db.Tb_AdmissEmpHist.Where(z => z.AdmissionId == id).OrderByDescending(z => z.Id).ToList());
        }



        [HttpPost]
        public ActionResult _EmploymentHisCreate(Tb_AdmissEmpHist EmployHist, string PositionHeld, string Fdate, string Tdate, string Company, string JobResponsibility, int id = 0)
        {

            //MaAdmission maadmission = db.MaAdmission.Find(id);
            EmployHist.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            EmployHist.AdmissionId = id;
            int? srno = db.Tb_AdmissHigQual.Where(z => z.AdmissionId == id).Select(z => z.SrNo).Max();
            if (srno == null || srno == 0)
            {
                srno = 1;
            }
            else
            {
                srno = srno + 1;
            }
            EmployHist.SrNo = srno;
            try
            {
                EmployHist.Fdate = Convert.ToDateTime(Fdate);
                EmployHist.Tdate = Convert.ToDateTime(Tdate);
            }
            catch (Exception ex) { }
            EmployHist.Status = true;
            db.Tb_AdmissEmpHist.Add(EmployHist);
            db.SaveChanges();
            return RedirectToAction("_EmploymentHisCreate");

        }

        //Tb_AdmissEmpHist edit start here
        public ActionResult EmploymentHisEdit(int id, int admisid = 0, string course = "No")
        {

            Tb_AdmissEmpHist emphist = db.Tb_AdmissEmpHist.Find(id);
            return View(emphist);

        }

        [HttpPost]
        public ActionResult EmploymentHisEdit(Tb_AdmissEmpHist emphist, string course = "No")
        {
            if (ModelState.IsValid)
            {
                db.Entry(emphist).State = System.Data.Entity.EntityState.Modified;

                emphist.Status = true;
                db.SaveChanges();

                if (course == "Edit")
                {
                    return RedirectToAction("Edit", new { @id = emphist.AdmissionId });
                }
                else
                {
                    return RedirectToAction("CourseHistory", new { @id = emphist.AdmissionId });
                }
            }

            return View(emphist);
        }

        //employement history detail
        public ActionResult _EmpHistDetail(int id, int admisid = 0)
        {
            ViewBag.Id = id;
            if (admisid > 0)
            {
                return PartialView(db.Tb_AdmissEmpHist.Where(z => z.AdmissionId == admisid).OrderByDescending(z => z.Id).ToList());
            }
            return PartialView(db.Tb_AdmissEmpHist.Where(z => z.AdmissionId == id).OrderByDescending(z => z.Id).ToList());
        }


        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult DeleteEmpHist(int id)
        {
            try
            {
                Tb_AdmissEmpHist emphist = db.Tb_AdmissEmpHist.Find(id);
                db.Tb_AdmissEmpHist.Remove(emphist);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        #endregion
        //---------------------------employmenthistory end here.-------------------------------

        //------------------------------hihghest qualification starts here.-----------------------------

        #region HigeshtQualification


        public ActionResult _ListHighestQualification(int id = 0, int admisid = 0)
        {

            ViewBag.Id = id;

            if (admisid > 0)
            {
                return PartialView(db.Tb_CourseHist.Where(z => z.AdmissionId == admisid).OrderByDescending(z => z.Id).ToList());
            }
            return PartialView(db.Tb_AdmissHigQual.Where(z => z.AdmissionId == id).OrderByDescending(z => z.Id).ToList());
        }




        //Tb_AdmissHigQual edit start here
        public ActionResult HighestQualificationEdit(int id, int admisid = 0, string course = "No")
        {

            Tb_AdmissHigQual highestqual = db.Tb_AdmissHigQual.Find(id);
            return View(highestqual);

        }

        [HttpPost]
        public ActionResult HighestQualificationEdit(Tb_AdmissHigQual highestqual, string course = "No")
        {
            if (ModelState.IsValid)
            {
                db.Entry(highestqual).State = System.Data.Entity.EntityState.Modified;

                highestqual.Status = true;
                db.SaveChanges();

                if (course == "Edit")
                {
                    return RedirectToAction("Edit", new { @id = highestqual.AdmissionId });
                }
                else
                {
                    return RedirectToAction("CourseHistory", new { @id = highestqual.AdmissionId });
                }
            }

            return View(highestqual);
        }



        public ActionResult _HighestQualifCreate(int id)
        {
            return PartialView("_HighestQualifCreate");
        }

        [HttpPost]
        public ActionResult _HighestQualifCreate(Tb_AdmissHigQual highestquali, string Qualification = "", DateTime? YearOfPassing = null, double CGPA = 0, string Board = "", int id = 0)
        {
            highestquali.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            highestquali.AdmissionId = id;
            highestquali.Status = true;
            int? srno = db.Tb_AdmissHigQual.Where(z => z.AdmissionId == id).Select(z => z.SrNo).Max();
            if (srno == null || srno == 0)
            {
                srno = 1;
            }
            else
            {
                srno = srno + 1;
            }
            highestquali.SrNo = srno;
            highestquali.CGPA = CGPA;
            db.Tb_AdmissHigQual.Add(highestquali);
            db.SaveChanges();

            //return RedirectToAction("_HighestQualifCreate");
            return RedirectToAction("CourseHistory", new { @id = id });
        }

        //highest qualification detail
        public ActionResult _HighestQualiDetail(int id, int admisid = 0)
        {
            ViewBag.Id = id;

            if (admisid > 0)
            {
                return PartialView(db.Tb_CourseHist.Where(z => z.AdmissionId == admisid).OrderByDescending(z => z.Id).ToList());
            }
            return PartialView(db.Tb_AdmissHigQual.Where(z => z.AdmissionId == id).OrderByDescending(z => z.Id).ToList());
        }


        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult DelHighestQualifi(int id)
        {
            try
            {
                Tb_AdmissHigQual highestquali = db.Tb_AdmissHigQual.Find(id);
                db.Tb_AdmissHigQual.Remove(highestquali);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }


        #endregion

        //------------------------------hihghest qualification end here.-----------------------------

        /// <summary>
        /// it return the installment PopUplist  
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult _InstallmentList(int id)
        {

            if (id > 0)
            {
                MaAdmission objadmission = db.MaAdmissions.Find(id);
                ViewBag.Sid = id;
            }
            Tb_CourseHist coursehist = db.Tb_CourseHist.Find(id);
            //List<MaDetail> install = db.MaDetails.Where(z => z.CourseId == coursehist.CourseId).ToList();
            List<Tb_Installment> install = db.Tb_Installment.Where(m => m.CourseHistId == id).ToList();
            return PartialView(install);
        }

        //it show the student detail 
        public ActionResult _StudentDetail(int id)
        {

            MaAdmission lst = db.MaAdmissions.Find(id);
            return PartialView(lst);
        }



        private IList<MaPackage> GetPackage(int id, string hours)
        {
            if (hours != "" && hours != null)
            {
                return db.MaPackages.Where(m => m.CourseId == id && m.PackageHours == hours && m.PackageDays != null && m.Amount > 0).ToList();
            }
            else
            {
                return db.MaPackages.Where(m => m.CourseId == id && m.PackageDays != null && m.Amount > 0).ToList();

            }

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadPackageByCourse(string id, string Hour)
        {

            var PackageList = this.GetPackage(Convert.ToInt32(id), Hour);

            var colourData = PackageList.Select(c => new SelectListItem()
            {
                Text = c.PackageName,
                Value = c.Id.ToString(),

            });

            return Json(colourData, JsonRequestBehavior.AllowGet);
        }


        private IList<MaGroupTime_View> GetGroupTime(string id, string Hour)
        {
            if (Hour != "" && Hour != null)
            {
                return db.MaGroupTime_View.Where(m => m.GroupName == id && m.Status == true && m.PackageHours == Hour).OrderBy(z => z.BatchTime).ToList();
            }
            else
            {
                return db.MaGroupTime_View.Where(m => m.GroupName == id && m.Status == true).OrderBy(z => z.BatchTime).ToList();

            }
        }


        private IList<St_CourseGroup> GetGroupList(int id)
        {
            return db.St_CourseGroup.Where(m => m.CourseId == id).OrderBy(z => z.GroupName).ToList();

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadTimeByGroup(string id, string Hour)
        {
            var PackageList = this.GetGroupTime(id, Hour);

            var colourData = PackageList.Select(c => new SelectListItem()
            {
                Text = c.BatchTime,
                Value = c.Timeid.ToString(),

            });

            return Json(colourData, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetAvailSeat(int id, int PkgId, int CourseId, string GroupId)
        {
            MaTime time = db.MaTimes.Find(id);
            int? Avail = time.AvailSeat;

            if (Avail == null)
            {
                Avail = 0;
            }
            MaTime time2 = db.MaTimes.Where(x => x.TCode == time.TCode && x.TMCode == "9-6").FirstOrDefault();
            int Count = db.Tb_CourseHist.Where(x => x.TimeId == id && x.IsActive == true && x.CourseId == CourseId && x.GroupName == GroupId && x.MaAdmission.IsLeft == false).Count();
            int Count2 = db.Tb_CourseHist.Where(x => x.TimeId == time2.Id && x.IsActive == true && x.CourseId == CourseId && x.GroupName == GroupId && x.MaAdmission.IsLeft == false).Count();

            int? Tot = Avail - (Count + Count2);

            string Color = "";
            if (Tot <= 0)
            {
                Color = "#e74b4b";
            }
            else
            {
                Color = "#399d38";
            }
            var Colordata = new
            {
                Count = Tot,
                Color = Color
            };
            return Json(Colordata, JsonRequestBehavior.AllowGet);
        }




        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadTimeLstByPackage(string id, string Hour)
        {
            var PackageList = this.GetPackage(Convert.ToInt32(id), Hour);

            var colourData = PackageList.Select(c => new SelectListItem()
            {
                Text = c.PackageName,
                Value = c.Id.ToString(),

            });

            return Json(colourData, JsonRequestBehavior.AllowGet);
        }



        //Load Course group as bases of course

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadGroupByCourse(int id)
        {
            var PackageList = this.GetGroupList(id);

            var colourData = PackageList.Select(c => new SelectListItem()
            {
                Text = c.GroupName,
                Value = c.GroupName.ToString(),

            });

            return Json(colourData, JsonRequestBehavior.AllowGet);
        }


        /* -------------------------StudentCourse History-----------------*/


        public ActionResult _StudentCourseHistory(int id, int aid)
        {
            DateTime datee = Convert.ToDateTime(ConfigurationManager.AppSettings["Datee"]);
            //int stuid = ViewBag.Sid;
            //List<StudentCourseHisorty> objstud = db.StudentCourseHisorties.Where(m => m.StudentId == id).OrderByDescending(z => z.Id).ToList();
            DateTime? cda = db.StudentCourseHisorties.Where(a => a.StudentId == aid).Select(a => a.CDate).FirstOrDefault();
            if (cda < datee)
            {
                List<StudentCourseHisorty> objstud = db.StudentCourseHisorties.Where(m => m.StudentId == aid).ToList();
                return PartialView("_StudentCourseHistory", objstud);
            }
            else
            {
                List<StudentCourseHisorty> objstud = db.StudentCourseHisorties.Where(a => a.CourseHistId == id && a.StudentId == aid).ToList();
                return PartialView("_StudentCourseHistory", objstud);
            }
            //  Tb_CourseHist objstud = db.Tb_CourseHist.Where(a => a.Id == id && a.AdmissionId == aid).FirstOrDefault();

        }


        /*-----------------------Installment works starts here-----------------*/
        public ActionResult InstallmentIndex(int id)
        {

            ViewBag.Sid = id;

            ViewBag.RollNo = db.Tb_CourseHist.Where(z => z.Id == id).Select(z => z.MaAdmission.RollNo).First();
            string fname = db.Tb_CourseHist.Where(z => z.Id == id).Select(z => z.MaAdmission.FirstName).First();
            string lname = db.Tb_CourseHist.Where(z => z.Id == id).Select(z => z.MaAdmission.LastName).First();

            ViewBag.StudentName = fname + " " + lname;
            Tb_CourseHist coursehist = db.Tb_CourseHist.Find(id);
            //List<MaDetail> install = db.MaDetails.Where(z => z.CourseId == coursehist.CourseId).ToList();
            List<Tb_Installment> install = db.Tb_Installment.Where(m => m.CourseHistId == id).ToList();
            return View(install);
        }

        public ActionResult InstallmentEdit(int id)
        {
            try
            {
                Tb_Installment obj = db.Tb_Installment.Where(z => z.Id == id).First();
                ViewBag.CourseName = obj.MaCourse.CourseName;
                return View(obj);
            }
            catch (Exception ex)
            {
                return View();
            }
        }




        [HttpPost]
        public ActionResult InstallmentEdit(Tb_Installment objinstalment)
        {
            try
            {
                Tb_Installment obj = db.Tb_Installment.Where(z => z.Id == objinstalment.Id).First();
                obj.InstallmentNo = objinstalment.InstallmentNo;
                obj.Amount = objinstalment.Amount;
                obj.NextDayDuration = objinstalment.NextDayDuration;
                obj.DueDate = objinstalment.DueDate;
                db.SaveChanges();

                int courseid = (int)obj.CourseHistId;

                cu.UpdateCourseHistoryAmout(courseid);

                double amt = (double)obj.Tb_CourseHist.Amout;
                int rollno = (int)obj.Tb_CourseHist.MaAdmission.RollNo;

                CommonUtil.UpdateToLedger(courseid, amt, 0.0, String.Format("{0}", "Course Fee Against Admission"), DateTime.Now.AddMinutes(StaticLogic.TimeDifference), rollno, "CourseFees");

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(objinstalment);
            }
        }

        //
        // GET: /MaAdmission/Delete/5

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult InstallmentDelete(int id)
        {
            try
            {
                Tb_Installment obj = db.Tb_Installment.Where(z => z.Id == id).First();
                int courseid = (int)obj.CourseHistId;
                db.Tb_Installment.Remove(obj);

                db.SaveChanges();
                cu.UpdateCourseHistoryAmout(courseid);

                //=============================================================
                Tb_CourseHist courseHistneObj = db.Tb_CourseHist.Find(courseid);




                //======Code added on 12 july-2013============================
                double amt = (double)courseHistneObj.Amout;
                int rollno = (int)courseHistneObj.MaAdmission.RollNo;
                CommonUtil.UpdateToLedger(courseid, amt, 0.0, String.Format("{0}", "Course Fee Against Admission"), DateTime.Now.AddMinutes(StaticLogic.TimeDifference), rollno, "CourseFees");

                //=============================================================


                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        public ActionResult InstallmentCreate(int id)
        {
            int cid = (int)db.Tb_CourseHist.Where(z => z.Id == id).Select(z => z.CourseId).First();
            Tb_Installment obj = new Tb_Installment();
            obj.CourseHistId = id;
            obj.CourseId = cid;
            ViewBag.CourseName = db.Tb_CourseHist.Where(z => z.Id == id).Select(z => z.MaCourse.CourseName).First();

            int? max = db.Tb_Installment.Where(z => z.CourseHistId == id).Select(z => z.InstallmentNo).Max();
            if (max == 0 || max == null)
            {
                max = 1;
            }
            else
            {
                max += 1;
            }
            obj.InstallmentNo = max;
            return View(obj);

        }
        [HttpPost]
        public ActionResult InstallmentCreate(Tb_Installment installment)
        {
            try
            {
                int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieUserid].Value);
                installment.Status = true;

                int id = (int)installment.CourseHistId; //gets course history id to get the last value from the Tb_installment

                /* in case if user creates new entry, then pick up the last entry days value and add them in new days*/

                Tb_Installment prevobj = new Tb_Installment();
                if (db.Tb_Installment.Where(z => z.CourseHistId == id).OrderByDescending(z => z.Id).Count() > 0)
                {
                    prevobj = db.Tb_Installment.Where(z => z.CourseHistId == id).OrderByDescending(z => z.Id).First();
                    installment.DueDate = prevobj.DueDate.Value.AddDays(Convert.ToInt32(prevobj.NextDayDuration));
                }
                installment.BranchId = branchid;
                installment.IsFeesPaid = false;
                db.Tb_Installment.Add(installment);

                db.SaveChanges();
                int courseid = (int)installment.CourseHistId;
                cu.UpdateCourseHistoryAmout(courseid);
                Tb_CourseHist courseHistneObj = db.Tb_CourseHist.Find(courseid);

                // db.Tb_CourseHist.Where(z => z.Id == installment.CourseHistId).Select(z => z.MaCourse.CourseName).First();

                ViewBag.CourseName = courseHistneObj.MaCourse.CourseName;
                //======Code added on 12 july-2013============================
                double amt = (double)courseHistneObj.Amout;
                int rollno = (int)courseHistneObj.MaAdmission.RollNo;
                CommonUtil.UpdateToLedger(courseid, amt, 0.0, String.Format("{0}", "Course Fee Against Admission"), DateTime.Now.AddMinutes(StaticLogic.TimeDifference), rollno, "CourseFees");
                //=============================================================
                return RedirectToAction("Index");
            }
            //Tb_Installment obj = new Tb_Installment();
            //obj.CourseHistId = id;
            //ViewBag.CourseName = db.Tb_CourseHist.Where(z => z.Id == id).Select(z => z.MaCourse.CourseName).First();
            catch (Exception ex)
            {
                return View(installment);
            }

        }

        public ActionResult InstallmentDetail(int id)
        {
            Tb_Installment objinst = db.Tb_Installment.Where(z => z.Id == id).First();
            return View(objinst);
        }


        public ActionResult GoToReceipt(int admid)
        {
            ViewBag.admid = admid;
            return View();
        }




        public ActionResult AllStudent(string name = "", string mobile = "", string branch = "", string rollno = "", string fdate = "", string tdate = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ScoreReport, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            List<MaAdmission> lst = db.MaAdmissions.OrderByDescending(z => z.Id).ToList();

            if (name != "")
            {
                lst = lst.Where(z => z.FirstName.ToUpper().Contains(name.ToUpper())).ToList();
                ViewBag.name = name;
            }

            if (mobile != "")
            {

                lst = lst.Where(z => z.Mobile.Contains(mobile)).ToList();
                ViewBag.mobile = mobile;
            }

            if (rollno != "")
            {
                try
                {
                    int rno = Convert.ToInt32(rollno);
                    lst = lst.Where(z => z.RollNo == rno).ToList();
                }
                catch (Exception ex)
                {
                }
                ViewBag.rollno = rollno;
            }

            if (fdate != "" && tdate != "")
            {
                ViewBag.fdate = fdate;
                ViewBag.tdate = tdate;
                try
                {
                    DateTime dt1 = Convert.ToDateTime(fdate);
                    DateTime dt2 = Convert.ToDateTime(tdate);
                    lst = lst.Where(z => z.Cdate >= dt1 && z.Cdate <= dt2).ToList();
                }
                catch (Exception ex)
                {
                }

            }

            if (branch != "")
            {
                lst = lst.Where(z => z.MaBranch.BramchName.ToUpper().Contains(branch.ToUpper())).ToList();
            }

            ViewBag.TotalCount = lst.Count;
            return View(lst);
        }

        public PartialViewResult _VisaAdmDetail(int id, string reftype)
        {
            int? matchid = 0;
            MaAdmission maadmission = new MaAdmission();
            if (reftype == "IELTS")
            {
                matchid = db.Tb_IELTSResult.Where(z => z.Id == id).Select(z => z.StudentId).FirstOrDefault();

                maadmission = db.MaAdmissions.Find(id);
            }
            else if (reftype == "MockTest")
            {
                matchid = db.Tb_MockTest.Where(z => z.Id == id).Select(z => z.StudentId).FirstOrDefault();
                maadmission = db.MaAdmissions.Find(id);
            }
            else
            {
                maadmission = db.MaAdmissions.Find(id);
            }

            return PartialView(maadmission);
        }

        #region Remove Left Student

        public ActionResult RemoveLeftStudent()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.RemoveLeftStudent, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            return View("RemoveLeftStudent");
        }

        [HttpPost]
        public ActionResult RemoveLeftStudent(FormCollection form)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.RemoveLeftStudent, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            return View("RemoveLeftStudent");
        }
        #endregion


        /*----------------------Used for Lazy Loading--------------------------------*/

        #region Lazy-Load-Data

        public ActionResult LoadData(int id)
        {
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            var maadmissions = db.MaAdmissions.Where(z => z.BranchId == branchid).Where(z => z.Id < id).OrderByDescending(z => z.Id).ToList();
            ViewBag.TotalCount = maadmissions.Count;

            return PartialView("_LazyLoadData", maadmissions.Take(25).ToList());
        }

        #endregion


        //======show enquiry detail test

        //===used in visa payment when type is enquiry because in ielts entry can be from enquiry
        public ActionResult IeltsTestDetail(int id = 0)
        {
            //List<Tb_IELTSResult> lstIeltsResult = db.Tb_IELTSResult.Where(m => m.StudentType == "Enquiry").Where(m => m.StudentId == id).OrderByDescending(m => m.Id).ToList();
            List<Tb_IELTSResult> lstIeltsResult = db.Tb_IELTSResult.Where(m => m.StudentType == "Admission").Where(m => m.StudentId == id).OrderByDescending(m => m.Id).ToList();

            if (lstIeltsResult.Count > 0)
            {
                Tb_IELTSResult obj = lstIeltsResult.First();

                return PartialView("_IeltsLastScore", obj);
            }


            return HttpNotFound();


        }

        //===used in visa payment
        public ActionResult MockTestDetail(int id = 0)
        {
            List<Tb_MockTest> lstMocktestResult = db.Tb_MockTest.Where(m => m.StudentId == id).OrderByDescending(m => m.Id).ToList();

            if (lstMocktestResult.Count > 0)
            {
                Tb_MockTest obj = lstMocktestResult.First();

                return PartialView("_MockTestScore", obj);
            }

            return HttpNotFound();
        }


        //=== All Remark History (mock test,ielts)
        public ActionResult _RemarkHistory(int id = 0, string enqtype = "", string reftype = "", string stype = "", string name = "", int referid = 0, int visaid = 0, string rolno = "")
        {
            List<RemarkHistory> sturemarks = new List<Models.RemarkHistory>();
            RemarkHistory remrkhis = new RemarkHistory();

            if (enqtype == "Enquiry")
            {
                remrkhis.enqtype = reftype;

                //remrkhis.id = Mid;
                remrkhis.id = referid;
                remrkhis.visaid = visaid;
                remrkhis.rollno = Convert.ToInt32(rolno);
                remrkhis.name = name;
                int _countmocktst = db.Tb_MockTest.Where(m => m.StudentId == id).Select(m => m.Id).Count();

                if (_countmocktst > 0)
                {
                    int _mocktstid = db.Tb_MockTest.Where(m => m.StudentId == id).Select(z => z.Id).First();
                    remrkhis.mockid = _mocktstid;

                    remrkhis.mocktype = "VisaMockTest";


                }


                int count = db.Tb_IELTSResult.Where(m => m.StudentId == id).Select(m => m.Id).Count();
                if (count > 0)
                {

                    int ieltsid = db.Tb_IELTSResult.Where(m => m.StudentId == id).Select(m => m.Id).First();
                    if (ieltsid > 0)
                    {
                        remrkhis.ielttype = reftype;
                        remrkhis.enqtype = reftype;
                    }
                    else
                    {
                        remrkhis.ieltsid = 0;
                    }

                    remrkhis.ielttype = "IELTS";


                }
            }
            else if (enqtype == "Admission")
            {
                remrkhis.enqtype = reftype;

                remrkhis.id = referid;
                remrkhis.visaid = visaid;

                remrkhis.rollno = Convert.ToInt32(rolno);
                remrkhis.name = name;
                int _countmocktst = db.Tb_MockTest.Where(m => m.StudentId == id).Select(m => m.Id).Count();

                if (_countmocktst > 0)
                {
                    int _mocktstid = db.Tb_MockTest.Where(m => m.StudentId == id).OrderByDescending(z => z.StudentId).Select(z => z.Id).First();
                    remrkhis.mockid = _mocktstid;

                    remrkhis.mocktype = "MockTest";


                }


                int count = db.Tb_IELTSResult.Where(m => m.StudentId == id).Select(m => m.Id).Count();
                if (count > 0)
                {

                    int ieltsid = db.Tb_IELTSResult.Where(m => m.StudentId == id).Select(m => m.Id).First();
                    if (ieltsid > 0)
                    {
                        remrkhis.ieltsid = ieltsid;
                    }
                    else
                    {
                        remrkhis.ieltsid = 0;
                    }

                    remrkhis.ielttype = "IELTS";



                    if (stype == "IELTS")
                    {
                        remrkhis.ieltsid = id;
                    }
                }
            }
            sturemarks.Add(remrkhis);
            return PartialView("_RemarkHistory", sturemarks);



        }



        //-----------Upload_picture_or_not----------------
        #region

        private List<PhotoUploadHelper> GetData(string prntoptn)
        {

            List<PhotoUploadHelper> _lstphoto = new List<PhotoUploadHelper>();
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);

            if (prntoptn == "Uploaded")
            {
                ViewBag.check = "checked";

                List<MaAdmission> _lst = db.MaAdmissions.Where(z => z.StudPhoto != "NoImage.PNG" && z.BranchId == branchid).Where(z => z.IsLeft == false).ToList();

                foreach (MaAdmission obj in _lst)
                {
                    PhotoUploadHelper photo = new PhotoUploadHelper();
                    Tb_CourseHist objcrsehis = new Tb_CourseHist();
                    photo.Name = obj.FirstName + " " + obj.LastName;
                    photo.rollno = obj.RollNo.ToString();
                    int count = db.Tb_CourseHist.Where(z => z.AdmissionId == obj.Id).Count();
                    if (count > 0)
                    {
                        objcrsehis = db.Tb_CourseHist.Where(z => z.AdmissionId == obj.Id).First();
                        photo.GroupChoice = objcrsehis.GroupName;
                        photo.GroupTime = objcrsehis.MaTime.TCode + " " + objcrsehis.MaTime.TimeIn + " " + objcrsehis.MaTime.Indesc + " " + objcrsehis.MaTime.TimeOut + " " + objcrsehis.MaTime.OutDesc;
                    }

                    _lstphoto.Add(photo);
                }

                ViewBag.TotalCount = _lstphoto.Count();


            }
            else if (prntoptn == "NonUploaded")
            {
                ViewBag.check2 = "checked";
                List<MaAdmission> _lst = db.MaAdmissions.Where(z => z.StudPhoto == "NoImage.PNG" && z.BranchId == branchid).Where(z => z.IsLeft == false).ToList();

                foreach (MaAdmission obj in _lst)
                {
                    PhotoUploadHelper photo = new PhotoUploadHelper();
                    Tb_CourseHist objcrsehis = new Tb_CourseHist();
                    photo.Name = obj.FirstName + " " + obj.LastName;
                    photo.rollno = obj.RollNo.ToString();
                    int count = db.Tb_CourseHist.Where(z => z.AdmissionId == obj.Id).Count();
                    if (count > 0)
                    {
                        objcrsehis = db.Tb_CourseHist.Where(z => z.AdmissionId == obj.Id).First();
                        photo.GroupChoice = objcrsehis.GroupName;
                        photo.GroupTime = objcrsehis.MaTime.TCode + " " + objcrsehis.MaTime.TimeIn + " " + objcrsehis.MaTime.Indesc + " " + objcrsehis.MaTime.TimeOut + " " + objcrsehis.MaTime.OutDesc;
                    }
                    _lstphoto.Add(photo);
                }

            }
            return _lstphoto;
        }

        public ActionResult PhotoUpload(string prntoptn)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.PhotUploadReport, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            List<PhotoUploadHelper> _lstphoto = new List<PhotoUploadHelper>();
            _lstphoto = GetData(prntoptn);

            ViewBag.TotalCount = _lstphoto.Count();

            return View(_lstphoto);


        }


        //this is print of uploded photo
        public ActionResult PrintUplodedPhoto(string prntoptn)
        {
            var Business = new RecieptBuisness();
            var reportViewModel = Business.GetUploadphotoReportList(prntoptn, GetData(prntoptn));
            var renderedBytes = reportViewModel.RenderReport();
            if (reportViewModel.ViewAsAttachment)
                Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
            return File(renderedBytes, reportViewModel.LastmimeType);

        }


        #endregion

        #region Inquiry_Data

        public ActionResult InquiryData(string fdate = "", string tdate = "")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Performance, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            List<InquiryDataHelper> _lst = new List<InquiryDataHelper>();

            if (fdate.Length > 0 && tdate.Length > 0)
            {
                CommonUtil cu = new CommonUtil();
                int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
                int id = db.Users.Where(m => m.username == User.Identity.Name).Select(m => m.Id).Single();
                ViewBag.EnqHandle = new SelectList(cu.GetUserList(branchid), "Id", "username", id);


                //List<MaEnqFollow> lstenqfollow = db.MaEnqFollow.Where(z => z.FollowUpType == "Enquiry" && (z.Enqstatus == "Close" || z.Enqstatus == "Open")).ToList();
                //List<MaEnquiry> lstenquiry = new List<MaEnquiry>();
                //foreach (MaEnqFollow enqfollow in lstenqfollow)
                //{
                //    if (db.MaEnquiries.Where(z => z.Id == enqfollow.ReferId).Count() > 0)
                //    {
                //        //MaEnquiry obj = db.MaEnquiries.Where(z => z.Id == enqfollow.ReferId && z.BranchId == branchid).First();
                //        MaEnquiry obj = db.MaEnquiries.Where(z => z.Id == enqfollow.ReferId).First();
                //        lstenquiry.Add(obj);

                //    }
                //}

                List<MaEnquiry> lstenquiry = new List<MaEnquiry>();
                lstenquiry = db.MaEnquiries.ToList();
                //lstenquiry = lstenquiry.Where(z => z.BranchId == branchid).ToList();

                var lst2 = (from ta in lstenquiry
                            select new
                            {
                                ta.EnqHandledBy,

                            }).Distinct();


                DateTime fdate1 = DateTime.Parse(fdate);
                DateTime tdate1 = DateTime.Parse(tdate);

                ViewBag.fdate = fdate;
                ViewBag.tdate = tdate;

                int totalenquiryhandled = 0;
                int totalenquiryconvertered = 0;
                foreach (var obj in lst2)
                {
                    InquiryDataHelper _inqry = new InquiryDataHelper();
                    //_inqry.CounselorName = db.Users.Where(Z => Z.Id == obj.EnqHandledBy && Z.BranchId==branchid).Select(Z => Z.username).FirstOrDefault();
                    _inqry.CounselorName = db.Users.Where(Z => Z.Id == obj.EnqHandledBy).Select(Z => Z.username).FirstOrDefault();
                    int? enqhndid = obj.EnqHandledBy;

                    _inqry.enquirieshandled = lstenquiry.Where(z => z.EnqHandledBy == enqhndid && z.Cdate >= fdate1 && z.Cdate <= tdate1).Count();
                    totalenquiryhandled += _inqry.enquirieshandled;
                    ViewBag.totalenqryhandl = totalenquiryhandled;

                    _inqry.enquiriesconverted = lstenquiry.Where(z => z.EnqHandledBy == enqhndid && z.Cdate >= fdate1 && z.Cdate <= tdate1 && z.IsAdmission == true).Count();

                    totalenquiryconvertered += _inqry.enquiriesconverted;
                    try
                    {
                        _inqry.percenconversion = Math.Round((Convert.ToDouble(_inqry.enquiriesconverted) / Convert.ToDouble(_inqry.enquirieshandled)) * 100);
                    }
                    catch (Exception ex)
                    { }
                    ViewBag.totalenquiryconvertered = totalenquiryconvertered;

                    _lst.Add(_inqry);

                }

                ViewBag.TotalCount = _lst.Count();
                return View("InquiryData", _lst);
            }

            return View(_lst);

        }

        #endregion


        #region Demo History
        public ActionResult _DemoHistory(int id)
        {
            List<Tb_DemoHistory> obj = new List<Tb_DemoHistory>();
            obj = db.Tb_DemoHistory.Where(z => z.EnquiryId == id).ToList();
            return PartialView(obj);
        }

        #endregion

        public ActionResult _Index1(int id)
        {
            return PartialView(db.Tb_CourseHist.Where(z => z.AdmissionId == id).ToList());
        }


        #region Left Student Report Print

        public ActionResult LeftStudentPrint(string dt1 = "", string dt2 = "", string format = "")
        {
            if (dt1 == "" || dt2 == "")
            {
                ViewBag.fdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date.ToShortDateString();
                ViewBag.tdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(1.0).Date.ToShortDateString();
                return View();
            }
            else
            {

                string usrtype = Request.Cookies[CommonUtil.CookieUsertype].Value;
                if (usrtype == CommonUtil.UserTypeCounselor)
                {
                    try
                    {
                        DateTime fromdate = Convert.ToDateTime(dt1);
                        if (fromdate < DateTime.Now.AddMinutes(StaticLogic.TimeDifference).AddDays(-3))
                        {
                            return Content("You don't have permission to go to this date");
                        }
                    }
                    catch (Exception ex)
                    {
                        return Content("You don't have permission to go to this date");
                    }
                }

                var Business = new RecieptBuisness();
                var reportViewModel = Business.GetLeftStudentReport(dt1, dt2, format);

                var renderedBytes = reportViewModel.RenderReport();
                if (reportViewModel.ViewAsAttachment)
                    Response.AddHeader("content-disposition", reportViewModel.ReporExportFileName);
                return File(renderedBytes, reportViewModel.LastmimeType);
            }
        }
        #endregion


        public string GetHours(int pack)
        {
            string hour = "";

            try
            {
                hour = db.MaPackages.Where(a => a.Id == pack).Select(a => a.PackageHours).FirstOrDefault();
            }
            catch (Exception)
            {
            }

            return hour;
        }



    }

}
