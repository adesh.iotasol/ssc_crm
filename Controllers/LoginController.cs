﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SSCMvc.Models;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    //[Authorize]
    public class LoginController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /Login/

        public ActionResult Index()
        {
            return View("Login");

        }

        public ActionResult Login(string msg)
        {
            if (msg == "Y")
            {
                ViewBag.reset = "Your Password Has been Reset Successfully.Please Check Your Email.";

            }
            else if (msg == "N")
            {

                ViewBag.reset = "Please Enter Your UserName To  Reset The Password.";
            }
            else if (msg == "WrngPass")
            {
                ViewBag.reset = "The user name or password provided is incorrect.";
            }
            else if (msg == "EXPRE")
            {
                ViewBag.reset = "Your account is expied ,contact to admin.";
            }
            else if (msg == "Changed")
            {
                ViewBag.reset = "Your Password is Successfully Changed Login Here Again.";
            }
            else if (msg == "ad")
            {
                ViewBag.reset = "Access Denied";

            }
            return View();
        }

        public ActionResult LogOff()
        {
            if (Request.Cookies[CommonUtil.CookieBranchid] != null)
            {
                var c = new HttpCookie(CommonUtil.CookieBranchid);
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);

            }

            if (Request.Cookies[CommonUtil.CookieUserid] != null)
            {
                var c1 = new HttpCookie(CommonUtil.CookieUserid);
                c1.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c1);
            }

            if (Request.Cookies[CommonUtil.CookieUsertype] != null)
            {
                var c2 = new HttpCookie(CommonUtil.CookieUsertype);
                c2.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c2);
            }

            if (Request.Cookies[CommonUtil.CookieRoleid] != null)
            {
                var c3 = new HttpCookie(CommonUtil.CookieRoleid);
                c3.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c3);

            }
            FormsAuthentication.SignOut();
            if (Request.Cookies[CommonUtil.CookieBranchid] == null)
            {
                return new HttpUnauthorizedResult();
            }


            return new HttpUnauthorizedResult();
        }

        [HttpPost]
        public ActionResult Login(User model, string username, string password, string msg)
        {
            string expirydate = "";
            int count = (from p in db.Users where p.username == username && p.Password == password && p.Status == true select p).Count();

            User user = db.Users.Where(x => x.username == username && x.Password == password && x.Status == true).FirstOrDefault();
            if (user == null)
            {
                return RedirectToAction("Login", "Login", new { @msg = "WrngPass" });
            }
            string SUser = user.username;
            string SPassword = user.Password;
            if (SUser.Equals(username) && SPassword.Equals(password))
            {

                //return RedirectToAction("Login", "Login", new { @msg = "WrngPass" });


                //get record of the particular user on the base of user name and password
                User objuser = (from p in db.Users where p.username == username && p.Password == password && p.Status == true select p).FirstOrDefault();
                Tb_Prefrence tb_pref = db.Tb_Prefrence.FirstOrDefault();
                string IPAddress = GetComputer_InternetIP();
                try
                {
                    expirydate = objuser.expirydate.ToString();
                }
                catch (Exception ex)
                { }


                if (count == 1)
                {
                    //commented for testing 
                    if (tb_pref.IPAddress.Contains(IPAddress) || objuser.Usertype == CommonUtil.UserTypeAdmin || objuser.Usertype == CommonUtil.UserTypeSuperAdmin || objuser.MaBranch.SkipIP == true)
                    {
                        if (count > 0)
                        {


                            if (DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date >= Convert.ToDateTime(expirydate).AddMinutes(StaticLogic.TimeDifference).Date)
                            {
                                FormsAuthentication.SetAuthCookie(username, false);
                                GetAuthentication(objuser);
                                return RedirectToAction("ChangePassword", "Account");
                            }

                            if (objuser.Usertype == CommonUtil.UserTypeSuperAdmin || objuser.Usertype == CommonUtil.UserTypeVisaCounselor)
                            {
                                FormsAuthentication.SetAuthCookie(username, false);
                                GetAuthentication(objuser);
                                return RedirectToAction("Index", "Home");
                            }
                            else
                            {
                                if (Convert.ToDateTime(expirydate).AddMinutes(StaticLogic.TimeDifference).Date > DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date)
                                {
                                    int diffDays = (Convert.ToDateTime(expirydate).AddMinutes(StaticLogic.TimeDifference) - DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date).Days;

                                    if (diffDays < 10)
                                    {

                                        if (count == 1)
                                        {
                                            FormsAuthentication.SetAuthCookie(username, false);

                                            GetAuthentication(objuser);

                                            //here to send the email to the user for password reset reminder.
                                            SendReminderToUsers(username, password, diffDays);

                                            if (objuser.Usertype == CommonUtil.UserTypeAdmin)
                                            {
                                                return RedirectToAction("Index", "Home");
                                            }

                                            if (objuser.Usertype == CommonUtil.UserTypeCounselor || objuser.Usertype == CommonUtil.UserTypeNewCounselor)
                                            {

                                                //get user type
                                                bool _IsChckuser = objuser.IsNewUser;
                                                if (_IsChckuser == false)
                                                {
                                                    return RedirectToAction("ChangePassword", "Account");
                                                }
                                                else
                                                {
                                                    return RedirectToAction("Index", "Counselor");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("", "The user name or password provided is incorrect.");
                                        }

                                    }
                                    if (diffDays > 10)
                                    {
                                        if (count == 1)
                                        {
                                            FormsAuthentication.SetAuthCookie(username, false);

                                            GetAuthentication(objuser);

                                            if (objuser.Usertype == CommonUtil.UserTypeAdmin)
                                            {
                                                return RedirectToAction("Index", "Home");
                                            }

                                            if (objuser.Usertype == CommonUtil.UserTypeCounselor || objuser.Usertype == CommonUtil.UserTypeNewCounselor)
                                            {
                                                //get user type
                                                bool _IsChckuser = objuser.IsNewUser;
                                                if (_IsChckuser == false)
                                                {
                                                    return RedirectToAction("ChangePassword", "Account");
                                                }
                                                else
                                                {

                                                    return RedirectToAction("Index", "Counselor");
                                                }
                                            }

                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return RedirectToAction("Login", "Login", new { @msg = "ad" });
            }
            else
            {
                return RedirectToAction("Login", "Login", new { @msg = "WrngPass" });
            }
        }




        public void GetAuthentication(User objuser)
        {
            int? branchid = objuser.BranchId;
            HttpCookie cke = new HttpCookie(CommonUtil.CookieBranchid);
            cke.Value = branchid.ToString();
            Response.Cookies.Add(cke);



            string branchname = objuser.MaBranch.BramchName;
            HttpCookie ckbranch = new HttpCookie(CommonUtil.BranchName);
            ckbranch.Value = branchname;
            Response.Cookies.Add(ckbranch);

            //get user type

            string usertype = objuser.Usertype;
            HttpCookie ckeusertype = new HttpCookie(CommonUtil.CookieUsertype);
            ckeusertype.Value = usertype;
            Response.Cookies.Add(ckeusertype);

            //get user id

            int userid = objuser.Id;
            HttpCookie ckeuserid = new HttpCookie(CommonUtil.CookieUserid);
            ckeuserid.Value = userid.ToString();
            Response.Cookies.Add(ckeuserid);

            //get Role id

            int? roleid = objuser.RollId;
            HttpCookie ckroleid = new HttpCookie(CommonUtil.CookieRoleid);
            ckroleid.Value = roleid.ToString();
            Response.Cookies.Add(ckroleid);

            HttpCookie chbranch = new HttpCookie("chbranch");
            chbranch.Value = "N";
            Response.Cookies.Add(chbranch);

        }



        public void SendReminderToUsers(string username, string password, int diffDays)
        {
            try
            {
                User objuser = (from p in db.Users where p.username == username && p.Password == password && p.Status == true select p).FirstOrDefault();


                string mail = ConfigurationManager.AppSettings["Mail"];

                MailMessage message = new MailMessage();
                message.Subject = " User ID created for SSC Software : ";
                message.Body = "<p>Dear " + objuser.username + "</p> <br/> <p>To access SSC <b></b> software, please reset your password.</p> <br /> <p>and Your password will expire after " + diffDays + "days </p><br /> In case of any issue please contact at techteam@ssceducation.org";
                message.From = new MailAddress(mail);
                message.To.Add(new MailAddress(objuser.Email));

                message.IsBodyHtml = true;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                SmtpClient smtpClient = new SmtpClient(smtpserver);
                smtpClient.Port = Int32.Parse("8889");
                smtpClient.Credentials = new NetworkCredential(mail, ConfigurationManager.AppSettings["credential"].ToString());
                smtpClient.EnableSsl = false;
                smtpClient.Send(message);

            }
            catch (Exception ex)
            { }


        }



        public ActionResult ResetPassword(string username)
        {

            if (username != "")
            {
                CommonUtil util = new CommonUtil();
                util.ResetPassword(username);

                return RedirectToAction("Login", new { @msg = "Y" });
            }
            else
            {
                return RedirectToAction("Login", "Login", new { @msg = "N" });
            }
            return RedirectToAction("Login", "Login");
        }


        private string GetComputer_InternetIP()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}