﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class MaPackageController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        MasterListing listing = new MasterListing();
        //
        // GET: /MaPackage/

        public ActionResult Index()
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Package, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            string usertype = Request.Cookies["usertype"].Value;
            if (usertype != "Admin")
            {


                var mapackages = db.MaPackages.OrderByDescending(z => z.Id).ToList();
                ViewBag.TotalCount = mapackages.Count;
                return View(mapackages);
            }

            var packalist = db.MaPackages.OrderByDescending(z => z.Id).ToList();
            ViewBag.TotalCount = packalist.Count;
            return View(packalist.ToList());
        }

        //
        // GET: /MaPackage/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Package, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            MaPackage mapackage = db.MaPackages.Find(id);
            if (mapackage == null)
            {
                return HttpNotFound();
            }
            return View(mapackage);
        }

        //
        // GET: /MaPackage/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Package, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            ViewBag.PackageHours = new SelectList(listing.GetPackageHours, "PackageHours", "PackageHours");
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName");
            return View();

        }




        public ActionResult _PackageDetail(int id)
        {
            // Get data by id for Mapackage because this will provide us with package id and course id
            MaPackage mapackage = db.MaPackages.Find(id);

            ViewBag.CourseName = mapackage.MaCourse.CourseName;

            MaDetail madetail = new MaDetail();

            madetail.PackageId = id;
            madetail.CourseId = mapackage.CourseId;

            return PartialView(madetail);


        }


        [HttpPost]
        public ActionResult _PackageDetail(MaDetail madetail)
        {



            madetail.EnterBy = User.Identity.Name;
            madetail.EnterDate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
            db.MaDetails.Add(madetail);
            db.SaveChanges();
            /*-------------------this was used to check if the total installment amout is equal to the package amount or not------------*/
            //currentamount = (double)madetail.InstallAmt;               
            //if (CheckInstallmntDetail(currentamount, (int)madetail.PackageId))
            //{

            //    db.MaDetails.Add(madetail);
            //    db.SaveChanges();
            //    return Content("Success");
            //}

            return Content("Success");
        }

        /// <summary>
        /// Check Installments
        /// </summary>
        /// <param name="currentamt"></param>
        /// <param name="packageid"></param>
        /// <returns></returns>
        public bool CheckInstallmntDetail(double currentamt, int packageid)
        {
            double totalamount = 0;


            double packageamount = 0;

            List<MaDetail> mdetail = db.MaDetails.Where(m => m.PackageId == packageid).ToList();
            if (mdetail.Count > 1)
            {
                totalamount = (double)mdetail.Sum(m => m.InstallAmt);

            }


            totalamount = totalamount + currentamt;

            MaPackage mapackage = db.MaPackages.Find(packageid);

            packageamount = (double)mapackage.Amount;

            if (totalamount < packageamount)
            {
                return true;
            }

            return false;
        }


        public ActionResult _InstallmentList(int id)
        {
            List<MaDetail> mdetail = db.MaDetails.Where(m => m.PackageId == id).ToList();

            ViewBag.TotalFees = mdetail.Sum(m => m.InstallAmt);

            return PartialView(mdetail);
        }

        //
        // POST: /MaPackage/Create
        [HttpPost]
        public ActionResult Create(MaPackage mapackage)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Package, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            if (ModelState.IsValid)
            {

                mapackage.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                mapackage.Status = true;
                db.MaPackages.Add(mapackage);

                db.SaveChanges();
                return RedirectToAction("Index");
            }


            ViewBag.PackageHours = new SelectList(listing.GetPackageHours, "PackageHours", "PackageHours", mapackage.PackageHours);

            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", mapackage.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", mapackage.CourseId);
            return View(mapackage);
        }

        //
        // GET: /MaPackage/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Package, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            MaPackage mapackage = db.MaPackages.Find(id);
            if (mapackage == null)
            {
                return HttpNotFound();
            }


            ViewBag.PackageHours = new SelectList(listing.GetPackageHours, "PackageHours", "PackageHours", mapackage.PackageHours);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", mapackage.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", mapackage.CourseId);
            return View(mapackage);
        }

        //
        // POST: /MaPackage/Edit/5

        [HttpPost]
        public ActionResult Edit(MaPackage mapackage)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Package, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            if (ModelState.IsValid)
            {
                mapackage.Status = true;
                db.Entry(mapackage).State = EntityState.Modified;
                //mapackage.Cdate = DateTime.Now.Date;

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PackageHours = new SelectList(listing.GetPackageHours, "PackageHours", "PackageHours", mapackage.PackageHours);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", mapackage.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", mapackage.CourseId);
            return View(mapackage);
        }

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Delete(int id)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Package, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            try
            {
                MaPackage mapackage = db.MaPackages.Find(id);

                db.MaPackages.Remove(mapackage);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        /*--------Updates package amount according to the instalment plans--------------*/

    }
}