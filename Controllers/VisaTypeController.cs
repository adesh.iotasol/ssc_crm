﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    public class VisaTypeController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        // GET: VisaType
        public ActionResult Index(string msg)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.VisaType, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            if (msg == "Updt")
            {
                ViewBag.message = "Record Updated Successfully";
            }
            if (msg == "sve")
            {
                ViewBag.message = "Record Added Successfully";
            }
            if (msg == "dup")
            {
                ViewBag.error = "Record With Same name already exists";
            }
            return View(db.St_VisaType.ToList());
        }

        // GET: VisaType/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            St_VisaType st_VisaType = db.St_VisaType.Find(id);
            if (st_VisaType == null)
            {
                return HttpNotFound();
            }
            return View(st_VisaType);
        }

        // GET: VisaType/Create
        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.VisaType, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }
            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout");
            return View();
        }

        // POST: VisaType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,EnqAbt,VisaType")] St_VisaType st_VisaType)
        {
            if (ModelState.IsValid)
            {
                db.St_VisaType.Add(st_VisaType);
                db.SaveChanges();
                return RedirectToAction("Index", new { @msg = "sve" });
            }
            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout");
            return View(st_VisaType);
        }

        // GET: VisaType/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.VisaType, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            St_VisaType st_VisaType = db.St_VisaType.Find(id);
            if (st_VisaType == null)
            {
                return HttpNotFound();
            }
            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout",st_VisaType.EnqAbt);
            return View(st_VisaType);
        }

        // POST: VisaType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,EnqAbt,VisaType")] St_VisaType st_VisaType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(st_VisaType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EnqAbt = new SelectList(db.St_EnqAbout, "EnqAbout", "EnqAbout");
            return View(st_VisaType);
        }

        // GET: VisaType/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.VisaType, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }
            }
            catch (Exception)
            {
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            St_VisaType st_VisaType = db.St_VisaType.Find(id);
            if (st_VisaType == null)
            {
                return HttpNotFound();
            }
            return View(st_VisaType);
        }

        // POST: VisaType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            St_VisaType st_VisaType = db.St_VisaType.Find(id);
            db.St_VisaType.Remove(st_VisaType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
