﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class TbFeeAdjustmentController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();
        CommonUtil cu = new CommonUtil();
        //
        // GET: /TbFeeAdjustment/

        public ActionResult Index()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeAdjustmentString, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }




            // var tb_feeadjustment = db.Tb_FeeAdjustment.Include(t => t.MaAdmission).Include(t => t.MaBranch).Include(t => t.MaCourse).Include(t => t.MaPackage);
            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            var tb_feeadjustment = db.Tb_FeeAdjustment.OrderByDescending(z => z.Id).Where(z => z.BranchId == branchid);
            ViewBag.TotalCount = tb_feeadjustment.Count();
            return View(tb_feeadjustment.ToList());
        }


       
        //
        // GET: /TbFeeAdjustment/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeAdjustmentString, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            Tb_FeeAdjustment tb_feeadjustment = db.Tb_FeeAdjustment.Find(id);
            if (tb_feeadjustment == null)
            {
                return HttpNotFound();
            }
            return View(tb_feeadjustment);
        }

        //
        // GET: /TbFeeAdjustment/Create

        public ActionResult Create()
        {


            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeAdjustmentString, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            ViewBag.Date = DateTime.Now.AddMinutes(StaticLogic.TimeDifference);


            //get adjustment no that is auto incremented.
            int branchid = Convert.ToInt32(Request.Cookies["branchid"].Value);


            int? rc = db.Tb_FeeAdjustment.Where(z => z.BranchId == branchid).Select(z => z.AdjustmentNo).Max();
            if (rc >= 1)
            {
                rc = rc + 1;
            }
            else
            {
                rc = 1;
            }
            ViewBag.AdjustmentNo = rc;






            ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname");
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName");
            ViewBag.PackageId = new SelectList(db.MaPackages, "Id", "PackageName");
            return View();
        }

        //
        // POST: /TbFeeAdjustment/Create

        [HttpPost]
        public ActionResult Create(Tb_FeeAdjustment tb_feeadjustment)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeAdjustmentString, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }



            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            try
            {
                int? srno = db.Tb_FeeAdjustment.Select(z => z.AdjustmentNo).Max();
                if (srno == null || srno == 0)
                {
                    srno = 1;
                }
                else
                {
                    srno += 1;
                }
                tb_feeadjustment.AdjustmentNo = srno;
                if (ModelState.IsValid)
                {

                    tb_feeadjustment.BranchId = branchid;
                    db.Tb_FeeAdjustment.Add(tb_feeadjustment);
                    db.SaveChanges();
                    //---------------------insert in to ledger-----------------

                    int studentid = (int)tb_feeadjustment.StudentId;
                    int rollno = (int)db.MaAdmissions.Where(z => z.Id == studentid).Select(z => z.RollNo).First();
                    if (tb_feeadjustment.AdjustType == "Pay")
                    {

                        CommonUtil.InsertToLedger(tb_feeadjustment.Id, 0.0, tb_feeadjustment.AdjustAmount, String.Format("{0}", "Fee Adjustment"), rollno, DateTime.Now.AddMinutes(StaticLogic.TimeDifference), CommonUtil.FeeAdjustment);
                    }
                    if (tb_feeadjustment.AdjustType == "Receipt")
                    {
                        CommonUtil.InsertToLedger(tb_feeadjustment.Id, tb_feeadjustment.AdjustAmount, 0.0, String.Format("{0}", "Fee Adjustment"), rollno, DateTime.Now.AddMinutes(StaticLogic.TimeDifference), CommonUtil.FeeAdjustment);
                    }
                    return RedirectToAction("Index");
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }

            }
            //ViewBag.StudentId = new SelectList(db.MaAdmission, "Id", "FirstName", tb_feeadjustment.StudentId);
            ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_feeadjustment.StudentId);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_feeadjustment.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", tb_feeadjustment.CourseId);
            ViewBag.PackageId = new SelectList(db.MaPackages, "Id", "PackageName", tb_feeadjustment.PackageId);
            return View(tb_feeadjustment);
        }

        //
        // GET: /TbFeeAdjustment/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeAdjustmentString, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            Tb_FeeAdjustment tb_feeadjustment = db.Tb_FeeAdjustment.Find(id);
            if (tb_feeadjustment == null)
            {
                return HttpNotFound();
            }
            ViewBag.Date = tb_feeadjustment.Edate.ToString().Replace(" 12:00:00 AM", "");

            ViewBag.adjustmentno = tb_feeadjustment.AdjustmentNo;

            ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_feeadjustment.StudentId);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_feeadjustment.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", tb_feeadjustment.CourseId);
            ViewBag.PackageId = new SelectList(db.MaPackages, "Id", "PackageName", tb_feeadjustment.PackageId);
            return View(tb_feeadjustment);
        }

        //
        // POST: /TbFeeAdjustment/Edit/5

        [HttpPost]
        public ActionResult Edit(Tb_FeeAdjustment tb_feeadjustment)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeAdjustmentString, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            if (ModelState.IsValid)
            {
                db.Entry(tb_feeadjustment).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                //update into ledger
                int studentid = (int)tb_feeadjustment.StudentId;
                int rollno = (int)db.MaAdmissions.Where(z => z.Id == studentid).Select(z => z.RollNo).First();
                if (tb_feeadjustment.AdjustType == "Pay")
                {

                    CommonUtil.UpdateToLedger(tb_feeadjustment.Id, 0.0, tb_feeadjustment.AdjustAmount, String.Format("{0}", "Fee Receipt"), DateTime.Now.AddMinutes(StaticLogic.TimeDifference), rollno, CommonUtil.FeeAdjustment);
                }
                if (tb_feeadjustment.AdjustType == "Receipt")
                {
                    CommonUtil.UpdateToLedger(tb_feeadjustment.Id, tb_feeadjustment.AdjustAmount, 0.0, String.Format("{0}", "Fee Receipt"), DateTime.Now.AddMinutes(StaticLogic.TimeDifference), rollno, CommonUtil.FeeAdjustment);
                }


                return RedirectToAction("Index");
            }
            ViewBag.StudentId = new SelectList(cu.GetStudents(branchid), "Id", "fullname", tb_feeadjustment.StudentId);
            //ViewBag.StudentId = new SelectList(db.MaAdmission, "Id", "FirstName", tb_feeadjustment.StudentId);
            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", tb_feeadjustment.BranchId);
            ViewBag.CourseId = new SelectList(db.MaCourses, "Id", "CourseName", tb_feeadjustment.CourseId);
            ViewBag.PackageId = new SelectList(db.MaPackages, "Id", "PackageName", tb_feeadjustment.PackageId);
            return View(tb_feeadjustment);
        }

        //
        // GET: /TbFeeAdjustment/Delete/5

        public ActionResult Delete(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeAdjustmentString, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            Tb_FeeAdjustment tb_feeadjustment = db.Tb_FeeAdjustment.Find(id);
            if (tb_feeadjustment == null)
            {
                return HttpNotFound();
            }
            return View(tb_feeadjustment);
        }

        //
        // POST: /TbFeeAdjustment/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.FeeAdjustmentString, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            Tb_FeeAdjustment tb_feeadjustment = db.Tb_FeeAdjustment.Find(id);
            db.Tb_FeeAdjustment.Remove(tb_feeadjustment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        /// <summary>
        ///  get courses  by student
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private IList<Tb_CourseHist> GetCourse(int id)
        {


            return db.Tb_CourseHist.Where(m => m.AdmissionId == id && m.IsActive == true).ToList();

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadPackageByCourse(string id)
        {
            var courselist = this.GetCourse(Convert.ToInt32(id));

            var colourData = courselist.Select(c => new SelectListItem()
            {
                Text = c.MaCourse.CourseName,
                Value = c.CourseId.ToString(),

            });

            return Json(colourData, JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        ///  get packages by course
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private IList<MaPackage> GetPackages(int id, int stuid)
        {

            int packid = (int)db.Tb_CourseHist.Where(z => z.CourseId == id && z.AdmissionId == stuid).Select(z => z.PackageId).First();



            return db.MaPackages.Where(m => m.Id == packid).ToList();

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadPackageBypackages(string id, string stuid)
        {
            var package = this.GetPackages(Convert.ToInt32(id), Convert.ToInt32(stuid));

            var colourData = package.Select(c => new SelectListItem()
            {
                Text = c.PackageName,
                Value = c.Id.ToString(),

            });

            return Json(colourData, JsonRequestBehavior.AllowGet);
        }



        public ActionResult Search(string name = "", string course = "", string rollno = "0")
        {

            int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
            List<Tb_FeeAdjustment> lst = db.Tb_FeeAdjustment.Where(z => z.BranchId == branchid).ToList();

            if (rollno != "")
            {
                try
                {
                    int rolno = Convert.ToInt32(rollno);
                    lst = lst.Where(z => z.MaAdmission.RollNo == rolno).ToList();
                    ViewBag.rollno = rollno;
                }
                catch (Exception ex)
                {
                }
            }

            if (name != "")
            {

                lst = lst.Where(z => z.MaAdmission.FirstName.ToUpper().Contains(name.ToUpper())).ToList();
                ViewBag.name = name;
            }


            if (course != "")
            {
                lst = lst.Where(z => z.MaCourse.CourseName.ToUpper().Contains(course.ToUpper())).ToList();
                ViewBag.course = course;
            }
            ViewBag.TotalCount = lst.Count();
            return View("Index", lst);
        }


    }
}