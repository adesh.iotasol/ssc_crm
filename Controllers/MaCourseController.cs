﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class MaCourseController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /MaCourse/

        public ActionResult Index()
        {

            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Course, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            string usertype = Request.Cookies[CommonUtil.CookieUsertype].Value;
            if (usertype != "Admin")
            {

                var macourses = db.MaCourses.OrderByDescending(z => z.Id).ToList();
                ViewBag.TotalCount = macourses.Count;
                return View(macourses.ToList());
            }

            var lstcourses = db.MaCourses.ToList();
            ViewBag.TotalCount = lstcourses.Count;
            return View(lstcourses.ToList());

        }

        //
        // GET: /MaCourse/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Course, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            MaCourse macourse = db.MaCourses.Find(id);
            if (macourse == null)
            {
                return HttpNotFound();
            }
            return View(macourse);
        }

        //
        // GET: /MaCourse/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Course, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            //ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName");
            return View();
        }

        //
        // POST: /MaCourse/Create

        [HttpPost]
        public ActionResult Create(MaCourse macourse)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Course, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
                if (Request.Cookies[CommonUtil.CookieBranchid].Value != null)
                {
                    int branchid = Convert.ToInt32(Request.Cookies[CommonUtil.CookieBranchid].Value);
                    macourse.BranchId = branchid;
                    macourse.Cdate = DateTime.Now.AddMinutes(StaticLogic.TimeDifference).Date;
                    macourse.CourseName = macourse.CourseName.ToUpper();
                    macourse.Status = true;
                    //ViewBag.Cdate = macourse.Cdate;

                    db.MaCourses.Add(macourse);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", macourse.BranchId);
            return View(macourse);
        }

        #region Assign_Group

        public ActionResult AssignCode(string course = "Select", string group = "Select")
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.AssignTimeCode, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (course != "Select")
            {
                int cid = Convert.ToInt32(course);
                List<MaTime> lsttime = db.MaTimes.OrderByDescending(z => z.Id).ToList();
                foreach (MaTime obj in lsttime)
                {
                    MaGroupAssign gassign = new MaGroupAssign();
                    gassign.CourseId = cid;
                    gassign.GroupName = group;
                    gassign.Timeid = obj.Id;
                    //gassign.TimeOut = obj.TimeOut;
                    //gassign.OutDesc = obj.OutDesc;
                    //gassign.Indesc = obj.Indesc;
                    //gassign.TCode = obj.TCode;
                    gassign.Status = true;
                    try
                    {
                        db.MaGroupAssigns.Add(gassign);

                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    { }

                }
                return View(db.MaGroupAssigns.ToList());
            }
            return View();

        }

        public ActionResult _GroupList(string sid = "Select")
        {
            if (sid != "Select")
            {
                return PartialView(db.MaGroupTime_View.Where(z => z.GroupName == sid).ToList());
            }

            return PartialView();


        }
        [HttpPost]
        public ActionResult _GroupList(List<MaGroupAssign> lst, FormCollection fc)
        {
            return PartialView();
        }

        #endregion








        //
        // GET: /MaCourse/Edit/5






        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Course, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            MaCourse macourse = db.MaCourses.Find(id);
            //if (macourse == null)
            //{
            //    return HttpNotFound();
            //}
            // ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", macourse.BranchId);
            return View(macourse);
        }

        //
        // POST: /MaCourse/Edit/5

        [HttpPost]
        public ActionResult Edit(MaCourse macourse)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Course, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            if (ModelState.IsValid)
            {
                db.Entry(macourse).State = System.Data.Entity.EntityState.Modified;
                macourse.Status = true;
                macourse.CourseName = macourse.CourseName.ToUpper();
                //macourse.Cdate = DateTime.Now.Date;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            // ViewBag.BranchId = new SelectList(db.MaBranches, "Id", "BramchName", macourse.BranchId);
            return View(macourse);
        }


        // GET: /MaCourse/Delete/5

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Delete(int id)
        {
            try
            {
                int roleid = StaticLogic.RoleId();

                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.Course, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }
            try
            {
                MaCourse macourse = db.MaCourses.Find(id);

                db.MaCourses.Remove(macourse);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }






        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }







        public ActionResult UpdateCode(string id, string st)
        {
            try
            {
                id = id.Trim();

                int gid = Convert.ToInt32(id);
                MaGroupAssign obj = db.MaGroupAssigns.Where(z => z.Id == gid).First();
                obj.Status = Convert.ToBoolean(st);

                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
            }

            return View();

        }







    }
}