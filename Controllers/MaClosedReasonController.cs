﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSCMvc.Models;
using SSCMvc.Webutil;

namespace SSCMvc.Controllers
{
    [Authorize]
    public class MaClosedReasonController : Controller
    {
        private SSCWebDBEntities1 db = new SSCWebDBEntities1();

        //
        // GET: /MaClosedReason/

        public ActionResult Index()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ClosedReason, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }




            ViewBag.TotalCount = db.MaClosedReasons.Count();
            return View(db.MaClosedReasons.ToList());
        }

        //
        // GET: /MaClosedReason/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ClosedReason, UserAction.View);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            MaClosedReason maclosedreason = db.MaClosedReasons.Find(id);
            if (maclosedreason == null)
            {
                return HttpNotFound();
            }
            return View(maclosedreason);
        }

        //
        // GET: /MaClosedReason/Create

        public ActionResult Create()
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ClosedReason, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            return View();
        }

        //
        // POST: /MaClosedReason/Create

        [HttpPost]
        public ActionResult Create(MaClosedReason maclosedreason)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ClosedReason, UserAction.Add);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            if (ModelState.IsValid)
            {
                maclosedreason.cdate = DateTime.Now;
                db.MaClosedReasons.Add(maclosedreason);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(maclosedreason);
        }

        //
        // GET: /MaClosedReason/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ClosedReason, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            MaClosedReason maclosedreason = db.MaClosedReasons.Find(id);
            if (maclosedreason == null)
            {
                return HttpNotFound();
            }
            return View(maclosedreason);
        }

        //
        // POST: /MaClosedReason/Edit/5

        [HttpPost]
        public ActionResult Edit(MaClosedReason maclosedreason)
        {
            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ClosedReason, UserAction.Edit);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }


            if (ModelState.IsValid)
            {
                db.Entry(maclosedreason).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(maclosedreason);
        }

        //
        // GET: /MaClosedReason/Delete/5
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(int id)
        {

            try
            {
                int roleid = StaticLogic.RoleId();
                bool Prmsn = false;
                Prmsn = Permission.CheckPermission(roleid, CommonUtil.ClosedReason, UserAction.Delete);

                if (Prmsn == false)
                {
                    return View("~/Views/Shared/AccessDenied.cshtml");
                }

            }
            catch (Exception)
            {
            }

            try
            {
                MaClosedReason maclosedreason = db.MaClosedReasons.Find(id);
                db.MaClosedReasons.Remove(maclosedreason);
                db.SaveChanges();
                return Content(Boolean.TrueString);
            }
            catch (Exception ex)
            {
                return JavaScript("errormessage();");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}